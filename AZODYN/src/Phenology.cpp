// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include<iostream>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AFISOL {
using namespace vle::discrete_time;

class Phenology : public DiscreteTimeDyn
{
public:
    Phenology(
       const vd::DynamicsInit& atom,
       const vd::InitEventList& events)
        : DiscreteTimeDyn(atom, events)
    {
        SeSLe = vv::toDouble(events.get("SeSLe"));
        DateS = vv::toDouble(events.get("DateS"));
        SeLeIf = vv::toDouble(events.get("SeLeIf"));
        DateDf = vv::toDouble(events.get("DateDf"));
        SeDfDrg = vv::toDouble(events.get("SeDfDrg"));
        NEtpot = vv::toDouble(events.get("NEtpot"));
        VDrg = vv::toDouble(events.get("VDrg"));
        SeMaxFslaMp = vv::toDouble(events.get("SeMaxFslaMp"));
        Nstruct = vv::toDouble(events.get("Nstruct"));
        P1Gmax = vv::toDouble(events.get("P1Gmax"));
        Tbase = vv::toDouble(events.get("Tbase"));
        DateJanv = vv::toDouble(events.get("DateJanv"));
        StSLe.init(this, "StSLe", events);
        StLeIf.init(this, "StLeIf", events);
        Phase.init(this, "Phase", events);
        DateLe.init(this, "DateLe", events);
        StIfDf.init(this, "StIfDf", events);
        DateIf.init(this, "DateIf", events);
        SeIfDf.init(this, "SeIfDf", events);
        DateDrg.init(this, "DateDrg", events);
        StDfDrg.init(this, "StDfDrg", events);
        StDrgFsla.init(this, "StDrgFsla", events);
        DateFsla.init(this, "DateFsla", events);
        DateMp.init(this, "DateMp", events);
        StFslaMp.init(this, "StFslaMp", events);
        StLe.init(this, "StLe", events);
        StS.init(this, "StS", events);
        Tmoyb.init(this, "Tmoyb", events);
        SFi.init(this, "SFi", events);
        SFf.init(this, "SFf", events);
        NjFsla.init(this, "NjFsla", events);
        StSFsla.init(this, "StSFsla", events);
        StSDf.init(this, "StSDf", events);
        StSDrg.init(this, "StSDrg", events);
        DateMpPrev.init(this, "DateMpPrev", events);
        NjMp.init(this, "NjMp", events);
        NumJour.init(this, "NumJour", events);
        NumJAnS.init(this, "NumJAnS", events);
        Tmin.init(this, "Tmin", events);
        Tmax.init(this, "Tmax", events);
        IndicMP.init(this, "IndicMP", events);
        P1G.init(this, "P1G", events);
        Nveg.init(this, "Nveg", events);
    }

    virtual ~Phenology()
    {}

//@@begin:compute@@
virtual void compute(const vd::Time& time)
{


	//Calcul de la temperature moyenne en tenant compte de la Tbase
  {
	Tmoyb = std::max(0.0,((Tmin() + Tmax())/2 -Tbase));}


 //Phase avant Semis
  if(Phase(-1)==0){
  	if(time==DateS){
  		Phase=1;	
  	}
  }
//Phase Semis-Levee
//*le semis a lieu à une date : DateS qui est une variable d’entrée qui varie entre situations simulées.Le modèle calcule une somme de degrés jours depuis le semis (depuis DateS) nommée StS.*//
//* La période entre le stade semis(DateS)et le stade suivant  levée  est la phase de phénologie 0.On dispose pendant cette période d’un seuil SeSLe qui est un seuil de la somme de degrés-jours qui déclenche le stade suivant:la levée.*// 


 if((Phase(-1)==1)&&(time>DateS)){
 	
 	StSLe=Tmoyb()+StSLe(-1);
	if(StSLe()>=SeSLe){
 		DateLe=time;
 		Phase=Phase(-1)+1;
 		
  		std::cout<<"DateLe"<<std::endl;}
 }


//Phase Levée-Initiation florale
//*la levee correspond à la variable DateLe qui est le jour où le seuil SeSLe est atteint. Le modèle calcule une somme de degrés-jours depuis la levée (depuis DateLe) nommée StLe.*//
//*La période entre le stade « Levée » (DateLe) et le stade suivant « initiation florale est la phase de phénologie 1. On dispose pendant cette phase d’un seuil SeLeIf qui est un seuil de la somme de dégrés-jours qui déclenche le stade suivant : l’initiation florale.*//


 
 if(Phase(-1)==2){
 	
 	StLeIf=Tmoyb()+StLeIf(-1);
 		if(StLeIf()>=SeLeIf){
 			DateIf=time;
 			Phase=Phase(-1)+1;
 		
  			std::cout<<"DateIf"<<std::endl;}
}


//Phase Initiation florale-Debut floraison
//*I'initiation florale correspond à la variable DateIf qui est le jour où le seuil SeLeIf est atteint. Le modèle calcule une somme de degrés-jours depuis la levée (depuis DateIf) nommée StIf.*//
//*La période entre le stade « initiation florale » (DateIf) et le stade suivant « début floraison est la phase de phénologie 2. On dispose pendant cette phase d’un seuil SeIfDf qui est un seuil de la somme de dégrés-jours qui déclenche le stade suivant : le début floraison.*//

 
 if(Phase(-1)==3){
 	StIfDf=Tmoyb()+StIfDf(-1);
 	
 	if(time==DateDf){
 		
 		Phase=Phase(-1)+1;}
 }


//Phase Debut floraison-Debut remplissage des graines
//*le debut floraison correspond à la variable DateDf, le jour où le seuil SeIfDf est atteint. Le modèle calcule une somme de degrés-jours depuis le début floraison (depuis DateDf) nommé StDf.*//
//*La période entre le stade « début floraison » et le stade suivant « début remplissage des graines » est la phase de phénologie 3. On dispose pendant cette phase d’un seuil SeDfDrg qui est un seuil de la somme de degrés-jours qui déclenche le stade suivant : début remplissage des graines.*// 

 
 if(Phase(-1)==4){
	StDfDrg=Tmoyb()+StDfDrg(-1);
	
	if(StDfDrg()>=SeDfDrg){
		DateDrg=time;
		Phase=Phase(-1)+1;
  		std::cout<<"DateDrg"<<std::endl;}
 }
 


//*Phase Debut remplissage-Fin f. seuil limite av.*//

 
 double valtemp=(VDrg*(NEtpot-1));
 if(Phase(-1)==5){
 	StDrgFsla=Tmoyb()+StDrgFsla(-1);
 	if(StDrgFsla()>= valtemp){
 		Phase=Phase(-1)+1;
 		DateFsla=time;
  		std::cout<<"DateFsla"<<std::endl;}
 }


		

//*Phase Fsla-Mp*//

 
 if(Phase(-1)==6){	
 	StFslaMp=Tmoyb()+StFslaMp(-1);
	if(Nveg(-1)<(Nstruct/100)){
		Phase=Phase(-1)+1;
		DateMp=time;}
	else{
		if(P1G(-1)>=P1Gmax){
			Phase=Phase(-1)+1;
			DateMp=time;}
		else{
			if(IndicMP(-1)==1){
				Phase=Phase(-1)+1;
				DateMp=time;}
			else{
				if(StFslaMp()>=SeMaxFslaMp){
					Phase=Phase(-1)+1;
 					DateMp=time;}
				else{
					Phase=Phase(-1);}
			}
		}
	}
}				
 
	
 		
 //======================================================================================
 

 //*Calcul de StLed somme des degre jour depuis la levee pour le calcul de Nfixaer et Nfixmaxaer


  if(Phase(-1)<2){
  	StLe=0;}else{
  	if(Phase(-1)==2){
  		StLe=StLeIf();}else{
  		StLe=StLe(-1)+Tmoyb();}
  		
  }



  //Calcul de la somme des températures depuis le semis StS

	if(Phase(-1)<1){
  		StS=0;}
	else{
		if((Phase(-1)==0)&&(Phase()==1)){
			StS=0;}
		else{
			StS=StS(-1)+Tmoyb();}
	}	



 //Calcul de SFi
    {
	SFi = SeSLe + (40.0 * 0.5) ;}
	


 //Calcul de SFf
    {
	SFf = SeSLe + (40.0 * 1.5);}


//Calcul de la somme des températures du Semis à la DF StSDf
  
  if(Phase(-1)<1){
         StSDf = 0;}
  else {
         if(Phase(-1)<4){
         	StSDf = Tmoyb()+StSDf(-1);}
         else{
         	StSDf = StSDf(-1);}
   }



//Calcul de la somme des températures du Semis à la DRG StSDrg
  
  if(Phase(-1)<1){
         StSDrg = 0;}
  else {
         if(Phase(-1)<5){
         	StSDrg = Tmoyb()+StSDrg(-1);}
         else{
         	StSDrg = StSDrg(-1);}
   }



 //Calcul de la somme des températures du Semis à FSLA

  if(Phase(-1)<1){
         StSFsla = 0;}
  else {
         if(Phase(-1)<6){
         	StSFsla = Tmoyb()+StSFsla(-1);}
         else{
         	StSFsla = StSFsla(-1);}
   }


 
//Cacul de la la NjFsla qui est le nombre de jours depuis la dateFsla

   
    if(Phase(-1)<6){
	NjFsla=0;}
    else{

 	  if((Phase(-1)==5)&&(Phase()==6)){
    		NjFsla=0;}
    	  else{
        	NjFsla=NjFsla(-1) +1;}

    }
   



//Calcul du nombre de jour à partir de la maturité physiologique NjMp
    

     if(Phase(-1)<7){
     	NjMp=0;}
     else{
	NjMp=NjMp(-1)+1;}




//Calcul de la DateMP prévisionnelle DateMpPrev

    if(NjMp()==7){
	DateMpPrev=time;}


//Calcul du numero de jour depuis le semis NumJour


   if(DateS<DateJanv) {
   	NumJour = 394;}
   else{
        NumJour = 32;}




//Calcul du numero de jour depuis l'année de semis NumJAnS

    
   {
	NumJAnS = time - DateJanv + 1;}
}



//@@end:compute@@

//@@begin:initValue@@
virtual void initValue(const vd::Time& /*time*/)
{ }
//@@end:initValue@@

private:
//@@begin:user@@
//@@end:user@@

    double SeSLe;
    double DateS;
    double SeLeIf;
    double DateDf;
    double SeDfDrg;
    double NEtpot;
    double VDrg;
    double SeMaxFslaMp;
    double Nstruct;
    double P1Gmax;
    double Tbase;
    double DateJanv;
    Var StSLe;
    Var StLeIf;
    Var Phase;
    Var DateLe;
    Var StIfDf;
    Var DateIf;
    Var SeIfDf;
    Var DateDrg;
    Var StDfDrg;
    Var StDrgFsla;
    Var DateFsla;
    Var DateMp;
    Var StFslaMp;
    Var StLe;
    Var StS;
    Var Tmoyb;
    Var SFi;
    Var SFf;
    Var NjFsla;
    Var StSFsla;
    Var StSDf;
    Var StSDrg;
    Var DateMpPrev;
    Var NjMp;
    Var NumJour;
    Var NumJAnS;
    /*Sync*/ Var Tmin;
    /*Sync*/ Var Tmax;
    /*Nosync*/ Var IndicMP;
    /*Nosync*/ Var P1G;
    /*Nosync*/ Var Nveg;
};

} // namespace AFISOL

DECLARE_DYNAMICS(AFISOL::Phenology)

