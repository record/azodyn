/*
 * @file src/phenologie_types.hpp
 *
 * @author The RECORD Development Team (INRA)
 */
/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AZODYN_PHENOLOGIE_HPP_
#define AZODYN_PHENOLOGIE_HPP_

namespace AZODYN
{

    enum PHENOLOGIE
    {
        SOL_NU /*0*/,
        JOUR_SEMIS /*1*/,
        PRE_LEVEE /*2*/,
        JOUR_LEVEE /*3*/,
        VEGETATIF /*4*/,
        JOUR_F1 /*5*/,
        FLORAISON /*6*/,
        JOUR_G1 /*7*/,
        G1_G4 /*8*/,
        JOUR_G4 /*9*/,
        G4_MI_REMPLISSAGE_GRAIN /*10*/,
        JOUR_MI_REMPLISSAGE_GRAIN /*11*/,
        FIN_REMPLISSAGE_GRAIN /*12*/,
        JOUR_MATURITE /*13*/,
        GRAIN_MUR /*14*/,
        JOUR_RECOLTE /*15*/
    };

}

#endif
