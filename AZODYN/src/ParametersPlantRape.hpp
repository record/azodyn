/**
 *
 * Copyright (C) 2016-2017 INRA.
 *
 * TODO licence
 */

#ifndef AZODYNCOLZA_PARAMETERS_PLANT_RAPE_HPP
#define AZODYNCOLZA_PARAMETERS_PLANT_RAPE_HPP

#include <vle/utils/Exception.hpp>
#include <vle/utils/DateTime.hpp>

#include "AzodynUtils.hpp"

namespace AZODYN
{

    using namespace vle::utils;

    struct ParametersPlantRape
    {

        //---------------------------------------------------------------------------------//

        // Designation de la culture
        std::string crop;
        // Designation de la variete
        std::string cultivar;

        //---------------------------------------------------------------------------------//
        // Paramètres liés aux Décisions de l'utilisateur (apports d'azote et stades phénologiques observés

        // 1er janvier de l'année de recolte
        std::string January1st;

        // Date de LEVEE observee
        std::string jourLevee;

        // Date de FLORAISON observee
        std::string jourFloraison;

        // Date de MATURITE observee
        std::string jourMaturite;

        // Densite semis
        double densiteSemis;

        //---------------------------------------------------------------------------------//
        // Paramètres liés à l'initialisation du modèle

        // date d'initialisation du module hydrique
        double date_init_H;

        // date a laquelle on initialise les calculs pour le bilan azoté.
        // doit impérativement être après la date d'initialisation du bilan hydrique
        // date d'initialisation du module azote
        double date_init_N;

        // date d initialisation du module mineralisation (va généralement etre a
        //  la recolte du precedent cultural)
        double date_init_miner_res;

        // date d'initialisation de la matiere seche
        double date_init_MS;

        //---------------------------------------------------------------------------------//
        // Paramètres liés à la phénologie du colza

        // Date de FLORAISON simulee
        // Intercept
        double AF1;
        // Pente Temp
        double BTF1;
        double moyTjanuary1st;
        // Pente Photo
        double BPF1;
        double moyPjanuary1st;
        // Pente Lat
        double BLF1;
        double LAT;

        // Somme des températures depuis SEMIS qui déclenche le passage au stade LEVEE
        double somme_temperature_levee;

        // Somme des températures depuis LEVEE qui déclenche le passage au stade FLORAISON
        double somme_temperature_floraison;

        // Somme des températures depuis FLORAISON qui déclenche le passage au stade G4
        double somme_temperature_G4;

        // Somme des températures depuis FLORAISON qui termine le remplissage des grains
        double somme_temperature_FRG;

        //---------------------------------------------------------------------------------//
        // Paramètres liés à Enracinement

        double Velong;

        double ProfRacmax;

        //---------------------------------------------------------------------------------//
        // Paramètres liés à INN

        // Allocation d'azote dans les feuilles + tige
        double ApartQNGreen;
        double BpartQNGreen;

        // Teneur en azote dans les feuilles + tige
        double AQNGreen;
        double BQNGreen;

        // Allocation d'azote dans les feuilles
        double ApartQNLeaves;
        double BpartQNLeaves;
        double CpartQNLeaves;
        double DpartQNLeaves;

        // Allocation d'azote dans les tiges
        double ApartQNStem;
        double BpartQNStem;

        // Allocation d'azote dans les siliques
        double ApartQNPod;
        double BpartQNPod;

        // Seuil de MS pour courbe max
        double MS_Nmax_seuil;

        // Rapport entre matière sèche totale et aérienne ou Quantité d’azote totale sur aérien
        double coef_tot_a;

        // Teneur en azote max pour des biomasses faibles
        double tNmax_Nmax;

        // Paramètre de courbe max
        double coef_Nmax;

        // Paramètre de courbe max
        double pente_Nmax;

        double Vmax;

        double BQNT;

        //---------------------------------------------------------------------------------//
        // Paramètres liés à MSQN

        // Calcul du ratio MS Feuilles/MS Aérienne (pour la chute des feuilles)
        double ApartMSLeaves;
        double BpartMSLeaves;
        double CpartMSLeaves;
        double DpartMSLeaves;

        // Calcul du ratio MS Cosse/MS Siliques
        double ApartMSPodWall;
        double BpartMSPodWall;

        // Calcul de la MS aérienne
        double AMST;
        double BMST;

        // Calcul de la MS des feuilles
        double AMSLeaves;
        double BMSLeaves;

        // Calcul de la MS des tiges
        double AMSStem;
        double BMSStem;

        // Calcul de la MS des siliques
        double AMSPod;
        double BMSPod;

        // Coefficient conversion rayonnement global en actif
        double Ec;

        // Teneur en N critique pour des biomasses faibles
        double tNc_Nc;

        // Paramètre de courbe critique
        double coef_Nc;

        // Paramètre de courbe critique
        double pente_Nc;

        // Surface et surface massique des cotylédons
        double SFCOTY;
        double SLACOTY;

        double GEL;

        //---------------------------------------------------------------------------------//
        // Paramètres liés à LAI/PAI

        // Paramètres LAI reelle
        double ALAI;
        double BLAI;

        // Paramètres PAI
        double APAI;
        double BPAI;

        // Paramètres Ei LAI/PAI
        double EILMAX;
        double EIPMAX;
        double K;

        // Paramètres Eb LAI/PAI
        double TMINEB0;
        double TMINEB1;
        double AEB;
        double BEB;
        double CEB;
        double DEB;
        double EEB;

        // Paramètres spec Eb PAI
        double ARG;
        double BRG;

        //---------------------------------------------------------------------------------//
        // Paramètres liés à CohorteFeuilles (mortes)

        // Seuil d'âge physiologique entrainant la chute des feuilles

        double VIEFM;

        double AVIEFM;
        double BVIEFM;
        double CVIEFM;
        double DVIEFM;
        double EVIEFM;
        double FVIEFM;

        // INN des feuilles mortes
        double ANFM;
        double BNFM;

        //---------------------------------------------------------------------------------//
        // Paramètres communs aux Rendements statiques et dynamiques

        // Nombre de grains potentiel
        double ANGPOT;
        double BNGPOT;

        // Stess azoté réduction NGM2pot
        double AISN;
        double BISN;
        double CISN;

        // Seuil INN Floraison
        double XII;

        // PMG max varietal
        double PMGmax_var;

        //---------------------------------------------------------------------------------//
        // Paramètres liés au Rendement statique

        // INN G4
        double NC1G4;
        double NC2G4;

        // Nombre grains seuil
        double NGSEUIL;

        // Application paramètres eau + quotient photothermique + temp moyenne + temp max sur PMG
        double PMGEAU;
        double PMGQP;
        double PMGTMOY;
        double PMGTMAX;

        //---------------------------------------------------------------------------------//
        // Paramètres liés au Rendement dynamique

        // RDT max varietal
        double RDTmax_var;

        // Poids d'un grain à Floraison
        double P1G_FLO;

        // Humidité des grains
        double GrainsHumidity;

        // Taux protéique potentiel des grains
        double ATPpot;
        double BTPpot;

        // Stess hydrique réduction NGM2pot
        double AStressH;
        double BStressH;

        // Allocation d'azote aux grains potentielle
        double ANalloc;

        // Azote disponible pour les grains
        double ANdispo;

        // Azote réelle des grains
        double AQNG;

        // Azote remobilisable vers grains
        double ANremob;
        double BNremob;
        double CNremob;

        // Offre en matière sèche des grains
        double AOffMSG;

        void initialiser(const vle::devs::InitEventList &events)
        {

            // Paramètres liés à la desiniation de la culture et de la variete

            crop = Utils::extractString(events, "crop");
            cultivar = Utils::extractString(events, "cultivar");

            //---------------------------------------------------------------------------------//
            // Paramètres liés aux Décisions de l'utilisateur (apports d'azote et stades phénologiques observés)

            January1st = Utils::extractString(events, "January1st");
            densiteSemis = Utils::extractDouble(events, "densiteSemis");
            jourLevee = Utils::extractString(events, "jourLevee");
            jourFloraison = Utils::extractString(events, "jourFloraison");
            jourMaturite = Utils::extractString(events, "jourMaturite");
            

            //---------------------------------------------------------------------------------//
            // Paramètres liés l'initialisation du modèle
	    date_init_H = Utils::extractDate(events, "date_init_H");
            date_init_N = Utils::extractDate(events, "date_init_N");
            date_init_miner_res = Utils::extractDate(events, "date_init_miner_res");
            date_init_MS = Utils::extractDate(events, "date_init_MS");

            // verifications

            if (date_init_miner_res > date_init_N)
            {
                throw ArgError("date_init_miner_res > date_init_N");
            }

            if (date_init_H > date_init_N)
            {
                throw ArgError("date_init_H > date_init_N");
            }

            if (date_init_N > date_init_MS)
            {
                throw ArgError("date_init_N > date_init_MS");
            }

            //---------------------------------------------------------------------------------//
            // Paramètres liés à la phénologie du colza
            AF1 = Utils::extractDouble(events, "AF1");
            BTF1 = Utils::extractDouble(events, "BTF1");
            moyTjanuary1st = Utils::extractDouble(events, "moyTjanuary1st");
            BPF1 = Utils::extractDouble(events, "BPF1");
            moyPjanuary1st = Utils::extractDouble(events, "moyPjanuary1st");
            BLF1 = Utils::extractDouble(events, "BLF1");
            LAT = Utils::extractDouble(events, "LAT");
            somme_temperature_levee = Utils::extractDouble(events, "somme_temperature_levee");
            somme_temperature_floraison = Utils::extractDouble(events, "somme_temperature_floraison");
            somme_temperature_G4 = Utils::extractDouble(events, "somme_temperature_G4");
	    somme_temperature_FRG = Utils::extractDouble(events, "somme_temperature_FRG");
            

            // Paramètres liés à Enracinement

                Velong = Utils::extractDouble(events, "Velong");

                ProfRacmax = Utils::extractDouble(events, "ProfRacmax");

            // Paramètres liés à INN

                ApartQNGreen = Utils::extractDouble(events, "ApartQNGreen");

                BpartQNGreen = Utils::extractDouble(events, "BpartQNGreen");

                AQNGreen = Utils::extractDouble(events, "AQNGreen");

                BQNGreen = Utils::extractDouble(events, "BQNGreen");
                ApartQNLeaves = Utils::extractDouble(events, "ApartQNLeaves");

                BpartQNLeaves = Utils::extractDouble(events, "BpartQNLeaves");

                CpartQNLeaves = Utils::extractDouble(events, "CpartQNLeaves");

                DpartQNLeaves = Utils::extractDouble(events, "DpartQNLeaves");
                ApartQNStem = Utils::extractDouble(events, "ApartQNStem");

                BpartQNStem = Utils::extractDouble(events, "BpartQNStem");

                ApartQNPod = Utils::extractDouble(events, "ApartQNPod");

                BpartQNPod = Utils::extractDouble(events, "BpartQNPod");

                coef_tot_a = Utils::extractDouble(events, "coef_tot_a");
                tNmax_Nmax = Utils::extractDouble(events, "tNmax_Nmax");

                coef_Nmax = Utils::extractDouble(events, "coef_Nmax");

                pente_Nmax = Utils::extractDouble(events, "pente_Nmax");

                Vmax = Utils::extractDouble(events, "Vmax");

                BQNT = Utils::extractDouble(events, "BQNT");

            // Paramètres liés à MSQN

                ApartMSLeaves = Utils::extractDouble(events, "ApartMSLeaves");

                BpartMSLeaves = Utils::extractDouble(events, "BpartMSLeaves");

                CpartMSLeaves = Utils::extractDouble(events, "CpartMSLeaves");
                DpartMSLeaves = Utils::extractDouble(events, "DpartMSLeaves");
                ApartMSPodWall = Utils::extractDouble(events, "ApartMSPodWall");
                BpartMSPodWall = Utils::extractDouble(events, "BpartMSPodWall");
                Ec = Utils::extractDouble(events, "Ec");
                AMSLeaves = Utils::extractDouble(events, "AMSLeaves");
                BMSLeaves = Utils::extractDouble(events, "BMSLeaves");
                AMSStem = Utils::extractDouble(events, "AMSStem");
                BMSStem = Utils::extractDouble(events, "BMSStem");
                AMSPod = Utils::extractDouble(events, "AMSPod");
                BMSPod = Utils::extractDouble(events, "BMSPod");
                AMST = Utils::extractDouble(events, "AMST");
                BMST = Utils::extractDouble(events, "BMST");
                tNc_Nc = Utils::extractDouble(events, "tNc_Nc");
                coef_Nc = Utils::extractDouble(events, "coef_Nc");
                pente_Nc = Utils::extractDouble(events, "pente_Nc");
                SFCOTY = Utils::extractDouble(events, "SFCOTY");
                SLACOTY = Utils::extractDouble(events, "SLACOTY");
                GEL = Utils::extractDouble(events, "GEL");
            // Paramètres liés à LAI/PAI

                ALAI = Utils::extractDouble(events, "ALAI");
                BLAI = Utils::extractDouble(events, "BLAI");
                APAI = Utils::extractDouble(events, "APAI");
                BPAI = Utils::extractDouble(events, "BPAI");
                EILMAX = Utils::extractDouble(events, "EILMAX");
                EIPMAX = Utils::extractDouble(events, "EIPMAX");
                ARG = Utils::extractDouble(events, "ARG");
                BRG = Utils::extractDouble(events, "BRG");
                TMINEB0 = Utils::extractDouble(events, "TMINEB0");
                TMINEB1 = Utils::extractDouble(events, "TMINEB1");
                AEB = Utils::extractDouble(events, "AEB");
                BEB = Utils::extractDouble(events, "BEB");
                CEB = Utils::extractDouble(events, "CEB");
                DEB = Utils::extractDouble(events, "DEB");
                EEB = Utils::extractDouble(events, "EEB");
                K = Utils::extractDouble(events, "K");

            // Paramètres liés à CohorteFeuille

                VIEFM = Utils::extractDouble(events, "VIEFM");
                AVIEFM = Utils::extractDouble(events, "AVIEFM");
                BVIEFM = Utils::extractDouble(events, "BVIEFM");
                CVIEFM = Utils::extractDouble(events, "CVIEFM");
                DVIEFM = Utils::extractDouble(events, "DVIEFM");
                EVIEFM = Utils::extractDouble(events, "EVIEFM");
                FVIEFM = Utils::extractDouble(events, "FVIEFM");
                ANFM = Utils::extractDouble(events, "ANFM");
                BNFM = Utils::extractDouble(events, "BNFM");

            // Paramètres liés au Rendement statique

                BNGPOT = Utils::extractDouble(events, "BNGPOT");
                ANGPOT = Utils::extractDouble(events, "ANGPOT");
                NC1G4 = Utils::extractDouble(events, "NC1G4");
                NC2G4 = Utils::extractDouble(events, "NC2G4");
                AISN = Utils::extractDouble(events, "AISN");
                BISN = Utils::extractDouble(events, "BISN");
                CISN = Utils::extractDouble(events, "CISN");
                NGSEUIL = Utils::extractDouble(events, "NGSEUIL");
                PMGEAU = Utils::extractDouble(events, "PMGEAU");
                PMGQP = Utils::extractDouble(events, "PMGQP");
                PMGTMOY = Utils::extractDouble(events, "PMGTMOY");
                XII = Utils::extractDouble(events, "XII");
                PMGTMAX = Utils::extractDouble(events, "PMGTMAX");

            // Paramètres liés au Rendement dynamique

                PMGmax_var = Utils::extractDouble(events, "PMGmax_var");
                RDTmax_var = Utils::extractDouble(events, "RDTmax_var");
                P1G_FLO = Utils::extractDouble(events, "P1G_FLO");
                GrainsHumidity = Utils::extractDouble(events, "GrainsHumidity");
                ATPpot = Utils::extractDouble(events, "ATPpot");
                BTPpot = Utils::extractDouble(events, "BTPpot");
                AStressH = Utils::extractDouble(events, "AStressH");
                BStressH = Utils::extractDouble(events, "BStressH");
                ANalloc = Utils::extractDouble(events, "ANalloc");
                ANdispo = Utils::extractDouble(events, "ANdispo");
                AQNG = Utils::extractDouble(events, "AQNG");
                ANremob = Utils::extractDouble(events, "ANremob");
                BNremob = Utils::extractDouble(events, "BNremob");
                CNremob = Utils::extractDouble(events, "CNremob");
                AOffMSG = Utils::extractDouble(events, "AOffMSG");
        }
    };

} // namespace

#endif
