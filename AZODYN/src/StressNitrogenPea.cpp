// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <Phenology.hpp>
#include <ParametersPlant.hpp>
#include <ParametersPlantPea.hpp>

#include<iostream>
#include<cstdlib>
#include<time.h>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;
using namespace vle::utils;

class StressNitrogenPea : public DiscreteTimeDyn
{
public:
    StressNitrogenPea(const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        pp.initialiser(events);

        Stade.init(this, "Stade", events);//cycle
        QNG.init(this, "QNG", events);//cycle
        QN_tot.init(this, "QN_tot", events);
        QNc.init(this, "QNc", events);
        MS.init(this, "MS", events);
        MSG.init(this, "MSG", events);//cycle yield
        Tmoy.init(this, "Tmoy", events);
        STapLEVEE.init(this, "STapLEVEE", events);
        QNveg.init(this, "QNveg", events);//cycle yield
        MS_DRG.init(this, "MS_DRG", events);//cycle yield

        INN.init(this, "INN", events);
        aireINN.init(this, "aireINN", events);
        INNint.init(this, "INNint", events);
        stressN_Eb.init(this, "stressN_Eb", events);
        INNint_DF.init (this, "INNint_DF",events) ; 
    }

    virtual ~StressNitrogenPea()
    {}

    virtual void compute(const vd::Time& /*j*/)
    {

        // indice de nutrition azotee le jour j  (sans unite)

        if(Stade() < VEGETATIF){
            INN=0.0;
        } else {
            INN= QNveg()/QNc();
        } 

        // Aire INN cumule depuis initialisation

        if(Stade() < VEGETATIF){
            aireINN=0.0;
        } else if (Stade(-1)==SOL_NU and Stade()==VEGETATIF){
            aireINN= std::max(0.0, INN()*Tmoy());
        } else if (Stade() < DRG_FSLA){
            aireINN= aireINN(-1)+(std::min(INN(),1.0)+std::min(INN(-1),1.0))/2.0
                    * std::max(0.0,Tmoy());
        } else {
            aireINN=aireINN(-1);
        }

        // INN integre le jour j

        if(Stade() < VEGETATIF){
            INNint=0.0;
        } else if (Stade(-1)==SOL_NU and Stade()==VEGETATIF){
            INNint= aireINN()/Tmoy();
        } else if (Stade() < DRG_FSLA){
            INNint= aireINN()/ STapLEVEE();
        } else {
            INNint=INNint(-1);
        }

        // Facteur de diminution du Eb en fonction de l INN
        //(de 0 :pas de stress a 1 : stress TOTAL)

        if(Stade() < VEGETATIF or (Stade(-1)==SOL_NU and Stade()==VEGETATIF)){
            stressN_Eb=1;
        } else {
            stressN_Eb = std::min(1.0, pp.stressN_Eb2*(1-
                   pp.stressN_Eb3*std::exp(pp.stressN_Eb1*INNint())));
        }
    

        //Indice de nutrition azotée à début floraison (g/m²)

        if (Stade () < DF_DRG) {
            INNint_DF= 0.0;
        } else if(Stade(-1) == VEGETATIF and Stade() == DF_DRG ) {
            INNint_DF= INNint();
        } else {
            INNint_DF=INNint_DF(-1);
        }
    }
	
private:

    ParametersPlantPea pp;

    /*Sync*/ Var Stade;
    /*Sync*/ Var QNG;
    /*Sync*/ Var QN_tot;
    /*Sync*/ Var QNc;
    /*Sync*/ Var MS;
    /*Sync*/ Var MSG;
    /*Sync*/ Var Tmoy;
    /*Sync*/ Var STapLEVEE;
    /*Sync*/ Var QNveg;
    /*Sync*/ Var MS_DRG;

    Var INN;
    Var aireINN;
    Var INNint;
    Var stressN_Eb;
    Var INNint_DF ;

//    double date_DRG;//TODO init with pheno
//    double MS_DRG; //TODO not found
//    double pMSapDRG; //TODO not found
};

} // namespace

DECLARE_DYNAMICS(AZODYN::StressNitrogenPea)

