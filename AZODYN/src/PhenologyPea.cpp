// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <ParametersPlant.hpp>
#include<iostream>
#include<cstdlib>
#include<time.h>

#include "ParametersPlantPea.hpp"
#include "Phenology.hpp"

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;

class PhenologyPea : public DiscreteTimeDyn
{
public:
    PhenologyPea(const vd::DynamicsInit& atom,
                 const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        pp.initialiser(events);

        Tmin.init(this, "Tmin", events);
        Tmax.init(this, "Tmax", events);

        IndicMP.init(this, "IndicMP", events);

        Tmoy.init(this, "Tmoy", events);
        Tmoy_base.init(this, "Tmoy_base", events);
        Stade.init(this, "Stade", events),
        date_LEVEE.init(this, "date_LEVEE", events);
        date_DRG.init(this, "date_DRG", events);
        date_FSLA.init(this, "date_FSLA", events);
        date_MP.init(this, "date_MP", events);
        date_MP_prev.init(this, "date_MP_prev", events);

        STapS.init(this, "STapS", events);
        STapLEVEE.init(this, "STapLEVEE", events);
        STapDF.init(this, "STapDF", events);
        STapDRG.init(this, "STapDRG", events);
        STapFSLA.init(this, "STapFSLA", events);

        Nj_LEVEE.init(this, "Nj_LEVEE", events);
        Nj_DRG.init(this, "Nj_DRG", events);
        Nj_FSLA.init(this, "Nj_FSLA", events);
        Nj_MP.init(this, "Nj_MP", events);
        Nj_S_DF.init(this, "Nj_S_DF", events);
        Nj_S_MP.init(this, "Nj_S_MP", events);

    }

    virtual ~PhenologyPea()
    {}

    virtual void compute(const vd::Time& j)
    {

        // Calcul de la temperature moyenne au jour j
        Tmoy= (Tmin()+Tmax())/2;

        // Degrés efficaces pour le developement de la plante,  
        //en prenant en compte un Tempeature de base
        Tmoy_base = Tmoy() - pp.Tbase;

        // Temps thermique depuis le semis
        if (pp.begin_date + j < pp.date_S) {
            STapS = 0.0;
        } else  {
            STapS = STapS(-1) + Tmoy_base();
        }

        //calcul Stade // TODO pas besoin d'initialiser la variable stade ? 
        if ((Stade(-1) == SOL_NU) and (STapS(-1) >= pp.ST_S_LEVEE)) {
            Stade = VEGETATIF;
        } else if (Stade(-1) ==VEGETATIF and pp.begin_date + j >= pp.date_FLO) {
            Stade = DF_DRG;
        } else if (Stade(-1) == DF_DRG and STapDF(-1) >= pp.ST_DF_DRG) {
            Stade = DRG_FSLA;
        } else if (Stade(-1) == DRG_FSLA and
                STapDRG(-1) >= (pp.DRG_ET*(pp.NETpot-1))) {
            Stade = FSLA_MP;
        } else if ((Stade(-1) == FSLA_MP) and (

                (IndicMP(-1)==1) or
                (STapFSLA(-1) >= pp.STmax_FSLA_MP))){
            Stade=MP;
        } else {
            Stade = Stade(-1);
        }

        //calcul date levee
        if (Stade(-1) == SOL_NU and Stade() == VEGETATIF) {
            date_LEVEE = pp.begin_date + j;
        } else {
            date_LEVEE = date_LEVEE(-1);
        }

        //calcul date DRG
        if (Stade(-1) == DF_DRG and Stade() == DRG_FSLA) {
            date_DRG = pp.begin_date + j;
        } else {
            date_DRG = date_DRG(-1);
        }

        //calcul date FSLA
        if (Stade(-1) == DRG_FSLA and Stade() == FSLA_MP) {
            date_FSLA = pp.begin_date + j;
        } else {
            date_FSLA = date_FSLA(-1);
        }

        //calcul date MP
        if (Stade(-1) == FSLA_MP and Stade() == MP) {
            date_MP = pp.begin_date + j;
        } else {
            date_MP = date_MP(-1);
        }
       // Temps thermique depuis la levee
        if (Stade() < VEGETATIF) {
            STapLEVEE = 0.0;
        } else  {
            STapLEVEE = STapLEVEE(-1) + Tmoy_base();
        }

        // Temps thermique depuis  debut floraison
        if (Stade() < DF_DRG) {
            STapDF = 0.0;
        } else  {
            STapDF = STapDF(-1) + Tmoy_base();
        }

        // Temps thermique depuis  DRG
        if (Stade() < DRG_FSLA) {
            STapDRG = 0.0;
        } else  {
            STapDRG = STapDRG(-1) + Tmoy_base();
        }

        // Temps thermique depuis  FSLA
        if (Stade() < FSLA_MP) {
            STapFSLA = 0.0;
        } else  {
            STapFSLA = STapFSLA(-1) + Tmoy_base();
        }

        //Cacul de la la NjLEVEE qui est le nombre de jours depuis le jour de la levee
        if (Stade() < VEGETATIF){
            Nj_LEVEE =0;
        } else {
            Nj_LEVEE=Nj_LEVEE(-1) +1;
        }

        //Cacul de la la NjDRG qui est le nombre de jours depuis le debut de remplissage des grains
        if (Stade() < DRG_FSLA) {
            Nj_DRG=0;
        } else {
            Nj_DRG=Nj_DRG(-1) +1;
        }

        //Cacul de la la NjFsla qui est le nombre de jours depuis la dateFsla
        if (Stade() < FSLA_MP){
            Nj_FSLA=0;
        } else {
            Nj_FSLA=Nj_FSLA(-1) +1;
        }

        //Cacul de NjMP qui est le nombre de jours depuis la maturite physio
        if (Stade() < MP){
            Nj_MP=0;
        } else {
            Nj_MP=Nj_MP(-1) +1;
        }

        //Calcul de la de maturite previsionnelle
        if(Nj_MP()==7){
            date_MP_prev=pp.begin_date + j;
        }
		
        //Nombre de jours entre le semis et la date de début floraison
	if (Stade() >= DF_DRG){
            Nj_S_DF= pp.date_FLO - pp.date_S;
        }
		
	//Nombre de jours entre le semis et la date de de maturité physiologique
	if (Stade() >= MP){
            Nj_S_MP= date_MP() - pp.date_S;
        }	
    }



    virtual void initValue(const vd::Time& /*time*/)
    {
        Stade  = SOL_NU;
    }

private:

    ParametersPlantPea pp;

    /*Sync*/ Var Tmin;
    /*Sync*/ Var Tmax;
    /*Nosync*/ Var IndicMP;

    Var Tmoy;
    Var Tmoy_base;
    Var Stade;
    Var date_LEVEE;
    Var date_DRG;
    Var date_FSLA;
    Var date_MP;
    Var date_MP_prev;
    Var STapS;
    Var STapLEVEE;
    Var STapDF;
    Var STapDRG;
    Var STapFSLA;
    Var Nj_LEVEE;
    Var Nj_DRG;
    Var Nj_FSLA;
    Var Nj_MP;
    Var Nj_S_DF;
    Var Nj_S_MP;
};

} // namespace AZODYN

DECLARE_DYNAMICS(AZODYN::PhenologyPea)

