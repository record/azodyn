// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <ParametersPlantRape.hpp>
#include <phenologie_types.hpp>

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

namespace AZODYN
{

    using namespace std;

    class INN : public DiscreteTimeDyn
    {
    public:
        INN(const vd::DynamicsInit &model, const vd::InitEventList &evts) : DiscreteTimeDyn(model, evts)
        {

            // declaration paramètres du module

            pp.initialiser(evts);

            // declaration variables d'entrée du module

            DeltaMSAg.init(this, "DeltaMSAg", evts);
            QN_offre.init(this, "QN_offre", evts);

            Stade.init(this, "Stade", evts);
            STlevee.init(this, "STlevee", evts);
            MSAr.init(this, "MSAr", evts);
            MSfm.init(this, "MSfm", evts);
            NAcr.init(this, "NAcr", evts);
            Tmoy.init(this, "Tmoy", evts);
            QNAcr.init(this, "QNAcr", evts);
            QNaccess_C0.init(this, "QNaccess_C0", evts);
            QNaccess_C1.init(this, "QNaccess_C1", evts);
            STsemis.init(this, "STsemis", evts);

            // declaration variables du module
            partQNGreen.init(this, "partQNGreen", evts);
            partQNLeaves.init(this, "partQNLeaves", evts);
            partQNPod.init(this, "partQNPod", evts);
            QNGreen.init(this, "QNGreen", evts);
            QNLeaves.init(this, "QNLeaves", evts);
            QNPod.init(this, "QNPod", evts);
            INNintvec.init(this, "INNintvec", evts);
            AireINNvec.init(this, "AireINNvec", evts);
            SommeAireINNvec.init(this, "SommeAireINNvec", evts);
            INNvec.init(this, "INNvec", evts);
            Nar.init(this, "Nar", evts);
            QNAr.init(this, "QNAr", evts);
            QNAg.init(this, "QNAg", evts);
            DeltaQNAg.init(this, "DeltaQNAg", evts);
            QNTr.init(this, "QNTr", evts);
            QNTg.init(this, "QNTg", evts);
            QNTmax.init(this, "QNTmax", evts);
            Nabs.init(this, "Nabs", evts);
            QNabs_C0.init(this, "QNabs_C0", evts);
            QNabs_C1.init(this, "QNabs_C1", evts);
            Besoins.init(this, "Besoins", evts);
            Ncmax.init(this, "Ncmax", evts);
            QNdeadLeaves.init(this, "QNdeadLeaves", evts);
        }
        ~INN()
        {
        }
        //@@begin:compute@@

        virtual void compute(const vd::Time & /*time*/)
        {
            updateNmax();
            updateBesoins();
            updateNabs();
            updateQNabs_C0();
            updateQNabs_C1();
            updateQNdeadLeaves();
            updateQNTg();
            updateQNTr();
            updateQNAr();
            updateQNAg();
            updateDeltaQNAg();
            updateNar();
            updateINNvec();
            updateAireINNvec();
            updateINNintvec();
            updatePartQNGreen();
            updateQNGreen();
            updatePartQNLeaves();
            updateQNLeaves();
            updatePartQNPod();
            updateQNPod();
        }

    private:
        /// Absorption d'azote par la plante

        // Calcul de la quantité maximale d'azote (kg/ha) et de la teneur en azote critique de la plante (%)

        void updateNmax()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                Ncmax = 0.0;
                QNTmax = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
                Ncmax = min(pp.tNmax_Nmax, pp.coef_Nmax * pow((MSAr(-1) + DeltaMSAg(-1)) / 1000, pp.pente_Nmax));
                QNTmax = pp.coef_tot_a * ((MSAr(-1) + DeltaMSAg(-1)) * (Ncmax() / 100)) + pp.BQNT;
                break;
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                Ncmax = 0.0;
                QNTmax = 0.0;
                break;
            }
        }

        // Calcul des besoins en azote de la plante (kg/ha/j)

        void updateBesoins()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                Besoins = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
                Besoins = max(0.0,
                              min(pp.Vmax * Tmoy(),
                                  QNTmax() - (pp.coef_tot_a * MSAr(-1) * min(pp.tNmax_Nmax, pp.coef_Nmax * pow((MSAr(-1) / 1000), pp.pente_Nmax)) / 100 + pp.BQNT)));
                break;
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                Besoins = 0.0;
                break;
            }
        }

        // Calcul de la quantité d'azote absorbée (kg/ha)

        // dans les couches avec racines

        void updateNabs()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                Nabs = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
                Nabs = max(0.0, min(min(QN_offre(-1), Besoins()), pp.Vmax * Tmoy()));
                break;
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                Nabs = 0.0;
                break;
            }
        }

        // dans la couche de minéralisation C0

        void updateQNabs_C0()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNabs_C0 = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
                QNabs_C0 = QNaccess_C0() / (QNaccess_C0() + QNaccess_C1()) * Nabs();
                break;
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNabs_C0 = 0.0;
                break;
            }
        }

        // dans la couche C1

        void updateQNabs_C1()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNabs_C1 = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
                QNabs_C1 = QNaccess_C1() / (QNaccess_C0() + QNaccess_C1()) * Nabs();
                break;
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNabs_C1 = 0.0;
                break;
            }
        }

        /// Calcul des teneurs en azote

        // Compartiment Azote des feuilles mortes

        void updateQNdeadLeaves()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNdeadLeaves = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNdeadLeaves = QNdeadLeaves(-1) + (MSfm() - MSfm(-1)) * (pp.ANFM * INNvec(-1) + pp.BNFM) / 100;
                break;
            }
        }

        // Compartiment Azote Total

        // Calcul de la quantité d'azote totale absorbée par la plante (kg/ha)

        void updateQNTg()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNTg = 0.0;
                break;
            case JOUR_LEVEE:
                QNTg = pp.coef_tot_a * QNAcr() + pp.BQNT;
                break;
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNTg = QNTg(-1) + Nabs();
                break;
            }
        }

        // Calcul de la quantité d'azote totale réelle de la plante (kg/ha)

        void updateQNTr()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNTr = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNTr = QNTg() - QNdeadLeaves();
                break;
            }
        }

        // Compartiment Azote Aérien

        // Calcul de la quantité d'azote réelle dans les parties aériennes de la plante (kg/ha)

        void updateQNAr()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNAr = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNAr = (QNTr() - pp.BQNT) / pp.coef_tot_a;
                ;
                break;
            }
        }

        // Calcul de la quantité d'azote absorbée dans les parties aériennes de la plante (kg/ha)

        void updateQNAg()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNAg = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNAg = (QNTg() - pp.BQNT) / pp.coef_tot_a;
                break;
            }
        }

        // Calcul de la variation d'azote aérien absorbée dans les parties aériennes de la plante (kg/ha)

        void updateDeltaQNAg()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                DeltaQNAg = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                DeltaQNAg = QNAg() - QNAg(-1);
                break;
            }
        }

        // Calcul de la teneur en azote dans les parties aériennes de la plante (%)

        void updateNar()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                Nar = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                Nar = (MSAr() > 1e-15) ? QNAr() * 100 / MSAr() : 0.0;
                break;
            }
        }

        // Calcul de l'INN instantané

        void updateINNvec()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                INNvec = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                INNvec = (NAcr() > 1e-15) ? Nar() / NAcr() : 0.0;
                break;
            }
        }

        // Calcul de l'aire sous la courbe d'INN instantané

        void updateAireINNvec()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                AireINNvec = 0.0;
                SommeAireINNvec = 0.0;
                break;
            case JOUR_LEVEE:
                AireINNvec = 1.0;
                SommeAireINNvec = 1.0;
                break;
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                AireINNvec = ((min(INNvec(), 1.0) + min(INNvec(-1), 1.0)) / 2.0) * max(0.0, Tmoy());
                SommeAireINNvec = SommeAireINNvec(-1) + AireINNvec();
                break;
            }
        }

        // Calcul de l'INN intégré

        void updateINNintvec()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                INNintvec = 0.0;
                break;
            case JOUR_LEVEE:
                INNintvec = INNvec();
                break;
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                INNintvec = SommeAireINNvec() / STlevee();
                break;
            }
        }

        // Compartiment Azote Vert (tige + hampes + feuilles)

        // Calcul de l'allocation d'azote dans les tiges + feuilles vertes issu des parties aériennes (QNAr) (sans dim)

        void updatePartQNGreen()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                partQNGreen = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
                partQNGreen = 1.0;
                break;
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                partQNGreen = min(1.0, max(0.0, pp.ApartQNGreen + pp.BpartQNGreen * STsemis()));
                break;
            }
        }

        // Calcul de la quantité d'azote dans les tiges + feuilles vertes (kg/ha)

        void updateQNGreen()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNGreen = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
                QNGreen = pp.AQNGreen + pp.BQNGreen * QNAr();
                break;
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNGreen = partQNGreen() * QNAr();
                break;
            }
        }

        // Compartiment Azote des feuilles

        // Calcul de l'allocation d'azote dans les feuilles issu de l'azote vert (QNGreen) (sans dim)

        void updatePartQNLeaves()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                partQNLeaves = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
                partQNLeaves = min(1.0, max(0.0, pp.ApartQNLeaves + pp.BpartQNLeaves * STsemis()));
                break;
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                partQNLeaves = min(1.0, max(0.0, pp.CpartQNLeaves + pp.DpartQNLeaves * STsemis()));
                break;
            }
        }

        // Calcul de la quantité d'azote dans les feuilles (kg/ha)

        void updateQNLeaves()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNLeaves = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNLeaves = QNGreen() * partQNLeaves();
                break;
            }
        }

        // Compartiment Azote des siliques

        // Calcul de l'allocation d'azote dans les siliques issu de l'azote aérien (QNAr) (sans dim)

        void updatePartQNPod()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
                partQNPod = 0.0;
                break;
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                partQNPod = min(1.0, max(0.0, pp.ApartQNPod + pp.BpartQNPod * STsemis()));
                break;
            }
        }

        // Calcul de la quantité d'azote dans les siliques (kg/ha)

        void updateQNPod()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
                QNPod = 0.0;
                break;
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNPod = partQNPod() * QNAr();
                break;
            }
        }

        ParametersPlantRape pp;

        Var STfloraison;
        Var DeltaMSAg;

        Var Stade;
        Var STlevee;
        Var MSAr;
        Var MSfm;
        Var NAcr;
        Var Tmoy;
        Var QNAcr;
        Var QNaccess_C0;
        Var QNaccess_C1;
        Var QN_offre;
        Var STsemis;

        Var INNintvec;
        Var AireINNvec;
        Var SommeAireINNvec;
        Var INNvec;
        Var Nar;
        Var QNAr;
        Var QNLeaves;
        Var partQNLeaves;
        Var QNAg;
        Var DeltaQNAg;
        Var QNTr;
        Var QNTg;
        Var Nabs;
        Var QNabs_C0;
        Var QNabs_C1;
        Var Besoins;
        Var Ncmax;
        Var QNTmax;
        Var partQNGreen;
        Var QNGreen;
        Var partQNPod;
        Var QNPod;
        Var QNdeadLeaves;
    };

} // namespace

DECLARE_DYNAMICS(AZODYN::INN)
