// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <ParametersSoilRape.hpp>

#include <cmath>     //log
#include <numeric>   //std::accumulate
#include <algorithm> //std::rotate

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

namespace AZODYN
{

    class Fertilisation : public DiscreteTimeDyn
    {
    public:
        Fertilisation(const vd::DynamicsInit &model, const vd::InitEventList &evts) : DiscreteTimeDyn(model, evts)
        {
            sommePluie.resize(7, 0.0);

            ps.initialiser(evts);

            MSAg.init(this, "MSAg", evts);
            MSAg.history_size(8);
            MSAr.init(this, "MSAr", evts);
            STlevee.init(this, "STlevee", evts);
            STlevee.history_size(8);
            Pluie.init(this, "Pluie", evts);
            I.init(this, "I", evts);
            Napport.init(this, "Napport", evts);

            QNEng.init(this, "QNEng", evts);
            NapportPresent.init(this, "NapportPresent", evts);
            sPluie7.init(this, "sPluie7", evts);
            CAU.init(this, "CAU", evts);
        }

        virtual ~Fertilisation(){};

        virtual void compute(const vd::Time & /*time*/)

        {

            std::rotate(sommePluie.rbegin(), sommePluie.rbegin() + 1, sommePluie.rend());
            sommePluie[0] = Pluie() + I();
            sPluie7 = std::accumulate(sommePluie.begin(), sommePluie.begin() + 7, 0.);

            double napportPresent = 0;
            napportPresent = NapportPresent(-1) + Napport();
            // calcul de la somme des pluies sur les 7 derniers jours

            if ((napportPresent > 0) && (sPluie7() >= ps.PLUIEVALOR))
            {
                // l'azote apporté est mobilise

                CAU = std::min(100.0, std::max(0.0,
                                               (ps.lambda + ((ps.mu * 100. * (log(MSAg() / 10) - log(MSAg(-7) / 10)) * (MSAr() / 10)) / (STlevee() - STlevee(-7))))));

                NapportPresent = 0;
                QNEng = napportPresent * CAU() / 100;
            }
            else
            {
                // pas d'azote apporté mobilisé
                NapportPresent = napportPresent;
                QNEng = 0;
            }
        }

    private:
        ParametersSoilRape ps;

        std::vector<double> sommePluie;

        Var MSAg;
        Var MSAr;
        Var STlevee;
        Var Pluie;
        Var I;

        Var QNEng;
        Var Napport;
        Var NapportPresent;
        Var sPluie7;
        Var CAU;
    };

} // namespace AzodynColza

DECLARE_DYNAMICS(AZODYN::Fertilisation)
