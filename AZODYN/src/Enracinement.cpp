// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <ParametersPlantRape.hpp>

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

namespace AZODYN
{

    class Enracinement : public DiscreteTimeDyn
    {
    public:
        /**
         * @brief Construction de Enracinement
         */
        Enracinement(const vd::DynamicsInit &model, const vd::InitEventList &evts) : DiscreteTimeDyn(model, evts)
        {
            pp.initialiser(evts);

            // Variables gérées par ce composant
            ProfRac.init(this, "ProfRac", evts);

            // Variables gérées par un autre composant
            STlevee.init(this, "STlevee", evts);
        }

        /**
         * @brief Destruction de Enracinement
         */
        ~Enracinement(){};

        /**
         * @brief Calcul des variables de Enracinement
         */
        virtual void compute(const vd::Time & /*time*/)
        {
            ProfRac = std::min(pp.ProfRacmax, pp.Velong * STlevee());
        }

    private:
        ParametersPlantRape pp;

        Var ProfRac; // profondeur racinaire en mm

        Var STlevee;
    };

} // namespace

DECLARE_DYNAMICS(AZODYN::Enracinement)
