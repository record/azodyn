// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <Phenology.hpp>
#include <ParametersPlantPea.hpp>
#include<iostream>
#include<cstdlib>
#include<time.h>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {

using namespace vle::utils;
using namespace vle::discrete_time;

class NitrogenPlantPea : public DiscreteTimeDyn
{
public:
    NitrogenPlantPea(const vd::DynamicsInit& atom,
                     const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        pp.initialiser(events);

        Stade.init(this, "Stade", events);
        MS.init(this, "MS", events);
        MS_DRG.init(this,"MS_DRG", events);
        dQNfix_tot.init(this, "dQNfix_tot", events);
        Tmoy.init(this, "Tmoy", events);
        QN_offre.init(this, "QN_offre", events);
        QNaccess_C0.init(this, "QNaccess_C0", events);
        QNaccess_C1.init(this, "QNaccess_C1", events);
        densRac.init(this, "densRac", events);
        QNveg.init(this, "QNveg", events);
        MSG.init(this, "MSG", events);

        dQN_besoin.init(this, "dQN_besoin", events);
        dQN_demande.init(this, "dQN_demande", events);
        dQNabs.init(this, "dQNabs", events);
        QN_tot.init(this, "QN_tot", events);
        QNc.init(this, "QNc", events);
        dQNabs_C0.init(this, "dQNabs_C0", events);
        dQNabs_C1.init(this, "dQNabs_C1", events);
        tNa.init(this, "tNa", events);
        tNa_DF.init(this, "tNa_DF", events);
        tNveg.init(this, "tNveg", events);
        tNveg_MP.init(this, "tNveg_MP", events);
    }

    virtual ~NitrogenPlantPea()
    {}

    virtual void compute(const vd::Time& /*j*/)
    {
        //Quantite d azote totale accumulee par la culture le jour j (kg N/ha)

        if(Stade() < VEGETATIF){
            QN_tot=0.0;
        } else if ((Stade(-1)==SOL_NU) and (Stade()==VEGETATIF)){
            if (pp.QNa_init == -1) {//initialization of QNa available
                if (MS()/100<pp.MS_Nc_seuil){
                    QN_tot= MS()*10.0* (pp.tNc_Nc/100)*pp.coef_tot_a;
                } else {
                    QN_tot= MS()*10.0*pp.coef_Nc/100*
                            std::pow((MS()/100),pp.pente_Nc)*pp.coef_tot_a;
                }
            } else {
                QN_tot=pp.QNa_init*pp.coef_tot_a;
            }
        } else {
            QN_tot= QN_tot(-1) + dQNabs(-1) + dQNfix_tot(-1);
        }

        //  besoin de la culture le jour j  (kg N/ha)
        if (Stade() < VEGETATIF or Stade () ==MP){
            dQN_besoin = 0.0 ;
        } else if(Stade() <DRG_FSLA){
            if((MS()/100)<pp.MS_Nmax_seuil) {
                dQN_besoin= std::max(0.0,(pp.coef_tot_a*MS()*10.0*pp.tNmax_Nmax/100)-
                        (pp.coef_tot_a*MS(-1)*10.0*pp.tNmax_Nmax/100)) ;
            } else {
                dQN_besoin= std::max(0.0,pp.coef_tot_a*MS()*10.0*pp.coef_Nmax/100*std::pow((MS()/100),pp.pente_Nmax) -
                        pp.coef_tot_a*MS(-1)*10.0*pp.coef_Nmax/100*std::pow((MS(-1)/100),pp.pente_Nmax));
            }
        } else {
            dQN_besoin= std::max(0.0,pp.coef_tot_a*MS()*10.0*pp.coef_Nmax/100*std::pow(MS_DRG()*pp.pMSapDRG/100,pp.pente_Nmax) -
                        pp.coef_tot_a*MS(-1)*10.0*pp.coef_Nmax/100*std::pow(MS_DRG()*pp.pMSapDRG/100,pp.pente_Nmax));
        }


        // demande en azote total de la culture en prenant en compte ses besoins et sa vitesse maximale d'absorption  (kg N/ha)

        if(Tmoy() <= 0.0 or Stade() < VEGETATIF){
            dQN_demande=0.0;
        } else {
            dQN_demande=std::min(std::max(0.0,Tmoy(-1))*pp.Vmax,dQN_besoin());
        }


        //Quantite d azote absorbe par la culture le jour j (en kg.ha-1)
        if (Stade() < VEGETATIF){
            dQNabs = 0.0 ;
        } else {
            dQNabs=std::max(0.0,std::min(dQN_demande()- dQNfix_tot(),
                    QN_offre()*densRac()/100));
        }


        //quantites d'azote absorbees dans C0 (kg/ha)

        if (QN_offre() == 0.0) {
            dQNabs_C0 = 0;
        } else {
            dQNabs_C0 = QNaccess_C0()/(QNaccess_C0() + QNaccess_C1()) * dQNabs();
        }

        //quantites d'azote absorbees dans C1 (kg/ha)

        if (QN_offre() == 0.0) {
            dQNabs_C1 = 0;
        } else {
            dQNabs_C1 = QNaccess_C1()/(QNaccess_C0() + QNaccess_C1()) * dQNabs();
        }

        //teneur en azote aerien (%)

        if(Stade() < VEGETATIF){
            tNa = 0;
        } else if (MS()/100<pp.MS_Nmax_seuil) {
            tNa = pp.tNmax_Nmax;
        } else {
            tNa = 100*(QN_tot()/pp.coef_tot_a)/(MS()*10);
        }
			
         //Teneur en azote des parties aériennes à début floraison
	
	if(Stade() < DF_DRG) {
            tNa_DF = 0 ;
        } else if (Stade(-1) == VEGETATIF and Stade() == DF_DRG) {
            tNa_DF = tNa() ;
        } else { 
            tNa_DF = tNa_DF(-1) ;
        }
		

        //teneur en azote de la partie vegetative, partie aerienne(sans les graines) (%)


        if(Stade() < VEGETATIF){
            tNveg = 0;
        } else if (Stade() <= DRG_FSLA) {
            tNveg = tNa();
       	} else {
            tNveg = std::min(100*QNveg() /((MS()-MSG())*10 ),tNveg(-1));
        }
		
		//teneur en azote des parties aériennes végétatives à maturité physiologique (%)


        if(Stade() < MP){
            tNveg_MP = 0;
        } else if (Stade(-1) == FSLA_MP and Stade() == MP) {
            tNveg_MP = tNveg();
       	} else {
            tNveg_MP = tNveg_MP(-1);
        }

        // Quantite d azote critique le jour j  (kg N/ha)


		
        if (Stade() < VEGETATIF ){
            dQN_besoin = 0.0 ;
        } else if(Stade() <DRG_FSLA){
            if((MS()/100)<pp.MS_Nmax_seuil) {
                QNc= MS()*10.0* (pp.tNc_Nc/100);
            } else {
                QNc= MS()*10.0*pp.coef_Nc/100*std::pow(MS()/100,pp.pente_Nc);
            }
        } else {
            QNc= MS()*10.0*pp.coef_Nc/100*std::pow((MS_DRG()*pp.pMSapDRG/100),pp.pente_Nc);
        }

    }

private:
    ParametersPlantPea pp ;

    /*Sync*/ Var Stade;
    /*Sync*/ Var MS;
    /*Sync*/ Var dQNfix_tot;
    /*Sync*/ Var Tmoy;
    /*Sync*/ Var QN_offre;
    /*Sync*/ Var QNaccess_C0;
    /*Sync*/ Var QNaccess_C1;
    /*Sync*/ Var densRac;
    /*Sync*/ Var QNveg;
    /*Sync*/ Var MSG;
    /*Sync*/ Var MS_DRG ;

    Var dQN_besoin;
    Var dQN_demande;
    Var dQNabs;
    Var QN_tot;
    Var QNc;
    Var dQNabs_C0;
    Var dQNabs_C1;
    Var tNa;
    Var tNveg;
    Var tNa_DF ;
    Var tNveg_MP ; 
};

} // namespace

DECLARE_DYNAMICS(AZODYN::NitrogenPlantPea)

