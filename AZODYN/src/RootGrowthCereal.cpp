// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include "ParametersPlantCereal.hpp"
#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;

class RootGrowthCereal : public DiscreteTimeDyn
{
public:
    RootGrowthCereal(const vd::DynamicsInit& atom,
               const vd::InitEventList& events)
        : DiscreteTimeDyn(atom, events)
    {
        pp.initialiser(events);

        STapLEVEE.init(this, "STapLEVEE", events);

        ProfRacmax.init(this, "ProfRacmax", events);
        ProfRac.init(this, "ProfRac", events);

    }

    virtual ~RootGrowthCereal()
    {}

    virtual void compute(const vd::Time& j)
    {

        // Croissance des racines

        ProfRacmax = pp.ProfRacmax;

        // Profondeur racinaire le jour j  (mm)
        if(pp.begin_date + j<pp.date_LEVEE){
            ProfRac = 0.0;
        } else {
            ProfRac = std::min(pp.ProfRacmax, pp.vRac_ST*STapLEVEE());
        }



    }


private:

    ParametersPlantCereal pp;

    /*Sync*/ Var STapLEVEE;

    Var ProfRacmax;
    Var ProfRac;


};

} // namespace

DECLARE_DYNAMICS(AZODYN::RootGrowthCereal)

