// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <algorithm>
#include <numeric>
#include <vle/DiscreteTime.hpp>
#include "ParametersPlantCereal.hpp"

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {

using namespace vle::utils;
using namespace vle::discrete_time;

class NitrogenLoss : public DiscreteTimeDyn
{
public:
    NitrogenLoss(
            const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        //syncs
        CAU.init(this, "CAU", events);
        StockNH4.init(this, "StockNH4", events);
        QNEng.init(this, "QNEng", events);
        QNlixiv_tot.init(this, "QNlixiv_tot", events);
	//no syncs
        jMatur.init(this, "jMatur", events);
        //vars
        QNEng_loss.init(this, "QNEng_loss", events);
        QNEng_loss_tot.init(this, "QNEng_loss_tot", events);
        QN_loss_tot.init(this, "QN_loss_tot", events);
        QN_loss_tot_harvest.init(this, "QN_loss_tot_harvest", events);
    }

    virtual ~NitrogenLoss()
    {}

    virtual void compute(const vd::Time& /*j*/)
    {
        if (QNEng() > 0.0) {
            //Valorisation: loss is what is not used
            QNEng_loss = QNEng() * (100.0 - CAU())/CAU();
        } else {
            QNEng_loss = std::max(0.0, (StockNH4(-1) - StockNH4()));
        }
        QNEng_loss_tot = QNEng_loss_tot(-1)+QNEng_loss();
        
        QN_loss_tot = QNEng_loss_tot() + QNlixiv_tot();
        
        if (jMatur(-1) == 0) {//si avant recolte
          QN_loss_tot_harvest = QNEng_loss_tot() + QNlixiv_tot();
        } else {
          QN_loss_tot_harvest = QN_loss_tot_harvest(-1);
        }
    }


private:
    /*Sync*/ Var QNEng;
    /*Sync*/ Var CAU;
    /*Sync*/ Var StockNH4;
    /*Sync*/ Var QNlixiv_tot;
    /*Nosync*/ Var jMatur;
    Var QNEng_loss;
    Var QNEng_loss_tot;
    Var QN_loss_tot;
    Var QN_loss_tot_harvest;
};

} // namespace

DECLARE_DYNAMICS(AZODYN::NitrogenLoss)

