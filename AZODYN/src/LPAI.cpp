// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <ParametersPlantRape.hpp>
#include <phenologie_types.hpp>

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

namespace AZODYN
{
    class LPAI : public DiscreteTimeDyn
    {
    public:
        LPAI(const vd::DynamicsInit &model, const vd::InitEventList &evts) : DiscreteTimeDyn(model, evts)
        {
            // declaration paramètres du module

            pp.initialiser(evts);

            // declaration variables d'entrée du module

            QNLeaves.init(this, "QNLeaves", evts);
            QNPod.init(this, "QNPod", evts);
            STsemis.init(this, "STsemis", evts);
            Stade.init(this, "Stade", evts);
            INNvec.init(this, "INNvec", evts);
            RG.init(this, "RG", evts);
            Tmoy.init(this, "Tmoy", evts);

            // declaration variables du module

            LAI.init(this, "LAI", evts);
            EiLeaves.init(this, "EiLeaves", evts);
            EbLeaves.init(this, "EbLeaves", evts);
            RGintercept.init(this, "RGintercept", evts);
            PAI.init(this, "PAI", evts);
            EiPod.init(this, "EiPod", evts);
            EbPod.init(this, "EbPod", evts);
            partMSPodWall.init(this, "partMSPodWall", evts);
            MSPod.init(this, "MSPod", evts);
            MSPodWall.init(this, "MSPodWall", evts);
        }

        virtual ~LPAI()
        {
        }

        virtual void compute(const vd::Time & /*time*/)
        {
            updateLAI();
            updateEiLeaves();
            updateEbLeaves();
            updateMSPod();
            updatePartMSPodWall();
            updateMSPodWall();
            updatePAI();
            updateEiPod();
            updateEbPod();
            updateRGintercept();
        }

    private:
        /// Compartiment Feuilles

        // Calcul de la LAI (m2/m2)

        void updateLAI()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                LAI = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                LAI = exp(pp.ALAI + pp.BLAI * log(QNLeaves()));
                break;
            }
        }

        // Calcul de l'efficience d'interception des feuilles

        void updateEiLeaves()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                EiLeaves = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                EiLeaves = pp.EILMAX * (1 - exp(-pp.K * LAI()));
                break;
            }
        }

        // Calcul de l'efficience de conversion des feuilles

        void updateEbLeaves()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                EbLeaves = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                EbLeaves = (Tmoy() < pp.TMINEB0)
                               ? 1.0
                           : (Tmoy() < pp.TMINEB1)
                               ? (1 - (pow(((Tmoy() - pp.CEB) / (pp.DEB - pp.EEB)), 2))) * 3.5 * std::min(1.0, pp.AEB * INNvec() + pp.BEB)
                               : (1 - (pow(((Tmoy() - pp.CEB) / (pp.EEB - pp.CEB)), 2))) * 3.5 * std::min(1.0, pp.AEB * INNvec() + pp.BEB);
                break;
            }
        }

        /// Compartiment Siliques

        // Calcul de la matière sèche des siliques (kg/ha)

        void updateMSPod()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
                MSPod = 0.0;
                break;
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                MSPod = exp(pp.AMSPod + pp.BMSPod * log(QNPod()));
                break;
            }
        }

        // Calcul du rapport MS cosse/MS siliques pour calcul de la MS des Cosses (sans dim)

        void updatePartMSPodWall()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
                partMSPodWall = 0.0;
                break;
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                partMSPodWall = std::min(1.0, std::max(0.0, pp.ApartMSPodWall + pp.BpartMSPodWall * STsemis()));
                break;
            }
        }

        // Calcul de la matière sèche des Cosses (kg/ha)

        void updateMSPodWall()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
                MSPodWall = 0.0;
                break;
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                MSPodWall = MSPod() * partMSPodWall();
                break;
            }
        }

        // Calcul du PAI (m2/m2)

        void updatePAI()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
                PAI = 0.0;
                break;
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                PAI = exp(pp.APAI + pp.BPAI * log(MSPodWall()));
                break;
            }
        }

        // Calcul de l'efficience d'interception des siliques

        void updateEiPod()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
                EiPod = 0.0;
                break;
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                EiPod = pp.EIPMAX * (1 - exp(-pp.K * PAI()));
                break;
            }
        }

        // Calcul de l'efficience de conversion des siliques

        void updateEbPod()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
                EbPod = 0.0;
                break;
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                EbPod = (Tmoy() < pp.TMINEB0)
                            ? 1.0
                        : (Tmoy() < pp.TMINEB1)
                            ? (1 - (pow(((Tmoy() - pp.CEB) / (pp.DEB - pp.EEB)), 2))) * (pp.ARG * (pp.Ec * RG()) + pp.BRG) *
                                  std::min(1.0, pp.AEB * INNvec() + pp.BEB)
                            : (1 - (pow(((Tmoy() - pp.CEB) / (pp.EEB - pp.CEB)), 2))) * (pp.ARG * (pp.Ec * RG()) + pp.BRG) *
                                  std::min(1.0, pp.AEB * INNvec() + pp.BEB);
                break;
            }
        }

        // Calcul du rayonnement intercepté par les feuilles et les siliques (utilisé pour calculer le nb de grains dans YieldRapeseed et Rendement)

        void updateRGintercept()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                RGintercept = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                if ((EiLeaves() + EiPod()) <= 0.95)
                {
                    RGintercept = RG() * pp.Ec * (EiLeaves() + EiPod());
                }
                else
                {
                    RGintercept = RG() * pp.Ec * (std::max(0.0, (0.95 - EiPod())) + EiPod());
                }
                break;
            }
        }

        ParametersPlantRape pp;

        double dateRecolte;

        // Sync
        Var QNLeaves;
        Var STsemis;
        Var Stade;
        Var INNvec;
        Var RG;
        Var Tmoy;
        Var QNPod;
        // State Var
        Var LAI;
        Var EiLeaves;
        Var EbLeaves;
        Var RGintercept;
        Var PAI;
        Var EiPod;
        Var EbPod;
        Var partMSPodWall;
        Var MSPod;
        Var MSPodWall;
    };

} // namespace AzodynColza

DECLARE_DYNAMICS(AZODYN::LPAI)
