// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include "ParametersPlantCereal.hpp"

#include<iostream>
#include<cstdlib>
#include<time.h>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {

using namespace vle::discrete_time;
using namespace vle::utils;

class NitrogenPlantCereal : public DiscreteTimeDyn
{
public:
    NitrogenPlantCereal(const vd::DynamicsInit& atom,
                   const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        pp.initialiser(events);

        SJNapFLO.init(this, "SJNapFLO", events);
        STapFLO.init(this, "STapFLO", events);
        MS.init(this, "MS", events);
        Tmoy.init(this, "Tmoy", events);
        QN_offre.init(this, "QN_offre", events);
        QNaccess_C0.init(this, "QNaccess_C0", events);
        QNaccess_C1.init(this, "QNaccess_C1", events);

        QN_besoin.init(this, "QN_besoin", events);
        QN_demande.init(this, "QN_demande", events);
        QNabs.init(this, "QNabs", events);
        QNabs_C0.init(this, "QNabs_C0", events);
        QNabs_C1.init(this, "QNabs_C1", events);
        QN_tot.init(this, "QN_tot", events);
        QNc.init(this, "QNc", events);
        CoefNc.init(this, "CoefNc", events);
        tN_LAG.init(this, "tN_LAG", events);

    }

    virtual ~NitrogenPlantCereal()
    {}

    virtual void compute(const vd::Time& j)
    {
        //Quantite d azote totale par la culture le jour j (kg N/ha)

        if(pp.begin_date + j < pp.date_init_MS){
            QN_tot=0.0;
        } else if (pp.begin_date + j==pp.date_init_MS){
            if (pp.QNa_init == -1) {//initialization of QNa not available
                 if (pp.MS_init/100<pp.MS_Nc_seuil){
                      QN_tot= pp.MS_init*10.0* (pp.tNc_Nc/100)*pp.coef_tot_a*pp.stressINN_init;
                       // calculation of theorical N content impacted by initial INN stress 
                       //stressINN_value value =1 if not available
                 } else {
                      QN_tot= pp.MS_init*10.0*(pp.coef_Nc*
                      std::pow(pp.MS_init/100,pp.pente_Nc))/100*pp.coef_tot_a*pp.stressINN_init;
                        }
            } else {
                QN_tot=pp.QNa_init*pp.coef_tot_a;
                
                
                QN_tot=pp.QNa_init*pp.coef_tot_a;
            }
        } else {
            QN_tot= QN_tot(-1) + QNabs(-1);
        }

         // Teneur en azote a floraison plus LAG
        if (STapFLO() < pp.LAG) {
            tN_LAG= 0.0 ;
        } else if (tN_LAG(-1)>0.0) {
            tN_LAG= tN_LAG(-1) ;
        } else {
            tN_LAG= (QN_tot()/pp.coef_tot_a)/MS();
        }

        //  besoin de la culture le jour j  (kg N/ha)
        
        if (pp.begin_date + j<=pp.date_init_MS){
            QN_besoin = 0.0 ;
        } else {
           if(SJNapFLO() < pp.SJNmax_FLO_RG){
          
//std::cout << " dbgJNapFLO " << vle::utils::DateTime::toJulianDay(j) << " " << SJNapFLO() << ""<< STapFLO() <<"\n" ;

                if(STapFLO() < pp.LAG) {
                    if((MS()/100)<pp.MS_Nmax_seuil) {
                        QN_besoin= (pp.coef_tot_a*(MS()-MS(-1))*10.0*pp.tNmax_Nmax/100) ;
                    } else {
                        QN_besoin= (pp.coef_tot_a*MS()*10.0*(pp.coef_Nmax*std::pow(MS()/100,pp.pente_Nmax))/100)-
                                   (pp.coef_tot_a*MS(-1)*10.0*(pp.coef_Nmax*std::pow(MS(-1)/100,pp.pente_Nmax))/100);
                    }
                } else {
                    QN_besoin= pp.coef_tot_a*MS()*tN_LAG()-
                               pp.coef_tot_a*MS(-1)*tN_LAG();
                }
            }   else {
                QN_besoin = 0.0;
            }
         }

        // demande en azote total de la culture en prenant en compte ses besoins et sa vitesse maximale d'absorption  (kg N/ha)

        if(Tmoy(-1) <= 0.0 or pp.begin_date + j<=pp.date_init_MS){
            QN_demande=0.0;
        } else {
            QN_demande=std::min(Tmoy(-1)*pp.Vmax,QN_besoin());
        }

        
        //Quantite d azote absorbe par la culture le jour j (en kg.ha-1)

        QNabs=std::min(QN_demande(), QN_offre());
        
        //quantites d'azote absorbees dans C0
        
        if (QN_offre() == 0.0) {
          QNabs_C0 = 0.0;
        } else {
          QNabs_C0 = QNaccess_C0()/(QNaccess_C0() + QNaccess_C1()) * QNabs();
        }
        
        //quantites d'azote absorbees dans C1
        
        if (QN_offre() == 0.0) {
          QNabs_C1 = 0;
        } else {
          QNabs_C1 = QNaccess_C1()/(QNaccess_C0() + QNaccess_C1()) * QNabs();
        }

        // teneur d azote critique le jour j  (en %)

        if(pp.begin_date + j < pp.date_init_MS){
            CoefNc=0.0;
        } else if (MS()/100<pp.MS_Nc_seuil){
//            std::cout << " dbgQnc " << vle::utils::DateTime::toJulianDay(j) << " " << MS() << "\n";

            CoefNc= pp.tNc_Nc;
        } else {
//            std::cout << " dbgQnc2 " << vle::utils::DateTime::toJulianDay(j) << " " << MS() << "\n";
            CoefNc= pp.coef_Nc*std::pow(MS()/100,pp.pente_Nc);
        }

        // Quantite d azote critique le jour j  (kg N/ha)
        QNc = CoefNc()/100 * MS()*10.0;

}

private:

    ParametersPlantCereal pp;
  
    /*Sync*/ Var SJNapFLO;
    /*Sync*/ Var STapFLO;
    /*Sync*/ Var MS;
    /*Sync*/ Var Tmoy;
    /*Sync*/ Var QN_offre;
    /*Sync*/ Var QNaccess_C0;
    /*Sync*/ Var QNaccess_C1;

    Var QN_besoin;
    Var QN_demande;
    Var QNabs;
    Var QN_tot;
    Var QNc;
    Var CoefNc;
    Var QNabs_C0;
    Var QNabs_C1;
    Var tN_LAG;
    
};

} // namespace

DECLARE_DYNAMICS(AZODYN::NitrogenPlantCereal)

