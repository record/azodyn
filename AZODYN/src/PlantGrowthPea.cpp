// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>


#include<iostream>
#include<cstdlib>
#include<time.h>

#include <Phenology.hpp>
#include "ParametersPlant.hpp"
#include "ParametersPlantPea.hpp"

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;

class PlantGrowthPea : public DiscreteTimeDyn
{
public:
    PlantGrowthPea(
            const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {

        pp.initialiser(events);

        IndicMP.init(this, "IndicMP", events);
        tNveg.init(this, "tNveg", events);
        QNc.init(this, "QNc", events);
        stressN_Eb.init(this, "stressN_Eb", events);
        stressH.init(this, "stressH", events);
        stressT.init(this, "stressT", events);
        stressGEL.init(this, "stressGEL", events);

        Stade.init(this, "Stade", events);
        STapS.init(this, "STapS", events);
        STapDF.init(this, "STapDF", events);
        Tmoy.init(this, "Tmoy", events);
        RG.init(this, "RG", events);

        Ebpot.init(this, "Ebpot", events);
        Eb.init(this, "Eb", events);
        Ei.init(this, "Ei", events);
        MS.init(this, "MS", events);
        LAI.init(this, "LAI", events);
        MS_DF.init(this, "MS_DF", events);
        MS_DRG.init(this, "MS_DRG", events);
        MS_FSLA.init(this, "MS_FSLA", events);
        MS_MP.init(this, "MS_MP", events);
        MS_noGEL.init(this, "MS_noGEL", events);
        NjstressN_Eb.init (this, "NjstressN_Eb",events) ; 
        stressN_cumul.init (this, "stressN_cumul",events) ; 
        NjstressH.init(this, "NjstressH", events);
        stressH_cumul.init(this, "stressH_cumul", events);
        NjstressT.init(this, "NjstressT", events);
        stressT_cumul.init(this, "stressT_cumul", events);
    }

    virtual ~PlantGrowthPea()
    {}

    virtual void compute(const vd::Time& j)
    {
        // calcul de l'indice foliaire (LAI) le jour j

        if((Stade() < VEGETATIF) or (IndicMP(-1)==1)){
            LAI=0.0;
        }  else if (STapDF(-1) < (pp.FLO_ET*(pp.NETpot-1))) {
            LAI=pp.coef_demMS_demN * QNc(-1);
        } else {
            LAI=LAI(-1);
        }

        //efficience d'interception  du rayonnement globale le jour j

        if((Stade() < VEGETATIF) or (IndicMP(-1)==1)){
            Ei= 0;
        } else if ( Stade() >= DRG_FSLA and tNveg(-1)<pp.tNveg_seuil){
            Ei = pp.Eimax*(1-std::exp(-pp.k*LAI()))*(tNveg(-1)- pp.tNstruct)
                                     /(pp.tNveg_seuil - pp.tNstruct);
        } else {
            Ei = pp.Eimax*(1-std::exp(-pp.k*LAI()));
        }

        // Efficience de conversion potentielle du rayonnement le jour j 
        // (sans stress N et H) en g/MJ

        if ((Stade() < VEGETATIF) or (IndicMP(-1)==1)){
            Ebpot = 0;
        } else if (pp.begin_date + j< pp.date_FLO) {
            Ebpot = pp.Ebv;
        } else if ( Stade() >= DRG_FSLA and tNveg(-1)<pp.tNveg_seuil){
            Ebpot = std::max(0.0,std::min(pp.Ebr,
                    pp.Ebr*(tNveg(-1) - pp.tNstruct)
                    /(pp.tNveg_seuil - pp.tNstruct)));
        } else {
            Ebpot = (pp.Ebv +((pp.Ebr - pp.Ebv)
                    *(STapDF()/pp.ST_DF_DRG)));
        }


        // ------ fonctionnement de la plante avec prise en compte des stress N et H --------------------------------

        // efficience de conversion du rayonnement (g/MJ)

       if (Stade() < VEGETATIF )  {
            Eb=0.0;
        } else {
            Eb=Ebpot()*stressT(-1)*stressH(-1)*stressN_Eb(-1);
        }

        // matiere seche accumulee depuis l initilisation jusqu au jour j

        if (Stade() < VEGETATIF){
            MS= 0.0;
        } else if(Stade(-1)==SOL_NU and Stade()==VEGETATIF) {
            if (pp.MS_init == -1) {//MS_init not available
                MS= (pp.densite_semis * pp.PMGmoy_var/1000) / pp.coef_MS_init;
            } else {
                MS = pp.MS_init;
            }
        } else if (Ei()==0.0) {
            MS=MS(-1)*stressGEL(-1);
        } else{
            MS=(MS(-1)+ RG()/100.0*pp.Ec*Ei()*Eb())*(stressGEL(-1));
        }

        // matiere seche accumulee depuis l initilisation jusqu a DF (g/m²)

        if (pp.begin_date + j< pp.date_FLO) {
            MS_DF= 0.0;
        } else if(pp.begin_date + j==pp.date_FLO) {
            MS_DF= MS();
        } else {
            MS_DF=MS_DF(-1);
        }

        // matiere seche accumulee depuis l initilisation jusqu a DRG (g/m²)

        if (Stade() < DRG_FSLA) {
            MS_DRG= 0.0;
        } else if((Stade(-1)== DF_DRG) and (Stade() == DRG_FSLA)) {
            MS_DRG= MS();
        } else {
            MS_DRG=MS_DRG(-1);
        }

        // matiere seche accumulee depuis l initilisation jusqu a FSLA (g/m²)

        if (Stade() < FSLA_MP) {
            MS_FSLA= 0.0;
        } else if((Stade(-1)== DRG_FSLA) and (Stade() == FSLA_MP)) {
            MS_FSLA= MS();
        } else {
            MS_FSLA=MS_FSLA(-1);
        }

        // matiere seche à maturité physiologique (g/m²)


        if (Stade() < MP) {
            MS_MP= 0.0;
        } else if((Stade(-1)== FSLA_MP) and (Stade() == MP)) {
            MS_MP= MS();
        } else {
            MS_MP=MS_MP(-1);
        }
		
        //Variables intégratives de stress sur la totalité du cycle de culture
        //Nombre de jour de stressN_Eb : Cumul des jours où stressN_Eb<1
        if (Stade () < VEGETATIF ) {
            NjstressN_Eb = 0;
        } else if (Stade () >= MP or stressN_Eb () ==1 ) {
            NjstressN_Eb = NjstressN_Eb(-1);
        } else if (stressN_Eb ()< 1) {
            NjstressN_Eb = NjstressN_Eb(-1) + 1 ;
        }
		
        //Intensité cumulée de stressN_Eb sur tout le cycle: cumul de 1-stressN_Eb
        if (Stade () < VEGETATIF ) {
            stressN_cumul = 0;
        } else if (Stade () >= MP  ) {
            stressN_cumul = stressN_cumul(-1) ;
        } else {
            stressN_cumul = stressN_cumul(-1) + (1-stressN_Eb()) ;		
        }
		
        //Nombre de jour de stressH : Cumul des jours où stressH<1
        if (Stade () < VEGETATIF ) {
            NjstressH = 0;
        } else if (Stade () >= MP or stressH () ==1 ) {
            NjstressH = NjstressH(-1);
        } else if (stressH ()< 1) {
            NjstressH = NjstressH(-1) + 1 ;
        }
		
		//Intensité cumulée de stressH sur tout le cycle: cumul de 1-stressH
		if (Stade () < VEGETATIF ) {
			stressH_cumul = 0;
		}else if (Stade () >= MP  ) {
			stressH_cumul = stressH_cumul(-1) ;
		}else {
			stressH_cumul = stressH_cumul(-1) + (1-stressH()) ;		
		}
		
		//Nombre de jour de stressT : Cumul des jours où stressT<0.8
		if (Stade () < VEGETATIF ) {
			NjstressT = 0;
		}else if (Stade () >= MP or stressT () >= 0.8 ) {
			NjstressT = NjstressT(-1);
		}else if (stressT ()< 0.8) {
			NjstressT = NjstressT(-1) + 1 ;
		
		}
		
		//Intensité cumulée de stressT sur tout le cycle, jours ou stressT<0.8: cumul de 1-stressH
		if (Stade () < VEGETATIF ) {
			stressT_cumul = 0;
		}else if (Stade () >= MP or stressT () >= 0.8  ) {
			stressT_cumul = stressT_cumul(-1) ;
		}else if (stressT ()< 0.8){
			stressT_cumul = stressT_cumul(-1) + (1-stressT()) ;		
		}
    }  



private:

    ParametersPlantPea pp;

    /*Nosync*/ Var IndicMP;
    /*Nosync*/ Var tNveg;
    /*Nosync*/ Var QNc;
    /*Nosync*/ Var stressN_Eb;
    /*Nosync*/ Var stressH;
    /*Nosync*/ Var stressT;
    /*Nosync*/ Var stressGEL;

    /*Sync*/ Var Stade;
    /*Sync*/ Var STapS;
    /*Sync*/ Var STapDF;
    /*Sync*/ Var Tmoy;
    /*Sync*/ Var RG;

    Var Ebpot;
    Var Eb;
    Var Ei;
    Var MS;
    Var LAI;
    Var MS_DF;
    Var MS_DRG;
    Var MS_FSLA;
    Var MS_MP;
    Var MS_noGEL;
    Var	NjstressN_Eb ; 
    Var stressN_cumul;
    Var NjstressH;
    Var stressH_cumul;
    Var NjstressT;
    Var stressT_cumul;
};

} // namespace

DECLARE_DYNAMICS(AZODYN::PlantGrowthPea)
