// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <ParametersSoil.hpp>
#include <ParametersPlant.hpp>
#include <time.h>
#include <cmath>
#include <cstdlib>
#include <iostream>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;

class NitrogenSoil : public DiscreteTimeDyn
{
public:
    NitrogenSoil(const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {

        ps.initialiser(events);
        pp.initialiser(events);

        QNEng.init(this, "QNEng", events);
        QNabs_C0.init(this, "QNabs_C0", events);
        QNabs_C1.init(this, "QNabs_C1", events);
        
        Tmoy.init(this, "Tmoy", events);
        stressH_C0.init(this, "stressH_C0", events);
        ProfRac.init(this, "ProfRac", events);
        ProfRacmax.init(this, "ProfRacmax", events);

        StockEauD_C0.init(this, "StockEauD_C0", events);
        RUmax_C0.init(this, "RUmax_C0", events);
        StockEauD_C1.init(this, "StockEauD_C1", events);
        RUmax_C1.init(this, "RUmax_C1", events);
        StockEauD_C2.init(this, "StockEauD_C2", events);
        RUmax_C2.init(this, "RUmax_C2", events);

        Ft_min.init(this, "Ft_min", events);
        JN.init(this, "JN", events);
        k_res.init(this, "k_res", events);
        QC_res.init(this, "QC_res", events);
        QN_res.init(this, "QN_res", events);
        k_micr.init(this, "k_micr", events);
        QC_micr.init(this, "QC_micr", events);
        QN_micr.init(this, "QN_micr", events);
        k_hum.init(this, "k_hum", events);
        QC_hum.init(this, "QC_hum", events);
        QN_hum.init(this, "QN_hum", events);
        dQN_Mr.init(this, "dQN_Mr", events);
        dQC_Mr.init(this, "dQC_Mr", events);
        dQN_Mh.init(this, "dQN_Mh", events);
        QN_C0.init(this, "QN_C0", events);
        QNaccess_C0.init(this, "QNaccess_C0", events);
        pNlixiv_C0.init(this, "pNlixiv_C0", events);
        QNlixiv_C0.init(this, "QNlixiv_C0", events);
        QNaccess_C1.init(this, "QNaccess_C1", events);
        TN_C2.init(this, "TN_C2", events);
        pNlixiv_C1.init(this, "pNlixiv_C1", events);
        QNlixiv_C1.init(this, "QNlixiv_C1", events);
        QN_C2.init(this, "QN_C2", events);
        pNlixiv_C2.init(this, "pNlixiv_C2", events);
        QNlixiv_C2.init(this, "QNlixiv_C2", events);
        QNlixiv_tot.init(this, "QNlixiv_tot", events);
        QNsol_tot.init(this, "QNsol_tot", events);
        QNnondispo.init(this, "QNnondispo", events);
        QN_offre.init(this, "QN_offre", events);

    }

    virtual ~NitrogenSoil()
    {}

    //@@begin:compute@@
    virtual void compute(const vd::Time& j)
    {


        //------------- Couche C0

        // Mineralisation des residus le jour j (kg/ha) 

        // Fonction temperature pour le caclul de la minéralistion des résidus
        if (Tmoy() <= 0.0){
           Ft_min= 0.0;
        } else {
           Ft_min = std::pow(ps.a+ ps.b* std::exp(ps.c *Tmoy()/ps.Tref),ps.d);
        }
       
        
        // fonction de calcul des jours normalisés

        if (pp.begin_date + j <= pp.date_init_miner_res){
            JN = 0;
        } else {
            JN = JN(-1)+ 1*Ft_min()*stressH_C0();
        }


        // Decompostion des résidus
        // Taux de décomposition des résidus
        
        k_res = std::exp(-ps.para_kres1 * JN());
        
        // Quantite de carbonne dans le pool des résidus
        
        QC_res = ps.C_res * k_res();
        
        // Quantite d Azode dans le pool des residus
        
        QN_res = QC_res() / ps.C_Nres;
        
               
        // Decompostion de la biomasse microbienne
        // Taux de décomposition de la biomasse microbienne
        
        double coef_k_micr = (ps.para_kres1* ps.Y) / (ps.para_kreshum -ps.para_kres1);
        
        k_micr = coef_k_micr* (k_res() -std::exp(-ps.para_kreshum * JN()));
        
        // Quantite de carbonne dans le pool de la biomasse microbienne
        
        QC_micr = ps.C_res * k_micr();
        
        // rapport C_N de la biomasse microbienne
        
        double C_Nmicr;
        if (ps.C_Nres <ps.paraC_Nres1){
            C_Nmicr = ps.paraC_Nres4;
        } else {
            C_Nmicr = ps.paraC_Nres1+ ps.paraC_Nres1/ps.C_Nres;
        }
        
        // Quantite d Azode dans le pool de la biomasse microbienne
        
        QN_micr = QC_micr() / C_Nmicr;
 
                
        // Decompostion de l'humus
        // Taux de décomposition de l'humus
        
        double h = 1- (ps.f * ps.C_Nres) / (ps.g + ps.C_Nres );
        
        double coef_khum = (ps.Y * h)/ (ps.para_kreshum - ps.para_kres1);
        
        
        k_hum = std::max(0.0,ps.Y* h + (coef_khum * (ps.para_kres1* std::exp(-ps.para_kreshum *JN())) -
                       ps.para_kreshum* k_res()));
        
        // Quantite de carbonne dans le pool de la biomasse microbienne
        
        QC_hum = ps.C_res * k_hum();
        
        // Quantite d Azode dans le pool de l'humus
        
        QN_hum = QC_hum() / ps.C_Nhum;
        
        
        
        // quantite d'azote et de carbonne liberes
        
        if (pp.begin_date + j<=pp.date_init_N) {
            dQN_Mr = 0.0;
        } else {
            dQN_Mr = (ps.C_res / ps.C_Nres -(QN_res()+QN_hum()+QN_micr()))-
                    (ps.C_res / ps.C_Nres -(QN_res(-1)+QN_hum(-1)+QN_micr(-1)));
        }
        
        if (pp.begin_date + j<=pp.date_init_N) {
            dQC_Mr = 0.0;
        } else {
            dQC_Mr = (ps.C_res -(QC_res()+QC_hum()+QC_micr()))-
                    (ps.C_res  -(QC_res(-1)+QC_hum(-1)+QC_micr(-1)));
        }


        // Mineralisation de l'humus (kg/ha)

        if(Tmoy()<0  or pp.begin_date + j<=pp.date_init_N){
            dQN_Mh=0.0;
        } else {
            dQN_Mh=stressH_C0()*ps.fsdc*ps.coef_k2 * std::exp(ps.alpha*(Tmoy()-ps.Tref))
                  / ((ps.Arg+ps.gamma)*(ps.CaCO3+ps.delta))
                    *(ps.ep_C0/10.0*ps.da_C0*1000.0) *ps.tNorg*ps.beta ;
        }

        // Fraction (proportion) d'azote qui sera lixivee en provenance de la couche C0 (calculee a partir du drainage de la couche C0)

        if ((StockEauD_C0() + RUmax_C0()) == 0.0) {
            pNlixiv_C0 = 0.0;
        } else {
            pNlixiv_C0 = std::pow(StockEauD_C0()/(StockEauD_C0()+(RUmax_C0()/100)),
                      (ps.ep_C0/10.0/2.0));
        }

        //Quantite d'azote lixiviee en provenance de la couche C0

        QNlixiv_C0 = QN_C0(-1) * pNlixiv_C0();

        //Quantite totale d'azote contenue dans la couche C0 le jour j (kg/ha)

        if (pp.begin_date + j < pp.date_init_N) {
            QN_C0 = 0.0;
        } else if (pp.begin_date + j == pp.date_init_N) {
            QN_C0 = ps.QNsol_init_C0;
        } else {
            QN_C0 = std::max(0.0,QN_C0(-1) + dQN_Mr(-1) + dQN_Mh(-1) + QNEng(-1) - QNlixiv_C0() - QNabs_C0(-1));
        }

        // Quantite d'azote accessible par les racines pour la culture dans la couche C0 (kg/ha)

        if (ProfRac() <= ps.ep_C0) {
            QNaccess_C0 = QN_C0() * ProfRac() / ps.ep_C0;
        } else {
            QNaccess_C0 = QN_C0();
        }



        //-------------- Couche C1

        //quantite d'azote contenue dans la couche C1

        if (pp.begin_date + j < pp.date_init_N) {
            QNaccess_C1 = 0.0;
        } else if (pp.begin_date + j == pp.date_init_N ){
            if (ProfRac() <= ps.ep_C0) {
                QNaccess_C1 = 0.0;
            } else {
                QNaccess_C1 =  (ps.QNsol_init_tot - ps.QNsol_init_C0)/
                                       (ps.Prof_Reliquat_tot- ps.ep_C0)
                              * (ProfRac() -ps.ep_C0);
            }
        } else {
            if (ProfRac() <= ps.ep_C0) {
                QNaccess_C1 = 0.0;
            } else {
                QNaccess_C1 = std::max(0.0, QNaccess_C1(-1)+QNlixiv_C0(-1)-QNlixiv_C1(-1)
                -QNabs_C1(-1)-(ProfRac() - ProfRac(-1))*TN_C2(-1));
            }
        }

        // Fraction (proportion) d'azote qui sera lixivee en provenance de la
        //couche C1 (calculee a partir du drainage de la couche C0)

        if ((StockEauD_C1() + RUmax_C1()) == 0.0) {
            pNlixiv_C1 = 0.0;
        } else {
            pNlixiv_C1 = std::pow(StockEauD_C1()/(StockEauD_C1()+(RUmax_C1()/100.0)),
                      ((ProfRac() - ps.ep_C0)/10.0/2.0));
        }

        //Quantite d'azote lixiviee en provenance de la couche C1

        QNlixiv_C1 = QNaccess_C1() * pNlixiv_C1();

        //-------------- Couche C2

        //quantite d'azote contenue dans la couche C2

        if (pp.begin_date + j < pp.date_init_N) {
            QN_C2 = 0.0;
        } else if (pp.begin_date + j == pp.date_init_MS){
              QN_C2 = (ps.QNsol_init_tot - ps.QNsol_init_C0)/(ps.Prof_Reliquat_tot- ps.ep_C0)
                           * std::min((ProfRacmax() -ps.ep_C0),
                                       (ProfRacmax() - ProfRac()));
        } else {
            QN_C2 = std::max(0.0, QN_C2(-1)+QNlixiv_C1(-1)-QNlixiv_C2(-1) -
                    (ProfRac() - ProfRac(-1))*TN_C2(-1));
        }

        // Fraction (proportion) d'azote qui sera lixivee en provenance de la
        //couche C2 (calculee a partir du drainage de la couche C1)


        if ((StockEauD_C2() + RUmax_C2()) == 0.0) {
            pNlixiv_C2 = 0.0;
        } else {
            pNlixiv_C2 = std::pow(StockEauD_C2()/(StockEauD_C2()+(RUmax_C2()/100.0)),
                      ((ProfRacmax() - ProfRac())/10/2));
        }

        //Quantite d'azote lixiviee en provenance de la couche C2

        QNlixiv_C2 = QN_C2(-1) * pNlixiv_C2();

        //Quantite d'azote par cm de sol dans la couche C2

        if (ProfRac() == ProfRacmax()) {
            TN_C2 = 0.0;
        } else {
            TN_C2 = QN_C2() / (ProfRacmax() - std::max(ProfRac(), ps.ep_C0));
        }

        //Quantite d'azote lixivie cumule depuis l'initialisation

        if (pp.begin_date + j <= pp.date_init_N) {
            QNlixiv_tot = 0.0;
        } else {
            QNlixiv_tot = QNlixiv_tot(-1)+QNlixiv_C2();
        }

        //Quantite d'azote minerale totale dans le sol
        QNsol_tot = QN_C0() + QNaccess_C1() + QN_C2();

        //-------------- sur plusieurs couches

        //Azote non disponible pour la culture

        QNnondispo = ps.QNND * (ProfRac() / 10.0) / 10.0;

        // Quantite totale d'azote qui sera disponible pour la culture

        QN_offre = std::max(0.0, QNaccess_C0() + QNaccess_C1() - QNnondispo());

    }


private:
    ParametersSoil ps;
    ParametersPlant pp;

    /*Nosync*/ Var QNEng;
    /*Nosync*/ Var QNabs_C0;
    /*Nosync*/ Var QNabs_C1;

    /*Sync*/ Var Tmoy;
    /*Sync*/ Var stressH_C0;
    /*Sync*/ Var ProfRac;
    /*Sync*/ Var ProfRacmax;
    /*Sync*/ Var StockEauD_C0;
    /*Sync*/ Var RUmax_C0;
    /*Sync*/ Var StockEauD_C1;
    /*Sync*/ Var RUmax_C1;
    /*Sync*/ Var StockEauD_C2;
    /*Sync*/ Var RUmax_C2;

    Var Ft_min;
    Var JN;
    Var k_res;
    Var QC_res;
    Var QN_res;
    Var k_micr;
    Var QN_micr;
    Var QC_micr;
    Var k_hum;
    Var QC_hum;
    Var QN_hum;
    Var dQN_Mr;
    Var dQC_Mr;
    Var dQN_Mh;
    Var QN_C0;
    Var QNaccess_C0;
    Var pNlixiv_C0;
    Var QNlixiv_C0;
    Var QNaccess_C1;
    Var TN_C2;
    Var pNlixiv_C1;
    Var QNlixiv_C1;
    Var QN_C2;
    Var pNlixiv_C2;
    Var QNlixiv_C2;
    Var QNlixiv_tot;
    Var QNsol_tot;
    Var QNnondispo;
    Var QN_offre;

};

} // namespace

DECLARE_DYNAMICS(AZODYN::NitrogenSoil)

