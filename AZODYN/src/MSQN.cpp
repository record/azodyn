// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <ParametersPlantRape.hpp>
#include <phenologie_types.hpp>

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;
using namespace std;

namespace AZODYN
{

    class MSQN : public DiscreteTimeDyn
    {
    public:
        MSQN(const vd::DynamicsInit &model, const vd::InitEventList &evts) : DiscreteTimeDyn(model, evts)
        {
            // declaration paramètres du module

            pp.initialiser(evts);

            // declaration variables d'entrée du module
            STsemis.init(this, "STsemis", evts);
            MSfm.init(this, "MSfm", evts);
            Stade.init(this, "Stade", evts);
            DensiteSemis.init(this, "DensiteSemis", evts);

            RG.init(this, "RG", evts);
            EiLeaves.init(this, "EiLeaves", evts);
            EbLeaves.init(this, "EbLeaves", evts);
            EiPod.init(this, "EiPod", evts);
            EbPod.init(this, "EbPod", evts);
            stressH.init(this, "stressH", evts);

            // declaration variables du module
            partMSLeaves.init(this, "partMSLeaves", evts);
            MSTg.init(this, "MSTg", evts);
            MSTr.init(this, "MSTr", evts);
            MSAr.init(this, "MSAr", evts);
            MSAg.init(this, "MSAg", evts);
            NAcg.init(this, "NAcg", evts);
            QNAcg.init(this, "QNAcg", evts);
            NAcr.init(this, "NAcr", evts);
            QNAcr.init(this, "QNAcr", evts);
            DeltaMSAg.init(this, "DeltaMSAg", evts);
            DeltaQNAcg.init(this, "DeltaQNAcg", evts);
        }

        virtual ~MSQN(){};

        virtual void compute(const vd::Time & /*time*/)
        {
            updateMSTg();
            updateMSTr();
            updateMSAr();
            updateMSAg();
            updateDeltaMSAg();
            updateNAcr();
            updateQNAcr();
            updateNAcg();
            updateQNAcg();
            updateDeltaQNAcg();
            updatePartMSLeaves();
        }

    private:
        /// Compartiment MS Totale

        // Calcul de la matière sèche totale générée à partir du rayonnement intercepté par les feuilles et les siliques (kg/ha)

        void updateMSTg()
        {
            if (DensiteSemis() > 0) { // local storage of sowing density to be used at later date 
                densiteSemis = DensiteSemis();
            }

            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
                MSTg = 0.0;
                break;
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
                MSTg = pp.AMST * (pp.SFCOTY * pp.SLACOTY * densiteSemis * 10) + pp.BMST;
                break;
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                if ((EiLeaves(-1) + EiPod(-1)) <= 0.95)
                {
                    MSTg = MSTg(-1) + RG(-1) * pp.Ec * 10 * stressH(-1) * EiLeaves(-1) * EbLeaves(-1) + RG(-1) * pp.Ec * 10 * stressH(-1) * EiPod(-1) * EbPod(-1);
                }
                else
                {
                    MSTg = MSTg(-1) + RG(-1) * pp.Ec * 10 * stressH(-1) * max(0.0, (0.95 - EiPod(-1))) * EbLeaves(-1) + RG(-1) * pp.Ec * 10 * stressH(-1) * EiPod(-1) * EbPod(-1);
                }
                densiteSemis = 0;
                break;
            }
        }

        // Calcul de la matière sèche totale réelle (kg/ha)

        void updateMSTr()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                MSTr = 0.0;
                break;
            case JOUR_LEVEE:
                MSTr = MSTg();
                break;
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                MSTr = MSTg() - MSfm();
                break;
            }
        }

        /// Compartiment MS Aerienne

        // Calcul de la matière sèche aérienne réelle (kg/ha)

        void updateMSAr()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                MSAr = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                MSAr = (MSTr() - pp.BMST) / pp.AMST;
                break;
            }
        }

        // Calcul de la matière sèche aérienne générée (réelle + chutee) (kg/ha)

        void updateMSAg()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                MSAg = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                MSAg = MSAr() + MSfm();
                break;
            }
        }

        // Calcul de la variation de la matière sèche aérienne générée (réelle + chutee) (kg/ha)

        void updateDeltaMSAg()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                DeltaMSAg = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                DeltaMSAg = MSAg() - MSAg(-1);
                break;
            }
        }

        // Calcul de la teneur en azote critique réelle dans les parties aériennes à partir de MS aérienne réelle (%)

        void updateNAcr()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                NAcr = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                NAcr = min(pp.tNc_Nc, max(pp.coef_Nc * pow(MSAr() / 1000, pp.pente_Nc), 0.0));
                break;
            }
        }

        // Calcul de la quantité d'azote critique aérien réelle à partir de MS aérienne réelle (kg/ha)

        void updateQNAcr()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNAcr = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNAcr = MSAr() * NAcr() / 100;
                break;
            }
        }

        // Calcul de la teneur en azote critique aérien générée à partir de MS aérienne générée (réelle + chutee) (%)

        void updateNAcg()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                NAcg = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                NAcg = min(pp.tNc_Nc, max(pp.coef_Nc * pow(MSAg() / 1000, pp.pente_Nc), 0.0));
                break;
            }
        }

        // Calcul de la quantité d'azote critique aérien générée à partir de MS aérienne générée (kg/ha)

        void updateQNAcg()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                QNAcg = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                QNAcg = MSAg() * NAcg() / 100;
                break;
            }
        }

        // Calcul de la variation de la quantité d'azote critique aérien générée à partir de MS aérienne générée (kg/ha)

        void updateDeltaQNAcg()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                DeltaQNAcg = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                DeltaQNAcg = QNAcg() - QNAcg(-1);
                break;
            }
        }

        // Calcul du rapport MS feuilles/MSAr appliqué aux feuilles mortes (sans dim)

        void updatePartMSLeaves()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                partMSLeaves = 0.0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
                partMSLeaves = min(1.0, max(0.0, pp.ApartMSLeaves + pp.BpartMSLeaves * STsemis()));
                break;
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                partMSLeaves = min(1.0, max(0.0, pp.CpartMSLeaves + pp.DpartMSLeaves * STsemis()));
                break;
            }
        }

        ParametersPlantRape pp;

        Var EiLeaves;
        Var EbLeaves;
        Var EiPod;
        Var EbPod;
        Var stressH;

        Var STsemis;
        Var Stade;
        Var RG;
        Var MSfm;
        Var DensiteSemis;

        Var partMSLeaves;
        Var MSTg;
        Var MSTr;
        Var MSAr;
        Var MSAg;
        Var NAcg;
        Var NAcr;
        Var QNAcr;
        Var DeltaMSAg;
        Var QNAcg;
        Var DeltaQNAcg;

        double densiteSemis;
    };

} // namespace

DECLARE_DYNAMICS(AZODYN::MSQN)
