// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <ParametersPlant.hpp>

#include <ParametersSoil.hpp>
#include <ParametersPlant.hpp>
#include <ParametersPlantPea.hpp>
#include<iostream>
#include<cstdlib>
#include<time.h>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;

class RootGrowthPea : public DiscreteTimeDyn
{
public:
    RootGrowthPea(const vd::DynamicsInit& atom,
                  const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        ps.initialiser(events);
        pp.initialiser(events);

        Tmoy.init(this, "Tmoy", events);
        RG.init(this, "RG", events);

        ProfRacmax.init(this, "ProfRacmax", events);
        ProfRac.init(this, "ProfRac", events);
        ElongRac.init(this, "ElongRac", events);
        densRac.init(this, "densRac", events);
    }

    virtual ~RootGrowthPea()
    {}

    virtual void compute(const vd::Time& j)
    {

        // calcul de l elongation racinaire le jour j (ElongRac)

        if(pp.begin_date + j<pp.date_S){
            ElongRac=0.0;
        } else if (Tmoy() < 0.0) {
            ElongRac = 0.0;
        } else {
            //RG est donne en joules/cm2
            ElongRac = RG()/100.0 * pp.vRac_RG;
        }


        // calcul de la profondeur racinaire maximum (mm)
        ProfRacmax = pp.ProfRacmax_pot *
                (1-(pp.coef_Mottes_Profmax * ps.MottesDelta));

        // calcul de la profondeur racinaire le jour j (mm)
        if (pp.begin_date + j<pp.date_S){
            ProfRac = 0.0;
        } else if ((pp.begin_date + j<pp.date_FLO)
                and (ProfRac(-1) < ProfRacmax())){
            ProfRac = std::min(ProfRac(-1) + ElongRac(),ProfRacmax());
        } else {
            ProfRac = ProfRac(-1);
        }

        // calcul de la densite racinaire (%)
        densRac = pp.densRac1 * ps.MottesDelta + pp.densRac2;
    }


private:
    ParametersSoil     ps;
    ParametersPlantPea pp;

    /*Sync*/ Var Tmoy;
    /*Sync*/ Var RG;
    Var ProfRacmax;
    Var ProfRac;
    Var ElongRac;
    Var densRac;


};

} // namespace

DECLARE_DYNAMICS(AZODYN::RootGrowthPea)

