// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <phenologie_types.hpp>
#include <ParametersPlantRape.hpp>

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

namespace AZODYN
{

   class YieldRapeseed : public DiscreteTimeDyn
   {
   public:
      YieldRapeseed(const vd::DynamicsInit &model, const vd::InitEventList &evts) : DiscreteTimeDyn(model, evts)
      {

         // declaration paramètres du module
         pp.initialiser(evts);

         // declaration variables d'entrée du module

         Nabs.init(this, "Nabs", evts);
         MSAg.init(this, "MSAg", evts);

         RG.init(this, "RG", evts);
         RGintercept.init(this, "RGintercept", evts);
         INNvec.init(this, "INNvec", evts);
         STfloraison.init(this, "STfloraison", evts);
         Stade.init(this, "Stade", evts);
         FTSW.init(this, "FTSW", evts);
         QNAr.init(this, "QNAr", evts);

         // declaration variables du module

         dynSRG.init(this, "dynSRG", evts);
         dynNGM2pot.init(this, "dynNGM2pot", evts);
         dynNGM2.init(this, "dynNGM2", evts);
         dynP1Gpot.init(this, "dynP1Gpot", evts);
         dynStockNremob.init(this, "dynStockNremob", evts);
         dynQNremob.init(this, "dynQNremob", evts);
         dynINN_FLO.init(this, "dynINN_FLO", evts);
         dynINN_G4.init(this, "dynINN_G4", evts);
         dynSPAR.init(this, "dynSPAR", evts);
         dynFTSW_PAR.init(this, "dynFTSW_PAR", evts);
         dynStressH_NGM2.init(this, "dynStressH_NGM2", evts);
         dynDemandeMS1G.init(this, "dynDemandeMS1G", evts);
         dynDemandeMSG.init(this, "dynDemandeMSG", evts);
         dynDemandeQNG.init(this, "dynDemandeQNG", evts);
         dynOffreQNG.init(this, "dynOffreQNG", evts);
         dynQNG.init(this, "dynQNG", evts);
         dynQNveg.init(this, "dynQNveg", evts);
         dynQNa_FLO.init(this, "dynQNa_FLO", evts);
         dynTPpot.init(this, "dynTPpot", evts);
         dynMSG_offre.init(this, "dynMSG_offre", evts);
         dynMSG.init(this, "dynMSG", evts);
         dynPMG.init(this, "dynPMG", evts);
         dynYieldDry.init(this, "dynYieldDry", evts);
         dyndQNG.init(this, "dyndQNG", evts);
         dynYieldHum.init(this, "dynYieldHum", evts);
         dynQtPhototherm.init(this, "dynQtPhototherm", evts);
         dynSumRG.init(this, "dynSumRG", evts);
      }

      virtual ~YieldRapeseed(){};

      virtual void compute(const vd::Time & /*time*/)
      {
         updateDynSRG();
         updateDynNGM2pot();
         updateDynINN();
         updateDynTPpot();
         updateDynSPAR();
         updateDynStressH_NGM2();
         updateDynQtPhototherm();
         updateDynNGM2();
         updateDynP1Gpot();
         updateDynDemandeMS1G();
         updateDynDemandeMSG();
         updateNremob();
         updateDyndQNG();
         updateDynQNG();
         updateDynQNveg();
         updateDynQNa_FLO();
         updateDynQNremob();
         updateDynMSG();
         updatePMG();
         updateYield();
      }

   private:
      // Calcul de somme des rayonements interceptés par les feuilles et siliques de Floraison à G4

      void updateDynSRG()
      {

         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynSRG = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
            dynSRG = dynSRG(-1) + RGintercept();
            break;
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynSRG = dynSRG(-1);
            break;
         }
      }

      // Calcul du nombre de grains potentiel (m^-2)

      void updateDynNGM2pot()

      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynNGM2pot = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
            dynNGM2pot = pp.ANGPOT * dynSRG() + pp.BNGPOT;
            break;
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynNGM2pot = dynNGM2pot(-1);
            break;
         }
      }

      // Calcul des INN à Floraison et G4

      void updateDynINN()

      {

         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynINN_FLO = 0.0;
            dynINN_G4 = 0.0;
            break;
         case JOUR_F1:
            dynINN_FLO = INNvec();
            dynINN_G4 = 0.0;
            break;
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
            dynINN_FLO = dynINN_FLO(-1);
            dynINN_G4 = 0.0;
            break;
         case JOUR_G4:
            dynINN_FLO = dynINN_FLO(-1);
            dynINN_G4 = INNvec();
            break;
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynINN_FLO = dynINN_FLO(-1);
            dynINN_G4 = dynINN_G4(-1);
            break;
         }
      }

      // Calcul du taux de protéines potentiel(%)

      void updateDynTPpot()
      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
         case JOUR_F1:
            dynTPpot = pp.ATPpot * dynINN_FLO() + pp.BTPpot;
            break;
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynTPpot = dynTPpot(-1);
            break;
         }
      }

      // Calcul de la somme des PAR et des PAR liés au taux de remplissage des couche C0 et C1 (ou C1 seulement?) de F1 à G4

      void updateDynSPAR()

      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynSPAR = 0.0;
            dynFTSW_PAR = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
            dynSPAR = dynSPAR(-1) + pp.Ec * RG();
            dynFTSW_PAR = dynFTSW_PAR(-1) + FTSW() * pp.Ec * RG();
            break;
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynSPAR = dynSPAR(-1);
            dynFTSW_PAR = dynFTSW_PAR(-1);
            break;
         }
      }

      // Calcul des indices de réduction du nombre de grains (stress azotés et hydriques)

      void updateDynStressH_NGM2()
      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynStressH_NGM2 = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
            dynStressH_NGM2 = pp.BStressH + 2 / (1 + exp(pp.AStressH * dynFTSW_PAR() / dynSPAR()));
            break;
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynStressH_NGM2 = dynStressH_NGM2(-1);
            break;
         }
      }

      // Calcul du quotient photothermique

      void updateDynQtPhototherm()
      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynSumRG = 0.0;
            dynQtPhototherm = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
            dynSumRG = dynSumRG(-1) + RG();
            dynQtPhototherm = dynSumRG() / std::max(0.0001, STfloraison());
            break;
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynQtPhototherm = dynQtPhototherm(-1);
            break;
         }
      }

      // Calcul du nombre de grain réel (m^-2)

      void updateDynNGM2()

      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynNGM2 = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
            dynNGM2 = dynNGM2pot() * std::min(1.0, dynStressH_NGM2() * dynQtPhototherm());
            break;
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynNGM2 = dynNGM2(-1);
            break;
         }
      }

      // Calcul du poids d'un grain potentiel (en g/grain)

      void updateDynP1Gpot()

      {

         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynP1Gpot = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
            dynP1Gpot = std::min(pp.PMGmax_var / 1000, 10 * pp.RDTmax_var / dynNGM2());
            break;
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynP1Gpot = dynP1Gpot(-1);
            break;
         }
      }

      // Calcul de la matiere sèche d'un grain potentielle (g/m2)

      void updateDynDemandeMS1G()

      {

         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynDemandeMS1G = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
            dynDemandeMS1G = (dynP1Gpot()) /
                             (1 + (std::pow(
                                      ((dynP1Gpot() * 1000 - pp.P1G_FLO * 1000) / (pp.P1G_FLO * 1000)),
                                      ((((1350 / 15) / 2) - STfloraison()) / ((1350 / 15) / 2)))));
            break;
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynDemandeMS1G = dynDemandeMS1G(-1);
            break;
         }
      }

      // Calcul de la matiere sèche des grains potentielle (g/m2)

      void updateDynDemandeMSG()

      {

         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynDemandeMSG = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
            dynDemandeMSG = dynDemandeMS1G() * dynNGM2();
            break;
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynDemandeMSG = dynDemandeMSG(-1);
            break;
         }
      }

      // Calcul de la demande en azote des grains et de l'azote remobilisable dans les grains (kg/ha)

      void updateNremob()
      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynDemandeQNG = 0.0;
            dynOffreQNG = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
            dynDemandeQNG = (dynDemandeMSG() - dynDemandeMSG(-1)) * pp.ANalloc * 10;
            dynOffreQNG = dynQNremob(-1) + Nabs(-1) / pp.ANdispo;
            break;
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynDemandeQNG = dynDemandeQNG(-1);
            dynOffreQNG = dynOffreQNG(-1);
            break;
         }
      }

      // Dynamique de remplissage des grains (kg/ha)

      void updateDyndQNG()

      {

         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dyndQNG = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
            dyndQNG = std::max(0.001, std::min(dynDemandeQNG(), dynOffreQNG()));
            break;
         case FIN_REMPLISSAGE_GRAIN:
            dyndQNG = dynOffreQNG();
            break;
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dyndQNG = dyndQNG(-1);
            break;
         }
      }

      // Calcul de la teneur en azote des grains réelle (kg/ha)

      void updateDynQNG()

      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynQNG = 0.0;
            break;
         case JOUR_F1:
            dynQNG = (dynNGM2() * pp.P1G_FLO / 1000) * 10 * (pp.AQNG / 100);
            break;
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
            dynQNG = dynQNG(-1) + dyndQNG(-1);
            break;
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynQNG = dynQNG(-1);
            break;
         }
      }

      // Calcul de la teneur azote dans les parties végétatives réelle (kg/ha)

      void updateDynQNveg()

      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
            dynQNveg = 0.0;
            break;
         case JOUR_LEVEE:
         case VEGETATIF:
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynQNveg = QNAr() - dynQNG();
            break;
         }
      }

      // Calcul de la matiere sèche végétative à floraison (kg/ha)

      void updateDynQNa_FLO()

      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynQNa_FLO = 0.0;
            break;
         case JOUR_F1:
            dynQNa_FLO = dynQNveg();
            break;
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynQNa_FLO = dynQNa_FLO(-1);
            break;
         }
      }

      // Calcul de la quantité d'azote remobilisable dans les grains (kg/ha)

      void updateDynQNremob()

      {
         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynStockNremob = 0.0;
            dynQNremob = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:

            dynStockNremob = std::max(0.0, dynQNveg() - (1 - pp.ANremob) * dynQNa_FLO());

            if (dynQNveg(-1) < (1 - pp.ANremob) * dynQNa_FLO())
            {
               dynQNremob = 0.0;
            }
            else
            {
               dynQNremob = dynStockNremob() * (pp.BNremob * STfloraison() + pp.CNremob);
            }
            break;
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynQNremob = dynQNremob(-1);
            break;
         }
      }

      // Calcul de la matiere sèche des grains possible et réelle (g/m2)

      void updateDynMSG()

      {

         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynMSG_offre = 0.0;
            dynMSG = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
            dynMSG_offre = dynMSG_offre(-1) + (pp.AOffMSG * dynQNG(-1) / 10) + (MSAg(-1) / 10);
            dynMSG = std::min(dynMSG_offre(), dynDemandeMSG());
            break;
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynMSG_offre = dynMSG_offre(-1);
            dynMSG = dynMSG(-1);
            break;
         }
      }

      // Calcul de PMG

      void updatePMG()

      {

         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynPMG = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynPMG = dynMSG() / dynNGM2() * 1000;
            break;
         }
      }

      // Calcul du rendement

      void updateYield()

      {

         switch ((PHENOLOGIE)Stade())
         {
         case SOL_NU:
         case JOUR_SEMIS:
         case PRE_LEVEE:
         case JOUR_LEVEE:
         case VEGETATIF:
            dynYieldDry = 0.0;
            dynYieldHum = 0.0;
            break;
         case JOUR_F1:
         case FLORAISON:
         case JOUR_G1:
         case G1_G4:
         case JOUR_G4:
         case G4_MI_REMPLISSAGE_GRAIN:
         case JOUR_MI_REMPLISSAGE_GRAIN:
         case FIN_REMPLISSAGE_GRAIN:
         case JOUR_MATURITE:
         case GRAIN_MUR:
         case JOUR_RECOLTE:
            dynYieldDry = dynPMG() * dynNGM2() / 10000;
            dynYieldHum = dynYieldDry() + dynYieldDry() * pp.GrainsHumidity / 100;
            break;
         }
      }

      ParametersPlantRape pp;

      Var Nabs;
      Var MSAg;

      Var STfloraison;
      Var INNvec;
      Var RGintercept;
      Var Stade;
      Var FTSW;
      Var RG;
      Var QNAr;

      Var dynSRG;
      Var dynNGM2pot;
      Var dynNGM2;
      Var dynQNremob;
      Var dynINN_FLO;
      Var dynINN_G4;
      Var dynSPAR;
      Var dynFTSW_PAR;
      Var dynStressH_NGM2;
      Var dynP1Gpot;
      Var dynDemandeMS1G;
      Var dynDemandeMSG;
      Var dynDemandeQNG;
      Var dynOffreQNG;
      Var dynQNG;
      Var dynQNveg;
      Var dynQNa_FLO;
      Var dynTPpot;
      Var dynMSG_offre;
      Var dynMSG;
      Var dynPMG;
      Var dynYieldDry;
      Var dyndQNG;
      Var dynYieldHum;
      Var dynQtPhototherm;
      Var dynSumRG;
      Var dynStockNremob;
   };

} // namespace AzodynColza

DECLARE_DYNAMICS(AZODYN::YieldRapeseed)
