// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends


#include <vle/DiscreteTime.hpp>

#include <Phenology.hpp>
#include <ParametersPlant.hpp>
#include <ParametersPlantPea.hpp>
#include<iostream>
#include<cstdlib>
#include<time.h>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {

using namespace vle::discrete_time;

class YieldPea : public DiscreteTimeDyn
{
public:
    YieldPea(const vd::DynamicsInit& atom, const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        pp.initialiser(events);

        QN_tot.init(this,"QN_tot", events);

        Stade.init(this,"Stade", events);
        date_DRG.init(this,"date_DRG", events);
        MS.init(this,"MS", events);
        MS_DRG.init(this,"MS_DRG", events);
        MS_DF.init(this,"MS_DF", events);
        MS_MP.init(this,"MS_MP", events);
        Nj_FSLA.init(this,"Nj_FSLA", events);
        MS_FSLA.init(this,"MS_FSLA", events);
        STapFSLA.init(this,"STapFSLA", events);
        STapDF.init(this,"STapDF", events);
        STapDRG.init(this,"STapDRG", events);
        Tmoy.init(this,"Tmoy", events);

        QNGfsla.init(this,"QNGfsla",events);
        QNveg.init(this,"QNveg", events);
        QNG.init(this,"QNG", events);
        QNdispo.init(this,"QNdispo", events);
        Ngunit.init(this,"Ngunit", events);
        NG.init(this,"NG", events);
        vNmax.init(this,"vNmax", events);
        vN.init(this,"vN", events);
        MSG.init(this,"MSG", events);
        MSG_MP.init(this, "MSG_MP", events) ;
        MSveg_MP.init(this, "MSveg_MP", events) ;
        Rdt.init(this,"Rdt", events);
        Rdt14.init(this,"Rdt14", events);
        TP.init(this,"TP", events);
        P1G.init(this,"P1G", events);
        P1G14.init(this,"P1G14", events);
        IndicMP.init(this,"IndicMP", events);
        vcrgr.init(this,"vcrgr", events);
        IR.init(this,"IR", events);
    }

    virtual ~YieldPea()
    {}

    virtual void compute(const vd::Time& /*j*/)
    {
       

        // Paramètre de correlation entre la vitesse de croissance des plantes et le nombre de grains

        Ngunit = pp.coef_Ngunit1 * pp.PMGmoy_var/1000.0 +
                pp.coef_Ngunit2;

        //nombre de grains(grain/m²)

        if(Nj_FSLA() >0) {
            NG = Ngunit() *(MS_FSLA()- MS_DF())/(STapDF() - STapFSLA());
        } else {
            NG = 0;
        }
		
        //vitesse de croissance d'une graine (en gramme/grain/deg jours)

        vcrgr = pp.coef_vcrgr1 * pp.PMGmoy_var/1000.0 + pp.coef_vcrgr2;

        //Accumulation de MS dans les graines jusqu'au jour j (g/m²)

        if(Stade() < FSLA_MP) {
            MSG=0;
		} else if (Stade() == MP){
			MSG = MSG(-1) ;
        } else if(Nj_FSLA()==1) {
            MSG=NG()/2*(STapDRG() - STapFSLA())*vcrgr();
        } else if(Nj_FSLA()<=20) {
            MSG= std::min(MSG(-1) + NG()*Tmoy()*vcrgr(),MS());
        } else{
            MSG= MSG(-1) + std::min((NG() * Tmoy() * vcrgr()),(MS()-MS(-1)));
        }

		//QNGfsla : quantité d'azote dans les graines le jour de FFSLA (kg/ha)
		
		if (Nj_FSLA()==1) {
            QNGfsla= pp.pNfsla /100 *  MSG()*10.0;
		}else{
			QNGfsla=QNGfsla(-1);
		}
	
		//QNveg: Quantite d'azote cumulee dans les parties vegetatives aériennes au jour j (kg/ha)

		if (Nj_FSLA()==1){
			QNveg=QN_tot()/pp.coef_tot_a -QNGfsla() ;
		}else{
            QNveg =    std::max(
                    pp.tNstruct/100.0 * MS_DRG()*10.0*pp.pMSapDRG ,
                    QN_tot() / pp.coef_tot_a - (QNG(-1)));
		}
		
        //QNdispo: quantité d'azote dans les parties vegetatives aériennes disponibles
        //pour la remobilisation (sans l'azote structural) pour une graine (mgN/gr)

        if (Stade() <= DRG_FSLA){
            QNdispo = 0;
        } else {
            QNdispo = 100*(std::max(0.0,
                    (QNveg()-(pp.tNstruct/100.0*MS_DRG()*10*pp.pMSapDRG ))/NG()));
        }

        // vNmax : vitesse max d'accumulation d'azote par graine (µgN/gr/DJ)

        vNmax = pp.vNmax1 * pp.PMGmoy_var + pp.vNmax2;


        // vN : vitesse d'accumulation d'azote par graine (µgN/gr/DJ)

        if (Stade() <= DRG_FSLA) {
            vN=0;
        } else {
            vN=vNmax() *(1 - (std::exp(pp.coef_vN * QNdispo())));
        }

		//Quantite d'azote accumule dans les graines jusqu'au jour j
        if (Stade() < FSLA_MP ){
            QNG=0;
        }else if (Nj_FSLA()==1) {
            QNG= QNGfsla();
		}else if (MSG()-MSG(-1)<=0){
		QNG=QNG(-1);
		}else {
            QNG= QNG(-1) + (NG() * vN() * Tmoy() /100000);
			} 


        //poids d'une graine à 0% d'humidité

        if(NG()>0) {
            P1G=std::min((MSG()/NG()), pp.PMGmax_var/1000);
        } else {
            P1G=0;
        }

		//poids d'une graine à 14% d'humidité

        if(NG()>0) {
            P1G14=P1G()/(1-0.14);
        } else {
            P1G14=0;
        }
		
        //calcul de l'indicateur de la date de maturite physiologique

        if (Stade() >= FSLA_MP) {
            if(P1G()==P1G(-1)) {
                IndicMP=1;
            } else {
                IndicMP=0;
            }
        }else{
            IndicMP=0;
        }


        //calcul de rendement a 0% d'humidite
        if (Stade() < MP) {
            Rdt=0;
        } else if ((Stade(-1)== FSLA_MP) and (Stade()== MP)) {
            Rdt = MSG()/10;
        } else {
            Rdt=Rdt(-1);
        }

		//Rendement à 14% calculé à partir du rendement à 0%
		if (Stade() < MP) {
            Rdt14=0;
        } else if ((Stade(-1)== FSLA_MP) and (Stade()== MP)) {
            Rdt14 = Rdt()/(1-0.14);
        } else {
            Rdt14=Rdt14(-1);
        }
		

        //calcul de la teneur en protÃ©ine pour ce rendement (%)

	
        if (Stade() <MP) {
            TP=0;
        } else if ((Stade(-1)== FSLA_MP) and (Stade()== MP)) {
            TP= 100*(QNG()/(MSG()*10))*pp.coef_prot_gr;
        } else {
            TP=TP(-1);
        }
				
		//Matière sèche des grains à maturité physiologique (g/m²)
		
		if (Stade() <MP) {
			MSG_MP= 0.0;
        } else if(Stade(-1) == FSLA_MP and Stade() == MP ) {
            MSG_MP= MSG();
        } else {
            MSG_MP=MSG_MP(-1);
        }
		
		//Matière sèche aérienne végétative à maturité physiologique (g/m²)
		
		if (Stade() < MP) {
			MSveg_MP= 0.0;
        } else if(Stade(-1) == FSLA_MP and Stade() == MP ) {
            MSveg_MP= MS_MP() - MSG_MP();
        }
		
		//Indice de récolte
		if (Stade() < MP) {
			IR= 0.0;
        } else if(Stade(-1) == FSLA_MP and Stade() == MP ) {
            IR= MSG_MP()/MS_MP() ;
        }
		

    }

private:
    ParametersPlantPea pp;

    /*Nosync*/ Var QN_tot ;

    /*Sync*/ Var Stade ;
    /*Sync*/ Var date_DRG ;
    /*Sync*/ Var MS ;
    /*Sync*/ Var MS_DRG ;
    /*Sync*/ Var MS_DF ;
    /*Sync*/ Var Nj_FSLA ;
    /*Sync*/ Var MS_FSLA ;
    /*Sync*/ Var STapFSLA;
    /*Sync*/ Var STapDF;
    /*Sync*/ Var STapDRG;
    /*Sync*/ Var Tmoy;
    /*Sync*/ Var MS_MP;
	
	

    Var QNGfsla ;
    Var QNveg ;
    Var QNG ;
    Var QNdispo ;
    Var Ngunit;
    Var NG ;
    Var vNmax ;
    Var vN ;
    Var MSG ;
    Var Rdt ;
    Var Rdt14 ;
    Var TP ;
    Var P1G ;
    Var P1G14 ;
    Var IndicMP ;
    Var vcrgr;
    Var MSG_MP ;
    Var MSveg_MP;
    Var IR ;
};

} // namespace AZODYN

DECLARE_DYNAMICS(AZODYN::YieldPea)

