// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>


#include <ParametersSoil.hpp>
#include <ParametersPlant.hpp>
#include<iostream>
#include<cstdlib>
#include<time.h>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;

class WaterSoil : public DiscreteTimeDyn
{
public:
    WaterSoil(const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        ps.initialiser(events);
        pp.initialiser(events);


        ETP.init(this, "ETP", events);
        P.init(this, "P", events);
        I.init(this, "I", events);
        Ei.init(this, "Ei", events);
        
        
        ProfRac.init(this, "ProfRac", events);
        ProfRacmax.init(this, "ProfRacmax", events);

        RUmax_C0.init(this, "RUmax_C0", events);
        TR.init(this, "TR", events);
        RU_C0.init(this, "RU_C0", events);
        RU_C1.init(this, "RU_C1", events);
        stressH.init(this, "stressH", events);
        RUavdr_C0.init(this, "RUavdr_C0", events);
        TR_C0.init(this, "TR_C0", events);
        FTSW_C0.init(this, "FTSW_C0", events);
        stressH_C0.init(this, "stressH_C0", events);
        JSA.init(this, "JSA", events);
        rEV.init(this, "rEV", events);
        EV.init(this, "EV", events);
        StockEauD_C0.init(this, "StockEauD_C0", events);
        RUmax_C1.init(this, "RUmax_C1", events);
        RUavdr_C1.init(this, "RUavdr_C1", events);
        TR_C1.init(this, "TR_C1", events);
        tEau_C2.init(this, "tEau_C2", events);
        StockEauD_C1.init(this, "StockEauD_C1", events);
        RUmax_C2.init(this, "RUmax_C2", events);
        RUavdr_C2.init(this, "RUavdr_C2", events);
        RU_C2.init(this, "RU_C2", events);
        StockEauD_C2.init(this, "StockEauD_C2", events);
        StockEauD_tot.init(this, "StockEauD_tot", events);
        FTSW.init(this, "FTSW", events);
	

    }

    virtual ~WaterSoil()
    {}

    virtual void compute(const vd::Time& j)
    {
        /// CODE FAIT AVEC LA LOGIQUE RU INITIALE EST UNE VRAIE RU (SANS LA Pf 4.2) EGAL PAS UN STOCK D’EAU (QUI COMPREND LE pf 4.2)
        /// IDEM pour RUMAX
        /// ON POURRAIT AUSSI TRAVAILLER QUE SUR DES STOCK QUI PRENNENT EN COMPTE la pf 4.2!

        //------------- Couche C0——————————————————

        // Calcul des flux sortant de C0

        // Quantite d’eau perdue par transpiration de la culture le jour j (mm) issue de la couche C0 et de la couche C1

        if(pp.begin_date + j<pp.date_init_H){
            TR=0.0;
        } else {
            if(pp.begin_date + j<pp.date_init_MS){
                TR = std::max(0.0,std::min((RU_C0(-1) + RU_C1(-1)), ETP(-1)*ps.Trans * stressH(-1)));
            } else {
                TR = std::max(0.0,std::min((RU_C0(-1) + RU_C1(-1)), ETP(-1)*Ei(-1)*ps.Trans * stressH(-1)));
            }
        }


        // Quantite d’eau perdue par transpiration de la culture le jour j (mm) issue de la couche C0

        if(pp.begin_date + j<pp.date_init_H){
            TR_C0=0.0;
        } else {
            if((RU_C0(-1)+RU_C1(-1))==0.0){
                TR_C0=0.0;}
            else{
                TR_C0 = TR()*RU_C0(-1)/(RU_C0(-1)+RU_C1(-1));
            }
        }


        //Nombre de jours cumules sans apport d eau depuis le dernier apport

        if(pp.begin_date + j<pp.date_init_H){
            JSA=0.0;
        } else if((P(-1)+I(-1))<ps.ApportEau_seuil){
            JSA=JSA(-1)+1 ;
        } else {
            JSA=0.0;
        }


        //Reduction de l evaporation en fonction du nombre de jours sans apports d eau (varie entre 0 et 1)
      
        if (JSA() >=ps.JSA_seuil) {
            rEV=std::pow((JSA()+1),ps.EVAPO)-std::pow(JSA(),ps.EVAPO);
        } else {
            rEV =1.0;
        }

 	
	// Quantite d eau perdue par evaporation du sol le jour j issu de la couche C0 (mm)

        if(pp.begin_date + j<pp.date_init_H){
            EV=0.0;
        } else {
            EV=std::max(0.0, ETP(-1)*(1-Ei(-1))*rEV());
        }

        // Quantite d eau maximale disponible contenue dans la couche C0 (mm)

        RUmax_C0=std::max(0.0,ps.ep_C0*ps.RUmax/ProfRacmax());


        // Quantite d eau dans la couche C0 le jour j avant la prise en compte du drainage issu de cette couche (mm)

        if(pp.begin_date + j<pp.date_init_H){
            RUavdr_C0 = 0.0;
        } else if(pp.begin_date + j==pp.date_init_H){
            RUavdr_C0 = std::max(0.0,ps.ep_C0*(ps.RU_init/ProfRacmax()));
        } else {
            RUavdr_C0 = std::max(0.0, RU_C0(-1)+ P(-1)+I(-1)-EV()-TR_C0());
        }


        // Reserve utile dans la couche C0 le jour j avec la prise en compte du drainage issu de cette couche

        if(pp.begin_date + j<pp.date_init_H){
            RU_C0=0.0;
        } else if(pp.begin_date + j==pp.date_init_H){
            RU_C0 = std::max(0.0,ps.ep_C0*ps.RU_init/ProfRacmax());
        } else {
            RU_C0 = std::min(RUmax_C0(), RUavdr_C0());
        }


        //Fraction d’eau transpirable dans le sol en C0 (varie entre 0 et 1)
    
        if (pp.begin_date + j < pp.date_init_H) {
            FTSW_C0 = 0.0;
        } else if (ps.MottesDelta > ps.Mottes_seuil) {
            FTSW_C0 = ps.coef_Mottes*RU_C0()/RUmax_C0();
        } else {
            FTSW_C0 = RU_C0()/RUmax_C0();
        }


        // Facteur de stress hydrique pour la couche C0 (varie entre 0 et 1)

        if(pp.begin_date + j<pp.date_init_H){
            stressH_C0=1.0;
        } else {
            stressH_C0 =ps.stressH1/(1+ps.stressH2*std::exp(-ps.stressH3*(FTSW_C0()-ps.stressH4))) ;
        }

        // Drainage de C0 vers la couche en dessous (mm)

        StockEauD_C0=std::max(0.0,RUavdr_C0()-RUmax_C0());


        //-------------- Couche C1——————————————

        // Quantite d eau maximale disponible contenue dans la couche C1 (mm)

        if(ProfRac()<= ps.ep_C0){
            RUmax_C1=0.0;
        } else {
            RUmax_C1 =std::max(0.0,(ProfRac()-ps.ep_C0)*ps.RUmax/ProfRacmax());
        }


        // Quantité d’eau perdue par transpiration de la culture le jour j (mm) issue de la couche C1

        if(pp.begin_date + j<pp.date_init_H){
            TR_C1=0.0;
        } else {
            if ((RU_C0(-1)+RU_C1(-1))==0.0){
                TR_C1=0.0;
            } else {
                TR_C1 = TR()*RU_C1(-1)/std::max(0.001,(RU_C0(-1)+RU_C1(-1)));//TODO robust version
                //TR_C1 = TR()*RU_C1(-1)/(RU_C0(-1)+RU_C1(-1));
            }
        }


        // Teneur en eau de la couche C2 qui va être ajoutée à C1 et retranchée à C2 (mm eau/mm sol)

        if(ProfRac()<ProfRacmax()){
            tEau_C2 =RU_C2(-1)/(ProfRacmax()-std::max(ps.ep_C0,ProfRac(-1)));
        } else {
            tEau_C2 =0.0;
        }


        // Stock d eau dans la couche C1 le jour j avant la prise en compte du drainage issu de cette couche (mm)

        if(ProfRac() <=ps.ep_C0){
            RUavdr_C1=0.0;
        } else {
            RUavdr_C1 = std::max(0.0,RU_C1(-1)+(ProfRac()-ProfRac(-1))*tEau_C2()+StockEauD_C0()-TR_C1()) ;
        }


        // Reserve utile dans la couche C1 le jour j avec la prise en compte du drainage issu de cette couche

         if(pp.begin_date + j<pp.date_init_H){
            RU_C1=0.0;
         } else {
            RU_C1 = std::min(RUmax_C1(), RUavdr_C1());
         }


         // Drainage de C1 vers la couche C2 (mm)

         StockEauD_C1=std::max(0.0,RUavdr_C1()-RUmax_C1());


       //-------------- Couche C2

      
         // Quantite d eau maximale disponible contenue dans la couche C2 (mm)

         if(ProfRac()<= ps.ep_C0){
             RUmax_C2 =std::max(0.0,(ProfRacmax() - ps.ep_C0)*ps.RUmax/ProfRacmax());
         } else {
             RUmax_C2 =std::max(0.0,(ProfRacmax()-ProfRac())*ps.RUmax/ProfRacmax());
         }


        // Reserve utile dans la couche C2 le jour j avant la prise en compte du drainage issu de cette couche (mm)

         if(pp.begin_date + j<pp.date_init_H){
             RUavdr_C2=0.0;
         } else {
             if (pp.begin_date + j == pp.date_init_H) {
                 RUavdr_C2=std::max(0.0,(ProfRacmax()-std::max(ps.ep_C0,ProfRac()))*(ps.RU_init/ProfRacmax()));
             } else if (ProfRac()<= ps.ep_C0){
                 RUavdr_C2 = RU_C2(-1)+ StockEauD_C0() ;
             } else {
                 RUavdr_C2 = std::max(0.0, RU_C2(-1) -(ProfRac()-ProfRac(-1))* tEau_C2()+ StockEauD_C1());
             }
         }


         // Réserve utile dans la couche C2 le jour j avec la prise en compte du drainage issu de cette couche (mm)

         if(pp.begin_date + j < pp.date_init_H){
             RU_C2=0.0;
         } else {
             RU_C2 = std::min(RUmax_C2(), RUavdr_C2());
         }

         // Drainage de C2 vers la couche en dessous (mm)

         StockEauD_C2=std::max(0.0,RUavdr_C2()-RUmax_C2());


         //Somme de l’eau drainee issue de C2 depuis l’initialisation jusqu’au jour j  (mm) (mm)

         if(pp.begin_date + j<pp.date_init_H){
             StockEauD_tot =0.0;
         } else {
             StockEauD_tot= StockEauD_tot(-1)+ StockEauD_C2();
         }



         //-------------- Calculs sur plusieurs couches


         //Fraction d’eau transpirable dans le sol en C0 et en C1 (varie entre 0 et 1)

         if (pp.begin_date + j < pp.date_init_H) {
             FTSW = 0.0;
         } else if(ProfRac()<=ps.ep_C0){
             FTSW= FTSW_C0();
         } else if (ps.MottesDelta > ps.Mottes_seuil){
             FTSW = ps.coef_Mottes*(RU_C0()+RU_C1())/(RUmax_C0()+RUmax_C1());
         } else {
             FTSW = (RU_C0()+RU_C1())/(RUmax_C0()+RUmax_C1());
         }

         //Facteur de stress hydrique total qui prend en compte la fraction d’eau transportable sur les couches C0 et C1 (varie entre 0 et 1)
        
         if(pp.begin_date + j<pp.date_init_H){
             stressH=1.0;
         } else {
             stressH =std::max(ps.stressH_seuil,ps.stressH1/
                               (1+ps.stressH2*std::exp(
                                   -ps.stressH3*(FTSW()-ps.stressH4))));
         }
    }


private:

    ParametersSoil ps;
    ParametersPlant pp;

    /*Nosync*/ Var ETP;
    /*Nosync*/ Var P;
    /*Nosync*/ Var I;
    /*Nosync*/ Var Ei;
    
    
    /*Sync*/ Var ProfRac;
    /*Sync*/ Var ProfRacmax;

    Var RUmax_C0;
    Var RU_C0;
    Var RU_C1;
    Var stressH;
    Var RUavdr_C0;
    Var TR;
    Var TR_C0;
    Var FTSW_C0;
    Var stressH_C0;
    Var JSA;
    Var rEV;
    Var EV;
    Var StockEauD_C0;
    Var RUmax_C1;
    Var RUavdr_C1;
    Var TR_C1;
    Var tEau_C2;
    Var StockEauD_C1;
    Var RUmax_C2;
    Var RUavdr_C2;
    Var RU_C2;
    Var StockEauD_C2;
    Var StockEauD_tot; 
    Var FTSW;
 
};

} // namespace

DECLARE_DYNAMICS(AZODYN::WaterSoil)

