/**
 *
 * Copyright (C) 2015-2016 INRA.
 *
 * TODO licence
 */

#ifndef AZODYN_PARAMETERS_PLANT_PEA_HPP
#define AZODYN_PARAMETERS_PLANT_PEA_HPP

#include "ParametersPlant.hpp"

namespace AZODYN {

using namespace vle::utils;

class ParametersPlantPea : public ParametersPlant
{
public:
    //somme des temperatures entre semis et levee
    double ST_S_LEVEE;
    //somme des temperatures entre floraison et remplissage des graines
    double ST_DF_DRG;
    //somme des temperatures entre fin du stade limite d'avortement et maturite
    //physilogique
    double STmax_FSLA_MP;

    //la teneur en azote minimale des parties végétatives (azote non mobilisables)
    double tNstruct;

    //nb etages florals potentiel
    double NETpot;

    //nombre de degre jours necessaires pour remplir les grains d'un etage
    double DRG_ET;

    //nombre de degre jours necessaires pour la floraison d'un etage
    double FLO_ET;

    /***  Fixation de l'azote par le poids ***/

    //profondeur optimale des nodosites
    //(mm)
    double Profnod_opt;

    //profondeur minimale des nodosites si tassement du sol
    //(mm)
    double Profnod_min;

    //densite apparente au dessus de laquelle la profondeur des nodosites
    //est minimale
    double damax_C0;

    //nbre de jours consecutifs de stress hydrique à partir duquel l'arret de
    //la fixation est irreversible
    double NJstress_nod_seuil;

    //sert pour calculer pNac_fix
    double coefNsol_fix1;

    //sert pour calculer pNac_fix
    double a_pNfix;

    //sert pour calculer pNac_fix, proportion maximale d'azote venant de la
    //fixation
    double pNfix_max;

    //sert pour calculer pNac_fix
    double coefNsol_fix2;

    //sert pour calculer pNac_fix
    double b_pNfix;

    //seuil des sommes de température en dessous duquel la fixation maximale
    //est à 0
    double STfixmax_seuil;

    //sert pour calculer QNfixmax
    double coef_fixmax1;
    double coef_fixmax2;

    /*** Plant growth **/

    //efficience biologique pendant la phase vegetative
    double Ebv;

    double coef_MS_init;


    /***  Croissance racinaire ***/

    //paramtere d'legonation racinaire en fonctions du rayonnement (mm/Mj/m^2)
    double vRac_RG;

    //teneur en azote seuil en dessous de laquelle on impacte Ei et Eb
    double tNveg_seuil;

    //profondeur racinaire macimale potentielle sans tassement
    double ProfRacmax_pot;

    //paramtre de reduction de la profondeur maximale en fonction du pourcentage
    //de MotteDelta
    double coef_Mottes_Profmax;

    //parametre de la densite racinaire
    double densRac1;

    //parametre de la densite racinaire
    double densRac2;

    /** Stress azote **/

    //pourcentage de MS restant apres DRG suite a la remobilisation du carbone
    double pMSapDRG;


    /** Rendement **/

    //parametre varietal de conversion de la vitesse de croissance en nombre
    //de grains
    double coef_Ngunit1;
    double coef_Ngunit2;

    //coefficient permetant de calculer vNmax
    double vNmax1;

    //coefficient permetant de calculer vNmax
    double vNmax2;

    //coefficient permetant de calculer vN
    double coef_vN;

    //proportion d'azote moyen d'une graine au stade FSLA
    double pNfsla;

    //parametres pour le calcul de la vitesse de croissance d'une graine
    double coef_vcrgr1;
    double coef_vcrgr2;

    //facteur de conversion pour passer d'un pourcentage d'azote en taux de
    //proteines
    double coef_prot_gr;

    // Poids de mille grain moyen
    double PMGmoy_var;

    void initialiser( const vle::devs::InitEventList& events ){
        ParametersPlant::initialiser(events);
        // Paramètres liés à la phénologie
        ST_S_LEVEE = Utils::extractDouble(events, "ST_S_LEVEE");
        ST_DF_DRG = Utils::extractDouble(events, "ST_DF_DRG");
        STmax_FSLA_MP = Utils::extractDouble(events, "STmax_FSLA_MP");
        tNstruct  = Utils::extractDouble(events, "tNstruct");
        NETpot  = Utils::extractDouble(events, "NETpot");
        DRG_ET  = Utils::extractDouble(events, "DRG_ET");
        FLO_ET  = Utils::extractDouble(events, "FLO_ET");
        Profnod_opt  = Utils::extractDouble(events, "Profnod_opt");
        Profnod_min  = Utils::extractDouble(events, "Profnod_min");
        damax_C0  = Utils::extractDouble(events, "damax_C0");
        NJstress_nod_seuil  = Utils::extractDouble(events, "NJstress_nod_seuil");
        coefNsol_fix1  = Utils::extractDouble(events, "coefNsol_fix1");
        a_pNfix  = Utils::extractDouble(events, "a_pNfix");
        pNfix_max  = Utils::extractDouble(events, "pNfix_max");
        coefNsol_fix2  = Utils::extractDouble(events, "coefNsol_fix2");
        b_pNfix  = Utils::extractDouble(events, "b_pNfix");
        STfixmax_seuil  = Utils::extractDouble(events, "STfixmax_seuil");
        coef_fixmax1  = Utils::extractDouble(events, "coef_fixmax1");
        coef_fixmax2  = Utils::extractDouble(events, "coef_fixmax2");
        Ebv  = Utils::extractDouble(events, "Ebv");
        coef_MS_init  = Utils::extractDouble(events, "coef_MS_init");
        vRac_RG  = Utils::extractDouble(events, "vRac_RG");
        tNveg_seuil  = Utils::extractDouble(events, "tNveg_seuil");
        ProfRacmax_pot  = Utils::extractDouble(events, "ProfRacmax_pot");
        coef_Mottes_Profmax  = Utils::extractDouble(events, "coef_Mottes_Profmax");
        densRac1  = Utils::extractDouble(events, "densRac1");
        densRac2  = Utils::extractDouble(events, "densRac2");
        pMSapDRG  = Utils::extractDouble(events, "pMSapDRG");
        coef_Ngunit1  = Utils::extractDouble(events, "coef_Ngunit1");
        coef_Ngunit2  = Utils::extractDouble(events, "coef_Ngunit2");
        vNmax1  = Utils::extractDouble(events, "vNmax1");
        vNmax2  = Utils::extractDouble(events, "vNmax2");
        coef_vN  = Utils::extractDouble(events, "coef_vN");
        pNfsla  = Utils::extractDouble(events, "pNfsla");
        coef_vcrgr1  = Utils::extractDouble(events, "coef_vcrgr1");
        coef_vcrgr2  = Utils::extractDouble(events, "coef_vcrgr2");        
        coef_prot_gr  = Utils::extractDouble(events, "coef_prot_gr");
        PMGmoy_var = Utils::extractDouble(events, "PMGmoy_var");
    }
};

} // namespace

#endif
