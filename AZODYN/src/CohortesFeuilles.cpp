// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <ParametersPlantRape.hpp>
#include <ParametersSoilRape.hpp>
#include <phenologie_types.hpp>

#include <vector>
#include <numeric> //std::accumulate

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;
using namespace std;
using std::accumulate;

namespace AZODYN
{

    class CohortesFeuilles : public DiscreteTimeDyn
    {
    public:
        CohortesFeuilles(const vd::DynamicsInit &model, const vd::InitEventList &evts) : DiscreteTimeDyn(model, evts)
        {
            pp.initialiser(evts);
            ps.initialiser(evts);
            if (evts.get("begin_date")->isDouble()) {
                begin_time = evts.getDouble("begin_date");
                begin_date = vle::utils::DateTime::toJulianDay(begin_time);
            } else if (evts.get("begin_date")->isString()) {
                begin_date = evts.getString("begin_date");
                begin_time = vle::utils::DateTime::toJulianDay(begin_date);
            }

            // declaration variables d'entrée du module
            partMSLeaves.init(this, "partMSLeaves", evts);
            INNvec.init(this, "INNvec", evts);
            MSAr.init(this, "MSAr", evts);
            MSAr.history_size(3);

            Tmoy.init(this, "Tmoy", evts);
            Stade.init(this, "Stade", evts);
            STsemis.init(this, "STsemis", evts);

            NMinTot.init(this, "NMinTot", evts);
            MSfm.init(this, "MSfm", evts);
            LeavesLife.init(this, "LeavesLife", evts);
        }

        virtual ~CohortesFeuilles()
        {
        }

        virtual void compute(const vd::Time &t)
        {
            vle::devs::Time time = begin_time+t;
            
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                NMinTot = 0;
                MSfm = 0;
                break;
            case JOUR_LEVEE:
                NMinTot = 0;
                MSfm = 0;
                jourLevee = (unsigned int)time;
                break;
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:

                // calcul des paramètres

                double CNfm = 1.67 * INNvec(-1) - 0.37;
                double CN = -48.4 * INNvec(-1) + 85.9;
                double fCN = exp(ps.ACfm * (ps.CN_opt - CN) / ps.CN_opt);
                double ft = (Tmoy() < 0) ? 0 : exp(ps.alpha * (Tmoy() - ps.Tref));

                calculLeavesLife();

                unsigned int nbJoursDepuisLevee = (unsigned int)time - jourLevee;
                bool nouveauJour = (nbJoursDepuisLevee > cohortes.size());
                // recuperer la cohorte cree au jour courant et mettre a jour MS, QN et duree de vie des feuilles
                cohorte &cohJourCourant = cohorteJourCourant(nouveauJour);
                cohJourCourant.life = LeavesLife() /*pp.VIEFM*/;
                cohJourCourant.deltaMSLeaves = max(0.0, partMSLeaves(-1) * (MSAr(-1) - MSAr(-2)));
                // calcul de la minéralisation, mat seche, LAI, de chaque cohorte et le cumul
                double minTotale = 0;
                double msfm = 0;

                for (unsigned int c = 0; c < cohortes.size(); c++)
                {
                    cohorte &coh = cohortes[c];
                    calculSTetChute(coh, nouveauJour, Tmoy(), nbJoursDepuisLevee);

                    switch (statut(coh, nbJoursDepuisLevee))
                    {
                    case NonChutee:
                        break;
                    case ChuteeLeJourMeme:
                    {
                        double &Nr = azoteResiduelJourCourant(coh, nouveauJour);
                        Nr = max(0.0, CNfm / 100 * (coh.deltaMSLeaves));
                        msfm = msfm + coh.deltaMSLeaves;
                        // std::cout << "CohortesFeuilles::compute Nr0 "<< Nr << std::endl;
                        break;
                    }
                    case ChuteeDepuisAuMoinsUnJour:
                    {
                        double &Nr = azoteResiduelJourCourant(coh, nouveauJour);
                        if (Tmoy() <= 0)
                        {
                            // Nr n'evolue pas
                            Nr = azoteResiduelJourPrecedent(coh);
                        }
                        else
                        {
                            Nr = (CNfm < ps.CN_opt) ? max(0.0, azoteResiduelJourPrecedent(coh) * exp(-ps.km * ft)) : max(0.0, azoteResiduelJourPrecedent(coh) * exp(-ps.km * fCN * ft));
                        }
                        minTotale += ps.CAUfm * (azoteResiduelJourPrecedent(coh) - Nr);
                        break;
                    }
                    }
                }
                NMinTot = minTotale;
                MSfm = MSfm(-1) + msfm;
                break;
            }
        }

    private:
        enum STATUT_COHORTE
        {
            NonChutee,
            ChuteeLeJourMeme,
            ChuteeDepuisAuMoinsUnJour
        };
        /**
         * @brief représentation d'une cohorte de feuille
         */
        struct cohorte
        {
            cohorte() : deltaMSLeaves(0), life(0),
                        jourChute(0), ST(0), Nr()
            {
            }

            double deltaMSLeaves;
            double life;
            unsigned int jourChute;
            double ST;
            std::vector<double> Nr;
        };

        /* Calcul de la duree de vie des cohortes en dynamiques
          selon leu jour de naissance en DJ depuis semis
        */

        void calculLeavesLife()
        {

            if (STsemis() <= 800)
            {
                LeavesLife = pp.AVIEFM + pp.BVIEFM * STsemis();
            }
            else if (STsemis() > 800 && STsemis() <= 1400)
            {
                LeavesLife = pp.CVIEFM + pp.DVIEFM * STsemis();
            }
            else
            {
                LeavesLife = pp.EVIEFM + pp.FVIEFM * STsemis();
            }
        }

        /**
         * @brief Met à jour la somme des températures depuis la naissance
         * d'une cohorte ainsi que le jour de chute (si chute il y a)
         */
        void calculSTetChute(cohorte &coh, bool nouveauJour, double tmoy,
                             unsigned int nbJoursDepuisLevee)
        {

            if (nouveauJour)
            {
                coh.ST = coh.ST + max(0.0, tmoy);
                if (coh.ST >= coh.life && coh.jourChute == 0)
                {
                    coh.jourChute = nbJoursDepuisLevee;
                }
            }
            else
            {
                // on fait l'hypothèse que Tmoy n'a pas changé
            }
        }
        /**
         * @brief Donne le statut de la cohorte une fois la somme des temps mise
         * à jour, ainsi que le jour de chute (si chute il y a)
         */
        STATUT_COHORTE statut(cohorte &coh, unsigned int nbJoursDepuisLevee)
        {
            if (coh.jourChute == nbJoursDepuisLevee)
            {
                return ChuteeLeJourMeme;
            }
            else if (coh.jourChute != 0)
            {
                return ChuteeDepuisAuMoinsUnJour;
            }
            else
            {
                return NonChutee;
            }
        }
        /**
         * @brief Donne l'azote residuel du jour précédent
         */
        double azoteResiduelJourPrecedent(cohorte &coh)
        {
            if (coh.Nr.size() >= 2)
            {
                return coh.Nr[coh.Nr.size() - 2];
            }
            else
            {
                return 0;
            }
        }
        /**
         * @brief Donne l'azote residuel du jour courant
         * (RQ : la subtilité est de rendre une reference sur le double à mettre
         * à jour)
         */
        double &azoteResiduelJourCourant(cohorte &coh, bool nouveauJour)
        {
            if (nouveauJour)
            {
                coh.Nr.push_back(0);
            }
            return coh.Nr[coh.Nr.size() - 1];
        }
        /**
         * @brief Donne la cohorte crée au jour courant
         * (RQ : la subtilité est de rendre une reference sur la cohorte à mettre
         * à jour)
         */
        cohorte &cohorteJourCourant(bool nouveauJour)
        {
            if (nouveauJour)
            {
                cohortes.push_back(cohorte());
            }
            return cohortes[cohortes.size() - 1];
        }

        ParametersSoilRape ps;
        ParametersPlantRape pp;

        // Stock des cohortes
        std::vector<cohorte> cohortes;

        // le jour de la levée
        unsigned int jourLevee;

        std::string  begin_date;
        double begin_time;

        // Variables d'etat gerees par ce composant

        Var NMinTot;
        Var MSfm;
        Var LeavesLife;

        // Sync
        Var Tmoy;
        Var Stade;
        Var STsemis;

        // Nosync
        Var partMSLeaves;
        Var INNvec;
        Var MSAr;
    };
} // namespace

DECLARE_DYNAMICS(AZODYN::CohortesFeuilles)
