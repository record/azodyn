// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <phenologie_types.hpp>
#include <ParametersPlantRape.hpp>

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

namespace AZODYN
{

    class Rendement : public DiscreteTimeDyn
    {
    public:
        Rendement(const vd::DynamicsInit &model, const vd::InitEventList &evts) : DiscreteTimeDyn(model, evts)
        {
            pp.initialiser(evts);

            Pluie.init(this, "Pluie", evts);
            ETP.init(this, "ETP", evts);
            RG.init(this, "RG", evts);
            Tmoy.init(this, "Tmoy", evts);
            Tmax.init(this, "Tmax", evts);
            Stade.init(this, "Stade", evts);
            STfloraison.init(this, "STfloraison", evts);
            INNvec.init(this, "INNvec", evts);
            MSAr.init(this, "MSAr", evts);
            QNAr.init(this, "QNAr", evts);
            RGintercept.init(this, "RGintercept", evts);

            QNAr_G4.init(this, "QNAr_G4", evts);
            MSAr_G4.init(this, "MSAr_G4", evts);
            SOMRGpot.init(this, "SOMRGpot", evts);
            Hydrique.init(this, "Hydrique", evts);
            QP.init(this, "QP", evts);
            ST25.init(this, "ST25", evts);
            Rend.init(this, "Rend", evts);
            INNflo.init(this, "INNflo", evts);
            ST10.init(this, "ST10", evts);
            PMGr.init(this, "PMGr", evts);
            NGr.init(this, "NGr", evts);
        }

        virtual ~Rendement(){};

        //@@begin:compute@@
        virtual void compute(const vd::Time & /*time*/)
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
                QNAr_G4 = 0;
                MSAr_G4 = 0;
                SOMRGpot = 0;
                Hydrique = 0;
                QP = 0;
                ST10 = 0;
                ST25 = 0;
                INNflo = 0;
                Rend = 0;
                PMGr = 0;
                NGr = 0;
                break;
            case JOUR_F1:
                QNAr_G4 = QNAr_G4(-1);
                MSAr_G4 = MSAr_G4(-1);
                SOMRGpot = SOMRGpot(-1);
                Hydrique = Hydrique(-1);
                QP = QP(-1);
                ST10 = ST10(-1);
                ST25 = ST25(-1);
                INNflo = INNvec();
                Rend = Rend(-1);
                PMGr = 0;
                NGr = 0;
                break;
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
                QNAr_G4 = QNAr_G4(-1);
                MSAr_G4 = MSAr_G4(-1);
                if (STfloraison() < 600)
                {
                    SOMRGpot = SOMRGpot(-1) + RGintercept();
                    QP = QP(-1) + (RG() * 100 / std::max(0.0001, Tmoy()));
                    Hydrique = Hydrique(-1) + (Pluie() - ETP());
                }
                else
                {
                    SOMRGpot = SOMRGpot(-1);
                    Hydrique = Hydrique(-1);
                    QP = QP(-1);
                }

                if ((STfloraison() > 600) and (STfloraison() <= 1350))
                {
                    ST10 = ST10(-1) + std::max(0.0, Tmoy() - 10);
                    ST25 = ST25(-1) + std::max(0.0, Tmax() - 25);
                }
                else
                {
                    ST10 = ST10(-1);
                    ST25 = ST25(-1);
                }

                INNflo = INNflo(-1);
                Rend = Rend(-1);
                PMGr = 0;
                NGr = 0;
                break;
            case JOUR_G4:
                QNAr_G4 = QNAr();
                MSAr_G4 = MSAr();
                SOMRGpot = SOMRGpot(-1);
                Hydrique = Hydrique(-1);
                QP = QP(-1);
                if ((STfloraison() > 600) and (STfloraison() <= 1350))
                {
                    ST10 = ST10(-1) + std::max(0.0, Tmoy() - 10);
                    ST25 = ST25(-1) + std::max(0.0, Tmax() - 25);
                }
                else
                {
                    ST10 = ST10(-1);
                    ST25 = ST25(-1);
                }
                INNflo = INNflo(-1);
                Rend = Rend(-1);
                PMGr = 0;
                NGr = 0;
                break;
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
                QNAr_G4 = QNAr_G4(-1);
                MSAr_G4 = MSAr_G4(-1);
                SOMRGpot = SOMRGpot(-1);
                Hydrique = Hydrique(-1);
                QP = QP(-1);
                if ((STfloraison() > 600) and (STfloraison() <= 1350))
                {
                    ST10 = ST10(-1) + std::max(0.0, Tmoy() - 10);
                    ST25 = ST25(-1) + std::max(0.0, Tmax() - 25);
                }
                else
                {
                    ST10 = ST10(-1);
                    ST25 = ST25(-1);
                }
                INNflo = INNflo(-1);
                Rend = Rend(-1);
                PMGr = 0;
                NGr = 0;
                break;
            case JOUR_RECOLTE:
                QNAr_G4 = QNAr_G4(-1);
                MSAr_G4 = MSAr_G4(-1);
                SOMRGpot = SOMRGpot(-1);
                Hydrique = Hydrique(-1);
                QP = QP(-1);
                ST10 = ST10(-1);
                ST25 = ST25(-1);
                INNflo = INNflo(-1);
                // calcul rendement
                double Na_G4 = QNAr_G4() * 100 / MSAr_G4();
                double INN_G4 = Na_G4 / (pp.NC1G4 * pow(MSAr_G4() / 1000, (-pp.NC2G4)));
                double NGpot = pp.BNGPOT + (pp.ANGPOT * SOMRGpot());
                // note modif Rendement (car trop faible) par Muriel 2012 - avant
                double ISN;
                if (INNflo() < pp.XII)
                {
                    ISN = std::min(1.0, pp.AISN + pp.BISN * INNflo() + pp.CISN * INN_G4);
                    // ancienne version :   ISN = std::min(1.0,pp.AISN) + pp.BISN * INNflo() + pp.CISN * INN_G4;
                }
                else
                {
                    ISN = 1;
                }
                NGr = ISN * NGpot;
                if (NGr() < pp.NGSEUIL)
                {
                    PMGr = std::min(pp.PMGmax_var, pp.PMGmax_var +
                                                       Hydrique() * pp.PMGEAU + ST10() * pp.PMGTMOY +
                                                       QP() * pp.PMGQP + ST25() * pp.PMGTMAX);
                }
                else
                {
                    PMGr = std::min(pp.PMGmax_var, (pp.PMGmax_var +
                                                    Hydrique() * pp.PMGEAU + ST10() * pp.PMGTMOY +
                                                    QP() * pp.PMGQP + ST25() * pp.PMGTMAX) *
                                                       pp.NGSEUIL / NGr());
                }
                Rend = PMGr() * NGr() / 10000;
                break;
            }
        }
        //@@end:compute@@

        //@@begin:initValue@@
        virtual void initValue(const vd::Time & /*time*/)
        {
        }
        //@@end:initValue@@

    private:
        ParametersPlantRape pp;

        Var Pluie;
        Var ETP;
        Var RG;
        Var Tmoy;
        Var Tmax;
        Var Stade;
        Var STfloraison;
        Var INNvec;
        Var MSAr;
        Var QNAr;
        Var RGintercept;

        Var QNAr_G4;
        Var MSAr_G4;
        Var SOMRGpot;
        Var Hydrique;
        Var QP;
        Var ST25;
        Var Rend;
        Var INNflo;
        Var ST10;
        Var PMGr;
        Var NGr;
    };

} // namespace AzodynColza

DECLARE_DYNAMICS(AZODYN::Rendement)
