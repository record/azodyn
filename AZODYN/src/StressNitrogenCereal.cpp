// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include<iostream>
#include<cstdlib>
#include<time.h>

#include <vle/DiscreteTime.hpp>

#include "ParametersPlantCereal.hpp"

namespace vd = vle::devs;

namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;
using namespace vle::utils;

class StressNitrogenCereal : public DiscreteTimeDyn
{
public:
    StressNitrogenCereal(const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        pp.initialiser(events);

        jMatur.init(this, "jMatur", events);

        QNG.init(this, "QNG", events);//cycle
        QN_tot.init(this, "QN_tot", events);
        QNc.init(this, "QNc", events);
        MS.init(this, "MS", events);
        MSG.init(this, "MSG", events);//cycle yield
        MS_FLO.init(this, "MS_FLO", events);
        Tmoy.init(this, "Tmoy", events);
        SJNapFLO.init(this, "SJNapFLO", events);//cycle de sync
        STapinitMS.init(this, "STapinitMS", events);
        QNveg.init(this, "QNveg", events);//cycle yield

        MSveg.init(this, "MSveg", events);
        INN.init(this, "INN", events);
        aireINN.init(this, "aireINN", events);
        INNint.init(this, "INNint", events);
        IC.init(this, "IC", events);
        DC.init(this, "DC", events);
        stressN_LAI.init(this, "stressN_LAI", events);
        stressN_Eb.init(this, "stressN_Eb", events);
        stressN_NGM2.init(this, "stressN_NGM2", events);

    }

    virtual ~StressNitrogenCereal()
    {}

    virtual void compute(const vd::Time& j)
    {

        // Matiere seche vegetative
        
         MSveg = (MS()-MSG())*10;
        
        // indice de nutrition azoté le jour j  (sans unité)

        if(pp.begin_date + j < pp.date_init_MS or jMatur(-1) != 0){
            INN=0.0;
        } else if (pp.begin_date + j<= pp.date_FLO){
            INN= (QN_tot()/pp.coef_tot_a)/QNc();
//            std::cout << " dbgINN 1 " << QN_tot()<< " " << QNc() << " " << INN() << "\n";
        } else {

            INN= (QNveg()/ MSveg())/
                    ((pp.coef_Nc*std::pow(MS_FLO()/100.0,pp.pente_Nc))/100);
//            std::cout << " dbgINN 2 " << MS_FLO()<< " " << (MS()-MSG()) <<" " << INN() << "\n";
        }
        
       

        // Aire INN cumulée depuis initialisation

        if(pp.begin_date + j < pp.date_init_MS or jMatur(-1) != 0){
            aireINN=0.0;
        } else if (pp.begin_date + j== pp.date_init_MS){
//            std::cout << " dbginn MS " <<  INN() << " " << Tmoy() <<"\n";
            aireINN= std::max(0.0, INN()*Tmoy());
        } else if (SJNapFLO() <= pp.SJNmax_FLO_RG){
//            std::cout << " dbginn >MS " <<  INN() << " " << Tmoy() <<"\n";
            aireINN= aireINN(-1) + (std::min(INN(),1.0)+ std::min(INN(-1),1.0))/2.0
                    * std::max(0.0,Tmoy());
        } else {
            aireINN=aireINN(-1);
        }

        // INN integre le jour j

        if(pp.begin_date + j < pp.date_init_MS or jMatur(-1) != 0){
            INNint=0.0;
        } else if (pp.begin_date + j== pp.date_init_MS){
//            std::cout << " dbg initMS " << aireINN() <<  "\n";
            INNint= aireINN()/Tmoy();
        } else if (SJNapFLO() <= pp.SJNmax_FLO_RG){
//            std::cout << " dbg >initMS " << aireINN()<< " " << STapinitMS() <<  "\n";
            INNint= aireINN()/ STapinitMS();
        } else {
            INNint=INNint(-1);
        }

        // Intensité de la carence azotée (sans unité)

        if(pp.begin_date + j < pp.date_init_MS or jMatur(-1) != 0){
            IC=0.0;
        } else {
            IC =std::max(0.0,std::max(IC(-1),1-INN()));
        }

        // Durée de la carence azotée (sans unité)

        if(pp.begin_date + j < pp.date_init_MS or jMatur(-1) != 0){
            DC=0.0;
        } else if (INN()< 0.9) {
            DC =DC(-1)+Tmoy();
        } else {
            DC = DC(-1);
        }

        // Facteur de diminution du LAI en fonction de lINN (de 0 :pas de stress à 1 : stress TOTAL)

        if(pp.begin_date + j < pp.date_init_MS or jMatur(-1) != 0){
            stressN_LAI=1;
        } else {
            stressN_LAI = std::max(0.4, std::min(1.0, pp.stressN_LAI2*(
                    1-pp.stressN_LAI3*std::exp(pp.stressN_LAI1*INN()))));
        }


        // Facteur de diminution du Eb en fonction de l INN (de 0 :pas de stress à 1 : stress TOTAL)

        if(pp.begin_date + j < pp.date_init_MS or jMatur(-1) != 0){
            stressN_Eb=1;
        } else {
            stressN_Eb = std::max(0.4, std::min(1.0, pp.stressN_Eb2*(1-
                   pp.stressN_Eb3 *std::exp(pp.stressN_Eb1*INN()))));
        }

//         Facteur de diminution du NGM2 en fonction de l INN (de 0 :pas de stress à 1 : stress TOTAL)
//        if (j< pp.date_FLO) {
//            stressN_NGM2= std::min(1.0,pp.stressN_NGM22
//                    -(pp.stressN_NGM21*DC()*IC()));
//        } else {
//            stressN_NGM2=stressN_NGM2(-1);
//        }


//          Deuxieme façon de calculer le stress NGM2
//         Facteur de diminution du NGM2 en fonction de l INN intégré (de 0 :pas de stress à 1 : stress TOTAL)
//        if (j<= pp.date_FLO) {
//            stressN_NGM2= std::min(1.0,pp.stressN_NGM2_int2
//                    +(pp.stressN_NGM2_int1*INNint()));
//        } else {
//            stressN_NGM2=stressN_NGM2(-1);
//        }

    
//        Troisieme façon de calculer le stress NGM2
//        Facteur de diminution du NGM2 en fonction de l INN_FLO (de 0 :pas de stress à 1 : stress TOTAL)

        if (pp.begin_date + j<  pp.date_init_MS) {
         stressN_NGM2= 1.0 ;
        } else if (pp.begin_date + j< pp.date_FLO) {
            stressN_NGM2= std::min(1.0,pp.alphaNG2 *(pp.stressN_NGM2_FLO2
                    +(pp.stressN_NGM2_FLO1*std::log(INN()))));
        } else {
            stressN_NGM2=stressN_NGM2(-1);
        }
    }


private:

    ParametersPlantCereal pp;

    /*Nosync*/ Var jMatur;

    /*Sync*/ Var QNG;
    /*Sync*/ Var QN_tot;
    /*Sync*/ Var QNc;
    /*Sync*/ Var MS;
    /*Sync*/ Var MSG;
    /*Sync*/ Var MS_FLO;
    /*Sync*/ Var Tmoy;
    /*Sync*/ Var SJNapFLO;
    /*Sync*/ Var STapinitMS;
    /*Sync*/ Var QNveg;

    Var MSveg;
    Var INN;
    Var aireINN;
    Var INNint;
    Var IC;
    Var DC;
    Var stressN_LAI;
    Var stressN_Eb;
    Var stressN_NGM2;


};

} // namespace

DECLARE_DYNAMICS(AZODYN::StressNitrogenCereal)

