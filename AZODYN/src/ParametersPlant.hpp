/**
 *
 * Copyright (C) 2015-2015 INRA.
 *
 * TODO licence
 */

//TODO Manque QNa init!!! dans les paramètres!!
#ifndef AZODYN_PARAMETERS_PLANT_HPP
#define AZODYN_PARAMETERS_PLANT_HPP

#include <vle/utils/Exception.hpp>
#include <vle/utils/DateTime.hpp>
#include "AzodynUtils.hpp"

namespace AZODYN {

using namespace vle::utils;

class ParametersPlant
{
public:
    //either wheat, barley or pea
    std::string crop;

    //---------------------------------------------------------------------------------//  
    // Paramètres liés à la phenologie

    //julian date of the begin date
    double begin_date;

    //date d'initialisation du module hydrique
    double date_init_H;

    //date a laquelle on initialise les calculs pour le bilan azoté.
    //doit impérativement être après la date d'initialisation du bilan hydrique
    //date d'initialisation du module azote
    double date_init_N;

    //date d'initialisation de la matiere seche TODO only for Cereal
    double date_init_MS;
    
    //date d initialisation du module mineralisation (va generallement etre a
    // la recolte du precedent cultural)
    double date_init_miner_res;

    //date semis
    double date_S;

    //date floraison
    double date_FLO;

    //temperature de base de la plante
    double Tbase;


    //---------------------------------------------------------------------------------//
    // Paramètres liés à la température

    // temperature minimale pour la plante
    double Tmin_plante;

    // temperature optimale pour la plante
    double Topt_plante;

    // temperature maximale pour la plante
    double Tmax_plante;

    // resistance maximale de la plante au froid au stade coleoptile
    double R_Coleo;

    //Seuil de température au dela duquel la destruction du
    //couvert est total
    double GELmax_seuil;

    // date de fin de l'application du gel hivernal
    double date_fin_GEL;

    //resistance minimale au froid de la plante
    double Rmin;

    //temperature en dessous de laquelle on va commencer
    //a avoir un endurcissement de la plante
    double Tmax_R;

    //temperature en dessous de laquelle la resitance potentielle
    //est maximale
    double Tmin_R;

    //nb feuilles seuil à partir duquel on augmente la résistance maximale
    double NbFi;

    //nb feuilles seuil à partir duquel la resistance se stabilise
    double NbFf;

    //phyllochron (deg jours/ feuille)
    double phyllo;

    //---- Parametres renseignes par l'utilisateur ----//

    //resistance maximale au froid
    double Rmax_var;

    //Nombre de jours necessaires pour un endurcissement total
    //de la plante
    double Njend;

    //Nombre de jours de desendurcisssement
    double Njdes;

    //---------------------------------------------------------------------------------//
    // Paramètres liés à l'absorption d azote par la plante

    //Paramètre de courbe critique
    double coef_Nc;

    //Paramètre de courbe max
    double coef_Nmax;

    //Rapport entre matière sèche totale et aérienne ou Quantité d’azote totale sur aérien
    double coef_tot_a;

    //Seuil de MS pour la courbe critique
    double MS_Nc_seuil;

    // Seuil de MS pour courbe max
    double MS_Nmax_seuil;

    //Paramètre de courbe critique
    double pente_Nc;

    //Paramètre de courbe max
    double pente_Nmax;

    //QNa_init ; (if == -1) alors utilisation equation qui utilise stressINN_init
    double QNa_init;

    //stressINN_init ; default = 1
    double stressINN_init;
    
    //Teneur en azote max pour des biomasses faibles
    double tNmax_Nmax;

    //Teneur en N critique pour des biomasses faibles
    double tNc_Nc ;

    // Vitesse maximale d’absorption de l’azote pour l’ensemble de la plante (aérien + racinaire)
    double Vmax;


    //---------------------------------------------------------------------------------//
    // Paramètres liés aux stress azotés

    //Réduction du Eb en fonction de l’INN
    double stressN_Eb1;

    //Réduction du Eb en fonction de l’INN
    double stressN_Eb2;

    //Réduction du Eb en fonction de l’INN
    double stressN_Eb3;

    //Facteur Reduction du LAI pour parametres genotypiques
    double alphaLAI;


    //---------------------------------------------------------------------------------//
    // pour la croissance de la plante

    //Efficience maximale d’interception du rayonnement lumineux
    double Eimax;

    //Coefficient d’extinction du rayonnement
    double k;

    //Coefficient conversion rayonnement global en actif
    double Ec;

    //EEfficience maximale de conversion du rayonnement lumineux après EP1 après DF en pois
    double Ebr ;
    //---- Parametres renseignes par l'utilisateur ----//

    //Matière sèche à date_init_MS
    // g/m²
    double MS_init;

    //Densité de semis
    //Plante/m²
    double densite_semis;

    //---------------------------------------------------------------------------------//
    // pour remplissage des grains

    //Coefficient liant la demande en Ms et la demande en N des grains
    double coef_demMS_demN;

    //---- Parametres renseignes par l'utilisateur ----//

    //Poids de mille grains maximum pour la variété considérée EN SEC
    double PMGmax_var;

    //Rendement maximum pour la variété considérée EN SEC
    double RDTmax_var;
    
    // coefficient entre PMG et calibrage
    double alphaCALI;


    void initialiser( const vle::devs::InitEventList& events ){
        if ( events.exist("crop") ){
            crop = events.getString("crop");
        } else {
            throw ArgError("parameter crop missing");
        }
        // Paramètres liés à la phénologie
        begin_date = Utils::extractDate(events, "begin_date");
        date_init_H = Utils::extractDate(events, "date_init_H");
        date_init_N = Utils::extractDate(events, "date_init_N");
        if ( events.exist("date_init_MS") ){
            date_init_MS = Utils::extractDate(events, "date_init_MS");
        } else {
            date_init_MS = std::numeric_limits<double>::infinity();//TODO for pea
        }
        date_init_miner_res = Utils::extractDate(events, "date_init_miner_res");
        date_S = Utils::extractDate(events,"date_S");
        date_FLO = Utils::extractDate(events, "date_FLO");
        Tbase = Utils::extractDouble(events, "Tbase");
        //params specific wheat and barley
        //verifications
        if (date_init_miner_res > date_init_N) {
            throw ArgError("date_init_miner_res > date_init_N");
        }
        if (date_init_H > date_init_N) {
            throw ArgError("date_init_H > date_init_N");
        }
        if (date_S > date_FLO) {
            throw ArgError("date_S > date_FLO");
        }
        // Paramètres liés à la température
        Tmin_plante = Utils::extractDouble(events, "Tmin_plante");
        Topt_plante = Utils::extractDouble(events, "Topt_plante");
        Tmax_plante = Utils::extractDouble(events, "Tmax_plante");
        R_Coleo = Utils::extractDouble(events, "R_Coleo");
        GELmax_seuil = Utils::extractDouble(events, "GELmax_seuil");
        date_fin_GEL = Utils::extractDate(events,"date_fin_GEL");
        Rmin = Utils::extractDouble(events, "Rmin");
        Tmax_R = Utils::extractDouble(events, "Tmax_R");
        Tmin_R = Utils::extractDouble(events, "Tmin_R");
        NbFi = Utils::extractDouble(events, "NbFi");
        NbFf = Utils::extractDouble(events, "NbFf");
        phyllo = Utils::extractDouble(events, "phyllo");
        Rmax_var = Utils::extractDouble(events, "Rmax_var");
        Njend = Utils::extractDouble(events, "Njend");
        Njdes = Utils::extractDouble(events, "Njdes");

        // Paramètres liés à l'absorption d azote par la plante

        coef_Nc = Utils::extractDouble(events, "coef_Nc");
        coef_Nmax = Utils::extractDouble(events, "coef_Nmax");
        coef_tot_a = Utils::extractDouble(events, "coef_tot_a");
        MS_Nc_seuil = Utils::extractDouble(events, "MS_Nc_seuil");
        MS_Nmax_seuil = Utils::extractDouble(events, "MS_Nmax_seuil");
        pente_Nc = Utils::extractDouble(events, "pente_Nc");
        pente_Nmax = Utils::extractDouble(events, "pente_Nmax");
        tNc_Nc = Utils::extractDouble(events, "tNc_Nc");
        tNmax_Nmax = Utils::extractDouble(events, "tNmax_Nmax");

        if ( events.exist("QNa_init") ){
            QNa_init = Utils::extractDouble(events, "QNa_init");
        } else {
            QNa_init = -1;
        }
        if ( events.exist("stressINN_init") ){
            stressINN_init = Utils::extractDouble(events, "stressINN_init");
        } else {
            stressINN_init = 1;
        }
        Vmax = Utils::extractDouble(events, "Vmax");

        // pour la croissance de la plante
        Eimax = Utils::extractDouble(events, "Eimax");
        k = Utils::extractDouble(events, "k");
        Ec = Utils::extractDouble(events, "Ec");
        Ebr  = Utils::extractDouble(events, "Ebr");
        MS_init  = Utils::extractDouble(events, "MS_init");
        densite_semis  = Utils::extractDouble(events, "densite_semis");

        // pour remplissage des grains

        coef_demMS_demN = Utils::extractDouble(events, "coef_demMS_demN");

        PMGmax_var = Utils::extractDouble(events, "PMGmax_var");
        RDTmax_var = Utils::extractDouble(events, "RDTmax_var");
        if (events.exist("alphaCALI")) {
            alphaCALI = Utils::extractDouble(events, "alphaCALI");
        } else {
            alphaCALI = 0;
        }

        // Paramètres liés aux stress azotés
        stressN_Eb1 = Utils::extractDouble(events, "stressN_Eb1");
        stressN_Eb2 = Utils::extractDouble(events, "stressN_Eb2");
        stressN_Eb3 = Utils::extractDouble(events, "stressN_Eb3");
        if (events.exist("alphaLAI")) {
            alphaLAI = Utils::extractDouble(events, "alphaLAI");
        } else {
            alphaLAI = 1.0;
        }
    }
};

} // namespace

#endif
