// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <algorithm>
#include <numeric>
#include <vle/DiscreteTime.hpp>
#include "ParametersPlantCereal.hpp"

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {

using namespace vle::utils;
using namespace vle::discrete_time;

class NitrogenFertilizationCereal : public DiscreteTimeDyn
{
public:
    NitrogenFertilizationCereal(
            const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        pp.initialiser(events);

        STapS.init(this, "STapS", events);
        DoseEngMinNH4.init(this, "DoseEngMinNH4", events);
        DoseEngMinNO3.init(this, "DoseEngMinNO3", events);
        P.init(this, "P", events);
        ETP.init(this, "ETP", events);
        I.init(this, "I", events);
        MS.init(this, "MS", events);

        PminusETP.resize(7, 0.0);

        joursDepuisValorisation.init(this, "joursDepuisValorisation", events);
        CAU.init(this, "CAU", events);
        StockNH4.init(this, "StockNH4", events);
        StockNO3.init(this, "StockNO3", events);
        QNEng.init(this, "QNEng", events);
        pluieACumuler.init(this, "pluieACumuler", events);
        cumulPluie.init(this, "cumulPluie", events);
        cumulPminusETP.init(this, "cumulPminusETP", events);
        nbDays.init(this, "nbDays", events);


        history_size("DoseEngMinNH4", (unsigned int)pp.NjreducNH4+10);
        history_size("MS",8);
        history_size("STapS",8);

    }

    virtual ~NitrogenFertilizationCereal()
    {}

    virtual void compute(const vd::Time& j)
    {
        std::rotate(PminusETP.rbegin(), PminusETP.rbegin() + 1, PminusETP.rend());
        PminusETP[0] = P()+I()-ETP();
        cumulPminusETP = std::accumulate(PminusETP.begin(), PminusETP.begin() + 5, 0.);
        nbDays = nbDays(-1) +1;
        if (nbDays() <= 7) {
            joursDepuisValorisation = 0;
            CAU= -1;
            QNEng = 0;
            StockNH4 = 0;
            StockNO3 = 0;
            pluieACumuler = 0;
            cumulPluie = 0.0;
        } else if((cumulPminusETP() > 0 and (DoseEngMinNH4()>0 or DoseEngMinNO3()>0))
                  or ((cumulPluie(-1)+P()+I() >= pp.PLUIEVALOR)
                     and (StockNH4(-1)>0 or StockNO3(-1)>0 or DoseEngMinNH4()>0
                          or DoseEngMinNO3()>0))) {
            //Coefficient apparent d'utilisation de l'azote mineral apporte
            //et quantite d'azote mobilise provenant de l'engrais (QNEng)

            joursDepuisValorisation = 0;
            if (pp.begin_date + j < pp.date_init_MS+7) {
                CAU = pp.lambda;
            } else {
                CAU=std::min(100.0,pp.lambda+(pp.mu*100.0*((
                    (std::log(MS())-std::log(MS(-7)))*MS()))/
                    (STapS()-STapS(-7))));
            }
            QNEng = (StockNH4(-1) + DoseEngMinNH4()
                      + StockNO3(-1) + DoseEngMinNO3()) * CAU()/100;
                
            StockNH4 = 0.0;
            StockNO3 = 0.0;
            pluieACumuler = 0;
            cumulPluie = 0.0;

        } else {
            joursDepuisValorisation = joursDepuisValorisation(-1)+1;
            CAU   = -1;
            QNEng = 0;

            //calcul StockNH4
            double StockNH4tmp = DoseEngMinNH4();
            for (int ja = 1; ja <= std::min( (int) pp.NjreducNH4,
                    (int)joursDepuisValorisation()); ja++) {
                if (joursDepuisValorisation() > (double) ja) {

                    StockNH4tmp = StockNH4tmp+DoseEngMinNH4(-ja) *
                            std::pow(pp.coefreducNH4, (double) ja);
                }

            }
            StockNH4 = StockNH4tmp;

            if (pp.NjreducNH4 < joursDepuisValorisation()) {
                //calcul StockNO3
                StockNO3 = StockNO3(-1) + DoseEngMinNO3()+
                        DoseEngMinNH4(-pp.NjreducNH4) *
                        std::pow(pp.coefreducNH4, pp.NjreducNH4);
            } else {
                StockNO3 = StockNO3(-1)+ DoseEngMinNO3();
            }
                     
            if (pluieACumuler(-1) == 1) {
                pluieACumuler = 1;
            } else if (DoseEngMinNO3() > 0 or DoseEngMinNH4() > 0) {
                pluieACumuler = 1;
            } else {
                pluieACumuler = 0;
            }
            if (pluieACumuler() == 0) {
                cumulPluie = 0;
            } else {
                cumulPluie = cumulPluie(-1)+P()+I();
            }
        }

    }


private:

    ParametersPlantCereal pp;

    /*Sync*/ Var STapS;
    /*Sync*/ Var DoseEngMinNH4;
    /*Sync*/ Var DoseEngMinNO3;
    /*Sync*/ Var P;
    /*Sync*/ Var ETP;
    /*Sync*/ Var I;
    /*Sync*/ Var MS;

    std::vector<double> PminusETP;
    Var joursDepuisValorisation;
    Var CAU;
    Var StockNH4;
    Var StockNO3;
    Var QNEng;
    Var pluieACumuler;
    Var cumulPluie;
    Var cumulPminusETP;
    Var nbDays;

};

} // namespace

DECLARE_DYNAMICS(AZODYN::NitrogenFertilizationCereal)

