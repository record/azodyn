// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/value/Value.hpp>
#include <vle/devs/Dynamics.hpp>
#include <vle/DiscreteTime.hpp>

#include "ParametersPlantCereal.hpp"

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;


class PhenologyCereal : public DiscreteTimeDyn
{
public:


    PhenologyCereal(
            const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {

        pp.initialiser(events);

        Tmin.init(this, "Tmin", events);
        Tmax.init(this, "Tmax", events);

        Tmoy_base.init(this, "Tmoy_base", events);
        Tmoy.init(this, "Tmoy", events);
        STapinitMS.init(this, "STapinitMS", events);
        STapS.init(this, "STapS", events);
        STapLEVEE.init(this, "STapLEVEE", events);
        STapFLO.init(this, "STapFLO", events);
        STapE1CM.init(this, "STapE1CM", events);
        SJNapFLO.init(this, "SJNapFLO", events);

    }

    virtual ~PhenologyCereal()
    {}

    virtual void compute(const vd::Time& j)
    {
    
        // Calcul de la température moyenne au jour j
        Tmoy= (Tmin()+Tmax())/2;

        // Tempértaure moyenne en prenant en compte un Température de base
        //pour le dévelopement de la plante
        Tmoy_base = Tmoy() - pp.Tbase;

        // Somme de température après semis
        if (pp.begin_date + j < pp.date_S) {
            STapS = 0.0;
        } else  {
            STapS = STapS(-1) + Tmoy_base();
        }

        // Somme de température après initialisation de la matière sèche
        if (pp.begin_date + j < pp.date_init_MS) {
            STapinitMS = 0;
        } else  {
            STapinitMS = STapinitMS(-1) + Tmoy_base();
        }

        // Somme de tempértaure après levee
        if (pp.begin_date + j < pp.date_LEVEE) {
            STapLEVEE = 0.0;
        } else  {
            STapLEVEE = STapLEVEE(-1) + Tmoy_base();
        }

        // Somme de température après stade E1CM
        if (pp.begin_date + j < pp.date_E1CM) {
            STapE1CM = 0.0;
        } else  {
            STapE1CM = STapE1CM(-1) + Tmoy_base();
        } 

        // Somme de température après Floraison
        if (pp.begin_date + j < pp.date_FLO) {
            STapFLO = 0;
        } else  {
            STapFLO = STapFLO(-1) + Tmoy_base();
        }


        // calcul des jours normalises depuis floraison

        if (pp.begin_date + j<=pp.date_FLO) {
            SJNapFLO= 0.0 ;
        } else if (Tmoy() <= pp.TSEUILA) {
            SJNapFLO = SJNapFLO(-1)+pow(pp.Q10A,((pp.TSEUILA-Tmoy())/pp.TSEUILA));
        } else if ((Tmoy() > pp.TSEUILA) && (Tmoy() <= pp.TSEUILB)) {
            SJNapFLO= SJNapFLO(-1)+pow(pp.Q10A,((Tmoy()-pp.TSEUILA)/pp.TSEUILA));
        } else if ((Tmoy() > pp.TSEUILB) && (Tmoy() <= pp.TSEUILC)) {
            SJNapFLO= SJNapFLO(-1)+pow(pp.Q10B,(Tmoy()-pp.TSEUILB)/pp.TSEUILA)*pow(pp.Q10A,pp.Q10C);
        } else if ((Tmoy() > pp.TSEUILC) && (Tmoy() <= pp.TSEUILD)) {
            SJNapFLO= SJNapFLO(-1)+pow(pp.Q10D,(Tmoy()-pp.TSEUILC)/pp.TSEUILA)*pow(pp.Q10B,pp.Q10C)*pow(pp.Q10A,pp.Q10C);
        } else if ((Tmoy() > pp.TSEUILD) && (Tmoy() <= pp.TSEUILE)) {
            SJNapFLO= SJNapFLO(-1)+pow(pp.Q10E,(Tmoy()-pp.TSEUILD)/pp.TSEUILA)*pow(pp.Q10D,pp.Q10C)*pow(pp.Q10B,pp.Q10C)*pow(pp.Q10A,pp.Q10C);
        } else if (Tmoy() > pp.TSEUILE) {
            SJNapFLO= SJNapFLO(-1)+pow(pp.Q10G,(Tmoy()-pp.TSEUILE)/pp.TSEUILA)*pow(pp.Q10E,pp.Q10F)*pow(pp.Q10D,pp.Q10C)*pow(pp.Q10B,pp.Q10C)*pow(pp.Q10A,pp.Q10C);
        }
    }


private:
    ParametersPlantCereal pp;

    /*Sync*/ Var Tmin;
    /*Sync*/ Var Tmax;

    Var Tmoy_base;
    Var Tmoy;
    Var STapS;
    Var STapinitMS;
    Var STapLEVEE;
    Var STapE1CM;
    Var STapFLO;
    Var SJNapFLO;

};

} // namespace

DECLARE_DYNAMICS(AZODYN::PhenologyCereal);

