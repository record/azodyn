// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends
#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp>

#include <ParametersPlantRape.hpp>
#include <phenologie_types.hpp>

#include <math.h>

#define PI 3.14159265

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

namespace AZODYN
{

    class Phenologie : public DiscreteTimeDyn
    {
    public:
        /**
         * @brief Construction de Enracinement
         */
        Phenologie(const vd::DynamicsInit &model, const vd::InitEventList &evts) : DiscreteTimeDyn(model, evts)
        {
            pp.initialiser(evts);

            if (evts.get("begin_date")->isDouble()) {
                begin_time = evts.getDouble("begin_date");
                begin_date = vle::utils::DateTime::toJulianDay(begin_time);
            } else if (evts.get("begin_date")->isString()) {
                begin_date = evts.getString("begin_date");
                begin_time = vle::utils::DateTime::toJulianDay(begin_date);
            }

            // Variables gérées par ce composant
            Stade.init(this, "Stade", evts);
            STsemis.init(this, "STsemis", evts);
            STlevee.init(this, "STlevee", evts);
            STfloraison.init(this, "STfloraison", evts);
            Tmin3.init(this, "Tmin3", evts);
            STflo_seuil.init(this, "STflo_seuil", evts);
            JulianDays.init(this, "JulianDays", evts);
            STjanuary1st.init(this, "STjanuary1st", evts);
            SPjanuary1st.init(this, "SPjanuary1st", evts);
            moySTjanuary1st.init(this, "moySTjanuary1st", evts);
            moySPjanuary1st.init(this, "moySPjanuary1st", evts);
            sunRevolution.init(this, "sunRevolution", evts);
            sunDeclination.init(this, "sunDeclination", evts);
            dayLength.init(this, "dayLength", evts);

            // Variables gérées par un autre composant
            Tmoy.init(this, "Tmoy", evts);
            Tmoy.history_size(6);
            Semis.init(this, "Semis", evts);
            Recolte.init(this, "Recolte", evts);
        }
        /**
         * @brief Destruction de Enracinement
         */
        ~Phenologie()
        {
        }

        /**
         * @brief Calcul des variables de Enracinement
         */
        virtual void compute(const vd::Time &t)
        {
            vle::devs::Time time = begin_time+t;
            // Calcul du stade phénologique
            updateJD(time);
            updateDayLength();
            updateSimF1(time);
            updateStade(time);
            // Variables meteo mises à jour (fction de Stade())
            updateSTlevee();
            updateSTfloraison();
            updateTmin3();
            updateSTsemis();
        }

    private:
        void updateStade(const vd::Time &time)
        {
            switch ((PHENOLOGIE)Stade(-1))
            {
            case SOL_NU:
            {
                bool jourSemis = (Semis() > 0);
                if (jourSemis)
                {
                    Stade = JOUR_SEMIS;
                }
                else
                {
                    Stade = SOL_NU;
                }
                break;
            }
            case JOUR_SEMIS: // 1 seul jour id=1 //renseignee par utilisateur
                Stade = PRE_LEVEE;
                break;
            case PRE_LEVEE:
            {
                if (pp.jourLevee == "")
                {
                    if ((STsemis(-1) >= pp.somme_temperature_levee))
                    {
                        Stade = JOUR_LEVEE;
                    }
                    else
                    {
                        Stade = PRE_LEVEE;
                    }
                }
                else
                {
                    if (pp.jourLevee == vu::DateTime::toJulianDayNumber(time))
                    {
                        Stade = JOUR_LEVEE;
                    }
                    else
                    {
                        Stade = PRE_LEVEE;
                    }
                }
                break;
            }
            case JOUR_LEVEE: // 1 seul jour id=3 //renseignee par utilisateur ou simulee
                Stade = VEGETATIF;
                break;
            case VEGETATIF:
            {
                if (pp.jourFloraison == "")
                {
                    if (STjanuary1st() > STflo_seuil())
                    {
                        Stade = JOUR_F1;
                        jourF1 = (unsigned int)time;
                    }
                    else
                    {
                        Stade = VEGETATIF;
                    }
                }
                else
                {
                    if (pp.jourFloraison == vu::DateTime::toJulianDayNumber(time))
                    {
                        Stade = JOUR_F1;
                        jourF1 = (unsigned int)time;
                    }
                    else
                    {
                        Stade = VEGETATIF;
                    }
                }
                break;
            }
            case JOUR_F1: // 1 seul jour id=5 //renseignee par utilisateur ou simulee
                Stade = FLORAISON;
                break;
            case FLORAISON:
            {
                nbJoursDepuisF1 = (unsigned int)time - jourF1;
                nbJoursDepuisF1 > 5 // 5 jours apres JOUR_F1
                    ? Stade = JOUR_G1
                    : Stade = FLORAISON;
                break;
            }
            case JOUR_G1: // 1 seul jour //simulee
                Stade = G1_G4;
                break;
            case G1_G4:
            {
                STfloraison(-1) >= pp.somme_temperature_G4
                    ? Stade = JOUR_G4
                    : Stade = G1_G4;
                break;
            }
            case JOUR_G4: // 1 seul jour //renseignee par utilisateur ou simulee
                Stade = G4_MI_REMPLISSAGE_GRAIN;
                break;
            case G4_MI_REMPLISSAGE_GRAIN:
            {
                STfloraison(-1) >= pp.somme_temperature_FRG / 2
                    ? Stade = JOUR_MI_REMPLISSAGE_GRAIN
                    : Stade = G4_MI_REMPLISSAGE_GRAIN;
                break;
            }
            case JOUR_MI_REMPLISSAGE_GRAIN: // 1 seul jour //simulee
                Stade = FIN_REMPLISSAGE_GRAIN;
                break;
            case FIN_REMPLISSAGE_GRAIN:
            {
                // si recolte avant maturite -> JOUR_MATURITE  et GRAIN_MUR  non simules
                if (Recolte() > 0)
                {
                    Stade = JOUR_RECOLTE;
                }
                else if (pp.jourMaturite == "")
                {
                    if (STfloraison(-1) >= pp.somme_temperature_FRG)
                    {
                        Stade = JOUR_MATURITE;
                    }
                    else
                    {
                        Stade = FIN_REMPLISSAGE_GRAIN;
                    }
                }
                else
                {
                    if (pp.jourMaturite == vu::DateTime::toJulianDayNumber(time))
                    {
                        Stade = JOUR_MATURITE;
                    }
                    else
                    {
                        Stade = FIN_REMPLISSAGE_GRAIN;
                    }
                }
                break;
            }
            case JOUR_MATURITE: // 1 seul jour //renseignee par utilisateur ou simulee
                Stade = GRAIN_MUR;
                break;
            case GRAIN_MUR:
            {
                bool jourRecolte = (Recolte() > 0);
                if (jourRecolte)
                {
                    Stade = JOUR_RECOLTE;
                }
                else
                {
                    Stade = GRAIN_MUR;
                }
                break;
            }
            case JOUR_RECOLTE: // 1 seul jour //renseignee par utilisateur
                Stade = SOL_NU;
                break;
            }
        }

        void updateJD(const vd::Time &time)

        {
            // Calcul des jours Julien
            JulianDays = vu::DateTime::dayOfYear(time);
        }

        // Calcul de la duree du jour (photoperiode) par le modele CMB (Forsythe et al. 1995)

        void updateDayLength()
        {
            // Calcul de l'angle de revolution du soleil (en gradians)

            sunRevolution = 0.2163108 + 2.0 * atan(0.9671396 * tan(0.00860) * (JulianDays() - 186.0));

            // Calcul de l'angle de déclinaison du soleil (en gradians)

            sunDeclination = asin(0.39795 * cos(sunRevolution()));

            // Calcul de la duree du jour (heures/jours)

            double p = 0.833;

            dayLength = 24 -
                        (24 / PI) *
                            acos((sin(p * PI / 180) + sin(pp.LAT * PI / 180) * sin(sunDeclination())) /
                                 (cos(pp.LAT * PI / 180) * cos(sunDeclination())));
        }

        // Modele date F1 (somme des temperatures depuis 1er janvier pour declencher deb Flo)

        void updateSimF1(const vd::Time &time)
        {

            if ((vu::DateTime::year(time) == vu::DateTime::year(begin_time)) && (vu::DateTime::dayOfYear(time) <= vu::DateTime::aYear(time)))
            {
                STjanuary1st = 0.0;
                SPjanuary1st = 0.0;
                moySTjanuary1st = 0.0;
                moySPjanuary1st = 0.0;
                STflo_seuil = 0.0;
            }
            else
            {
                STjanuary1st = STjanuary1st(-1) + std::max(0.0, Tmoy());
                SPjanuary1st = SPjanuary1st(-1) + dayLength();
                moySTjanuary1st = STjanuary1st() / JulianDays();
                moySPjanuary1st = SPjanuary1st() / JulianDays();

                STflo_seuil = pp.AF1 + pp.BTF1 * pp.moyTjanuary1st + pp.BPF1 * pp.moyPjanuary1st + pp.BLF1 * pp.LAT;
            }
        }

        void updateSTsemis()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
                STsemis = 0;
                break;
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                STsemis = STsemis(-1) + std::max(0.0, Tmoy());
                break;
            }
        }

        void updateSTlevee()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
                STlevee = 0;
                break;
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                STlevee = STlevee(-1) + std::max(0.0, Tmoy());
                break;
            }
        }

        void updateSTfloraison()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
                STfloraison = 0;
                break;
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                STfloraison = STfloraison(-1) + std::max(0.0, Tmoy());
                break;
            }
        }

        void updateTmin3()
        {
            switch ((PHENOLOGIE)Stade())
            {
            case SOL_NU:
            case JOUR_SEMIS:
                Tmin3 = 0;
                break;
            case PRE_LEVEE:
            case JOUR_LEVEE:
            case VEGETATIF:
            case JOUR_F1:
            case FLORAISON:
            case JOUR_G1:
            case G1_G4:
            case JOUR_G4:
            case G4_MI_REMPLISSAGE_GRAIN:
            case JOUR_MI_REMPLISSAGE_GRAIN:
            case FIN_REMPLISSAGE_GRAIN:
            case JOUR_MATURITE:
            case GRAIN_MUR:
            case JOUR_RECOLTE:
                Tmin3 = std::min(Tmoy(-2), std::min(Tmoy(-1), Tmoy()));
                break;
            }
        }

        ParametersPlantRape pp;

        std::string  begin_date;
        double begin_time;

        // Variables gérées par ce composant
        Var Stade;       /** Stade phénologique cf enum_types.hpp */
        Var STsemis;     /** Som temp depuis SEMIS, base 0 */
        Var STlevee;     /** Som temp depuis LEVEE, base 0 */
        Var STfloraison; /** Som temp depuis FLORAISON, base 4.5 */
        Var Tmin3;       /** le minimal Tmoy sur les 3 derniers jours à partir du semis */
        Var JulianDays;
        Var STjanuary1st;
        Var SPjanuary1st;
        Var moySTjanuary1st;
        Var moySPjanuary1st;
        Var STflo_seuil;
        Var sunRevolution;
        Var sunDeclination;
        Var dayLength;

        // Variables gérées par un autre composant
        Var Tmoy;
        Var Semis;
        Var Recolte;

        // divers
        unsigned int jourF1;
        unsigned int nbJoursDepuisF1;
    };

} // namespace

DECLARE_DYNAMICS(AZODYN::Phenologie)
