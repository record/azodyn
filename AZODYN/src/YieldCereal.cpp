// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends


#include <vle/DiscreteTime.hpp>

#include "ParametersPlantCereal.hpp"

#include <iostream>

#include <cmath>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;

class YieldCereal : public DiscreteTimeDyn
{
public:
    YieldCereal(const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {

        pp.initialiser(events);

        Eipot.init(this, "Eipot", events);
        RG.init(this, "RG", events);
        stressN_NGM2.init(this, "stressN_NGM2", events);
        INN.init(this, "INN", events);
        
        RR.init(this, "RR", events);
        ETPP.init(this, "ETPP", events);
        TX.init(this, "TX", events);
        
        Tmoy.init(this, "Tmoy", events);
        QNabs.init(this, "QNabs", events);
        QN_tot.init(this, "QN_tot", events);
        SJNapFLO.init(this, "SJNapFLO", events);
        MS.init(this, "MS", events);
        MS_FLO.init(this, "MS_FLO", events);
        
        SRG_E1CM_FLO.init(this, "SRG_E1CM_FLO", events);
        NGM2pot.init(this, "NGM2pot", events);
        NGM2.init(this, "NGM2", events);
        MSG_FLO.init(this, "MSG_FLO", events);
        QNG_FLO.init(this, "QNG_FLO", events);
        QNa_FLO.init(this, "QNa_FLO", events);
        WSC_FLO.init(this, "WSC_FLO", events);
        P1Gpot.init(this, "P1Gpot", events);
        demandeMS1G.init(this, "demandeMS1G", events);
        demandeMSG.init(this, "demandeMSG", events);
        pNremob.init(this, "pNremob", events);
        QNremob.init(this, "QNremob", events);
        StockNremob.init(this, "StockNremob", events);
        dQNG.init(this, "dQNG", events);
        QNG.init(this, "QNG", events);
        MSG_offre.init(this, "MSG_offre", events);
        MSG.init(this, "MSG", events);
        PMG.init(this, "PMG", events);
        P_FLO_REC.init(this, "P_FLO_REC", events);
        RG_FLO_REC.init(this, "RG_FLO_REC", events);
        Nbj_FLO_REC.init(this, "Nbj_FLO_REC", events);
        T29.init(this, "T29", events);
        CALI.init(this, "CALI", events);
        dMSG.init(this, "dMSG", events);
        jMatur.init(this, "jMatur", events);
        QNveg.init(this, "QNveg", events);
        RDT.init(this, "RDT", events);
        RDT_CALI.init(this, "RDT_CALI", events);
        TP.init(this, "TP", events);
        QNa_postFLO.init(this, "QNa_postFLO", events);

    }

    virtual ~YieldCereal()
    {}

    virtual void compute(const vd::Time& j)
    {
        //---------------- Calcul des variables a floraison ---------------------

         //TODO: Externaliser eq. SRG_E1CM et NGM2pot et NGM2 dans un module specifique barley et wheat 
        //Somme des rayonements cumules depuis epi 1cm jusqu a Floraison

        if(pp.begin_date + j <pp.date_E1CM){
            SRG_E1CM_FLO=0.0;
        } else if (pp.begin_date + j <pp.date_FLO) {
            SRG_E1CM_FLO= SRG_E1CM_FLO(-1)+ Eipot(-1)*pp.Ec*RG(-1)/100.0;
        } else {
            SRG_E1CM_FLO=SRG_E1CM_FLO(-1);
        }

        //Nombre de grains /m2 potentiel

        if(pp.begin_date + j <pp.date_FLO){
            NGM2pot=0.0;
            NGM2= NGM2pot();
        } else {
            if (pp.crop== "wheat") {
                // equation AZODYN ble
                NGM2pot= pp.alphaNG_soissons*pp.alphaNG
                        *(pp.PA3NG+pp.PA1NG*(SRG_E1CM_FLO()/pp.Ec));
               //Nombre de grains /m2
              NGM2= NGM2pot()*stressN_NGM2(-1);
            } else {
                // essai pour publi

              //NGM2pot=pp.alphaNG*(10292+7.65*MS_FLO());
              NGM2pot=pp.alphaNG*(pp.PA3NG+pp.PA1NG*MS_FLO());
               //Nombre de grains /m2
              NGM2= NGM2pot();
            }
        }


        //Matiere seche des grains a floraison (g.m2)
        
        if (pp.begin_date + j <= pp.date_FLO) {
            MSG_FLO= NGM2()*pp.P1G_FLO ;
        } else {
            MSG_FLO= MSG_FLO(-1);
        }

        //Quantite d'azote dans les grains a floraison

        if (pp.begin_date + j <= pp.date_FLO) {
            QNG_FLO= MSG_FLO()*10*pp.tNG_FLO/100;
        } else {
            QNG_FLO=QNG_FLO(-1);
        }

        //Quantite d'azote dans les parties aeriennes a floraison

        if(pp.begin_date + j <pp.date_FLO){
            QNa_FLO=0.0;
        } else if (pp.begin_date +j ==pp.date_FLO) {
            QNa_FLO= QN_tot()/pp.coef_tot_a-QNG_FLO();
            //TODO commun à plusieurs cultures et modules
        } else {
            QNa_FLO=QNa_FLO(-1);
        }

        // Quantite de sucres solubles a Floraison
        if (pp.begin_date + j <pp.date_FLO) {
            WSC_FLO = MS_FLO()*pp.WSCINN2/100;
        } else if (pp.begin_date + j == pp.date_FLO) {
            WSC_FLO = MS_FLO()*(pp.WSCINN2+INN(-1)*pp.WSCINN1)/100;
        } else {
            WSC_FLO = WSC_FLO(-1);
        }

        // ------ Calcul des potentiels varietaux--------------------------------

        //poids maximum d'un grain en fonction du nombre de grains etablis (g.m2)

        if (pp.begin_date + j < pp.date_FLO) {
            P1Gpot = 0.0;
        } else {
            P1Gpot = std::min(pp.PMGmax_var/1000,
             10*pp.RDTmax_var/NGM2());
        }

        // Matiere seche potentielle pour 1 grain (g.m2)

        if (pp.begin_date + j < pp.date_FLO) {
            demandeMS1G = 0.0;
        } else if (SJNapFLO()<= pp.SJNmax_FLO_RG){
            demandeMS1G= (P1Gpot())/
                   (1+(std::pow(
                   ((P1Gpot()*1000-pp.P1G_FLO*1000)/(pp.P1G_FLO*1000)),
                            (((pp.SJNmax_FLO_RG/2)-SJNapFLO())
                            /(pp.SJNmax_FLO_RG/2))
                            )
                            ));
        } else {
            demandeMS1G=demandeMS1G(-1);
        }

        // Matiere seche potentielle pour tous grains (g.m2)

        demandeMSG = demandeMS1G()*NGM2();


        // -------- Calcul de l'azote accumule dans les grains -----------------

        // proportion de l'azote remobilise le jour j = QN remobilise/QNremobilsable

        pNremob = SJNapFLO()* pp.REM1;

        // Quantite totale d'azote remobilisable le jour j (g.m2)

        if (pp.begin_date + j <= pp.date_FLO) {
            StockNremob = QNa_FLO() * pp.REM2;
        } else if (SJNapFLO() > pp.SJNmax_FLO_RG){
            StockNremob = 0.0;
        } else {
            StockNremob=std::max(0.0, QNveg(-1)-(QNa_FLO()*(1-pp.REM2)));
        }

        // Quantite d azote reellement remobilisee le jour j

        QNremob = StockNremob()*pNremob();


        // Quantite d'azote allouee au grains chaque jour (kg.ha)

        if (pp.begin_date + j < pp.date_FLO) {
            dQNG = 0.0;
        } else if (SJNapFLO() < pp.SJNmax_FLO_RG/2) {
            dQNG=std::max(0.001,//TODO pour assurer > 0 et continuer le remplissage des grains
                    std::min(pp.coefAA_MS*(demandeMSG()-demandeMSG(-1))*10,
                    QNremob()+QNabs()/pp.coef_tot_a));
        } else if (SJNapFLO() < pp.SJNmax_FLO_RG){
            dQNG= QNremob()+QNabs()/pp.coef_tot_a;
        } else {
            dQNG= 0.0;
        }

        // Quantite d'azote cumulee allouee aux grains (kg.ha)

        if (pp.begin_date + j < pp.date_FLO) {
            QNG = 0.0;
        } else if (pp.begin_date + j == pp.date_FLO){
            QNG= QNG_FLO();
        } else {
            QNG= QNG(-1)+dQNG(-1);
        }

        // Calcul de l'offre de matiere seche accumulee dans les grains (g.m2)

        if (pp.begin_date + j < pp.date_FLO) {
            MSG_offre = 0.0;
        } else if (dQNG()>0 and SJNapFLO()<  pp.SJNmax_FLO_RG){
            MSG_offre= (QNG()/10*pp.coefAA_MS)+(MS()-MS_FLO())+ (pp.WSC*WSC_FLO());
        } else {
            MSG_offre = 0.0;
        }
        
        // Calcul de la matiere seche accumulee dans les grains (g.m2)

        if (pp.begin_date + j < pp.date_FLO) {
            MSG = 0.0;
        } else if (pp.begin_date + j == pp.date_FLO){
            MSG= MSG_FLO();
        } else if (dQNG()>0 and SJNapFLO()<  pp.SJNmax_FLO_RG){
            MSG= MSG_FLO() + std::min(demandeMSG(),MSG_offre());
        } else {
            MSG = MSG(-1);
        }

        
        // Poids de mille grains
        
        if (pp.begin_date + j < pp.date_FLO) {
        PMG= 0.0 ;
        } else {
        PMG = MSG()*1000/NGM2();
        }
        
        
        // variables climatiques pour la calibrage
        
         // Nb de jour FLo REC
        if (pp.begin_date + j < pp.date_FLO) {
        Nbj_FLO_REC = 0.0 ;
        } else if (jMatur(-1) == 0) {
        Nbj_FLO_REC = Nbj_FLO_REC(-1)+ 1;
        }
        else {
        Nbj_FLO_REC = Nbj_FLO_REC(-1) ;
        }
        
        
        // somme P moins ETP
        if (pp.begin_date + j < pp.date_FLO) {
        P_FLO_REC = 0.0 ;
        } else if (jMatur(-1) == 0) {
        P_FLO_REC = P_FLO_REC(-1)+ (RR()-ETPP());
        }
        else {
        P_FLO_REC = P_FLO_REC(-1);
        }
        
        
               // somme RG
        if (pp.begin_date + j < pp.date_FLO) {
        RG_FLO_REC = 0.0 ;
        } else if (jMatur(-1) == 0) {
        RG_FLO_REC = RG_FLO_REC(-1)+ RG(-1);
        }
        else {
        RG_FLO_REC = RG_FLO_REC(-1);
        }
        
                     // somme T29
        if (pp.begin_date + j < pp.date_FLO) {
        T29 = 0.0 ;
        } else if (jMatur(-1) == 0) {
        T29 = T29(-1)+ (TX()>29)*TX();
        }
        else {
        T29 = T29(-1);
        }
        
        
        
        //Matiere seche accumulee dans le grains le jour j

        dMSG=MSG()-MSG(-1);


        //Azote contenu dans les parties vegetatives kg.ha-1

        if (pp.begin_date + j < pp.date_FLO) {
            QNveg = QN_tot()/pp.coef_tot_a;
        } else {
            QNveg= QN_tot()/pp.coef_tot_a - QNG();
        }

        //calcul Rendement (q/ha)
        RDT = std::max(RDT(-1), MSG()/10.0);

        //calcul de la teneur en proteines des grains (%)
        if (RDT() == 0.0) {
            TP = 0;
        } else {
            TP = (QNG())/(RDT())*pp.coefAA_MS;
        }

        if (jMatur(-1) == 0) {
            QNa_postFLO = 0;
        } else {
            QNa_postFLO = QN_tot()/pp.coef_tot_a - QNa_FLO();
        }

        // Calibrage des grains 
        

        
          if (RDT() == 0.0) {
            CALI = 0;
            RDT_CALI = 0;
        } else {

//          CALI=100*std::sin(
//         (-0.425+0.00000506*NGM2()+0.000139*P_FLO_REC()-0.000509*T29()+0.00000442*RG_FLO_REC()//+pp.alphaCALI*PMG())* 
//         (-0.425+0.00000506*NGM2()+0.000139*P_FLO_REC()-0.000509*T29()+0.00000442*RG_FLO_REC()+pp.alphaCALI*PMG()));

          CALI=100*std::sin(
         (pp.alphaCALI-0.434+0.00000463*NGM2()-0.000601*T29()+0.00000467*RG_FLO_REC()+0.0276*PMG())* 
         (pp.alphaCALI-0.434+0.00000463*NGM2()-0.000601*T29()+0.00000467*RG_FLO_REC()+0.0276*PMG()));
        
        RDT_CALI= RDT()*CALI()         
           
        ;
        
        }

        //jour de maturité simule

        if (pp.begin_date + j < pp.date_FLO) {
            jMatur = 0.0;
        } else {
            if (dMSG()>0.0){
                jMatur = 0.0;
            } else {
                jMatur = 1.0;
            }
        }

    }




private:

    ParametersPlantCereal pp;

    /*Nosync*/ Var Eipot;
    /*Nosync*/ Var RG;
    /*Nosync*/ Var stressN_NGM2;
    /*Nosync*/ Var INN;

    /*Sync*/ Var RR;
    /*Sync*/ Var ETPP;
    /*Sync*/ Var Tmoy;
    /*Sync*/ Var QNabs;
    /*Sync*/ Var QN_tot;
    /*Sync*/ Var SJNapFLO;
    /*Sync*/ Var MS;
    /*Sync*/ Var MS_FLO;
    /*Sync*/ Var TX;

    Var SRG_E1CM_FLO;
    Var NGM2pot;
    Var NGM2;
    Var MSG_FLO;
    Var QNG_FLO;
    Var QNa_FLO;
    Var WSC_FLO;
    Var P1Gpot;
    Var demandeMS1G;
    Var demandeMSG;
    Var pNremob;
    Var QNremob;
    Var StockNremob;
    Var dQNG;
    Var QNG;
    Var MSG_offre;
    Var MSG;
    Var PMG;
    Var P_FLO_REC;
    Var RG_FLO_REC;
    Var Nbj_FLO_REC;
    Var T29;
    Var CALI;
    Var dMSG;
    Var jMatur;
    Var QNveg;
    Var RDT;
    Var RDT_CALI;
    Var TP;
    Var QNa_postFLO;
};

} // namespace

DECLARE_DYNAMICS(AZODYN::YieldCereal)

