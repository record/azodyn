/**
 *
 * Copyright (C) 2015-2015 INRA.
 *
 * TODO licence
 */

#ifndef AZODYN_PARAMETERS_SOIL_RAPE_HPP
#define AZODYN_PARAMETERS_SOIL_RAPE_HPP

#include <vle/utils/Exception.hpp>

#include "AzodynUtils.hpp"

namespace AZODYN
{

    using namespace vle::utils;

    struct ParametersSoilRape
    {

        //------------------------------------------------------------------------------//
        // Paramètres liés au module NitrogenSOIL

        //---- Parametres par default dans le modele ----//

        // temperature de reference pour la mineralisation de l'humus
        double Tref;

        // parametre pour effet temperature sur mineralisation dans equation du k2
        double alpha;

        // parametre pour calcul mineralisation de l'humus (tient compte de la
        // mineralisation sous la couche labouree)
        double beta;

        // parametre pour calcul mineralisation de l'humus (coef argile dans
        // equation k2)
        double gamma;

        // parametre pour calcul mineralisation de l'humus (coef CaCO3 dans
        // equation k2
        double delta;

        // parametre pour calcul mineralisation de l'humus dans l'equation du k2
        double coef_k2;

        // parametre pour effet de la temperature sur la mineralisation
        double a;

        // parametre pour effet de la temperature sur la mineralisation
        double b;

        // parametre pour effet de la temperature sur la mineralisation
        double c;

        // parametre pour effet de la temperature sur la mineralisation
        double d;

        // parametre pour le calcul du taux de décomposition des résidus
        double para_kres1;

        // parametre pour le calcul du taux de décomposition de la biomasse microbienne
        double para_kreshum;

        // parametre pour le calcul du taux d'humification de la biomasse
        double f;

        // parametre pour le calcul du taux d'humification de la biomasse
        double g;

        // parametre rendement d'asimilation des résidus de carbonne par la biomasse microbienne
        double Y;

        // parametre pour le calcul de la biomasse microbienne
        double paraC_Nres1;

        // parametre pour le calcul de la biomasse microbienne
        double paraC_Nres2;

        // parametre pour le calcul de la biomasse microbienne
        double paraC_Nres3;

        // parametre pour le calcul de la biomasse microbienne
        double paraC_Nres4;

        // ratio C/N de l humus nouvellement formé
        double C_Nhum;

        //---- Parametres renseignes par l'utilisateur ----//

        // impact du système de culture sur la mineralisation de l'humus
        //(sans unite)
        double fsdc;

        // teneur en azote organique dans le sol (couche de mineralisation) pour le
        // calcul de la minéralisation de l'humus
        //(kg.ha^-1)
        double tNorg;

        // densite apparente de la couche de mineralisation du sol
        //(sans unite)
        double da_C0;

        // epaisseur de la couche labouree
        //(mm)
        double ep_C0;

        // teneur en argile de la couche labouree du sol
        //(0/00)
        double Arg;

        // teneur en calcaire de la couche labouree du sol
        //(0/00)
        double CaCO3;

        // reliquat d'azote min dans le sol à l'initialisation dans la couche
        // labouree (30cm) (kg.ha^-1)
        double QNsol_init_C0;

        // reliquat d'azote mineral dans le sol a l'initialisation sur l'ensemble de
        // la couche prelevee (kg.ha^-1)
        double QNsol_init_tot;

        // Profondeur sur laqualle a ete mesuree le reliquat total (mm)
        double Prof_Reliquat_tot;

        // Azote non disponible pour la culture par couche de 10 cm de sol
        //(kg.ha^-1)
        double QNND;

        // Ratio C/N des résidus de la culture precedente
        double C_Nres;

        // Quantite de carbone decomposable issu des residus
        double C_res;

        //------------------------------------------------------------------------------//
        // Paramètres liés au module WaterSoil

        // Seuil pour considerer que la pluie et l'irrigation ont un effet
        // sur l'Evaporation du sol
        double ApportEau_seuil;

        // Pour calcul de l'effet des jours sans precipitation sur levaporation;
        double EVAPO;

        // parametre pour calcul de la transpiration
        double Trans;

        // parametre pour calcul de la transpiration et stress hydrique
        double stressH1;

        // parametre pour calcul de la transpiration et stress hydrique
        double stressH2;

        // parametre pour calcul de la transpiration et stress hydrique
        double stressH3;

        // parametre pour calcul de la transpiration et stress hydrique
        double stressH4;

        // Seuil de mottes delta au dessus duquel la FTSW est affectee  (%)
        double Mottes_seuil;

        // Pourcentage de reduction de la FTSW en conditions de sol tasse
        //(= plus de Mottes_seuil % de Mottes delta)
        double coef_Mottes;

        // Nombre de jour seuil en dessous duquel le nombre de jour sans apport d'eau
        // n'a pas d'impact sur l'evaporation
        double JSA_seuil;

        //---------------------------------------------------------------------------------//
        // Paramètres liés à Fertilisation

        // parametre pour calcul du coefficient apparent d'utilisation de l'azote
        //(ord à l'origine)
        double lambda;

        // parametre pour calcul du coefficient apparent d'utilisation de l'azote
        //(pente)
        double mu;

        double PLUIEVALOR;

        //---- Parametres renseignes par l'utilisateur ----//

        // Reserve utile a la date d'initialisation du module eau
        //(mm)

        double RU_init;

        // Reserve utile maximale, prend en compte les potentielles remontees capilaires
        // d'eau en fond de profil de sol
        //(mm)
        double RUmax;

        // pourcentage de mottes delta ( = tassement du sol)
        //(%)
        double MottesDelta;

        //------------------------------------------------------------------------------//
        // Paramètres liés au module CohorteFeuilles

        //---- Parametres par default dans le modele ----//

        // CAU des feuilles mortes
        double CAUfm;

        // constante de décomposition
        double km;

        // C/N optimal pour la minéralisation des feuilles mortes
        double CN_opt;

        double ACfm;

        //------------------------------------------------------------------------------//
        // Paramètres liés au module NitrogenSoil

        void initialiser(const vle::devs::InitEventList &events)
        {
                Tref = Utils::extractDouble(events, "Tref");
                alpha = Utils::extractDouble(events, "alpha");
                beta = Utils::extractDouble(events, "beta");
                gamma = Utils::extractDouble(events, "gamma");
                delta = Utils::extractDouble(events, "delta");
                coef_k2 = Utils::extractDouble(events, "coef_k2");
                a = Utils::extractDouble(events, "a");
                b = Utils::extractDouble(events, "b");
                c = Utils::extractDouble(events, "c");
                d = Utils::extractDouble(events, "d");
                para_kres1 = Utils::extractDouble(events, "para_kres1");
                para_kreshum = Utils::extractDouble(events, "para_kreshum");
                f = Utils::extractDouble(events, "f");
                g = Utils::extractDouble(events, "g");
                Y = Utils::extractDouble(events, "Y");
                paraC_Nres1 = Utils::extractDouble(events, "paraC_Nres1");
                paraC_Nres2 = Utils::extractDouble(events, "paraC_Nres2");
                paraC_Nres3 = Utils::extractDouble(events, "paraC_Nres3");
                paraC_Nres4 = Utils::extractDouble(events, "paraC_Nres4");
                C_Nhum = Utils::extractDouble(events, "C_Nhum");
                fsdc = Utils::extractDouble(events, "fsdc");
                tNorg = Utils::extractDouble(events, "tNorg");
                da_C0 = Utils::extractDouble(events, "da_C0");
                ep_C0 = Utils::extractDouble(events, "ep_C0");
                Arg = Utils::extractDouble(events, "Arg");
                CaCO3 = Utils::extractDouble(events, "CaCO3");
                QNsol_init_C0 = Utils::extractDouble(events, "QNsol_init_C0");
                QNsol_init_tot = Utils::extractDouble(events, "QNsol_init_tot");
                Prof_Reliquat_tot = Utils::extractDouble(events, "Prof_Reliquat_tot");
                QNND = Utils::extractDouble(events, "QNND");
                C_Nres = Utils::extractDouble(events, "C_Nres");
                C_res = Utils::extractDouble(events, "C_res");

            //------------------------------------------------------------------------------//
            // Paramètres liés au module WaterSoil

                ApportEau_seuil = Utils::extractDouble(events, "ApportEau_seuil");
                EVAPO = Utils::extractDouble(events, "EVAPO");
                Trans = Utils::extractDouble(events, "Trans");
                stressH1 = Utils::extractDouble(events, "stressH1");
                stressH2 = Utils::extractDouble(events, "stressH2");
                stressH3 = Utils::extractDouble(events, "stressH3");
                stressH4 = Utils::extractDouble(events, "stressH4");
                Mottes_seuil = Utils::extractDouble(events, "Mottes_seuil");
                coef_Mottes = Utils::extractDouble(events, "coef_Mottes");
                JSA_seuil = Utils::extractDouble(events, "JSA_seuil");
                RU_init = Utils::extractDouble(events, "RU_init");
                RUmax = Utils::extractDouble(events, "RUmax");
                MottesDelta = Utils::extractDouble(events, "MottesDelta");

            // Paramètres liés Fertilisation

                lambda = Utils::extractDouble(events, "lambda");
                mu = Utils::extractDouble(events, "mu");
                PLUIEVALOR = Utils::extractDouble(events, "PLUIEVALOR");

            //------------------------------------------------------------------------------//
            // Paramètres liés au module CohorteFeuilles

                CAUfm = Utils::extractDouble(events, "CAUfm");
                km = Utils::extractDouble(events, "km");
                CN_opt = Utils::extractDouble(events, "CN_opt");
                ACfm = Utils::extractDouble(events, "ACfm");
        }
    };

} // namespace

#endif
