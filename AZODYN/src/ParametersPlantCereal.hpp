/**
 *
 * Copyright (C) 2015-2015 INRA.
 *
 * TODO licence
 */

#ifndef AZODYN_PARAMETERS_PLANT_CEREAL_HPP
#define AZODYN_PARAMETERS_PLANT_CEREAL_HPP

#include "ParametersPlant.hpp"

namespace AZODYN {

using namespace vle::utils;

class ParametersPlantCereal : public ParametersPlant
{
public:
    //---------------------------------------------------------------------------------//
    // Paramètres liés à la phenologie

    //date de la levee
    double date_LEVEE;
    //date epi 1 cm
    double date_E1CM;
    // Duree Epi 1cm  - epiason
    double ST_E1CM_EP;

    //-----------------------------------------------
    //Paramètres liés à TODO

    //Profondeur max de la racine (mm)
    double ProfRacmax ;

    // Vitesse de croissances racinaire (mm par degre jour)
    double vRac_ST;

    // Facteur de reduction de la senescence (pour ideotype genotypiques)
    double AlphaSenesc;

    //Paramètre de la relation LAI/QN en sénéscence
    double SENESC1;

    //Paramètre de la relation LAI/QN en sénéscence
    double SENESC2;

    double PLUIEVALOR;


    //Durée pendant laquelle la courbe critique est encore valable
    double LAG;

    //-----------------------------------------------
    //Paramètres liés à la fertilisation azoté


    //coefficient de réduction (=volatilisation) de l'amonium 
    //(pente)
    double coefreducNH4;

    //parametre pour calcul du coefficient apparent d'utilisation de l'azote
    //(ord à l'origine)
    double lambda;

    //parametre pour calcul du coefficient apparent d'utilisation de l'azote
    //(pente)
    double mu;

    //Nombre de jour pendant lesquels on a une volatilisation de l'engrais sous
    // forme d'amonium si il n'est pas valorisé par la plante
    //(pente)
    double NjreducNH4;

    //TODO doc
    double alphaNG;
    double alphaNG_soissons;

    //Coefficient de passage du %azote au % de protéines
    double coefAA_MS;


    //---------------------------------------------------------------------------------//
    // Paramètres liés aux stress azotés

    //Réduction du LAI en fonction de l’INN
    double stressN_LAI1;

    //Réduction du LAI en fonction de l’INN
    double stressN_LAI2;

    //Réduction du LAI en fonction de l’INN
    double stressN_LAI3;

    //Réduction du nombre de grains en fonction de l’INN_FLO
    double stressN_NGM2_FLO1;

    //Réduction du nombre de grains en fonction de l’INN_FLO
    double stressN_NGM2_FLO2;

    // Reduction du nombre de grains pour ideotypage
    double alphaNG2;

    //---------------------------------------------------------------------------------//
    // pour remplissage des grains

    //Poids d’un grain à floraison
    double P1G_FLO;
    //Paramètre Q10 de l’effet de la température sur la croissance des grains
    double Q10A;

    //Paramètre Q10 de l’effet de la température sur la croissance des grains
    double Q10B;

    //Paramètre Q10 de l’effet de la température sur la croissance des grains
    double Q10C;

    //Paramètre Q10 de l’effet de la température sur la croissance des grains
    double Q10D;

    //Paramètre Q10 de l’effet de la température sur la croissance des grains
    double Q10E;

    //Paramètre Q10 de l’effet de la température sur la croissance des grains
    double Q10F;

    //Paramètre Q10 de l’effet de la température sur la croissance des grains
    double Q10G;

    //Cinétique de remobilisation de ‘azote par jour normalisé
    double REM1;

    //pourcentage de l’azote floraison remobilisé à récolte
    double REM2;

    //Durée maximum de remplissage des grains en jours normalisés (somme jours normalisés : SJN) depuis floraison
    double SJNmax_FLO_RG;

    //Teneur en azote des grains à floraison
    double tNG_FLO;

    // Seuil de température sur la croissance des grains
    double TSEUILA;

    // Seuil de température sur la croissance des grains
    double TSEUILB;

    //Seuil de température sur la croissance des grains
    double TSEUILC;

    //Seuil de température sur la croissance des grains
    double TSEUILD;

    //Seuil de température sur la croissance des grains
    double TSEUILE;

    //Coefficient de relation entre quantité de sucres solubles et INN à floraison
    double WSCINN1;

    //Coefficient de relation entre quantité de sucres solubles et INN à floraison
    double WSCINN2;

    //Proportion de sucres solubles remobilisée
    double WSC;


    //Params temp TODO to improve
    double PA3NG;
    double PA1NG;

    void initialiser( const vle::devs::InitEventList& events ){
        ParametersPlant::initialiser(events);
        date_LEVEE = Utils::extractDate(events, "date_LEVEE");
        date_E1CM = Utils::extractDate(events, "date_E1CM");
        ST_E1CM_EP = Utils::extractDouble(events, "ST_E1CM_EP");
        //verifications
        if (date_init_N > date_init_MS) {
            throw ArgError("date_init_N > date_init_MS");
        }
        if (date_init_MS > date_E1CM) {
            //throw ArgError("date_init_MS > date_E1CM");
        }
        if (date_E1CM > date_FLO) {
            throw ArgError("date_E1CM > date_FLO");
        }

        // pour la croissance de la plante
        ProfRacmax  = Utils::extractDouble(events, "ProfRacmax");
        vRac_ST  = Utils::extractDouble(events, "vRac_ST");
        if ( events.exist("AlphaSenesc") ){
           AlphaSenesc  = Utils::extractDouble(events, "AlphaSenesc");
        } else {
           AlphaSenesc = 1 ;
        }
        SENESC1  = Utils::extractDouble(events, "SENESC1");
        SENESC2  = Utils::extractDouble(events, "SENESC2");
        PLUIEVALOR  = Utils::extractDouble(events, "PLUIEVALOR");
        LAG  = Utils::extractDouble(events, "LAG");

        //Paramètres liés à la fertilisation azoté
        coefreducNH4 = Utils::extractDouble(events, "coefreducNH4");
        lambda = Utils::extractDouble(events, "lambda");
        mu = Utils::extractDouble(events, "mu");
        NjreducNH4 = Utils::extractDouble(events, "NjreducNH4");
        alphaNG = Utils::extractDouble(events, "alphaNG");

        alphaNG_soissons = Utils::extractDouble(events, "alphaNG_soissons");
        coefAA_MS = Utils::extractDouble(events, "coefAA_MS");

        //stress azote
        stressN_LAI1 = Utils::extractDouble(events, "stressN_LAI1");
        stressN_LAI2 = Utils::extractDouble(events, "stressN_LAI2");
        stressN_LAI3 = Utils::extractDouble(events, "stressN_LAI3");
        stressN_NGM2_FLO1 = Utils::extractDouble(events, "stressN_NGM2_FLO1");
        stressN_NGM2_FLO2 = Utils::extractDouble(events, "stressN_NGM2_FLO2");
        if (events.exist("alphaNG2")) {
            alphaNG2 = Utils::extractDouble(events, "alphaNG2");
        } else {
            alphaNG2 = 1.0;
        }

        // remplissage des grains
        P1G_FLO = Utils::extractDouble(events, "P1G_FLO");
        Q10A = Utils::extractDouble(events, "Q10A");
        Q10B = Utils::extractDouble(events, "Q10B");
        Q10C = Utils::extractDouble(events, "Q10C");
        Q10D = Utils::extractDouble(events, "Q10D");
        Q10E = Utils::extractDouble(events, "Q10E");
        Q10F = Utils::extractDouble(events, "Q10F");
        Q10G = Utils::extractDouble(events, "Q10G");
        REM1 = Utils::extractDouble(events, "REM1");
        REM2 = Utils::extractDouble(events, "REM2");
        SJNmax_FLO_RG = Utils::extractDouble(events, "SJNmax_FLO_RG");
        tNG_FLO = Utils::extractDouble(events, "tNG_FLO");
        TSEUILA = Utils::extractDouble(events, "TSEUILA");
        TSEUILB = Utils::extractDouble(events, "TSEUILB");
        TSEUILC = Utils::extractDouble(events, "TSEUILC");
        TSEUILD = Utils::extractDouble(events, "TSEUILD");
        TSEUILE = Utils::extractDouble(events, "TSEUILE");
        WSCINN1 = Utils::extractDouble(events, "WSCINN1");
        WSCINN2 = Utils::extractDouble(events, "WSCINN2");
        WSC = Utils::extractDouble(events, "WSC");

        PA3NG = Utils::extractDouble(events, "PA3NG");
        PA1NG = Utils::extractDouble(events, "PA1NG");


    }
};

} // namespace

#endif
