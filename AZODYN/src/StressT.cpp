// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <ParametersPlant.hpp>
#include<iostream>
#include<cstdlib>
#include<time.h>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;

class StressT : public DiscreteTimeDyn
{
public:
    StressT(
            const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        pp.initialiser(events);

        Tmoy.init(this, "Tmoy", events);

        stressT.init(this, "stressT", events);
    }

    virtual ~StressT()
    {}

    virtual void compute(const vd::Time& /*j*/)
    {
        // Calcul du Facteur de diminution de l’efficience de conversion du rayonement en fonction de la température (sans unité)
        if (Tmoy()<=pp.Topt_plante){
            stressT=std::max(0.0,1-std::pow(
                    ((Tmoy()-pp.Topt_plante)/(pp.Tmin_plante-pp.Topt_plante)),2));
        } else {
            stressT=std::max(0.0,1-std::pow(
                    ((Tmoy()-pp.Topt_plante)/(pp.Tmax_plante-pp.Topt_plante)),2));
        }

    }

private:

    ParametersPlant pp;

    /*Sync*/ Var Tmoy;

    Var stressT;
};

} // namespace AZODYN

DECLARE_DYNAMICS(AZODYN::StressT)

