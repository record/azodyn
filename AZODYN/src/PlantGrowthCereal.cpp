// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include<iostream>
#include<cstdlib>
#include<time.h>
#include <vle/DiscreteTime.hpp>
#include "ParametersPlantCereal.hpp"

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;

class PlantGrowthCereal : public DiscreteTimeDyn
{
public:
    PlantGrowthCereal(const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {

        pp.initialiser(events);

        RG.init(this, "RG", events);
        stressGEL.init(this, "stressGEL", events);
        stressN_Eb.init(this, "stressN_Eb", events);
        stressH.init(this, "stressH", events);
        stressN_LAI.init(this, "stressN_LAI", events);
        QNveg.init(this, "QNveg", events);
        QNa_FLO.init(this, "QNa_FLO", events);
        jMatur.init(this, "jMatur", events);
        CoefNc.init(this, "CoefNc", events);

        STapE1CM.init(this, "STapE1CM", events);
        stressT.init(this, "stressT", events);


        LAIpot.init(this, "LAIpot", events);
        LAI.init(this, "LAI", events);
        Eipot.init(this, "Eipot", events);
        Ebpot.init(this, "Ebpot", events);
        Ei.init(this, "Ei", events);
        MS.init(this, "MS", events);
        MSpot.init(this, "MSpot", events);
        MS_FLO.init(this, "MS_FLO", events);
        LAI_FLO.init(this, "LAI_FLO", events);
    }

    virtual ~PlantGrowthCereal()
    {}

    virtual void compute(const vd::Time& j)
    {

        // ------ fonctionnement de la plante au potentiel (sans stress N et H) --------------------------------

        // ------ Calcul de la matiere seche potentielle --------------------------------

         // matiere seche potentielle accumulee depuis initialisation jusqu'au jour j (sans stress N et H) (g/m2)

         if (pp.begin_date + pp.begin_date + j<pp.date_init_MS or jMatur(-1) != 0) {
             MSpot= 0.0;
         } else {
             if(pp.begin_date + j == pp.date_init_MS) {
                 if (pp.MS_init == -1) {//MS_init not available
                     MSpot = pp.densite_semis * pp.PMGmax_var/1000;
                 } else {
                     MSpot = pp.MS_init;
                 }
             } else {
                 MSpot=MSpot(-1)+ RG(-1)/100.0*pp.Ec*Eipot(-1)*Ebpot(-1);
             }
         }

        // Surface foliaire (LAI) potentielle (sans stress N et H) le jour j (en m2/m2)

        if(pp.begin_date + j<pp.date_init_MS or jMatur(-1) != 0){
            LAIpot=0.0;
        } else {
            if(STapE1CM()< pp.ST_E1CM_EP) {
                if (MSpot()/100<pp.MS_Nc_seuil){
                    LAIpot = std::max(LAIpot(-1), pp.coef_demMS_demN*
                            MSpot()*10.0* (pp.tNc_Nc/100));
                } else {
                    LAIpot= std::max(LAIpot(-1), pp.coef_demMS_demN*
                            MSpot()*10*(pp.coef_Nc*std::pow(MSpot()/100,pp.pente_Nc))/100);
                }
            } else {
                LAIpot=LAIpot(-1);
            }
        }
        
        // Efficience d'interception potentielle du rayonnement lumineux (sans stress N et H) (sans unite)
        if(pp.begin_date + j<pp.date_init_MS or jMatur(-1) != 0){
            Eipot= 0.0 ;
        } else {
            Eipot=pp.Eimax*(1.0-std::exp(-pp.k*LAIpot()));
        }


        // ------ Calcul de Ebpot --------------------------------

        //Efficience de conversion potentielle du rayonnement le jour j (sans stress N et H)
        if(pp.begin_date + j<pp.date_init_MS or jMatur(-1) != 0){
            Ebpot= 0.0 ;
        } else {
            if (pp.begin_date + j < pp.date_E1CM) {
                Ebpot = (pp.Ebr/2)*stressT();
            } else {
                Ebpot = pp.Ebr*stressT();
            }
        }



        // ------ fonctionnement de la plante avec prise en compte des stress N et H --------------------------------


        //calcul de l'efficience d interception du rayonnement lumineux
  
        // calcul de la surface foliaire (LAI) le jour j

        if(pp.begin_date + j<pp.date_init_MS or jMatur(-1) != 0){
            LAI=0.0;
        } else if(pp.begin_date + j <= pp.date_FLO) {
            if(STapE1CM()< pp.ST_E1CM_EP) {
                LAI= std::min(LAIpot(), LAIpot()*stressN_LAI(-1)*pp.alphaLAI);

            } else {
                LAI=LAI(-1);
            }
        } else {
            LAI= std::max(0.0, std::min(LAI(-1),
                    LAI_FLO(-1)*pp.AlphaSenesc*(pp.SENESC1*QNveg(-1)/QNa_FLO(-1) + pp.SENESC2)));
        }

        // LAI à FLORAISON (en m2/m2)

        if (pp.begin_date + j<pp.date_FLO) {
            LAI_FLO= 0.0;
        } else if (pp.begin_date + j == pp.date_FLO){
            LAI_FLO= LAI();
        } else {
            LAI_FLO=LAI_FLO(-1);
        }


        //efficience  d'interception du rayonnement global le jour j

        Ei=pp.Eimax*(1.0-std::exp(-pp.k*LAI()));


        // matiere seche accumulee depuis l initilisation jusqu au jour j

        if (pp.begin_date + j<pp.date_init_MS) {
            MS= 0.0;
        } else if(pp.begin_date + j==pp.date_init_MS) {
            if (pp.MS_init == -1) {//MS_init not available
                MS = pp.densite_semis * pp.PMGmax_var/1000;
            } else {
                MS= pp.MS_init;
            }

        } else if (jMatur(-1) == 0){
//            std::cout << " dbgMS " << vle::utils::DateTime::toJulianDay(j)
//              << " " << Ei(-1) << " "  << Ebpot(-1)
//              << " " << stressH(-1) << " "  << stressN_Eb(-1)
//              << " " << stressGEL(-1)
//              << "\n";

       //    if (CoefNc(-1) <= (1.89*std::pow(MS(-1),-0.32))) {
        //    MS= std::min(
        //      MS(-1)+ RG(-1)/100.0*p.Ec*Ei(-1)*
          //    Ebpot(-1)*stressH(-1)*stressN_Eb(-1)*stressGEL(-1),
          //     std::pow((CoefNc(-1)/p.coef_Nc),(1/p.pente_Nc)));
           //    } else {              
             MS=MS(-1)+ RG(-1)/100.0*pp.Ec*Ei(-1)*
               Ebpot(-1)*stressH(-1)*stressN_Eb(-1)*pp.alphaLAI*
               stressGEL(-1);
       // }
         } else {
         MS= MS(-1);
         }

        // MS à FLORAISON

        if (pp.begin_date + j<pp.date_FLO) {
            MS_FLO= 0.0;
        } else if (pp.begin_date + j == pp.date_FLO){
            MS_FLO= MS();
        } else {
            MS_FLO=MS_FLO(-1);
        }
    }


private:

    ParametersPlantCereal pp;
    
    /*Nosync*/ Var RG;
    /*Nosync*/ Var stressGEL;
    /*Nosync*/ Var stressN_Eb;
    /*Nosync*/ Var stressH;
    /*Nosync*/ Var stressN_LAI;
    /*Nosync*/ Var QNveg;
    /*Nosync*/ Var QNa_FLO;
    /*Nosync*/ Var jMatur;
    /*Nosync*/ Var CoefNc; 
      
    /*Sync*/ Var STapE1CM;
    /*Sync*/ Var stressT;
    
    Var LAIpot;
    Var LAI;
    Var Eipot;
    Var Ebpot;
    Var Ei;
    Var MS;
    Var MS_FLO;
    Var MSpot;
    Var LAI_FLO;
   
};

} // namespace

DECLARE_DYNAMICS(AZODYN::PlantGrowthCereal)

