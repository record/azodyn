// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <ParametersPlant.hpp>
#include<iostream>
#include<cstdlib>
#include<time.h>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {

using namespace vle::utils;
using namespace vle::discrete_time;

class Frost : public DiscreteTimeDyn
{
public:
    Frost(const vd::DynamicsInit& atom,
          const vd::InitEventList& events)
: DiscreteTimeDyn(atom, events)
    {
        pp.initialiser(events);

        Tmoy.init(this, "Tmoy", events);
        Tmin.init(this, "Tmin", events);
        Tmax.init(this, "Tmax", events);
        STapS.init(this, "STapS", events);
        STapLEVEE.init(this, "STapLEVEE", events);

        ST_S_i.init(this, "ST_S_i", events);
        ST_S_f.init(this, "ST_S_f", events);
        Rmax.init(this, "Rmax", events);
        Rpot.init(this, "Rpot", events);
        dR.init(this, "dR", events);
        R.init(this, "R", events);
        stressGEL.init(this, "stressGEL", events);
        dDes.init(this, "dDes", events);
        Des_cumul.init(this, "Des_cumul", events);
        dindiceGEL.init(this, "dindiceGEL", events);
        indiceGEL_cumul.init(this, "indiceGEL_cumul", events);
        indiceGELmax.init(this, "indiceGELmax", events);
        NjGel.init(this, "NjGel", events);
        stressGEL_cumul.init(this, "stressGEL_cumul", events);
        dEnd.init(this, "dEnd", events);
        End_cumul.init(this, "End_cumul", events);
        pEnd.init(this, "pEnd", events);
        pEndmax.init(this, "pEndmax", events);
    }

    virtual ~Frost()
    {}

    virtual void compute(const vd::Time& j)
    {

        //---------------------------------//
        
        //Calcul de la somme de température entre le semis et le stade initial ou on l'augmente le Rmax
        ST_S_i = STapS() - STapLEVEE() + pp.NbFi * pp.phyllo;
      
        //Calcul de la somme de température entre le semis et le stade final ou on l'augmente plus le Rmax    
        ST_S_f = STapS() - STapLEVEE()  + pp.NbFf * pp.phyllo;

        //Calcul de Rmax
        if(STapS()<= ST_S_i()) {
            Rmax = pp.R_Coleo;
        } else if(STapS()<=ST_S_f()){
            Rmax =((pp.Rmax_var-pp.R_Coleo)*STapS()+
                    (pp.R_Coleo*ST_S_f()-pp.Rmax_var*ST_S_i()))/
                            (ST_S_f()-ST_S_i());
        } else{
            Rmax = pp.Rmax_var;
        }

        //Calcul de Rpot
        if(pp.begin_date + j<=pp.date_S){
            Rpot = pp.Rmin;
        } else if((Tmoy() <  pp.Tmax_R) and (Tmoy() < pp.Tmin_R)){
            Rpot = Rmax();
        } else if ((Tmoy() <  pp.Tmax_R) and (Tmoy() >= pp.Tmin_R)){
            Rpot =(((pp.Rmin - Rmax()) * Tmoy()) +
                    ((Rmax() * pp.Tmax_R) - (pp.Rmin * pp.Tmin_R)))/(pp.Tmax_R-pp.Tmin_R);
        } else {
            Rpot = pp.Rmin;
        }

        //Calcul de de la variation journaliere de la résistance (degres-jour)
        if(pp.begin_date + j<=pp.date_S){
            dR = 0;
        } else if(Rpot() < R(-1)){
            dR =(Rpot()-pp.Rmin)/pp.Njend;
        } else {
            dR =(pp.Rmin-Rmax()) * Tmoy()/(20.0 * pp.Njdes);
        }

        //Calcul de R la resistance
        if(pp.begin_date + j< pp.date_S){
            R=pp.Rmin;
        } else if(R(-1)==Rpot()){
            R=R(-1);
        } else if(R(-1)>Rpot()){
            R=std::max((R(-1)+dR()),Rpot());
        } else{
            R=std::min((R(-1)+dR()),Rpot());
        }

        //Calcul du stress ou ecart journalier entre la
        //resistance et la temperature minimale dindiceGEL
        if(pp.begin_date + j < pp.date_S ) {
            dindiceGEL = 0;
        } else {
            dindiceGEL = std::max(0.0,  R() - Tmin());
        }

        //Calcul de la destruction de la biomasse stressGEL (de 0: pas de stress à 1 : stress TOTAL)
        if((STapLEVEE() == 0) or (pp.begin_date + j  >= pp.date_fin_GEL)) {
            stressGEL = 1;
        } else  if (Tmin()>= R()){
            stressGEL=1;
        } else if((Tmin())<= R()- pp.GELmax_seuil){
            stressGEL = 0.0;
        } else {
            stressGEL= 1.0-(1/ pp.GELmax_seuil)*dindiceGEL();
        }

        //Intensité cumulée de stressGEL entre la levée et la date_fin_GEL: cumul de 1-stressGEL
        if (STapLEVEE() == 0 ) {
            stressGEL_cumul = 0;
        } else {
          stressGEL_cumul = stressGEL_cumul(-1) + (1-stressGEL()) ;		
        }
		
        //Calcul du desendurcissement journalier dDes
		if (R() > R(-1)){
            dDes = R() - R(-1);
        } else {
            dDes = 0;
        }

        //Calcul du desendurcissement cumule

        Des_cumul = dDes() + Des_cumul(-1);        

       //Calcul de l'endurcissement journalier dEnd (°C)
        if (R() < R(-1)){
            dEnd = R() - R(-1);
        } else {
            dEnd = 0;
        }

        //Calcul de l'endurcissement cumulé End_cumul (°C)

        End_cumul = dEnd() + End_cumul(-1);    

        //Pourcentage d'endurcissement pEnd
        if(pp.begin_date + j< pp.date_S){
             pEnd= 0;
        } else {
             pEnd= (R()-pp.Rmin) / (Rmax() -pp.Rmin)*100;
        }
        //Pourcentage d'endurcissement maximal, pEndmax 
        if(pp.begin_date + j< pp.date_S){
             pEndmax= 0;
        } else if (pEnd()>pEndmax(-1)){
             pEndmax= pEnd();
        } else {
             pEndmax=pEndmax(-1);
        }
        
        //Calcul du stress cumulé Gel
        if(dindiceGEL()==0) {
            indiceGEL_cumul = indiceGEL_cumul(-1);
        } else {
            indiceGEL_cumul = dindiceGEL() + indiceGEL_cumul(-1);
        }

        //Calcul de l'écart maimum journalier indiceGELmax
        if(dindiceGEL()> indiceGELmax(-1)){
            indiceGELmax = dindiceGEL();
        } else {
            indiceGELmax = indiceGELmax(-1);
        }

        //Calcul du nombre de jours de gel NjGel
        if(dindiceGEL()>0) {
            NjGel = NjGel(-1) + 1;
        } else {
            NjGel=NjGel(-1);
        }
    }

private:

    ParametersPlant pp;

    //double Njdes;

    /*Sync*/ Var Tmoy;
    /*Sync*/ Var Tmin;
    /*Sync*/ Var Tmax;
    /*Sync*/ Var STapS;
    /*Sync*/ Var STapLEVEE;

    Var ST_S_i;
    Var ST_S_f;
    Var Rmax;
    Var Rpot;
    Var dR;
    Var R;
    Var stressGEL;
    Var dDes;
    Var Des_cumul;
    Var dindiceGEL;
    Var indiceGEL_cumul;
    Var indiceGELmax;
    Var NjGel;
    Var stressGEL_cumul;
    Var dEnd;
    Var End_cumul;
    Var pEnd ; 
    Var pEndmax;
};

} // namespace AZODYN

DECLARE_DYNAMICS(AZODYN::Frost)

