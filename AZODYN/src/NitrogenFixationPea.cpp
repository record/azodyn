// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <Phenology.hpp>
#include <ParametersPlant.hpp>
#include <ParametersSoil.hpp>
#include <ParametersPlantPea.hpp>

#include<iostream>
#include<cstdlib>
#include<time.h>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {
using namespace vle::discrete_time;
using namespace vle::utils;

class NitrogenFixationPea : public DiscreteTimeDyn
{
public:
    NitrogenFixationPea(const vd::DynamicsInit& atom,
            const vd::InitEventList& events) : DiscreteTimeDyn(atom, events)
    {
        ps.initialiser(events);
        pp.initialiser(events);

        dQN_besoin.init(this, "dQN_besoin", events);
        QN_tot.init(this, "QN_tot", events);

        Stade.init(this, "Stade", events);
        RU_C0.init(this, "RU_C0", events);
        ProfRacmax.init(this, "ProfRacmax", events);
        QNaccess_C0.init(this, "QNaccess_C0", events);
        STapLEVEE.init(this, "STapLEVEE", events);
        date_DRG.init(this, "date_DRG", events);
        MS.init(this, "MS", events);
        coef_fixmax3.init(this, "coef_fixmax3", events);

        Profnod.init(this, "Profnod", events);
        RU_nod.init(this, "RU_nod", events);
        stressH_nod.init(this, "stressH_nod", events);
        NJstressH_nod.init(this, "NJstressH_nod", events);
        pNac_fix.init(this, "pNac_fix", events) ;
        dQNfixmax_tot.init(this, "dQNfixmax_tot", events);
        dQNfix_besoin.init(this, "dQNfix_besoin", events);
        dQNfix_tot.init(this, "dQNfix_tot", events);
        QNfix.init(this, "QNfix", events);
        Ndfa.init(this, "Ndfa", events);
    }

    virtual ~NitrogenFixationPea()
    {}

    virtual void compute(const vd::Time& j)
    {

        //Calcul du stress hydrique affectant la nodulation
        //Profondeur des nodosites jusqu'au jour j (mm)

        if(ps.da_C0<=ps.da_C0_opt){
            Profnod=pp.Profnod_opt;
        } else if(ps.da_C0<=pp.damax_C0){
            Profnod=((ps.da_C0-ps.da_C0_opt)*(pp.Profnod_opt-pp.Profnod_min)
                    /(ps.da_C0_opt - pp.damax_C0))+pp.Profnod_opt;
        } else{
            Profnod=pp.Profnod_min;
        }

        //  Quantite d'eau dans la couche avec nodosites(mm)
        if (pp.begin_date + j < pp.date_S){
            RU_nod = 0.0 ;
        } else {
            RU_nod = RU_C0()*Profnod()/ps.ep_C0;
        }


        // indication de la presence d'un stress hydrique pour le jour j (0 ou 1)   (sans unite)
        if (RU_nod() < (Profnod()/ProfRacmax()* ps.RUmax/3)){
            stressH_nod = 1 ;
        } else {
            stressH_nod = 0;
        }

        //calcul du nombre de jours ou la couche avec nodosites est en stress hydrique par rapport a la fixation
        if(pp.begin_date + j < pp.date_FLO){
            NJstressH_nod = 0.0 ;
        } else if (NJstressH_nod(-1) >= pp.NJstress_nod_seuil) {
            NJstressH_nod=NJstressH_nod(-1);
        } else if  (stressH_nod() ==0.0){
            NJstressH_nod=0.0;
        } else{
            NJstressH_nod= NJstressH_nod(-1)+stressH_nod();
        }

        //Calcul de la fixation
        //%total accumule qui provient de la fixation le jour j (traduit
        //l'inhibition de la fixation due a la presence d'azote mineral dans le sol)
        if (Stade() < VEGETATIF){
            pNac_fix = 0.0;
        } else if (pp.begin_date + j < pp.date_FLO) {
            pNac_fix = std::max(0.0,std::min(
                    pp.coefNsol_fix1* QNaccess_C0() + pp.a_pNfix,
                    pp.pNfix_max));
        } else{
            pNac_fix = std::max(0.0,std::min(
                    pp.coefNsol_fix2* QNaccess_C0() + pp.b_pNfix,
                    pp.pNfix_max));
        }

        //Quantite max d'azote fixe par jour (ne prend pas en compte l'inhibition
        //de la fixation par la presence d'N mineral dans le sol)(kgN/ha)
        if (STapLEVEE() < pp.STfixmax_seuil){
            dQNfixmax_tot = 0.0 ;
        } else if (Stade() < DRG_FSLA){
            dQNfixmax_tot = std::max(0.0,pp.coef_tot_a * (MS()-MS(-1))*10.0*
                    (pp.coef_fixmax1*STapLEVEE() + coef_fixmax3()));
        } else if (Stade() < FSLA_MP){
            dQNfixmax_tot =std::max(0.0,(MS()-MS(-1))*10.0*pp.coef_fixmax2 
                                         *pp.coef_tot_a);
        } else {
            dQNfixmax_tot=0.0;
        }
        //quantite d'azote dont la plante a besoin qui est issu de la fixation
        //(proportion des besoins totaux)(kgN/ha)

        dQNfix_besoin = dQN_besoin(-1)* pNac_fix(-1)/100;

        //quantite totale d'azote fixe le jour j (kgN/ha)

        if (pp.begin_date + j < pp.date_FLO or NJstressH_nod(-1) <= pp.NJstress_nod_seuil ){
            dQNfix_tot = std::min(dQNfix_besoin(),dQNfixmax_tot());
        } else {
            dQNfix_tot = 0.0;
        }

        //quantite d'azote dans les parties aeriennes issu de la fixation jusqu'au jour j (kgN/ha)
        if (Stade() < VEGETATIF){
            QNfix = 0.0;}
        else{
            QNfix = (dQNfix_tot()/pp.coef_tot_a)+QNfix(-1);
        }


        //Nitrogen derived from fixation: proportion d'azote fixe sur l'azote
        //total accumule dans les parties aeriennes jusqu'au jour j (%)
        if (Stade() < VEGETATIF){
            Ndfa = 0.0;
        } else {
            Ndfa = (QNfix(-1)/QN_tot(-1))/pp.coef_tot_a * 100;
        }
    }
private:
    ParametersSoil     ps;
    ParametersPlantPea pp;

    /*Nosync*/ Var dQN_besoin;
    /*Nosync*/ Var QN_tot;

    /*Sync*/ Var Stade;
    /*Sync*/ Var RU_C0;
    /*Sync*/ Var ProfRacmax;
    /*Sync*/ Var QNaccess_C0;
    /*Sync*/ Var STapLEVEE ;
    /*Sync*/ Var date_DRG;
    /*Sync*/ Var MS;
    /*Sync*/ Var coef_fixmax3;;

    Var Profnod;
    Var RU_nod ;
    Var stressH_nod ;
    Var NJstressH_nod;
    Var pNac_fix;
    Var dQNfixmax_tot;
    Var dQNfix_besoin ;
    Var dQNfix_tot;
    Var QNfix ;
    Var Ndfa ;
};

} // namespace

DECLARE_DYNAMICS(AZODYN::NitrogenFixationPea)

