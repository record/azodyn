// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends



#include <vle/DiscreteTime.hpp>
#include "AzodynUtils.hpp"

namespace vd = vle::devs;
namespace vv = vle::value;

namespace AZODYN {


using namespace vle::discrete_time;


class Decision : public DiscreteTimeDyn
{
public:
    std::vector<std::pair<long, double> > fertiMinNO3;
    std::vector<std::pair<long, double> > fertiMinNH4;
    std::vector<std::pair<long, double> > irrigation;

    Decision(
            const vd::DynamicsInit& atom,
            const vd::InitEventList& events)
    : DiscreteTimeDyn(atom, events), fertiMinNO3(),
      fertiMinNH4(), irrigation()
    {
        DoseEngMinNO3.init(this, "DoseEngMinNO3", events);
        DoseEngMinNH4.init(this, "DoseEngMinNH4", events);
        I.init(this, "I", events);

        begin_date = Utils::extractDate(events, "begin_date");


        //initialise mineral NO3 fertilizations
        bool finish = false;
        unsigned int i=0;
        while (!finish) {
            i++;
            std::stringstream ss;
            ss << "fertiMinNO3_" << i;
            finish = !events.exist(ss.str());
            if (! finish) {

                double date =0;
                double dose =0;
                Utils::extractApport(events, ss.str(), date, dose);
                if (dose > 0) {
                    fertiMinNO3.push_back(std::make_pair(date, dose));
                }

            }
        }

        //initialise mineral NH4 fertilizations
        finish = false;
        i=0;
        while (!finish) {
            i++;
            std::stringstream ss;
            ss << "fertiMinNH4_" << i;
            finish = !events.exist(ss.str());
            if (! finish) {
                double date =0;
                double dose =0;
                Utils::extractApport(events, ss.str(), date, dose);
                if (dose > 0) {
                    fertiMinNH4.push_back(std::make_pair(date, dose));
                }
            }
        }

        //initialise irrigations
        finish = false;
        i=0;
        while (!finish) {
            i++;
            std::stringstream ss;
            ss << "irrigation_" << i;
            finish = !events.exist(ss.str());
            if (! finish) {
                double date =0;
                double dose =0;
                Utils::extractApport(events, ss.str(), date, dose);
                if (dose > 0) {
                    irrigation.push_back(std::make_pair(date, dose));
                }
            }
        }
    }

    virtual ~Decision()
    {}

    virtual void compute(const vd::Time& j)
    {
        //Valorisation Minerale NO3
        bool found = false;
        for(unsigned int i=0;i < fertiMinNO3.size() and not found; i++) {
            const std::pair<long, double>& f = fertiMinNO3[i];
            found = (f.first == (long) begin_date + j);
            if (found) {
                DoseEngMinNO3 = f.second;
            }
        }
        if (not found) {
            DoseEngMinNO3 = 0;
        }

        //Valorisation Minerale NH4
        found = false;
        for(unsigned int i=0;i < fertiMinNH4.size() and not found; i++) {
            const std::pair<long, double>& f = fertiMinNH4[i];
            found = (f.first == (long) begin_date + j);
            if (found) {
                DoseEngMinNH4 = f.second;
            }
        }
        if (not found) {
            DoseEngMinNH4 = 0;
        }

        //Irrigation
        found = false;
        for(unsigned int i=0;i < irrigation.size() and not found; i++) {
            const std::pair<long, double>& f = irrigation[i];
            found = (f.first == (long) begin_date + j);
            if (found) {
                I = f.second;
            }
        }
        if (not found) {
            I = 0;
        }
    }

    Var DoseEngMinNO3;
    Var DoseEngMinNH4;
    Var I;
    double begin_date;

};

} // namespace

DECLARE_DYNAMICS(AZODYN::Decision);

