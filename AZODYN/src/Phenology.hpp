#ifndef AZODYN_PHENOLOGY_HPP
#define AZODYN_PHENOLOGY_HPP

namespace AZODYN {

enum Pea {
    SOL_NU    /*"0"*/,
    VEGETATIF /*"1"*/,
    DF_DRG    /*"2"*/, //debut floraison -> debut de remplissage
    DRG_FSLA  /*"3"*/, // -> fin du stade limite d'avortement
    FSLA_MP   /*"4"*/, // -> maturite
    MP        /*"5"*/, //
};

}//namespace
#endif


