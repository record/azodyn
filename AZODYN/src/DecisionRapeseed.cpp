// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp>

#include "AzodynUtils.hpp"

namespace vu = vle::utils;
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

namespace AZODYN
{

  class DecisionRapeseed : public DiscreteTimeDyn
  {
  public:
    DecisionRapeseed(const vd::DynamicsInit &model, const vd::InitEventList &evts) : DiscreteTimeDyn(model, evts), apportAzote()
    {
      if (evts.get("begin_date")->isDouble()) {
        begin_time = evts.getDouble("begin_date");
        begin_date = vle::utils::DateTime::toJulianDay(begin_time);
      } else if (evts.get("begin_date")->isString()) {
        begin_date = evts.getString("begin_date");
        begin_time = vle::utils::DateTime::toJulianDay(begin_date);
      }
      densiteSemis = Utils::extractDouble(evts, "densiteSemis");// 40
      dateSemis = vu::DateTime::toJulianDayNumber(evts.getString("dateSemis"));

      bool finish = false;
      unsigned int i=0;
      while (!finish) {
        i++;
        std::stringstream ss;
        ss << "apportAzote" << i;
        finish = !evts.exist(ss.str());
        if (! finish) {
          double date,dose =0;
          Utils::extractApport(evts, ss.str(), date, dose);
          if (dose > 0) {
            apportAzote.push_back(std::make_pair(date, dose));
          }
        }
      }
      dateRecolte = vu::DateTime::toJulianDayNumber(evts.getString("dateRecolte"));

      DensiteSemis.init(this, "DensiteSemis", evts);
      Napport.init(this, "Napport", evts);
      Recolte.init(this, "Recolte", evts);
    }

    virtual ~DecisionRapeseed()
    {
    }

    virtual void compute(const vd::Time &t)
    {
      vle::devs::Time time = begin_time+t;
      
      if (time == dateSemis)
      {
        DensiteSemis = densiteSemis;
      }
      else
      {
        DensiteSemis = 0.;
      }

      bool found = false;
      for(unsigned int i=0;i < apportAzote.size() and not found; i++) {
        const std::pair<long, double>& f = apportAzote[i];
        found = (f.first == (long) time);
        if (found) {
          Napport = f.second;
        }
      }
      if (not found) {
        Napport = 0;
      }

      if (time == dateRecolte)
      {
        Recolte = 1.;
      }
      else
      {
        Recolte = 0.;
      }
    }

  private:
    std::string  begin_date;
    double begin_time;

    double dateSemis;
    double densiteSemis; // 40

    std::vector<std::pair<long, double> > apportAzote;
    double dateRecolte;

    Var DensiteSemis;
    Var Napport;
    Var Recolte;
  };

} // namespace AzodynColza

DECLARE_DYNAMICS(AZODYN::DecisionRapeseed)
