---
title: "AZODYN package documentation"
---

<!-- 
R -e "rmarkdown::render('./AZODYN/doc/AzodynColza_pkg_doc.md',  encoding = 'UTF-8');"
-->

# Package AZODYN for vle-2.0.2

The [**AZODYN**](https://forgemia.inra.fr/record/azodyn/) package provide 12 atomic models and a simulator for Rapeseed crop using the AZODYN model.  
Sample input files are also provided in the */data* folder.

* [CohortesFeuilles](#p2)
* [DecisionRapeseed](#p3)
* [Enracinement](#p4)
* [Fertilisation](#p5)
* [INN](#p8)
* [LPAI](#p9)
* [MSQN](#p10)
* [NitrogenSoilRape](#p11)
* [Phenologie](#p12)
* [Rendement](#p13)
* [WaterSoilRape](#p16)
* [YieldRapeseed](#p17)

* [Shared structures](#annex)
  * [Phenological stages](#annex.pheno.stages)
  * [ParametersSoilRape](#annex.p.soil)
  * [ParametersPlantRape](#annex.p.plt)

---

## Package dependencies <a name="p1"></a>

List of required external packages (with link to distribution when available).

* [vle.discrete-time](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)
* [record.meteo](https://forgemia.inra.fr/record/RECORD/-/tree/master/pkgs/record.meteo) (required to run the simulator but not to build the package)

--- 

## Atomic model CohortesFeuilles <a name="p2"></a>

The **CohortesFeuilles** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#CohortesFeuilles_panel'>Show/Hide Model Details</button>
<div id="CohortesFeuilles_panel" class="collapse">

### Configuring a CohortesFeuilles Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : CohortesFeuilles

#### Parameters settings <a name="p2.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersSoilRape`](#annex.p.soil) (`ACfm`, `CN_opt`, `alpha`, `Tref`, `km`, `CAUfm`) and [`ParametersPlantRape`](#annex.p.plt) (`AVIEFM`, `BVIEFM`, `CVIEFM`, `DVIEFM`, `EVIEFM`, `FVIEFM`) to get all available parameters

#### Input settings <a name="p2.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Tmoy** : Température moyenne journalière (°C/j) (sync)
* **Stade** : Stade phénologique actuel (sans unité) (see [shared defined stage](#annex.pheno.stages)) (sync)
* **STsemis** : Somme des températures depuis le semis (°C) (sync)
* **INNvec** : INN journalier (sans unité) (Nosync)
* **partMSLeaves** : Proportion de biomasse de feuilles dans la biomasse aérienne réelle (sans unité). (Nosync)
* **MSAr** : Biomasse aérienne réelle (kgMS/ha) (Nosync)

#### Output settings <a name="p2.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **NMinTot** : Azote journalier minéralisé issu des feuilles mortes (pour l'ensemble des cohortes de feuilles) (kgN/ha/j)
* **MSfm** : Cumul de biomasse de feuilles mortes (kgMS/ha)
* **LeavesLife** : Variable guidant la chutte des feuilles (°C)

### Details <a name="p2.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p2.2.1"></a>

>$NMinTot(t) = f1(
    Tmoy(t),
    Stade(t),
    INNvec(t-1)
)$

>$MSfm(t) = f2(
    Stade(t),
    partMSLeaves(t-1),
    INNvec(t-1),
    MSAr(t-1),
    MSAr(t-2)
)$

>$LeavesLife(t) = f3(
    Stade(t),
    STsemis(t)
)$

</div>

--- 

## Atomic model DecisionRapeseed <a name="p3"></a>

The **DecisionRapeseed** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) is using fixed dates to trigger events and parameters to send values.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#DecisionRapeseed_panel'>Show/Hide Model Details</button>
<div id="DecisionRapeseed_panel" class="collapse">

### Configuring a DecisionRapeseed Model <a name="p3.1"></a>

#### Dynamics settings <a name="p3.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : DecisionRapeseed

#### Parameters settings <a name="p3.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

| Parameter name   |  Type  | Is mandatory? | Description                                                                                |
| ---------------- | :----: | :-----------: | ------------------------------------------------------------------------------------------ |
| **densiteSemis** | double |      [x]      | Densité de semis (plante.m^{-2})                                                           |
| **dateSemis**    | string |      [x]      | Date de semis (YYYY-MM-DD)                                                                 |
| **dateRecolte**  | string |      [x]      | Date de récolte (YYYY-MM-DD)                                                               |
| **apportAzoteX** | string |      [x]      | Apport de fertilisant X avec X un entier. "date=1992-02-26$dose=30" (??) |

#### Input settings <a name="p3.1.3"></a>

None

#### Output settings <a name="p3.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **Napport** : Quantité d'azote apportée par l'engrais, en instantanée (équivalent à la dose d'N apporté si on se situe a la date d'apport, sinon 0) (kgN/ha)
* **Recolte** : Vaut 1 quand la récolte a été effectuée, sinon vaut 0 (sans unité)
* **DensiteSemis** : Densité de semis à la date de semis, sinon 0 (plante/m²)

#### Observation settings <a name="p3.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p3.1.6"></a>

None

### Details <a name="p3.2"></a>

#### State variables equations <a name="p3.2.1"></a>

> $Napport$ takes the value of `dose` when the date `date` is reached (in `apportAzoteX` with X in [1-4]) otherwise value is 0

> $Recolte$ takes the value 1 when the date `dateRecolte` is reached otherwise value is 0

> $DensiteSemis$ takes the value `densiteSemis` when the date `dateSemis` is reached otherwise value is 0

</div>

--- 

## Atomic model Enracinement <a name="p4"></a>

The **Enracinement** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate root depth based on a linear relation with crop phenological thermal time

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Enracinement_panel'>Show/Hide Model Details</button>
<div id="Enracinement_panel" class="collapse">

### Configuring a Enracinement Model <a name="p4.1"></a>

#### Dynamics settings <a name="p4.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : Enracinement

#### Parameters settings <a name="p4.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantRape`](#annex.p.plt) (`ProfRacmax` and `Velong`)

#### Input settings <a name="p4.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **STlevee** : Somme des températures depuis la levée (°C) (sync)

#### Output settings <a name="p4.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **ProfRac** : Profondeur d'enracinement (mm)

#### Observation settings <a name="p4.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p4.1.6"></a>

None

### Details <a name="p4.2"></a>

#### State variables equations <a name="p4.2.1"></a>

> $$
ProfRac(t)=\min\left(
  \begin{array}{@{}ll@{}}
    ProfRacmax \\
    Velong * STlevee(t)
  \end{array}\right)
\label{eq:ProfRac}$$  


</div>

--- 

## Atomic model Fertilisation <a name="p5"></a>

The **Fertilisation** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based ??

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Fertilisation_panel'>Show/Hide Model Details</button>
<div id="Fertilisation_panel" class="collapse">

### Configuring a Fertilisation Model <a name="p5.1"></a>

#### Dynamics settings <a name="p5.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : Fertilisation

#### Parameters settings <a name="p5.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [ParametersSoilRape](#annex.p.soil) (`PLUIEVALOR`, `lambda`, `mu`)

#### Input settings <a name="p5.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Pluie** : Pluviométrie journalière (mm/j) (sync)
* **I** : Irrigation (mm/j) (sync)
* **STlevee** : Somme des températures depuis la levée (°C) (sync)
* **MSAr** : Biomasse aérienne réelle (kgMS/ha) (sync)
* **MSAq** : ?? (??) (sync)
* **Napport** : Quantité d'azote apportée par l'engrais, en instantanée (équivalent à la dose d'N apporté si on se situe a la date d'apport, sinon 0) (kgN/ha) (sync)

#### Output settings <a name="p5.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **sPluie7** : Somme des pluies et irrigation sur les 7 derniers jours (mm)
* **NapportPresent** : Quantité d'N issu des engrais, présent à la surface du sol et non solubilisé (kgN/ha)
* **CAU** : Coefficient Apparent d'Utilisation du dernier apport réalisé (%)
* **QNEng** : Quantité d'azote biodisponible journalière apportée par les engrais (kgN/ha)

#### Observation settings <a name="p5.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p5.1.6"></a>

None

### Details <a name="p5.2"></a>

#### State variables equations <a name="p5.2.1"></a>

> $sPluie7(t) = \sum_{i=0}^6 Pluie(t-i)+I(t-i)$

> $$
NapportPresent(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (NapportPresent(t-1) + Napport(t) > 0) \text{&} (sPluie7(t) >= PLUIEVALOR) \\
    NapportPresent(t-1) + Napport(t), & \text{otherwise} \\
  \end{array}\right.
\label{eq:NapportPresent}$$  

> $$CAU(t)=\left\{
  \begin{array}{@{}ll@{}}
    \min\left(
        \begin{array}{@{}ll@{}}
          100 \\
        \max\left(
            \begin{array}{@{}ll@{}}
            0 \\
            lambda + mu * 100 * \frac{(\log(\frac{MSAg(t)}{10}) - \log(\frac{MSAg(t-7)}{10})) * \frac{MSAr(t)}{10}}{STlevee(t) - STlevee(t-7)}
            \end{array}\right) \\
        \end{array}\right), & \text{if}\ (NapportPresent(t-1) + Napport(t) > 0) \text{&} (sPluie7(t) >= PLUIEVALOR) \\
    CAU(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:CAU}$$

> $$
QNEng(t)=\left\{
  \begin{array}{@{}ll@{}}
    (NapportPresent(t-1) + Napport(t)) * \frac{CAU(t)}{100}, & \text{if}\ (NapportPresent(t-1) + Napport(t) > 0) \text{&} (sPluie7(t) >= PLUIEVALOR) \\
    0, & \text{otherwise} \\
  \end{array}\right.
\label{eq:QNEng}$$  

</div>

--- 

## Atomic model INN <a name="p8"></a>

The **INN** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? using ??

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#INN_panel'>Show/Hide Model Details</button>
<div id="INN_panel" class="collapse">

### Configuring a INN Model <a name="p8.1"></a>

#### Dynamics settings <a name="p8.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : INN

#### Parameters settings <a name="p8.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantRape`](#annex.p.plt) (`tNmax_Nmax`, `coef_Nmax`, `pente_Nmax`, `coef_tot_a`, `BQNT`, `Vmax`, `ANFM`, `BNFM`, `ApartQNGreen`, `BpartQNGreen`, `AQNGreen`, `BQNGreen`, `ApartQNLeaves`, `BpartQNLeaves`, `CpartQNLeaves`, `DpartQNLeaves`, `ApartQNPod`, `BpartQNPod`) 

#### Input settings <a name="p8.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Tmoy** : Température moyenne journalière (°C/j) (sync)
* **Stade** : Stade phénologique actuel (sans unité) (see [shared defined stage](#annex.pheno.stages)) (sync)
* **STlevee** : Somme des températures depuis la levée (°C) (sync)
* **STsemis** : Somme des températures depuis le semis (°C) (sync)
* **MSAr** : Biomasse aérienne réelle (kgMS/ha) (sync)
* **MSfm** : Cumul de biomasse de feuilles mortes (kgMS/ha) (sync)
* **NAcr** : Teneur en azote critique de la biomasse aérienne réelle (%) (sync)
* **QNAcr** : Quantité d'azote critique dans la biomasse aérienne réelle (kgN/ha) (sync)
* **QNaccess_C0** : Quantité d'azote dans la part de la couche C0 qui est explorée par des racines (kgN/ha) (sync)
* **QNaccess_C1** : Quantité d'azote dans la couche C1 (kgN/ha) (sync)
* **DeltaMSAg** : Gain de biomasse aérienne générée journalier (kgMS/ha/j) (Nosync)
* **QN_offre** : Quantité d'azote minérale dans le sol disponible pour la culture (kgN/ha) (Nosync)

#### Output settings <a name="p8.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **Ncmax** : Teneur en azote critique maximum de la biomasse aérienne (%)
* **QNTmax** : Quantité d'azote critique maximum dans la plante entière (kgN/ha)
* **Besoins** : Besoins journaliers maximum en N de la plante entière (kgN/ha/j)
* **Nabs** : Quantité d'azote journalière absorbée par la plante entière (kgN/ha/j)
* **QNabs_C0** : Quantité d'azote absorbée par la plante entière provenant de la couche C0 (kgN/ha)
* **QNabs_C1** : Quantité d'azote absorbée par la plante entière provenant de la couche C1 (kgN/ha)
* **QNdeadLeaves** : Quantité d'azote présente dans les feuilles mortes en cumulé (kgN/ha)
* **QNTg** : Quantité d'azote totale absorbée par la plante entière (kgN/ha)
* **QNTr** : Quantité d'azote dans la biomasse totale réelle (kgN/ha)
* **QNAr** : Quantité d'azote dans la biomasse aérienne réelle (kgN/ha)
* **QNAg** : Quantité d'azote absorbée dans la biomasse aérienne générée (kgN/ha)
* **DeltaQNAg** : Quantité d'azote journalière absorbée par la biomasse aérienne (kgN/ha/j)
* **Nar** : Teneur en azote de la biomasse aérienne (%)
* **INNvec** : INN journalier (sans unité)
* **AireINNvec** : Aire sous la courbe d'INN (Tmoy * INNvec) (°C)
* **SommeAireINNvec** : Aire sous la courbe de l'INN en degrés depuis la levée (Somme des AirINNvec depuis la levée) (°C)
* **INNintvec** : INN moyen depuis la levée (INNintégré) (sans unité)
* **partQNGreen*** : Proportion d'azote contenu dans des tissus photosynthétiquement actif sur la biomasse aérienne (sans unité)
* **QNGreen** : Quantité d'azote contenus dans les tissus photosynthétiquement actif (kgN/ha)
* **partQNLeaves** : Proportion d'azote contenu dans les feuilles sur la biomasse aérienne (sans unité)
* **QNLeaves** : Quantité d'azote contenu dans les feuilles (kgN/ha)
* **partQNPod** : Proportion d'azote contenu dans les sillique sur la biomasse aérienne (sans unité)
* **QNPod** : Quantité d'azote contenu dans les siliques (kgN/ha)

#### Observation settings <a name="p8.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p8.1.6"></a>

None

### Details <a name="p8.2"></a>

#### State variables equations <a name="p8.2.1"></a>

> $$Ncmax(t)=\left\{
  \begin{array}{@{}ll@{}}
    \min\left(
        \begin{array}{@{}ll@{}}
          tNmax\_Nmax \\
          coef\_Nmax * {(\frac{MSAr(t-1) + DeltaMSAg(t-1)}{1000})}^{pente\_Nmax}
        \end{array}\right), & \text{if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:Ncmax}$$

> $$QNTmax(t)=\left\{
  \begin{array}{@{}ll@{}}
    coef\_tot\_a * ((MSAr(t-1) + DeltaMSAg(t-1)) * \frac{Ncmax(t)}{100}) + BQNT, & \text{if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:QNTmax}$$

> $$Besoins(t)=\left\{
  \begin{array}{@{}ll@{}}
    \max\left(
        \begin{array}{@{}ll@{}}
          0 \\
        \min\left(
            \begin{array}{@{}ll@{}}
            Vmax * Tmoy(t) \\
            QNTmax(t) - (\frac{coef\_tot\_a * MSAr(t-1) }{100} * \min\left(
                                                                            \begin{array}{@{}ll@{}}
                                                                            tNmax\_Nmax\\
                                                                            coef\_Nmax * (\frac{MSAr(t-1)}{1000})^{pente\_Nmax}
                                                                        \end{array}\right) + BQNT)\\
            \end{array}\right) \\
        \end{array}\right), & \text{if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:Besoins}$$

> $$Nabs(t)=\left\{
  \begin{array}{@{}ll@{}}
    \max\left(
        \begin{array}{@{}ll@{}}
          0 \\
        \min\left(
            \begin{array}{@{}ll@{}}
                QN\_offre(t-1) \\
                Besoins(t) \\
                Vmax * Tmoy(t)
            \end{array}\right) \\
        \end{array}\right), & \text{if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:Nabs}$$

> $$QNabs\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    \frac{QNaccess\_C0(t)}{QNaccess\_C0(t) + QNaccess\_C1(t)} * Nabs(t), & \text{if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:QNabs_C0}$$

> $$QNabs\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    \frac{QNaccess\_C1(t)}{QNaccess\_C0(t) + QNaccess\_C1(t)} * Nabs(t), & \text{if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:QNabs_C1}$$

> $$QNdeadLeaves(t)=\left\{
  \begin{array}{@{}ll@{}}
    QNdeadLeaves(t-1) + (MSfm(t) - MSfm(t-1)) * \frac{ANFM * INNvec(t-1) + BNFM}{100}, & \text{if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN, JOUR\_MATURITE, GRAIN\_MUR, JOUR\_RECOLTE\} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:QNdeadLeaves}$$

> $$QNTg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    coef\_tot\_a * QNAcr(t) + BQNT, & \text{if}\ Stade(t) \in \{JOUR\_LEVEE\} \\
    QNTg(t-1) + Nabs(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNTg}$$

> $$QNTr(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    QNTg(t) - QNdeadLeaves(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNTr}$$

> $$QNAr(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    \frac{QNTr(t) - BQNT}{coef\_tot\_a}, & \text{otherwise}
  \end{array}\right. 
\label{eq:QNAr}$$

> $$QNAg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    \frac{QNTg(t) - BQNT}{coef\_tot\_a}, & \text{otherwise}
  \end{array}\right. 
\label{eq:QNAg}$$

> $$DeltaQNAg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    QNAg(t) - QNAg(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:DeltaQNAg}$$

> $$Nar(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\}) \| (MSAr(t) \le 1e^{-15}) \\
    \frac{100 * QNAr(t)}{MSAr(t)}, & \text{otherwise} 
  \end{array}\right. 
\label{eq:Nar}$$

> $$INNvec(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\}) \| (NAcr(t) \le 1e^{-15}) \\
    \frac{Nar(t)}{NAcr(t)}, & \text{otherwise} 
  \end{array}\right. 
\label{eq:INNvec}$$

> $$AireINNvec(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    1, & \text{if}\ Stade(t) \in \{JOUR\_LEVEE\} \\
    \max\left(
        \begin{array}{@{}ll@{}}
            0 \\
            Tmoy(t) 
        \end{array}\right) * \frac{\min\left(
            \begin{array}{@{}ll@{}}
                1 \\
                INNvec(t) 
            \end{array}\right) + \min\left(
                \begin{array}{@{}ll@{}}
                    1 \\
                    INNvec(t-1)
                \end{array}\right)}{2}, & \text{otherwise}
  \end{array}\right. 
\label{eq:AireINNvec}$$

> $$SommeAireINNvec(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    1, & \text{if}\ Stade(t) \in \{JOUR\_LEVEE\} \\
    SommeAireINNvec(t-1) + AireINNvec(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:SommeAireINNvec}$$

> $$INNintvec(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    INNvec(t), & \text{if}\ Stade(t) \in \{JOUR\_LEVEE\} \\
    \frac{SommeAireINNvec(t)}{STlevee(t)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:INNintvec}$$

> $$partQNGreen(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    1, & \text{if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON\} \\
    \min\left(
        \begin{array}{@{}ll@{}}
            1 \\
            \max\left(
                \begin{array}{@{}ll@{}}
                    0 \\
                    ApartQNGreen + BpartQNGreen * STsemis(t) 
                \end{array}\right)  
        \end{array}\right) , & \text{otherwise}
  \end{array}\right. 
\label{eq:partQNGreen}$$

> $$QNGreen(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    AQNGreen + BQNGreen * QNAr(t), & \text{if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON\} \\
    partQNGreen(t) * QNAr(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNGreen}$$

> $$partQNLeaves(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    \min\left(
        \begin{array}{@{}ll@{}}
            1 \\
            \max\left(
                \begin{array}{@{}ll@{}}
                    0 \\
                    ApartQNLeaves + BpartQNLeaves * STsemis(t) 
                \end{array}\right)  
        \end{array}\right), & \text{if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON\} \\
     \min\left(
        \begin{array}{@{}ll@{}}
            1 \\
            \max\left(
                \begin{array}{@{}ll@{}}
                    0 \\
                    CpartQNLeaves + DpartQNLeaves * STsemis(t) 
                \end{array}\right)  
        \end{array}\right) , & \text{otherwise}
  \end{array}\right. 
\label{eq:partQNLeaves}$$

> $$QNLeaves(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    QNGreen(t) * partQNLeaves(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNLeaves}$$

> $$partQNPod(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON\} \\
    \min\left(
        \begin{array}{@{}ll@{}}
            1 \\
            \max\left(
                \begin{array}{@{}ll@{}}
                    0 \\
                    ApartQNPod + BpartQNPod * STsemis(t) 
                \end{array}\right)  
        \end{array}\right) , & \text{otherwise}
  \end{array}\right. 
\label{eq:partQNPod}$$

> $$QNPod(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON\} \\
    partQNPod(t) * QNAr(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNPod}$$

</div>

--- 

## Atomic model LPAI <a name="p9"></a>

The **LPAI** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? using ??

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#LPAI_panel'>Show/Hide Model Details</button>
<div id="LPAI_panel" class="collapse">

### Configuring a LPAI Model <a name="p9.1"></a>

#### Dynamics settings <a name="p9.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : LPAI

#### Parameters settings <a name="p9.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantRape`](#annex.p.plt) (`ALAI`, `BLAI`, `EILMAX`, `K`, `TMINEB0`, `TMINEB1`, `AEB`, `BEB`, `CEB`, `DEB`, `EEB`, `AMSPod`, `BMSPod`, `ApartMSPodWall`, `BpartMSPodWall`, `APAI`, `BPAI`, `EIPMAX`, `ARG`, `BRG`, `Ec`) 

#### Input settings <a name="p9.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **QNLeaves** : Quantité d'azote contenu dans les feuilles (kgN/ha) (sync)
* **QNPod** : Quantité d'azote contenu dans les siliques (kgN/ha) (sync)
* **STsemis** : Somme des températures depuis le semis (°C) (sync)
* **Stade** : Stade phénologique actuel (sans unité) (see [shared defined stage](#annex.pheno.stages)) (sync)
* **INNvec** : INN journalier (sans unité) (sync)
* **RG** : Rayonnement globale journalier (MJ/m²/j) (sync)
* **Tmoy** : Température moyenne journalière (°C/j) (sync)

#### Output settings <a name="p9.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **LAI** : Leaf Area Index (sans unité)
* **EiLeaves** : Efficience d'interception du rayonnement des feuilles (sans unité)
* **EbLeaves** : Efficience de conversion du rayonnement des feuilles (gMS/MJ)
* **MSPod** : Biomasse des siliques (kgMS/ha)
* **partMSPodWall** : Part de la biomasse des cosses de siliques sur la biomasse totale des siliques (sans unité)
* **MSPodWall** : Biomasse des cosses de silique (kgMS/ha)
* **PAI** : Pod Area Index (LAI pour les cosses de silique) (sans unité)
* **EiPod** : Efficience d'interception du rayonnement des siliques (sans unité)
* **EbPod** : Efficience de conversion du rayonnement des siliques (gMS/MJ)
* **RGintercept** : Rayonnement actif journalier intercepté par les feuilles et les siliques (MJ/m²/j)

#### Observation settings <a name="p9.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p9.1.6"></a>

None

### Details <a name="p9.2"></a>

#### State variables equations <a name="p9.2.1"></a>

> $$LAI(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    e^{ALAI + BLAI * \log(QNLeaves(t))}, & \text{otherwise}
  \end{array}\right. 
\label{eq:LAI}$$

> $$EiLeaves(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    EILMAX * (1 - e^{-K * LAI(t)}), & \text{otherwise}
  \end{array}\right. 
\label{eq:EiLeaves}$$

> $$EbLeaves(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    1, & \text{else if}\ Tmoy(t) < TMINEB0 \\
    (1 - (\frac{Tmoy(t) - CEB}{DEB - EEB})^2) * 3.5 * \min\left(\begin{array}{@{}ll@{}}
                                                              1 \\
                                                              AEB * INNvec(t) + BEB 
                                                      \end{array}\right), & \text{else if}\ Tmoy(t) < TMINEB1 \\
    (1 - (\frac{Tmoy(t) - CEB}{EEB - CEB})^2) * 3.5 * \min\left(\begin{array}{@{}ll@{}}
                                                              1 \\
                                                              AEB * INNvec(t) + BEB 
                                                      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:EbLeaves}$$

> $$MSPod(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON\} \\
    e^{AMSPod + BMSPod * \log(QNPod(t))}, & \text{otherwise}
  \end{array}\right. 
\label{eq:MSPod}$$

> $$partMSPodWall(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON\} \\
    \min\left(\begin{array}{@{}ll@{}}
      1 \\
      \max\left(\begin{array}{@{}ll@{}}
        0 \\
        ApartMSPodWall + BpartMSPodWall * STsemis(t) 
      \end{array}\right)
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:partMSPodWall}$$

> $$MSPodWall(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON\} \\
    MSPod(t) * partMSPodWall(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:MSPodWall}$$

> $$PAI(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON\} \\
    e^{APAI + BPAI * \log(MSPodWall(t))}, & \text{otherwise}
  \end{array}\right. 
\label{eq:PAI}$$

> $$EiPod(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    EIPMAX * (1 - e^{-K * PAI(t)}), & \text{otherwise}
  \end{array}\right. 
\label{eq:EiPod}$$

> $$EbPod(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    1, & \text{else if}\ Tmoy(t) < TMINEB0 \\
    (1 - (\frac{Tmoy(t) - CEB}{DEB - EEB})^2) * (ARG * (Ec * RG(t)) + BRG) * \min\left(\begin{array}{@{}ll@{}}
                                                              1 \\
                                                              AEB * INNvec(t) + BEB 
                                                      \end{array}\right), & \text{else if}\ Tmoy(t) < TMINEB1 \\
    (1 - (\frac{Tmoy(t) - CEB}{EEB - CEB})^2) * (ARG * (Ec * RG(t)) + BRG) * \min\left(\begin{array}{@{}ll@{}}
                                                              1 \\
                                                              AEB * INNvec(t) + BEB 
                                                      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:EbPod}$$

> $$RGintercept(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    RG(t) * Ec * (EiLeaves(t) + EiPod(t)), & \text{else if}\ (EiLeaves(t) + EiPod(t)) <= 0.95 \\
    RG(t) * Ec * (\max\left(\begin{array}{@{}ll@{}}
                          0 \\
                          0.95 - EiPod(t) 
                  \end{array}\right) + EiPod(t)), & \text{otherwise}
  \end{array}\right. 
\label{eq:RGintercept}$$

</div>

--- 

## Atomic model MSQN <a name="p10"></a>

The **MSQN** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? using ??

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#MSQN_panel'>Show/Hide Model Details</button>
<div id="MSQN_panel" class="collapse">

### Configuring a MSQN Model <a name="p10.1"></a>

#### Dynamics settings <a name="p10.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : MSQN

#### Parameters settings <a name="p10.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantRape`](#annex.p.plt) (`AMST`, `BMST`, `SFCOTY`, `SLACOTY`, `Ec`, `tNc_Nc`, `coef_Nc`, `pente_Nc`, `ApartMSLeaves`, `BpartMSLeaves`, `CpartMSLeaves`, `DpartMSLeaves`) 

#### Input settings <a name="p10.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **STsemis** : Somme des températures depuis le semis (°C) (sync)
* **Stade** : Stade phénologique actuel (sans unité) (see [shared defined stage](#annex.pheno.stages)) (sync)
* **MSfm** : Cumul de biomasse de feuilles mortes (kgMS/ha) (sync)
* **DensiteSemis** : Densité de semis à la date de semis, sinon 0 (plante/m²) (sync)
* **EiLeaves** : Efficience d'interception du rayonnement des feuilles (sans unité) (Nosync)
* **EbLeaves** : Efficience de conversion du rayonnement des feuilles (gMS/MJ) (Nosync)
* **EiPod** : Efficience d'interception du rayonnement des siliques (sans unité) (Nosync)
* **EbPod** : Efficience de conversion du rayonnement des siliques (gMS/MJ) (Nosync)
* **RG** : Rayonnement globale journalier (MJ/m²/j) (Nosync)
* **stressH** : Coefficient de stress hydrique du sol (sans unité) (Nosync)

#### Output settings <a name="p10.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **MSTg** : Biomasse plante entière générée (kgMS/ha)
* **MSTr** : Biomasse plante entière réelle (kgMS/ha)
* **MSAr** : Biomasse aérienne réelle (kgMS/ha)
* **MSAg** : Biomasse aérienne générée (kgMS/ha)
* **DeltaMSAg** : Gain de biomasse aérienne générée journalier (kgMS/ha/j)
* **NAcr** : Teneur en azote critique de la biomasse aérienne réelle (%)
* **QNAcr** : Quantité d'azote critique dans la biomasse aérienne réelle (kgN/ha)
* **NAcg** : Teneur en azote critique de la biomasse aérienne générée (%)
* **QNAcg** : Quantité d'azote critique dans la biomasse aérienne générée (kgN/ha)
* **DeltaQNAcg** : Gain de quantité d'azote critique journalier dans la biomasse aérienne générée (kgN/ha/j)
* **partMSLeaves** : Proportion de biomasse de feuilles dans la biomasse aérienne réelle (sans unité).

#### Observation settings <a name="p10.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p10.1.6"></a>

None

### Details <a name="p10.2"></a>

#### State variables equations <a name="p10.2.1"></a>

> $$MSTg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU\} \\
    AMST * SFCOTY * SLACOTY * densiteSemis * 10 + BMST, & \text{else if}\ Stade(t) \in \{JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE\} \\
    MSTg(t-1) + RG(t-1) * Ec * 10 * stressH(t-1) * (EiLeaves(t-1) * EbLeaves(t-1) + EiPod(t-1) * EbPod(t-1)), & \text{else if}\ (EiLeaves(t-1) + EiPod(t-1)) <= 0.95 \\
    MSTg(t-1) + RG(t-1) * Ec * 10 * stressH(t-1) * (EiPod(t-1) * EbPod(t-1) + \max\left(\begin{array}{@{}ll@{}}
                                                                                0 \\
                                                                                0.95 - EiPod(t-1) 
                                                                              \end{array}\right) * EbLeaves(t-1)), & \text{otherwise}
  \end{array}\right. 
\label{eq:MSTg}$$
> with 
> `densiteSemis` the last strictly positive value of `DensiteSemis` input

> $$MSTr(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    MSTg(t), & \text{else if}\ Stade(t) \in \{JOUR\_LEVEE\} \\
    MSTg(t) - MSfm(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:MSTr}$$

> $$MSAr(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    \frac{MSTr(t) - BMST}{AMST}, & \text{otherwise}
  \end{array}\right. 
\label{eq:MSAr}$$

> $$MSAg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    MSAr(t) + MSfm(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:MSAg}$$

> $$DeltaMSAg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    MSAg(t) - MSAg(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:DeltaMSAg}$$

> $$NAcr(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    \min\left(\begin{array}{@{}ll@{}}
        tNc\_Nc \\
        \max\left(\begin{array}{@{}ll@{}}
            0 \\
            coef\_Nc * (\frac{MSAr(t)}{1000})^{pente\_Nc}
        \end{array}\right)
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:NAcr}$$

> $$QNAcr(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    \frac{MSAr(t) * NAcr(t)}{100}, & \text{otherwise}
  \end{array}\right. 
\label{eq:QNAcr}$$

> $$NAcg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    \min\left(\begin{array}{@{}ll@{}}
        tNc\_Nc \\
        \max\left(\begin{array}{@{}ll@{}}
            0 \\
            coef\_Nc * (\frac{MSAg(t)}{1000})^{pente\_Nc}
        \end{array}\right)
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:NAcg}$$

> $$QNAcg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    \frac{MSAg(t) * NAcg(t)}{100}, & \text{otherwise}
  \end{array}\right. 
\label{eq:QNAcg}$$

> $$DeltaQNAcg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    QNAcg(t) - QNAcg(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:DeltaQNAcg}$$

> $$partMSLeaves(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    0, & \text{else if}\ Stade(t) \in \{JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON\} \\
    \min\left(\begin{array}{@{}ll@{}}
        1 \\
        \max\left(\begin{array}{@{}ll@{}}
            0 \\
            ApartMSLeaves + BpartMSLeaves * STsemis(t) 
        \end{array}\right)
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:partMSLeaves}$$

</div>

--- 

## Atomic model NitrogenSoilRape <a name="p11"></a>

The **NitrogenSoilRape** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? using ??

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#NitrogenSoilRape_panel'>Show/Hide Model Details</button>
<div id="NitrogenSoilRape_panel" class="collapse">

### Configuring a NitrogenSoilRape Model <a name="p11.1"></a>

#### Dynamics settings <a name="p11.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : NitrogenSoilRape

#### Parameters settings <a name="p11.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantRape`](#annex.p.plt) (`a`, `b`, `c`, `d`, `Tref`, `para_kres1`, `C_res`, `C_Nres`, `para_kreshum`, `paraC_Nres1`, `paraC_Nres4`, `f`, `g`, `Y`, `C_Nhum`, `tNorg`, `ep_C0`, `da_C0`, `coef_k2`, `Arg`, `CaCO3`, `gamma`, `delta`, `fsdc`, `alpha`, `beta`, `QNsol_init_C0`, `QNsol_init_tot`, `Prof_Reliquat_tot`, `QNND`) and [ParametersSoilRape](#annex.p.soil) (`date_init_miner_res`, `date_init_N`, `ProfRacmax`)

#### Input settings <a name="p11.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Tmoy** : Température moyenne journalière (°C/j) (sync)
* **ProfRac** : Profondeur d'enracinement (mm) (sync)
* **stressH_C0** : Coefficient de stress hydrique pour la couche C0 (sans unité) (sync)
* **StockEauD_C0** : Quantité d'eau journalière drainée de C0 vers C1 (mm/j) (sync)
* **StockEauD_C1** : Quantité d'eau journalière drainée de C1 vers C2 (mm/j) (sync)
* **StockEauD_C2** : Quantité d'eau journalière drainée de C2 vers la nappe (mm/j) (sync)
* **RUmax_C0** : Quantité d'eau disponible maximum contenue dans la couche C0 (mm) (sync)
* **RUmax_C1** : Quantité d'eau disponible maximum contenue dans la couche C1 (mm) (sync)
* **RUmax_C2** : Quantité d'eau disponible maximum contenue dans la couche C2 (mm) (sync)
* **QNabs_C0** : Quantité d'azote absorbée par la plante entière provenant de la couche C0 (kgN/ha) (sync)
* **QNabs_C1** : Quantité d'azote absorbée par la plante entière provenant de la couche C1 (kgN/ha) (sync)
* **QNEng** : Quantité d'azote biodisponible journalière apportée par les engrais (kgN/ha) (sync)
* **NMinTot** : Azote journalier minéralisé issu des feuilles mortes (pour l'ensemble des cohortes de feuilles) (kgN/ha/j) (sync)

#### Output settings <a name="p11.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **Ft_min** : Fonction de température dans le calcul des jours normalisés (sans unité)
* **JN** : Jours normalisés depuis `date_init_miner_res` (pour la minéralisation des résidus) (j)
* **k_res** : Part des résidus non minéralisée (sans unité)
* **QC_res** : Quantité de carbone organique stockée dans les résidus (kgC/ha)
* **QN_res** : Quantité d'azote organique stocké dans les résidus (kgN/ha)
* **k_micr** : Part des résidus organisés dans la biomasse microbienne (sans unité)
* **QC_micr** : Quantité de carbone organique stocké dans la biomasse microbienne (kgC/ha)
* **QN_micr** : Quantité d'azote organique stocké dans la biomasse microbienne (kgN/ha)
* **k_hum** : Part des résidus organisés dans les humus (sans unité)
* **QC_hum** : Quantité de carbone organique stocké dans les humus (kgC/ha)
* **QN_hum** : Quantité d'azote organique stocké dans les humus (kgN/ha)
* **dQN_Mr** : Quantité journalière d'azote minéralisée issue des résidus (kgN/ha/j)
* **dQC_Mr** : Quantité journalière de carbone minéralisé issue des résidus (kgC/ha/j)
* **dQN_Mh** : Quantité journalière d'azote minéralisée issue des humus (kgN/ha/j)
* **pNlixiv_C0** : Proportion d'azote journalière lixiviée de la couche C0 (sans unité)
* **QNlixiv_C0** : Quantité d'azote journalière lixiviée de la couche C0 (kgN/ha/j)
* **QN_C0** : Quantité d'azote minérale dans la couche C0 (kgN/ha)
* **QNaccess_C0** : Quantité d'azote dans la part de la couche C0 qui est explorée par des racines (kgN/ha)
* **QNaccess_C1** : Quantité d'azote dans la couche C1 (kgN/ha)
* **pNlixiv_C1** : Proportion d'azote journalière lixiviée de la couche C1 (sans unité)
* **QNlixiv_C1** : Quantité d'azote journalière lixiviée de la couche C1 (kgN/ha/j)
* **QN_C2** : Quantité d'azote minérale dans la couche C2 (kgN/ha)
* **pNlixiv_C2** : Proportion d'azote journalière lixiviée de la couche C2 (sans unité)
* **QNlixiv_C2** : Quantité d'azote journalière lixiviée de la couche C2 (kgN/ha/j)
* **TN_C2** : Quantité d'azote par mm de sol dans la couche C2 (kgN/ha/mm sol)
* **QNlixiv_tot** : Quantité d'azote lixiviée définitivement cumulée (issu de C2) (kgN/ha) 
* **QNsol_tot** : Quantité d'azote minérale présente dans l'ensemble de la profondeur du sol (kgN/ha)
* **QNnondispo** : Quantité d'azote minérale non disponible pour la culture sur la profondeur des racines (kgN/ha)
* **QN_offre** : Quantité d'azote minérale dans le sol disponible pour la culture (kgN/ha)

#### Observation settings <a name="p11.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p11.1.6"></a>

None

### Details <a name="p11.2"></a>

#### State variables equations <a name="p11.2.1"></a>

> $$Ft\_min(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Tmoy(t) <= 0.0 \\
    (a + b * e^{c * \frac{Tmoy(t)}{Tref}})^d, & \text{otherwise}
  \end{array}\right. 
\label{eq:Ft_min}$$

> $$JN(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time <= date\_init\_miner\_res \\
    JN(t-1) + Ft\_min(t) * stressH\_C0(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:JN}$$

> $$k\_res(t) = e^{-para\_kres1 * JN(t)}\label{eq:k_res}$$

> $$QC\_res(t) = C\_res * k\_res(t)\label{eq:QC_res}$$

> $$QN\_res(t) = \frac{QC\_res(t)}{C\_Nres}\label{eq:QN_res}$$

> $$k\_micr(t) = coef\_k\_micr * (k\_res(t) - e^{-para\_kreshum * JN(t)}) \label{eq:k_micr}$$
> with 
> $$coef\_k\_micr = \frac{para\_kres1 * Y}{para\_kreshum - para\_kres1}$$

> $$QC\_micr(t) = C\_res * k\_micr(t)\label{eq:QC_micr}$$

> $$QN\_micr(t) = \frac{QC\_micr(t)}{C\_Nmicr} \label{eq:QN_micr}$$
> with 
> $$C\_Nmicr = \left\{
  \begin{array}{@{}ll@{}}
    paraC\_Nres4, & \text{if}\ C\_Nres < paraC\_Nres1 \\
    paraC\_Nres1 + \frac{paraC\_Nres1}{C\_Nres}, & \text{otherwise}
  \end{array}\right.$$

> $$k\_hum(t)=\max\left(\begin{array}{@{}ll@{}}
                  0 \\
                  Y * h + (coef\_khum * (para\_kres1 * e^{-para\_kreshum * JN(t)}) - para\_kreshum * k\_res(t)) 
              \end{array}\right) 
\label{eq:k_hum}$$
> with 
> $$h = 1 - \frac{f * C\_Nres}{g + C\_Nres}$$
> and 
> $$coef\_khum = \frac{Y * h}{para\_kreshum - para\_kres1}$$

> $$QC\_hum(t) = C\_res * k\_hum(t)\label{eq:QC_hum}$$

> $$QN\_hum(t) = \frac{QC\_hum(t)}{C\_Nhum}\label{eq:QN_hum}$$

> $$dQN\_Mr(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time <= date\_init\_N \\
    (QN\_res(t-1) + QN\_hum(t-1) + QN\_micr(t-1)) - (QN\_res(t) + QN\_hum(t) + QN\_micr(t)), & \text{otherwise}
  \end{array}\right. 
\label{eq:dQN_Mr}$$

> $$dQC\_Mr(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time <= date\_init\_N \\
    (QC\_res(t-1) + QC\_hum(t-1) + QC\_micr(t-1)) - (QC\_res(t) + QC\_hum(t) + QC\_micr(t)), & \text{otherwise}
  \end{array}\right. 
\label{eq:dQC_Mr}$$

> $$dQN\_Mh(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (Tmoy(t) < 0) \| (time <= date\_init\_N) \\
    \frac{tNorg}{10} * \frac{ep\_C0}{10} * da\_C0 * (\frac{coef\_k2 * 100000}{(Arg + gamma * 10) * (CaCO3 + delta * 10)}) * fsdc * stressH\_C0(t) * e^{alpha * (Tmoy(t) - Tref)} * beta, & \text{otherwise}
  \end{array}\right. 
\label{eq:dQN_Mh}$$

> $$pNlixiv\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (StockEauD\_C0(t) + RUmax\_C0(t)) == 0.0 \\
    (\frac{StockEauD\_C0(t)}{StockEauD\_C0(t) + \frac{RUmax\_C0(t)}{100}})^{\frac{\frac{ep\_C0}{10}}{2}}, & \text{otherwise}
  \end{array}\right. 
\label{eq:pNlixiv_C0}$$

> $$QNlixiv\_C0(t) = QN\_C0(t-1) * pNlixiv\_C0(t)\label{eq:QNlixiv_C0}$$

> $$QN\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_N \\
    QNsol\_init\_C0, & \text{else if}\ time == date\_init\_N \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        QN\_C0(t-1) + dQN\_Mr(t-1) + dQN\_Mh(t-1) + QNEng(t-1) + NMinTot(t-1) - QNlixiv\_C0(t) - QNabs\_C0(t-1) 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:QN_C0}$$

> $$QNaccess\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    QN\_C0(t) * \frac{ProfRac(t)}{ep\_C0}, & \text{if}\ ProfRac(t) <= ep\_C0 \\
    QN\_C0(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNaccess_C0}$$

> $$QNaccess\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (time < date\_init\_N) \| (ProfRac(t) <= ep\_C0) \\
    \frac{QNsol\_init\_tot - QNsol\_init\_C0}{Prof\_Reliquat\_tot - ep\_C0} * (ProfRac(t) - ep\_C0), & \text{else if}\ time == date\_init\_N \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        QNaccess\_C1(t-1) + QNlixiv\_C0(t-1) - QNlixiv\_C1(t-1) - QNabs\_C1(t-1) + (ProfRac(t) - ProfRac(t-1)) * TN\_C2(t-1) 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNaccess_C1}$$

> $$pNlixiv\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (StockEauD\_C1(t) + RUmax\_C1(t)) == 0 \\
    (\frac{StockEauD\_C1(t)}{StockEauD\_C1(t)+\frac{RUmax\_C1(t)}{100}})^{\frac{\frac{ProfRac(t) - ep\_C0}{10}}{2}}, & \text{otherwise}
  \end{array}\right. 
\label{eq:pNlixiv_C1}$$

> $$QNlixiv\_C1(t) = QNaccess\_C1(t) * pNlixiv\_C1(t)\label{eq:QNlixiv_C1}$$

> $$QN\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_N \\
    \frac{QNsol\_init\_tot - QNsol\_init\_C0}{Prof\_Reliquat\_tot - ep\_C0} * \min\left(\begin{array}{@{}ll@{}}
                                                                              ProfRacmax - ep\_C0 \\
                                                                              ProfRacmax - ProfRac(t) 
                                                                          \end{array}\right), & \text{else if}\ time == date\_init\_N \\
    QN\_C2(t-1) + QNlixiv\_C0(t-1) - QNlixiv\_C2(t-1), & \text{else if}\ ProfRac(t) \leq ep\_C0 \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        QN\_C2(t-1) + QNlixiv\_C1(t-1) - QNlixiv\_C2(t-1) - (ProfRac(t) - ProfRact(t-1)) * TN\_C2(t-1) 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:QN_C2}$$

> $$pNlixiv\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (StockEauD\_C2(t) + RUmax\_C2(t)) == 0 \\
    (\frac{StockEauD\_C2(t)}{StockEauD\_C2(t) + \frac{RUmax\_C2(t)}{100}})^{\frac{\frac{ProfRacmax - ep\_C0}{10}}{2}}, & \text{otherwise}
  \end{array}\right. 
\label{eq:pNlixiv_C2}$$

> $$QNlixiv\_C2(t) = QN\_C2(t-1) * pNlixiv\_C2(t)\label{eq:QNlixiv_C2}$$

> $$TN\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ ProfRac(t) == ProfRacmax \\
    \frac{QN\_C2(t)}{ProfRacmax - \max\left(\begin{array}{@{}ll@{}}
                                      ProfRac(t) \\
                                      ep\_C0 
                                  \end{array}\right)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:TN_C2}$$

> $$QNlixiv\_tot(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (time <= date\_init\_N) \\
    QNlixiv\_tot(t-1) + QNlixiv\_C2(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNlixiv_tot}$$

> $$QNsol\_tot(t) = QN\_C0(t) + QNaccess\_C1(t) + QN\_C2(t)\label{eq:QNsol_tot}$$

> $$QNnondispo(t) = \frac{QNND * \frac{ProfRac(t)}{10}}{10}\label{eq:QNnondispo}$$

> $$QN\_offre(t)=\max\left(\begin{array}{@{}ll@{}}
                  0 \\
                  QNaccess\_C0(t) + QNaccess\_C1(t) - QNnondispo(t) 
              \end{array}\right) 
\label{eq:QN_offre}$$

</div>

--- 

## Atomic model Phenologie <a name="p12"></a>

The **Phenologie** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? using ??

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Phenologie_panel'>Show/Hide Model Details</button>
<div id="Phenologie_panel" class="collapse">

### Configuring a Phenologie Model <a name="p12.1"></a>

#### Dynamics settings <a name="p12.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : Phenologie

#### Parameters settings <a name="p12.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantRape`](#annex.p.plt) (`jourLevee`, `somme_temperature_levee`, `jourFloraison`, `somme_temperature_G4`, `somme_temperature_FRG`, `jourMaturite`, `LAT`, `AF1`, `BTF1`, `BPF1`, `BLF1`, `moyTjanuary1st`, `moyPjanuary1st`)

#### Input settings <a name="p12.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Tmoy** : Température moyenne journalière (°C/j) (sync)
* **Semis** : sowing indicator, value is 0 exept on sowing day (??) (sync)
* **Recolte** : Vaut 1 quand la récolte a été effectuée, sinon vaut 0 (sans unité) (sync)

#### Output settings <a name="p12.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **JulianDays** : Jour julien depuis le 1er janvier de l'année en cours (j)
* **sunRevolution** : Angle de révolution du soleil (radian)
* **sunDeclination** : Angle de déclinaison du soleil (radian)
* **dayLength** : Durée du jour (h)
* **STjanuary1st** : Somme des températures depuis le 1er janvier de l'année de récolte (°C)
* **SPjanuary1st** : Somme d'heure de jour depuis le 1er janvier de l'année de récolte (h)
* **moySTjanuary1st** : Moyenne des températures depuis le 1er janvier de l'année de récolte (°C/j)
* **moySPjanuary1st** : Moyenne du nombre d'heure de jour par jour depuis le 1er janvier de l'année de récolte (h/j)
* **STflo_seuil** : Seuil de somme de température pour passer au stade F1 (°C)
* **Stade** : Stade phénologique actuel (sans unité) (see [shared defined stage](#annex.pheno.stages))
* **STlevee** : Somme des températures depuis la levée (°C)
* **STfloraison** : Somme des températures depuis le stade F1 (°C)
* **Tmin3** : Température moyenne minimale des 3 derniers jours (°C)
* **STsemis** : Somme des températures depuis le semis (°C)


#### Observation settings <a name="p12.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p12.1.6"></a>

None

### Details <a name="p12.2"></a>

#### State variables equations <a name="p12.2.1"></a>

> $$JulianDays(t)= Day of the year 
\label{eq:JulianDays}$$

> $$sunRevolution(t) = 0.2163108 + 2 * \arctan(0.9671396 * \tan(0.00860) * (JulianDays(t) - 186))\label{eq:sunRevolution}$$

> $$sunDeclination(t) = \arcsin(0.39795 * \cos(sunRevolution(t)))\label{eq:sunDeclination}$$

> $$dayLength(t) = 24 - \frac{24}{PI} * \arccos( \frac{\sin(p * \frac{PI}{180}) + \sin(LAT * \frac{PI}{180}) * \sin(sunDeclination(t))}{\cos(LAT * \frac{PI}{180}) * \cos(sunDeclination(t))})\label{eq:dayLength}$$
> with 
> $$PI = 3.14159265$$
> and 
> $$p = 0.833$$

> $$STjanuary1st(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time before the 1st january and same year as the start of simulation  \\
    STjanuary1st(t-1) + \max\left(\begin{array}{@{}ll@{}}
                            0 \\
                            Tmoy(t) 
                        \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:STjanuary1st}$$

> $$SPjanuary1st(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time before the 1st january and same year as the start of simulation \\
    SPjanuary1st(t-1) + dayLength(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:SPjanuary1st}$$

> $$moySTjanuary1st(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time before the 1st january and same year as the start of simulation \\
    \frac{STjanuary1st(t)}{JulianDays(t)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:moySTjanuary1st}$$

> $$moySPjanuary1st(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time before the 1st january and same year as the start of simulation \\
    \frac{SPjanuary1st(t)}{JulianDays(t)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:moySPjanuary1st}$$

> $$STflo\_seuil(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time before the 1st january and same year as the start of simulation \\
    AF1 + BTF1 * moyTjanuary1st + BPF1 * moyPjanuary1st + BLF1 * LAT, & \text{otherwise}
  \end{array}\right. 
\label{eq:STflo_seuil}$$


> $$Stade(t)$$
> Valeur initiale:  `SOL_NU`
> ![Stade(t)](pheno.svg "Stade(t)")

> $$STlevee(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    STlevee(t-1) + \max\left(\begin{array}{@{}ll@{}}
                            0 \\
                            Tmoy(t) 
                        \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:STlevee}$$

> $$STfloraison(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    STfloraison(t-1) + \max\left(\begin{array}{@{}ll@{}}
                            0 \\
                            Tmoy(t) 
                        \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:STfloraison}$$

> $$Tmin3(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS\} \\
    \min\left(\begin{array}{@{}ll@{}}
        Tmoy(t) \\
        Tmoy(t-1) \\
        Tmoy(t-2)
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:Tmin3}$$

> $$STsemis(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU\} \\
    STsemis(t-1) + \max\left(\begin{array}{@{}ll@{}}
                            0 \\
                            Tmoy(t) 
                        \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:STsemis}$$

</div>

--- 

## Atomic model Rendement <a name="p13"></a>

The **Rendement** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? using ??

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Rendement_panel'>Show/Hide Model Details</button>
<div id="Rendement_panel" class="collapse">

### Configuring a Rendement Model <a name="p13.1"></a>

#### Dynamics settings <a name="p13.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : Rendement

#### Parameters settings <a name="p13.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantRape`](#annex.p.plt) (`NC1G4`, `NC2G4`, `ANGPOT`, `BNGPOT`, `XII`, `AISN`, `BISN`, `CISN`, `NGSEUIL`, `PMGmax_var`, `PMGEAU`, `PMGTMOY`, `PMGQP`, `PMGTMAX`)

#### Input settings <a name="p13.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Pluie** : Pluviométrie journalière (mm/j) (sync)
* **ETP** : Evapotranspiration potentielle de Penman journalière (mm/j) (sync)
* **RG** : Rayonnement globale journalier (MJ/m²/j) (sync)
* **Tmoy** : Température moyenne journalière (°C/j) (sync)
* **Tmax** : Température journalière maximum (°C) (sync)
* **Stade** : Stade phénologique actuel (sans unité) (see [shared defined stage](#annex.pheno.stages)) (sync)
* **STfloraison** : Somme des températures depuis le stade F1 (°C)
* **INNvec** : INN journalier (sans unité) (sync)
* **MSAr** : Biomasse aérienne réelle (kgMS/ha) (sync)
* **QNAr** : Quantité d'azote dans la biomasse aérienne réelle (kgN/ha) (sync)
* **RGintercept** : Rayonnement actif journalier intercepté par les feuilles et les siliques (MJ/m²/j) (sync)


#### Output settings <a name="p13.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **QNAr_G4** : Quantité d'azote dans la biomasse aérienne réelle au stade G4 (kgN/ha)
* **MSAr_G4** : Biomasse aérienne réelle au stade G4 (kgMS/ha)
* **SOMRGpot** : Cumul du rayonnement global intercepté depuis Floraison jusqu'à G4 (MJ/m²)
* **Hydrique** : Bilan hydrique (Pluie - ETP) cumulé depuis Floraison jusqu'à G4 (mm)
* **QP** : Rayonnement global intercepté cumulé par degré jour cumulé du stade F1 a G4 (J/cm²/°C)
* **ST25** : Somme des températures maximum journalières supérieures à 25°C entre les stades G4 et FRG (°C)
* **ST10** : Somme de degrés jours en base 10°C entre les stades G4 et FRG (°C)
* **INNflo** : INN au stade F1 (sans unité)
* **NGr** : Nombre de grains par m² au stade Récolte (grains/m²)
* **PMGr** : Poids de 1000 grains au stade Récolte (g/1000grains)
* **Rend** : Rendement en grains au stade Récolte (qtx/ha)

#### Observation settings <a name="p13.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p13.1.6"></a>

None

### Details <a name="p13.2"></a>

#### State variables equations <a name="p13.2.1"></a>

> $$QNAr\_G4(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    QNAr(t), & \text{else if}\ Stade(t) \in \{JOUR\_G4\} \\
    QNAr\_G4(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNAr_G4}$$

> $$MSAr\_G4(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    MSAr(t), & \text{else if}\ Stade(t) \in \{JOUR\_G4\} \\
    MSAr\_G4(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:MSAr_G4}$$

> $$SOMRGpot(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    SOMRGpot(t-1) + RGintercept(t), & \text{else if}\ (Stade(t) \in \{FLORAISON, JOUR\_G1, G1\_G4\}) \& (STfloraison(t) < 600) \\
    SOMRGpot(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:SOMRGpot}$$

> $$Hydrique(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    Hydrique(t-1) + Pluie(t) - ETP(t), & \text{else if}\ (Stade(t) \in \{FLORAISON, JOUR\_G1, G1\_G4\}) \& (STfloraison(t) < 600) \\
    Hydrique(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:Hydrique}$$

> $$QP(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    QP(t-1) + \frac{RG(t) * 100}{\max\left(\begin{array}{@{}ll@{}}
                                      0.0001 \\
                                      Tmoy(t) 
                                  \end{array}\right)}, & \text{else if}\ (Stade(t) \in \{FLORAISON, JOUR\_G1, G1\_G4\}) \& (STfloraison(t) < 600) \\
    QP(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:QP}$$

> $$ST25(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    ST25(t-1) + \max\left(\begin{array}{@{}ll@{}}
                    0 \\
                    Tmax(t) - 25 
                \end{array}\right), & \text{else if}\ (Stade(t) \in \{FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN, JOUR\_MATURITE, GRAIN\_MUR\}) \& (600 < STfloraison(t) <= 1350) \\
    ST25(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:ST25}$$

> $$ST10(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    ST10(t-1) + \max\left(\begin{array}{@{}ll@{}}
                    0 \\
                    Tmoy(t) - 10 
                \end{array}\right), & \text{else if}\ (Stade(t) \in \{FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN, JOUR\_MATURITE, GRAIN\_MUR\}) \& (600 < STfloraison(t) <= 1350) \\
    ST10(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:ST10}$$

> $$INNflo(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    INNvec(t), & \text{else if}\ (Stade(t) \in \{JOUR\_F1\}) \\
    INNflo(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:INNflo}$$

> $$NGr(t)=\left\{
  \begin{array}{@{}ll@{}}
    ISN * NGpot, & \text{if}\ Stade(t) \in \{JOUR\_RECOLTE\} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:NGr}$$
> with 
> $$ISN =\left\{
  \begin{array}{@{}ll@{}}
    1, & \text{if}\ INNflo(t) \geq XII \\
    \min\left(\begin{array}{@{}ll@{}}
                    1 \\
                    AISN + BISN * INNflo(t) + CISN * INN\_G4 
                \end{array}\right), & \text{otherwise}
  \end{array}\right.$$
> $$INN\_G4 = \frac{Na\_G4}{(NC1G4 * (\frac{MSAr\_G4(t)}{1000})^{-NC2G4}))}$$
> $$Na\_G4 = \frac{QNAr\_G4(t) * 100}{MSAr\_G4(t)}$$
> $$NGpot = BNGPOT + (ANGPOT * SOMRGpot(t))$$

> $$PMGr(t)=\left\{
  \begin{array}{@{}ll@{}}
    \min\left(\begin{array}{@{}ll@{}}
      PMGmax\_var \\
      PMGmax\_var + Hydrique(t) * PMGEAU + ST10(t) * PMGTMOY + QP(t) * PMGQP + ST25(t) * PMGTMAX 
    \end{array}\right) * \max\left(\begin{array}{@{}ll@{}}
                            1 \\
                            \frac{NGSEUIL}{NGr(t)} 
                        \end{array}\right), & \text{if}\ Stade(t) \in \{JOUR\_RECOLTE\} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:PMGr}$$

> $$Rend(t)=\left\{
  \begin{array}{@{}ll@{}}
    \frac{PMGr(t) * NGr(t)}{10000}, & \text{if}\ Stade(t) \in \{JOUR\_RECOLTE\} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:Rend}$$

</div>

--- 

## Atomic model WaterSoilRape <a name="p16"></a>

The **WaterSoilRape** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? using ??

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#WaterSoilRape_panel'>Show/Hide Model Details</button>
<div id="WaterSoilRape_panel" class="collapse">

### Configuring a WaterSoilRape Model <a name="p16.1"></a>

#### Dynamics settings <a name="p16.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : WaterSoilRape

#### Parameters settings <a name="p16.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [ParametersSoilRape](#annex.p.soil) (`Trans`, `ApportEau_seuil`, `JSA_seuil`, `EVAPO`, `ep_C0`, `RUmax`, `RU_init`, `MottesDelta`, `Mottes_seuil`, `coef_Mottes`, `stressH1`, `stressH2`, `stressH3`, `stressH4`) and [`ParametersPlantRape`](#annex.p.plt) (`date_init_H`, `date_init_MS`, `ProfRacmax`)

#### Input settings <a name="p16.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **ProfRac** : Profondeur d'enracinement (mm) (sync)
* **Pluie** : Pluviométrie journalière (mm/j) (Nosync)
* **ETP** : Evapotranspiration potentielle de Penman journalière (mm/j) (Nosync)
* **I** : Irrigation (mm/j) (Nosync)
* **EiLeaves** : Efficience d'interception du rayonnement des feuilles (sans unité) (Nosync)

#### Output settings <a name="p16.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **TR** : Quantité d'eau journalière transpirée (mm/j)
* **TR_C0** : Quantité d'eau journalière transpirée issue de C0 (mm/j)
* **JSA** : Nombre de jours cumulés depuis le dernier jour de pluie (et d'irrigation) significatif (j)
* **rEV** : Réduction de l'évaporation journalière selon le nombre de jour de sec (sans unité)
* **EV** : Quantité d'eau journalière évaporée (mm/j)
* **RUmax_C0** : Quantité d'eau disponible maximum contenue dans la couche C0 (mm)
* **RUavdr_C0** : Quantité d'eau disponible dans la couche C0 avant drainage (mm)
* **RU_C0** : Quantité d'eau disponible dans la couche C0 (mm)
* **FTSW_C0** : Niveau de remplissage en eau de C0 (sans unité)
* **stressH_C0** : Coefficient de stress hydrique pour la couche C0 (sans unité)
* **StockEauD_C0** : Quantité d'eau journalière drainée de C0 vers C1 (mm/j)
* **RUmax_C1** : Quantité d'eau disponible maximum contenue dans la couche C1 (mm)
* **TR_C1** : Quantité d'eau journalière transpirée issue de C1 (mm/j)
* **tEau_C2** : Teneur en eau de C2 (mm eau / mm sol)
* **RUavdr_C1** : Quantité d'eau disponible dans la couche C1 avant drainage (mm)
* **RU_C1** : Quantité d'eau disponible dans la couche C1 (mm)
* **FTSW_C1** : Niveau de remplissage en eau de C1 (sans unité)
* **StockEauD_C1** : Quantité d'eau journalière drainée de C1 vers C2 (mm/j)
* **RUmax_C2** : Quantité d'eau disponible maximum contenue dans la couche C2 (mm)
* **RUavdr_C2** : Quantité d'eau disponible dans la couche C2 avant drainage (mm)
* **RU_C2** : Quantité d'eau disponible dans la couche C2 (mm)
* **StockEauD_C2** : Quantité d'eau journalière drainée de C2 vers la nappe (mm/j)
* **StockEauD_tot** : Cumul de l'eau drainée issue de C2 depuis l'initialisation du module eau (mm)
* **FTSW** : Niveau de remplissage en eau du sol (sans unité)
* **stressH** : Coefficient de stress hydrique du sol (sans unité)

#### Observation settings <a name="p16.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p16.1.6"></a>

None

### Details <a name="p16.2"></a>

#### State variables equations <a name="p16.2.1"></a>

> $$TR(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_H \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        \min\left(\begin{array}{@{}ll@{}}
            RU\_C0(t-1) + RU\_C1(t-1) \\
            ETP(t-1) * Trans * stressH(t-1) 
        \end{array}\right) 
    \end{array}\right), & \text{else if}\ time < date\_init\_MS \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        \min\left(\begin{array}{@{}ll@{}}
            RU\_C0(t-1) + RU\_C1(t-1) \\
            ETP(t-1) * Trans * stressH(t-1) * EiLeaves(t-1)
        \end{array}\right) 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:TR}$$

> $$TR\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (time < date\_init\_H) \| ((RU\_C0(t-1) + RU\_C1(t-1)) == 0.0) \\
    TR(t) * \frac{RU\_C0(t-1)}{RU\_C0(t-1) + RU\_C1(t-1)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:TR_C0}$$

> $$JSA(t)=\left\{
  \begin{array}{@{}ll@{}}
    JSA(t-1) + 1, & \text{if}\ (Pluie(t-1) + I(t-1) < ApportEau\_seuil) \& (time >= date\_init\_H) \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:JSA}$$

> $$rEV(t)=\left\{
  \begin{array}{@{}ll@{}}
    (JSA(t) + 1)^{EVAPO} - JSA(t)^{EVAPO}, & \text{if}\ JSA(t) >= JSA\_seuil \\
    1, & \text{otherwise}
  \end{array}\right. 
\label{eq:rEV}$$

> $$EV(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_H \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        ETP(t-1) * (1 - EiLeaves(t-1)) * rEV(t) 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:EV}$$

> $$RUmax\_C0(t)=\max\left(
  \begin{array}{@{}ll@{}}
    0 \\
    ep\_C0 * \frac{RUmax}{ProfRacmax}
  \end{array}\right)
\label{eq:RUmax_C0}$$  

> $$RUavdr\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_H \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        ep\_C0 * \frac{RU\_init}{ProfRacmax} 
    \end{array}\right), & \text{else if}\ time == date\_init\_H \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        RU\_C0(t-1) + Pluie(t-1) + I(t-1) - EV(t) - TR\_C0(t) 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RUavdr_C0}$$

> $$RU\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_H \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        ep\_C0 * \frac{RU\_init}{ProfRacmax} 
    \end{array}\right), & \text{else if}\ time == date\_init\_H \\
    \min\left(\begin{array}{@{}ll@{}}
        RUmax\_C0(t) \\
        RUavdr\_C0(t) 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RU_C0}$$

> $$FTSW\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_H \\
    coef\_Mottes * \frac{RU\_C0(t)}{RUmax\_C0(t)}, & \text{else if}\ MottesDelta > Mottes\_seuil \\
    \frac{RU\_C0(t)}{RUmax\_C0(t)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:FTSW_C0}$$

> $$stressH\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    1, & \text{if}\ time < date\_init\_H \\
    \max\left(\begin{array}{@{}ll@{}}
        0.4 \\
        \frac{stressH1}{1 + stressH2 * e^{-stressH3 * (FTSW\_C0(t) - stressH4)}} 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:stressH_C0}$$

> $$StockEauD\_C0(t)=\max\left(
  \begin{array}{@{}ll@{}}
    0 \\
    RUavdr\_C0(t) - RUmax\_C0(t)
  \end{array}\right)
\label{eq:StockEauD_C0}$$  

> $$RUmax\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ ProfRac(t) <= ep\_C0 \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        (ProfRac(t) - ep\_C0) * \frac{RUmax}{ProfRacmax}
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RUmax_C1}$$

> $$TR\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (time < date\_init\_H) \| (RU\_C0(t-1) + RU\_C1(t-1) == 0.0) \\
    TR(t) * \frac{RU\_C1(t-1)}{\max\left(\begin{array}{@{}ll@{}}
                                    0.001 \\
                                    RU\_C0(t-1) + RU\_C1(t-1) 
                                \end{array}\right)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:TR_C1}$$

> $$tEau\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ ProfRac(t) >= ProfRacmax \\
    \frac{RU\_C2(t-1)}{ProfRacmax - \max\left(\begin{array}{@{}ll@{}}
                                        ep\_C0 \\
                                        ProfRac(t-1) 
                                    \end{array}\right)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:tEau_C2}$$

> $$RUavdr\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ ProfRac(t) <= ep\_C0 \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        RU\_C1(t-1) + (ProfRac(t) - ProfRac(t-1)) * tEau\_C2(t) + StockEauD\_C0(t) - TR\_C1(t) 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RUavdr_C1}$$

> $$RU\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_H \\
    \min\left(\begin{array}{@{}ll@{}}
        RUmax\_C1(t) \\
        RUavdr\_C1(t) 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RU_C1}$$

> $$FTSW\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ RUmax\_C1(t) == 0.0 \\
    coef\_Mottes * \frac{RU\_C1(t)}{RUmax\_C1(t)}, & \text{if}\ MottesDelta > Mottes\_seuil \\
    \frac{RU\_C1(t)}{RUmax\_C1(t)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:FTSW_C1}$$

> $$StockEauD\_C1(t)=\max\left(
  \begin{array}{@{}ll@{}}
    0 \\
    RUavdr\_C1(t) - RUmax\_C1(t)
  \end{array}\right)
\label{eq:StockEauD_C1}$$  

> $$RUmax\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        (ProfRacmax - ep\_C0) * \frac{RUmax}{ProfRacmax}
    \end{array}\right), & \text{if}\ ProfRac(t) <= ep\_C0 \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        (ProfRacmax - ProfRac(t)) * \frac{RUmax}{ProfRacmax}
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RUmax_C2}$$

> $$RUavdr\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_H \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        ProfRacmax -  \max\left(\begin{array}{@{}ll@{}}
                          ep\_C0 \\
                          ProfRac(t)
                      \end{array}\right) * \frac{RU\_init}{ProfRacmax}
    \end{array}\right), & \text{else if}\ time == date\_init\_H \\
    RU\_C2(t-1) + StockEauD\_C0(t), & \text{else if}\ ProfRac(t) <= ep\_C0 \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        RU\_C2(t-1) - (ProfRac(t) - ProfRac(t-1)) * tEau\_C2(t) + StockEauD\_C1(t)
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RUavdr_C2}$$

> $$RU\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_H \\
    \min\left(\begin{array}{@{}ll@{}}
        RUmax\_C2(t) \\
        RUavdr\_C2(t) 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RU_C2}$$

> $$StockEauD\_C2(t)=\max\left(
  \begin{array}{@{}ll@{}}
    0 \\
    RUavdr\_C2(t) - RUmax\_C2(t)
  \end{array}\right)
\label{eq:StockEauD_C2}$$  

> $$StockEauD\_tot(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_H \\
    StockEauD\_tot(t-1) + StockEauD\_C2(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:StockEauD_tot}$$

> $$FTSW(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ time < date\_init\_H \\
    FTSW\_C0(t), & \text{else if}\ ProfRac(t) <= ep\_C0 \\
    coef\_Mottes * \frac{RU\_C0(t) + RU\_C1(t)}{RUmax\_C0(t) + RUmax\_C1(t)}, & \text{else if}\ MottesDelta > Mottes\_seuil \\
    \frac{RU\_C0(t) + RU\_C1(t)}{RUmax\_C0(t) + RUmax\_C1(t)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:FTSW}$$

> $$stressH(t)=\left\{
  \begin{array}{@{}ll@{}}
    1, & \text{if}\ time < date\_init\_H \\
    \max\left(\begin{array}{@{}ll@{}}
        0.4 \\
        \frac{stressH1}{1 + stressH2 * e^{-stressH3 * (FTSW(t) - stressH4)}} 
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:stressH}$$

</div>

--- 

## Atomic model YieldRapeseed <a name="p17"></a>

The **YieldRapeseed** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? using ??

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#YieldRapeseed_panel'>Show/Hide Model Details</button>
<div id="YieldRapeseed_panel" class="collapse">

### Configuring a YieldRapeseed Model <a name="p17.1"></a>

#### Dynamics settings <a name="p17.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : YieldRapeseed

#### Parameters settings <a name="p17.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantRape`](#annex.p.plt) (`ANGPOT`, `BNGPOT`, `ATPpot`, `BTPpot`, `Ec`, `AStressH`, `BStressH`, `PMGmax_var`, `RDTmax_var`, `P1G_FLO`, `ANalloc`, `ANdispo`, `AQNG`, `ANremob`, `BNremob`, `CNremob`, `AOffMSG`, `GrainsHumidity`)

#### Input settings <a name="p17.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **RG** : Rayonnement globale journalier (MJ/m²/j) (sync)
* **Stade** : Stade phénologique actuel (sans unité) (see [shared defined stage](#annex.pheno.stages)) (sync)
* **STfloraison** : Somme des températures depuis le stade F1 (°C) (sync)
* **INNvec** : INN journalier (sans unité) (sync)
* **RGintercept** : Rayonnement actif journalier intercepté par les feuilles et les siliques (MJ/m²/j) (sync)
* **QNAr** : Quantité d'azote dans la biomasse aérienne réelle (kgN/ha) (sync)
* **FTSW** : Niveau de remplissage en eau du sol (sans unité) (sync)
* **Nabs** : Quantité d'azote journalière absorbée par la plante entière (kgN/ha/j) (Nosync)
* **MSAg** : Biomasse aérienne générée (kgMS/ha) (Nosync)
  
#### Output settings <a name="p17.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **dynSRG** : Somme des rayonnements actifs interceptés du stade F1 à G4 (MJ/m²)
* **dynNGM2pot** : Nombre de grains par m² potentiel du stade F1 à G4 (grains/m²)
* **dynINN_FLO** : INN au stade F1 (sans unité)
* **dynINN_G4** : INN au stade G4 (sans unité)
* **dynTPpot** : Teneur en protéines potentielle des grains calculé au stade F1 (%)
* **dynSPAR** : Cumul du PAR du stade F1 à G4 (MJ/m²)
* **dynFTSW_PAR** : Cumul du PAR atténué du remplissage de la RU, du stade F1 à G4 (MJ/m²)
* **dynStressH_NGM2** : Indice de stress hydrique journalier impactant le Nombre de grains par m² entre le stade F1 et G4 (sans unité)
* **dynSumRG** : Cumul du rayonnement global du stade F1 à G4 (MJ/m²)
* **dynQtPhototherm** : Rayonnement global cumulé par degré jour cumlé du stade F1 à G4 (MJ/m²/°C/j)
* **dynNGM2** : Nombre de grains par m² du stade F1 à G4 (grains/m²)
* **dynP1Gpot** : Poids d'un grain, potentiel, du stade F1 à FRG (g)
* **dynDemandeMS1G** : Biomasse potentielle (d'après les besoins) d'un grain du stade F1 à FRG (gMS/grains)
* **dynDemandeMSG** : Biomasse des grains potentielle (d'après les besoins) du stade F1 à FRG (gMS/m²)
* **dynDemandeQNG** : Gain journalier potentiel (d'après les besoins) d'azote dans les grains du stade F1 à FRG (kgN/ha/j)
* **dynOffreQNG** : Quantité d'azote journalière disponible pour les grains du stade F1 à FRG (kgN/ha/j)
* **dyndQNG** : Dynamique de remplissage des grains (kg/ha)
* **dynQNG** : Quantité d'azote dans les grains du stade F1 à FRG (kgN/ha)
* **dynQNveg** : Quantité d'azote dans la biomasse aérienne réelle végétative (kgN/ha)
* **dynQNa_FLO** : Quantité d'azote dans la biomasse aérienne réelle végétative au stade F1 (kgN/ha)
* **dynStockNremob** : Quantité d'azote remobilisable dans la biomasse aérienne réelle, du stade F1 à FRG (kgN/ha)
* **dynQNremob** : Quantité d'azote journalière remobilisable vers les grains du stade F1 à FRG (kgN/ha/j)
* **dynMSG_offre** : Biomasse des grains potentielle (d'après l'offre) du stade F1 à FRG (gMS/m²)
* **dynMSG** : Biomasse des grains du stade F1 à FRG (gMS/m²)
* **dynPMG** : Poids de 1000 grains (g/1000grains)
* **dynYieldDry** : Rendement en grains (qtxMS/ha)
* **dynYieldHum** : Rendement en grains corrigé à 9% d'humidité (qtx/ha)

#### Observation settings <a name="p17.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

Same as Output settings, see above.

#### Available configurations <a name="p17.1.6"></a>

None

### Details <a name="p17.2"></a>

#### State variables equations <a name="p17.2.1"></a>

> $$dynSRG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynSRG(t-1) + RGintercept(t), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    dynSRG(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynSRG}$$

> $$dynNGM2pot(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    ANGPOT * dynSRG(t) + BNGPOT, & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    dynNGM2pot(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynNGM2pot}$$

> $$dynINN\_FLO(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    INNvec(t), & \text{if}\ Stade(t) \in \{JOUR\_F1\} \\
    dynINN\_FLO(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynINN_FLO}$$

> $$dynINN\_G4(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF, JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    INNvec(t), & \text{if}\ Stade(t) \in \{JOUR\_G4\} \\
    dynINN\_G4(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynINN_G4}$$

> $$dynTPpot(t)=\left\{
  \begin{array}{@{}ll@{}}
    ATPpot * dynINN\_FLO(t) + BTPpot, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF, JOUR\_F1\} \\
    dynTPpot(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynTPpot}$$

> $$dynSPAR(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynSPAR(t-1) + Ec * RG(t), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    dynSPAR(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynSPAR}$$

> $$dynFTSW\_PAR(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynFTSW\_PAR(t-1) + FTSW(t) * Ec * RG(t), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    dynFTSW\_PAR(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynFTSW_PAR}$$

> $$dynStressH\_NGM2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    BStressH + \frac{2}{1 + e^{AStressH * \frac{dynFTSW\_PAR(t)}{dynSPAR(t)}}}, & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    dynStressH\_NGM2(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynStressH_NGM2}$$

> $$dynSumRG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynSumRG(t-1) + RG(t), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    dynSumRG(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynSumRG}$$

> $$dynQtPhototherm(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    \frac{dynSumRG(t)}{\max\left(\begin{array}{@{}ll@{}}
                            0.0001 \\
                            STfloraison(t) 
                        \end{array}\right)}, & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    dynQtPhototherm(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynQtPhototherm}$$

> $$dynNGM2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynNGM2pot(t) * \min\left(\begin{array}{@{}ll@{}}
                        1 \\
                        dynStressH\_NGM2(t) * dynQtPhototherm(t) 
                    \end{array}\right), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4\} \\
    dynNGM2(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynNGM2}$$

> $$dynP1Gpot(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    \min\left(\begin{array}{@{}ll@{}}
        \frac{PMGmax\_var}{1000} \\
        10 * \frac{RDTmax\_var}{dynNGM2(t)} 
    \end{array}\right), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN\} \\
    dynP1Gpot(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynP1Gpot}$$

> $$dynDemandeMS1G(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    \frac{dynP1Gpot(t)}{1 + (\frac{dynP1Gpot(t) * 1000 - P1G\_FLO * 1000}{P1G\_FLO * 1000})^{\frac{((1350 / 15) / 2) - STfloraison(t)}{((1350 / 15) / 2)}}}, & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN\} \\
    dynDemandeMS1G(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynDemandeMS1G}$$

> $$dynDemandeMSG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynDemandeMS1G(t) * dynNGM2(t), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN\} \\
    dynDemandeMSG(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynDemandeMSG}$$

> $$dynDemandeQNG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    (dynDemandeMSG(t) - dynDemandeMSG(t-1)) * ANalloc * 10, & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN\} \\
    dynDemandeQNG(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynDemandeQNG}$$

> $$dynOffreQNG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynQNremob(t-1) + \frac{Nabs(t-1)}{ANdispo}, & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN\} \\
    dynOffreQNG(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynOffreQNG}$$

> $$dyndQNG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    \max\left(\begin{array}{@{}ll@{}}
        0.001 \\
        \min\left(\begin{array}{@{}ll@{}}
            dynDemandeQNG(t) \\
            dynOffreQNG(t) 
        \end{array}\right) 
    \end{array}\right), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN\} \\
    dynOffreQNG(t), & \text{if}\ Stade(t) \in \{FIN\_REMPLISSAGE\_GRAIN\} \\
    dyndQNG(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dyndQNG}$$

> $$dynQNG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynNGM2(t) * \frac{P1G\_FLO}{1000} * 10 * \frac{AQNG}{100}, & \text{if}\ Stade(t) \in \{JOUR\_F1\} \\
    dynQNG(t-1) + dyndQNG(t-1), & \text{if}\ Stade(t) \in \{FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN\} \\
    dynQNG(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynQNG}$$

> $$dynQNveg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE\} \\
    QNAr(t) - dynQNG(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynQNveg}$$

> $$dynQNa\_FLO(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynQNveg(t), & \text{if}\ Stade(t) \in \{JOUR\_F1\} \\
    dynQNa\_FLO(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynQNa_FLO}$$

> $$dynStockNremob(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    \max\left(\begin{array}{@{}ll@{}}
        0 \\
        dynQNveg(t) - (1 - ANremob) * dynQNa\_FLO(t) 
    \end{array}\right), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN\} \\
    dynStockNremob(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynStockNremob}$$

> $$dynQNremob(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ (Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\}) \| (dynQNveg(t-1) < (1 - ANremob) * dynQNa\_FLO(t)) \\
    dynStockNremob(t) * (BNremob * STfloraison(t) + CNremob), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN\} \\
    dynQNremob(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynQNremob}$$

> $$dynMSG\_offre(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynMSG\_offre(t-1) + \frac{AOffMSG * dynQNG(t-1)}{10} + \frac{MSAg(t-1)}{10}, & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN\} \\
    dynMSG\_offre(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynMSG_offre}$$

> $$dynMSG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    \min\left(\begin{array}{@{}ll@{}}
        dynMSG\_offre(t) \\
        dynDemandeMSG(t) 
    \end{array}\right), & \text{if}\ Stade(t) \in \{JOUR\_F1, FLORAISON, JOUR\_G1, G1\_G4, JOUR\_G4, G4\_MI\_REMPLISSAGE\_GRAIN, JOUR\_MI\_REMPLISSAGE\_GRAIN, FIN\_REMPLISSAGE\_GRAIN\} \\
    dynMSG(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:dynMSG}$$

> $$dynPMG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    \frac{dynMSG(t)}{dynNGM2(t)} * 1000, & \text{otherwise}
  \end{array}\right. 
\label{eq:dynPMG}$$

> $$dynYieldDry(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    \frac{dynPMG(t) * dynNGM2(t)}{10000}, & \text{otherwise}
  \end{array}\right. 
\label{eq:dynYieldDry}$$

> $$dynYieldHum(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \in \{SOL\_NU, JOUR\_SEMIS, PRE\_LEVEE, JOUR\_LEVEE, VEGETATIF\} \\
    dynYieldDry(t) + dynYieldDry(t) * \frac{GrainsHumidity}{100}, & \text{otherwise}
  \end{array}\right. 
\label{eq:dynYieldHum}$$


</div>

--- 


## Shared structures  <a name="annex"></a>

### Phenological stages <a name="annex.pheno.stages"></a>

Defines the 15 + 1 available crop phenological stages. 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#pheno_stages_panel'>Show/Hide Table</button>
<div id="pheno_stages_panel" class="collapse">

| Stage name                    | Integer code | Description  |
| ----------------------------- | :----------: | ------------ |
| **SOL_NU**                    |      0       | bare soil    |
| **JOUR_SEMIS**                |      1       | sowing day   |
| **PRE_LEVEE**                 |      2       | ???          |
| **JOUR_LEVEE**                |      3       | ???          |
| **VEGETATIF**                 |      4       | ???          |
| **JOUR_F1**                   |      5       | ???          |
| **FLORAISON**                 |      6       | flowering    |
| **JOUR_G1**                   |      7       | ???          |
| **G1_G4**                     |      8       | ???          |
| **JOUR_G4**                   |      9       | ???          |
| **G4_MI_REMPLISSAGE_GRAIN**   |      10      | ???          |
| **JOUR_MI_REMPLISSAGE_GRAIN** |      11      | ???          |
| **FIN_REMPLISSAGE_GRAIN**     |      12      | ???          |
| **JOUR_MATURITE**             |      13      | maturity day |
| **GRAIN_MUR**                 |      14      | ???          |
| **JOUR_RECOLTE**              |      15      | harvest day  |
</div>

### ParametersSoilRape <a name="annex.p.soil"></a>

A set of 52 soil related parameters that can be used in multiple atomic models.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#p_soil_panel'>Show/Hide Table</button>
<div id="p_soil_panel" class="collapse">

| Parameter name | Type | Is mandatory? | Description | Internal type |
| -------------- | :--: | :-----------: | ----------- | :-----------: |
| **Tref** | double | [x] | Température de référence pour l'effet de la température sur la minéralisation (°C) | double |
| **alpha** | double | [x] | Coefficient pour l'effet de la température sur la minéralisation dans l'équation du K2 (sans unité) | double |
| **beta** | double | [x] | Paramètre pour le calcul de la minéralisation de l'humus (tient compte de la minéralisation sous la couche labourée) (sans unité) | double |
| **gamma** | double | [x] | Paramètre pour le calcul de la minéralisation de l'humus (coef argile) (?) | double |
| **delta** | double | [x] | Paramètre pour le calcul de la minéralisation de l'humus (coef CaCO3) (?) | double |
| **coef_k2** | double | [x] | Paramètre pour le calcul de la minéralisation de l'humus (?) | double |
| **a** | double | [x] | Paramètre pour l'effet de la température sur la décomposition des résidus (sans unité) | double |
| **b** | double | [x] | Paramètre pour l'effet de la température sur la décomposition des résidus (?) | double |
| **c** | double | [x] | Paramètre pour l'effet de la température sur la décomposition des résidus (?) | double |
| **d** | double | [x] | Paramètre pour l'effet de la température sur la décomposition des résidus (?) | double |
| **para_kres1** | double | [x] | Paramètre pour le calcul du taux de décomposition des résidus (?) | double |
| **para_kreshum** | double | [x] | Paramètre pour le calcul de la décompositions des résidus (?) | double |
| **f** | double | [x] | Paramètre pour le calcul du taux d'humification des résidus (?) | double |
| **g** | double | [x] | Paramètre pour le calcul du taux d'humification des résidus (?) | double |
| **Y** | double | [x] | Paramètre du rendement d'assimilation du carbone des résidus par la biomasse microbienne (?) | double |
| **paraC_Nres1** | double | [x] | Ratio C/N des résidus de culture (kgC/kgN) | double |
| **paraC_Nres2** | double | [x] | parametre pour le calcul de la biomasse microbienne (??) | double |
| **paraC_Nres3** | double | [x] | Paramètre pour le calcul du C/N de la biomasse microbienne (kgC/kgN) | double |
| **paraC_Nres4** | double | [x] | Ratio C/N de la biomasse microbienne quand le C/N des résidus est très faible (<paraC_Nres1) (kgC/kgN) | double |
| **C_Nhum** | double | [x] | Ratio C/N de l'humus issu par la décomposition des résidus (kgC/kgN) | double |
| **fsdc** | double | [x] | Effet du Système de Culture sur la minéralisation de l'humus (sans unité) | double |
| **tNorg** | double | [x] | Teneur en azote organique dans l'épaisseur labourée pour le calcul de la minéralisation de l'humus (gNorg/kgSol) | double |
| **da_C0** | double | [x] | Densité apparente de la couche C0 (kgSol/dm^3) | double |
| **ep_C0** | double | [x] | Epaisseur de la couche labourée (mm) | double |
| **Arg** | double | [x] | Teneur en argile de la couche C0 (g/kgSol) | double |
| **CaCO3** | double | [x] | Teneur en calcaire de la couche C0 (g/kgSol) | double |
| **QNsol_init_C0** | double | [x] | Quantité d'azote minéral dans le sol, à l'initialisation du module azote, dans la couche C0 (kgN/ha) | double |
| **QNsol_init_tot** | double | [x] | Quantité d'azote minéral dans le sol, à l'initialisation du module azote, sur la profondeur mesurée (kgN/ha) | double |
| **Prof_Reliquat_tot** | double | [x] | Profondeur sur laquelle a été mesurée le reliquat d'azote initial total (mm) | double |
| **QNND** | double | [x] | Quantité d'azote minéral non disponible pour la culture par couche de 10 cm de sol (kgN/ha/10cm) | double | 
| **C_Nres** | double | [x] | Ratio C/N des résidus de la culture precedente (??) | double |
| **C_res** | double | [x] | Quantité de carbone dans les résidus de culture (kgC/ha) | double |
| **ApportEau_seuil** | double | [x] | Seuil de pluie et d'irrigation sous lequel il y a réduction de l'évaporation (mm) | double |
| **EVAPO** | double | [x] | Paramètre pour le calcul de la réduction de l'évaporation en cas de sécheresse (sans unité) | double |
| **Trans** | double | [x] | Paramètre pour calcul de la transpiration (sans unité) | double |
| **stressH1** | double | [x] | Paramètre pour le calcul du stress hydrique (?) | double |
| **stressH2** | double | [x] | Paramètre pour le calcul du stress hydrique (?) | double |
| **stressH3** | double | [x] | Paramètre pour le calcul du stress hydrique (?) | double |
| **stressH4** | double | [x] | Paramètre pour le calcul du stress hydrique (?) | double |
| **Mottes_seuil** | double | [x] | Seuil de mottes delta au delà duquel la FTSW (humidité du sol) est réduite (%) | double |
| **coef_Mottes** | double | [x] | Paramètre pour le calcul de la réduction de la FTSW en conditions de sol tassé (Mottesdelta > Mottes_seuil) (sans unité) | double |
| **JSA_seuil** | double | [x] | Nombre de jours seuil au dessus duquel le nombre de jour sans apport d'eau réduit l'évaporation (j) | double |
| **lambda** | double | [x] | Paramètre pour le calcul du CAU de l'apport (sans unité) | double |
| **mu** | double | [x] | Paramètre pour le calcul du CAU de l'apport (sans unité) | double |
| **PLUIEVALOR** | double | [x] | Seuil de pluie (somme des 7 derniers jours) nécessaire à la fonte de l'engrais et à sa mise à disposition de la plante (mm) | double |
| **RU_init** | double | [x] | Réserve utile à la date d'initialisation du module eau (mm) | double |
| **RUmax** | double | [x] | Réserve utile maximale, prend en compte les potentielles remontées capillaires d'eau en fond de profil de sol (mm) | double |
| **MottesDelta** | double | [x] | Pourcentage de mottes de type delta (tassement du sol) (%) | double |
| **CAUfm** | double | [x] | CAU des feuilles mortes (sans unité) | double |
| **km** | double | [x] | Paramètre pour le calcul de la minéralisation des feuilles mortes (sans unité) | double |
| **CN_opt** | double | [x] | C/N optimal des feuilles mortes pour leur minéralisation (sans unité) | double |
| **ACfm** | double | [x] | Paramètre pour le calcul de la minéralisation des feuilles mortes (sans unité) | double |

</div>

### ParametersPlantRape <a name="annex.p.plt"></a>

A set of 117 Crop related parameters that can be used in multiple atomic models.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#p_plt_panel'>Show/Hide Table</button>
<div id="p_plt_panel" class="collapse">

| Parameter name | Type | Is mandatory? | Description | Internal type |
| -------------- | :--: | :-----------: | ----------- | :-----------: |
| **crop** | string | [x] | Designation de la culture (??) | string |
| **cultivar** | string | [x] | Designation de la variete (??) | string |
| **January1st** | string | [x] | Date du 1er janvier de l'année de récolte (YYYY-MM-DD) | string |
| **jourLevee** | string | [x] | Date de LEVEE (YYYY-MM-DD) | string |
| **jourFloraison** | string | [x] | Date de FLORAISON observee (YYYY-MM-DD) | string |
| **jourMaturite** | string | [x] | Date de MATURITE observee (YYYY-MM-DD) | string |
| **densiteSemis** | double | [x] | Densité de semis (plante.m^{-2}) | double |
| **date_init_H** | string | [x] | Date d'initialisation du module hydrique (YYYY-MM-DD) | double |
| **date_init_N** | string | [x] | Date d'initialisation du module azote du sol. Doit être après la date d'initialisation du bilan hydrique (YYYY-MM-DD) | double |
| **date_init_miner_res** | string | [x] | Date d'initialisation de la minéralisation des résidus (YYYY-MM-DD) | double |
| **date_init_MS** | string | [x] | Date d'initialisation de la matière sèche (doit correspondre à la date de levée) (YYYY-MM-DD) | double |
| **AF1** | double | [x] | Paramètre pour le calcul de la date de Floraison (?) | double |
| **BTF1** | double | [x] | Paramètre pour le calcul de la date de Floraison (effet température) (?) | double |
| **moyTjanuary1st** | double | [x] | Paramètre pour le calcul de la date de Floraison (effet température) (°C/j) | double |
| **BPF1** | double | [x] | Paramètre pour le calcul de la date de Floraison (effet photopériode) (?) | double |
| **moyPjanuary1st** | double | [x] | Paramètre pour le calcul de la date de Floraison (effet photopériode) (h/j) | double |
| **BLF1** | double | [x] | Paramètre pour le calcul de la date de Floraison (effet lattitude) (?) | double |
| **LAT** | double | [x] | Lattitude du lieu (°) | double |
| **somme_temperature_levee** | double | [x] | Somme des températures depuis SEMIS qui déclenche le passage au stade LEVEE (°C) | double |
| **somme_temperature_floraison** | double | [x] | Somme des températures depuis LEVEE qui déclenche le passage au stade FLORAISON (°C) | double |
| **somme_temperature_G4** | double | [x] | Somme des températures depuis FLORAISON qui déclenche le passage au stade G4 (°C) | double |
| **somme_temperature_FRG** | double | [x] | Somme des températures depuis FLORAISON qui termine le remplissage des grains (°C) | double |
| **Velong** | double | [x] | Vitesse d'élongation des racines (mm/°C) | double |
| **ProfRacmax** | double | [x] | Profondeur maximum d'enracinement (mm) | double |
| **ApartQNGreen** | double | [x] | Paramètre pour le calcul de QNGreen (sans unité) | double |
| **BpartQNGreen** | double | [x] | Paramètre pour le calcul de QNGreen (°C^-1) | double |
| **AQNGreen** | double | [x] | Paramètre pour le calcul de QNGreen (kgN/ha) | double |
| **BQNGreen** | double | [x] | Paramètre pour le calcul de QNGreen (sans unité) | double |
| **ApartQNLeaves** | double | [x] | Paramètre pour le calcul de QNLeaves (sans unité) | double |
| **BpartQNLeaves** | double | [x] | Paramètre pour le calcul de QNLeaves (°C^-1) | double |
| **CpartQNLeaves** | double | [x] | Paramètre pour le calcul de QNLeaves (sans unité) | double |
| **DpartQNLeaves** | double | [x] | Paramètre pour le calcul de QNLeaves (°C^-1) | double |
| **ApartQNStem** | double | [x] | ?? | double |
| **BpartQNStem** | double | [x] | ?? | double |
| **ApartQNPod** | double | [x] | Paramètre pour le calcul de QNPod (sans unité) | double |
| **BpartQNPod** | double | [x] | Paramètre pour le calcul de QNPod (°C^-1) | double |
| **MS_Nmax_seuil** | double | [x] | Seuil de MS pour courbe max (??) | double |
| **coef_tot_a** | double | [x] | Proportion de la quantité d'azote présente dans la plante entière par rapporr à la biomasse aérienne (QNT/QNA) (sans unité) | double |
| **tNmax_Nmax** | double | [x] | Teneur en azote max pour des biomasses faibles (%) | double |
| **coef_Nmax** | double | [x] | Paramètre "a" de la courbe maximum de dilution critique de l'azote (?) | double |
| **pente_Nmax** | double | [x] | Parametre "-b" de la courbe maximum de dilution critique de l'azote (sans unité) | double |
| **Vmax** | double | [x] | Vitesse d'absorption maximale d'azote par la plante (kgN/ha/°C) | double |
| **BQNT** | double | [x] | Paramètre ajouté ou soustrait (et accompagne coef_tot_a) pour passer de QNAr à QNT et QNT à QNAr respectivement (kgN/ha) | double |
| **ApartMSLeaves** | double | [x] | Paramètre pour le calcul de MSLeaves (sans unité) | double |
| **BpartMSLeaves** | double | [x] | Paramètre pour le calcul de MSLeaves (°C^-1) | double |
| **CpartMSLeaves** | double | [x] | Paramètre pour le calcul de MSLeaves (sans unité) | double |
| **DpartMSLeaves** | double | [x] | Paramètre pour le calcul de MSLeaves (°C^-1) | double |
| **ApartMSPodWall** | double | [x] | Paramètre pour le calcul de MSPodWall (sans unité) | double |
| **BpartMSPodWall** | double | [x] | Paramètre pour le calcul de MSPodWall (°C^-1) | double |
| **AMST** | double | [x] | Rapport entre MS total et MS aérienne (changé pour 1,3516 dans PIFEC) (sans unité) | double |
| **BMST** | double | [x] | Paramètre ajouté ou soustrait (et accompagne AMST) pour passer de MSAr à MST et MST à MSAr respectivement (kgMS/ha) | double |
| **AMSLeaves** | double | [x] | ?? | double |
| **BMSLeaves** | double | [x] | ?? | double |
| **AMSStem** | double | [x] | ?? | double |
| **BMSStem** | double | [x] | ?? | double |
| **AMSPod** | double | [x] | Paramètre pour le calcul de MSPod (?) | double |
| **BMSPod** | double | [x] | Paramètre pour le calcul de MSPod (?) | double |
| **Ec** | double | [x] | Coefficient de conversion du rayonnement global vers le rayonnement actif (sans unité) | double |
| **tNc_Nc** | double | [x] | Teneur en azote critique pour les biomasses faibles (%) | double |
| **coef_Nc** | double | [x] | Paramètre "a" de la courbe de dilution critique de l'azote (?) | double |
| **pente_Nc** | double | [x] | Paramètre "-b" de courbe de dilution critique de l'azote (sans unité) | double |
| **SFCOTY** | double | [x] | Surface foliaire des cotylédons (m²MS/plante) | double |
| **SLACOTY** | double | [x] | Specific Leaf Area des cotylédons (gMS/M²MS) | double |
| **GEL** | double | [x] | ?? | double |
| **ALAI** | double | [x] | Paramètre pour le calcul du LAI (?) | double |
| **BLAI** | double | [x] | Paramètre pour le calcul du LAI (?) | double |
| **APAI** | double | [x] | Paramètre dans le calcul du PAI (?) | double |
| **BPAI** | double | [x] | ?? | double |
| **EILMAX** | double | [x] | Efficience d'interception maximum du rayonnement par les feuilles (sans unité) | double |
| **EIPMAX** | double | [x] | Efficience d'interception maximum du rayonnement par les cosses de siliques (sans unité) | double |
| **K** | double | [x] | Paramètre pour le calcul de l'efficience d'interception du rayonnement (Ei) (?) | double |
| **TMINEB0** | double | [x] | Seuil de température pour le calcul de l'efficience de conversion du rayonnement (Eb) (°C) | double |
| **TMINEB1** | double | [x] | Seuil de température pour le calcul de l'efficience de conversion du rayonnement (Eb) (°C) | double |
| **AEB** | double | [x] | Paramètre dans le calcul de l'efficience de conversion du rayonnement (Eb) (effet azote) (sans unité) | double |
| **BEB** | double | [x] | Paramètre dans le calcul de l'efficience de conversion du rayonnement (Eb) (effet azote) (sans unité) | double |
| **CEB** | double | [x] | Paramètre dans le calcul de l'efficience de conversion du rayonnement (Eb) effet de la température (°C ?) | double |
| **DEB** | double | [x] | Paramètre dans le calcul de l'efficience de conversion du rayonnement (Eb) (effet température) (?) | double |
| **EEB** | double | [x] | Paramètre dans le calcul de l'efficience de conversion du rayonnement (Eb) (?) | double |
| **ARG** | double | [x] | Paramètre pour le calcul de EbPod (?) | double |
| **BRG** | double | [x] | Paramètre pour le calcul de EbPod (?) | double |
| **VIEFM** | double | [x] | Paramètre pour le calcul de la chutte des feuilles (°C) | double |
| **AVIEFM** | double | [x] | Paramètre pour le calcul de la chute des feuilles (°C) | double |
| **BVIEFM** | double | [x] | Paramètre pour le calcul de la chute des feuilles (sans unité) | double |
| **CVIEFM** | double | [x] | Paramètre pour le calcul de la chute des feuilles (°C) | double |
| **DVIEFM** | double | [x] | Paramètre pour le calcul de la chute des feuilles (sans unité) | double |
| **EVIEFM** | double | [x] | Paramètre pour le calcul de la chute des feuilles (°C) | double |
| **FVIEFM** | double | [x] | Paramètre pour le calcul de la chute des feuilles (sans unité) | double |
| **ANFM** | double | [x] | Paramètre pour le calcul de la quantité d'azote contenue dans les feuilles mortes (sans unité) | double |
| **BNFM** | double | [x] | Paramètre pour le calcul de la quantité d'azote contenue dans les feuilles mortes (%) | double |
| **ANGPOT** | double | [x] | Paramètre pour le calcul du nombre de grain potentiel (grains/MJ) | double |
| **BNGPOT** | double | [x] | Paramètre pour le calcul du nombre de grain potentiel (grains/m²) | double |
| **AISN** | double | [x] | Paramètre pour le calcul de l'effet de l'INN sur la réduction du nombre de grains du module Rendement (sans unité) | double |
| **BISN** | double | [x] | Paramètre pour le calcul de l'effet de l'INN sur la réduction du nombre de grains du module Rendement (sans unité) | double |
| **CISN** | double | [x] | Paramètre pour le calcul de l'effet de l'INN sur la réduction du nombre de grains du module Rendement (sans unité) | double |
| **XII** | double | [x] | Seuil d'INN au stade F1 au dessus duquel le nombre de grain du module Rendement n'est pas dégradé par la nutrition azotée (sans unité) | double |
| **PMGmax_var** | double | [x] | PMG maximum variétal (g/1000grains) | double |
| **NC1G4** | double | [x] | Paramètre "a" pour le calcul de la teneur en N critique de la biomasse à G4 (%/kgMS/ha) | double |
| **NC2G4** | double | [x] | Paramètre "b" pour le calcul de la teneur en N critique de la biomasse à G4 (%/kgMS/ha) | double |
| **NGSEUIL** | double | [x] | Seuil de nombre de grain au dessus duquel le PMG du module Rendement est réduit (grains/m²) | double |
| **PMGEAU** | double | [x] | Paramètre pour le calcul du PMG du modul Rendement (effet stress hydrique) (g/1000grains/mm) | double |
| **PMGQP** | double | [x] | Paramètre pour le calcul du PMG module Rendement (effet Rayonnement) (g/1000grains/MJ/cm²/°C) | double |
| **PMGTMOY** | double | [x] | Paramètre pour le calcul du PMG du module Rendement (effet température moyenne) (g/1000grains/°C) | double |
| **PMGTMAX** | double | [x] | Paramètre pour le calcul du PMG du module Rendement (effet forte température) (g/1000grains/°C) | double |
| **RDTmax_var** | double | [x] | Rendement maximum variétal (qtx/ha) | double |
| **P1G_FLO** | double | [x] | Poids d'un grain à floraison (g) | double |
| **GrainsHumidity** | double | [x] | Humidité des grains à la récolte (%) | double |
| **ATPpot** | double | [x] | Paramètre pour le calcul du taux de protéines potentiel dans le module YieldRapeseed (%) | double |
| **BTPpot** | double | [x] | Paramètre pour le calcul du taux de protéines potentiel dans le module YieldRapeseed (%) | double |
| **AStressH** | double | [x] | Paramètre pour le calcul de l'effet du stress hydrique sur le nombre de grains (?) | double |
| **BStressH** | double | [x] | Paramètre pour le calcul de l'effet du stress hydrique sur le nombre de grains (sans unité) | double |
| **ANalloc** | double | [x] | Teneur en azote maximum des grains, pour guider la demande en N des grains (sans unité) | double |
| **ANdispo** | double | [x] | Rapport entre l'azote total de la plante et l'azote disponible pour la remobilisation vers les grains (sans unité) | double |
| **AQNG** | double | [x] | Teneur en N des grains au stade F1 (%) | double |
| **ANremob** | double | [x] | Paramètre pour le calcul de l'azote remobilisé vers les grains (?) | double |
| **BNremob** | double | [x] | Paramètre pour le calcul de l'azote remobilisé (?) | double |
| **CNremob** | double | [x] | Paramètre pour le calcul de l'azote remobilisé (?) | double |
| **AOffMSG** | double | [x] | Paramètre pour le calcul de l'offre en MS pour la remobilisation vers les grains (?) | double |

Mandatory relations to respect between parameter values:  

> $$date\_init\_miner\_res > date\_init\_N$$  
 
> $$date\_init\_H > date\_init\_N$$  
 
> $$date\_init\_N > date\_init\_MS$$  

</div>
