---
title: "AZODYN package documentation"
---
<!-- 
R -e "rmarkdown::render('./AZODYN/doc/AzodynPea_pkg_doc.md',  encoding = 'UTF-8');"
-->

# Package AZODYN for vle-2.0.2

The [**AZODYN**](https://forgemia.inra.fr/record/azodyn/) package provide 13 atomic models and a simulator for Pea crop using the AZODYN model.  
Sample input files are also provided in the */data* folder.

* [Coef_fixmax3](#p1)
* [Decision](#p2)
* [Frost](#p3)
* [NitrogenFixationPea](#p4)
* [NitrogenPlantPea](#p5)
* [NitrogenSoil](#p6)
* [PhenologyPea](#p7)
* [PlantGrowthPea](#p8)
* [RootGrowthPea](#p9)
* [StressNitrogenPea](#p10)
* [StressT](#p11)
* [WaterSoil](#p12)
* [YieldPea](#p13)

* [Shared structures](#annex)
  * [Phenological stages](#annex.pheno.stages)
  * [ParametersSoil](#annex.p.soil)
  * [ParametersPlant](#annex.p.plt)
  * [ParametersPlantPea](#annex.p.plt.pea)

---

## Package dependencies 

List of required external packages (with link to distribution when available).

* [vle.discrete-time](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)
* [vle.reader](https://github.com/vle-forge/packages/tree/master2.1/vle.reader)
* [record.meteo](https://forgemia.inra.fr/record/RECORD/-/tree/master/pkgs/record.meteo) (required to run the simulator but not to build the package)

--- 

## Atomic model Coef_fixmax3 <a name="p1"></a>

The **Coef_fixmax3** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time) and [`vle.reader`](https://github.com/vle-forge/packages/tree/master2.1/vle.reader)) estimate coef_fixmax3 based on expected cummulated temperature phenological thresholds.

> ***Remark***: The purpose of this model is to estimate phenological cummulated temperature thresholds based on fixed dates and daily temperature, because we don't provide this threshold value directly as an parameter.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Coef_fixmax3_panel'>Show/Hide Model Details</button>
<div id="Coef_fixmax3_panel" class="collapse">

### Configuring a Coef_fixmax3 Model <a name="p1.1"></a>

#### Dynamics settings <a name="p1.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : Coef_fixmax3

#### Parameters settings <a name="p1.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

##### Crop parameters 
| Parameter name   |        Type         | Is mandatory? | Description                                                                    |
| ---------------- | :-----------------: | :-----------: | ------------------------------------------------------------------------------ |
| **date_S**       | string (YYYY-MM-DD) |      [x]      | Sowing date                                                                    |
| **date_FLO**     | string (YYYY-MM-DD) |      [x]      | Flowering date                                                                 |
| **ST_S_LEVEE**   |       double        |      [x]      | Crop required Temperature sum between Sowing and Emergence phenological stages |
| **coef_fixmax1** |       double        |      [x]      | ??                                                                             |
| **coef_fixmax2** |       double        |      [x]      | ??                                                                             |
| **ST_DF_DRG**    |       double        |      [x]      | Crop required Temperature sum between ?DF and ?DRG phenological stages         |

##### Weather file reader configuration 
| Parameter name   |  Type  | Is mandatory? | Description                                                                                |
| ---------------- | :------: | :-----------: | ------------------------------------------------------------------------------------------ |
| **PkgName** | string | [] | Name of the package where the data file is stored.<br>If not provided the **meteo_file** will be used as an absolute path or a relative path to the running environment |
| **meteo_file** | string | [x] | Name of the file to read.<br>(file expected inside the ***PkgName**/data* folder)<br>This can be a relative path from the data folder if it has subfolders |
| **meteo_type** | string (agroclim\|generic_with_header\|drias_ascii) | [x] | Type of file the model has to read.<br>(see details section of [record.meteo](https://forgemia.inra.fr/record/RECORD/-/tree/master/pkgs/record.meteo) for information on the different types). |
| **column_separator** | string | [] | Character used as column separator in the **meteo_file** file.<br>Default value is ";" if **meteo_type**=agroclim, or "\t" if **meteo_type**=generic_with_header. |
| **begin_date** | string (YYYY-MM-DD)<br>or<br>double | [] | Enables to start the reading of data at the specified date.<br>(If not provided datas are read from the begining of the data set) |
| **year_column** | string | [] | Name of the column defining the year of the date.<br>Default value is "AN". |
| **month_column** | string | [] | Name of the column defining the month of the date.<br>Default value is "MOIS". |
| **day_column** | string | [] | Name of the column defining the day of the date.<br>Default value is "JOUR". |

#### Input settings <a name="p1.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* None

This model reads and use values from the **TMC** (daily mean temperature) variable of the weather file (computations are done at initialization by reading "in the future")

#### Output settings <a name="p1.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **coef_fixmax3** : ?? (??)

### Details <a name="p1.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p1.2.1"></a>

>$coef\_fixmax3(t) = coef\_fixmax2 - (coef\_fixmax1 * (sumT\_emergence\_flowering + ST\_DF\_DRG))$
> with  
> `sumT_emergence_flowering` the expected temperature sum between emergence and flowering, computed using the daily mean temperature and crop parameters :  
> First from the day defined by `date_S` and the temperature threshold `ST_S_LEVEE`, the day of Emergence is estimated.  
> Then the daily temperature are cummulated until the day defined by `date_FLO`.  

</div>

--- 

## Atomic model Decision <a name="p2"></a>

The **Decision** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) is using fixed dates to trigger events and parameters to send values.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Decision_panel'>Show/Hide Model Details</button>
<div id="Decision_panel" class="collapse">

### Configuring a Decision Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : Decision

#### Parameters settings <a name="p2.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

| Parameter name    |                Type                 | Is mandatory? | Description                                                                                             |
| ----------------- | :---------------------------------: | :-----------: | ------------------------------------------------------------------------------------------------------- |
| **begin_date**    | string (YYYY-MM-DD)<br>or<br>double |      [x]      | Simulation starting date                                                                                |
| **fertiMinNO3_X** |               string                |      [x]      | mineral NO3 Fertilizer events X with X an integer. a string of the form "date=1992-02-26\$dose=30" (??) |
| **fertiMinNH4_X** |               string                |      [x]      | mineral NH4 Fertilizer events X with X an integer. a string of the form "date=1992-02-26\$dose=30" (??) |
| **irrigation_X**  |               string                |      [x]      | Irrigation events X with X an integer. a string of the form "date=1992-02-26\$dose=30" (??)             |

#### Input settings <a name="p2.1.3"></a>

None

#### Output settings <a name="p2.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **DoseEngMinNO3** : daily NO3 fertilizer amount (??)
* **DoseEngMinNH4** : daily NH4 fertilizer amount (??)
* **I** : daily Irrigation amount (mm)

### Details <a name="p2.2"></a>

#### State variables equations <a name="p2.2.1"></a>

> $DoseEngMinNO3$ takes the value of `dose` when the date `date` is reached (in `fertiMinNO3_X`) otherwise value is 0

> $DoseEngMinNH4$ takes the value of `dose` when the date `date` is reached (in `fertiMinNH4_X`) otherwise value is 0

> $I$ takes the value of `dose` when the date `date` is reached (in `irrigation_X`) otherwise value is 0

</div>

--- 

## Atomic model Frost <a name="p3"></a>

The **Frost** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Frost_panel'>Show/Hide Model Details</button>
<div id="Frost_panel" class="collapse">

### Configuring a Frost Model <a name="p3.1"></a>

#### Dynamics settings <a name="p3.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : Frost

#### Parameters settings <a name="p3.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlant`](#annex.p.plt) (`NbFi`, `phyllo`, `NbFf`, `R_Coleo`, `Rmax_var`, `begin_date`, `date_S`, `Rmin`, `Tmax_R`, `Tmin_R`, `Njend`, `Njdes`, `date_fin_GEL`, `GELmax_seuil`) to get all available parameters

#### Input settings <a name="p3.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Tmoy** : daily average temperature (°C) (sync)
* **Tmin** : daily minimal temperature (°C) (sync)
* **Tmax** : daily maximal temperature (°C) (sync)
* **STapS** : Temps thermique depuis le semis (°C.d) (sync)
* **STapLEVEE** : Temps thermique depuis la levee (°C.d) (sync)

#### Output settings <a name="p3.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **ST_S_i** : somme de température entre le semis et le stade initial ou on l'augmente le Rmax (°C.d)
* **ST_S_f** : somme de température entre le semis et le stade final ou on l'augmente plus le Rmax (°C.d)
* **Rmax** : ?? (??)
* **Rpot** : ?? (??)
* **dR** : variation journaliere de la résistance (°C.d)
* **R** : ?? (??)
* **dindiceGEL** : stress ou ecart journalier entre la resistance et la temperature minimale (??)
* **stressGEL** : destruction de la biomasse (de 0: pas de stress à 1 : stress TOTAL)  
* **stressGEL_cumul** : Intensité cumulée de stressGEL entre la levée et la date_fin_GEL: cumul de 1-stressGEL (??)
* **dDes** : desendurcissement journalier (??)
* **Des_cumul** : desendurcissement cumule (??)
* **dEnd** : endurcissement journalier (°C)
* **End_cumul** : endurcissement cumulé (°C)
* **pEnd** : Pourcentage d'endurcissement (%)
* **pEndmax** : Pourcentage d'endurcissement maximal (%)
* **indiceGEL_cumul** : stress cumulé Gel (??)
* **indiceGELmax** : écart maximum journalier (??)
* **NjGel** : nombre de jours de gel (d)

### Details <a name="p3.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p3.2.1"></a>

>$ST\_S\_i(t) = STapS(t) - STapLEVEE(t) + NbFi * phyllo$

>$ST\_S\_f(t) = STapS(t) - STapLEVEE(t)  + NbFf * phyllo$

> $$Rmax(t)=\left\{
  \begin{array}{@{}ll@{}}
    R\_Coleo, & \text{if}\ STapS(t) \leq ST\_S\_i(t) \\
    \frac{(Rmax\_var - R\_Coleo) * STapS(t) + (R\_Coleo * ST\_S\_f(t) - Rmax\_var * ST\_S\_i(t))}{ST\_S\_f(t) - ST\_S\_i(t)}, & \text{else if}\ STapS(t) \leq ST\_S\_f(t) \\
    Rmax\_var, & \text{otherwise}
  \end{array}\right. 
\label{eq:Rmax}$$

> $$Rpot(t)=\left\{
  \begin{array}{@{}ll@{}}
    Rmin, & \text{if}\ \text{current date is before date_S} \\
    Rmax(t), & \text{else if}\ (Tmoy(t) < Tmax\_R) and (Tmoy(t) < Tmin\_R) \\
    \frac{(Rmin - Rmax(t)) * Tmoy(t) + Rmax(t) * Tmax\_R - Rmin * Tmin\_R}{Tmax\_R - Tmin\_R}, & \text{else if}\ (Tmoy(t) < Tmax\_R) and (Tmoy(t) \leq Tmin\_R) \\
    Rmin, & \text{otherwise}
  \end{array}\right. 
\label{eq:Rpot}$$

> $$dR(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ \text{current date is before date_S} \\
    \frac{Rpot(t) - Rmin}{Njend}, & \text{else if}\ Rpot(t) < R(t-1) \\
    \frac{(Rmin - Rmax(t)) * Tmoy(t)}{20.0 * Njdes}, & \text{otherwise}
  \end{array}\right. 
\label{eq:dR}$$

> $$R(t)=\left\{
  \begin{array}{@{}ll@{}}
    Rmin, & \text{if}\ \text{current date is before date_S} \\
    R(t-1), & \text{else if}\ R(t-1)==Rpot(t) \\
    \max\left(
      \begin{array}{@{}ll@{}}
        R(t-1)+dR(t) \\
        Rpot(t)
    \end{array}\right), & \text{else if}\ R(t-1)>Rpot(t) \\
    \min\left(
      \begin{array}{@{}ll@{}}
        R(t-1)+dR(t) \\
        Rpot(t)
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:R}$$

> $$dindiceGEL(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ \text{current date is before date_S} \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0 \\
        R(t) - Tmin(t)
    \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:dindiceGEL}$$

> $$stressGEL(t)=\left\{
  \begin{array}{@{}ll@{}}
    1, & \text{if}\ (STapLEVEE(t) == 0) \text{or} \text{current date is before date_S} \\
    1, & \text{else if}\ Tmin(t) \geq R(t) \\
    0, & \text{else if}\ Tmin(t) \leq R(t) - GELmax\_seuil \\
    1 - \frac{dindiceGEL(t)}{GELmax\_seuil}, & \text{otherwise}
  \end{array}\right. 
\label{eq:stressGEL}$$

> $$stressGEL\_cumul(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ STapLEVEE(t) == 0 \\
    stressGEL\_cumul(t-1) + (1 - stressGEL(t)), & \text{otherwise}
  \end{array}\right. 
\label{eq:stressGEL_cumul}$$

> $$dDes(t)=\left\{
  \begin{array}{@{}ll@{}}
    R(t) - R(t-1), & \text{if}\ R(t) > R(t-1) \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:dDes}$$

>$Des\_cumul(t) = Des\_cumul(t-1) + dDes(t)$

> $$dEnd(t)=\left\{
  \begin{array}{@{}ll@{}}
    R(t) - R(t-1), & \text{if}\ R(t) < R(t-1) \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:dEnd}$$

>$End\_cumul(t) = End\_cumul(t-1) + dEnd(t)$

> $$pEnd(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ \text{current date is before date_S} \\
    100 * \frac{R(t) - Rmin}{Rmax(t) - Rmin}, & \text{otherwise}
  \end{array}\right. 
\label{eq:pEnd}$$

> $$pEndmax(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ \text{current date is before date_S} \\
    pEnd(t), & \text{else if}\ pEnd(t) > pEndmax(t-1) \\
    pEndmax(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:pEndmax}$$

> $$indiceGEL\_cumul(t)=\left\{
  \begin{array}{@{}ll@{}}
    indiceGEL\_cumul(t-1), & \text{if}\ dindiceGEL(t)==0 \\
    indiceGEL\_cumul(t-1) + dindiceGEL(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:indiceGEL_cumul}$$

> $$indiceGELmax(t)=\left\{
  \begin{array}{@{}ll@{}}
    dindiceGEL(t), & \text{if}\ dindiceGEL(t) > indiceGELmax(t-1) \\
    indiceGELmax(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:indiceGELmax}$$

> $$NjGel(t)=\left\{
  \begin{array}{@{}ll@{}}
    NjGel(t-1) + 1, & \text{if}\ dindiceGEL(t) > 0 \\
    NjGel(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:NjGel}$$

</div>

--- 

## Atomic model NitrogenFixationPea <a name="p4"></a>

The **NitrogenFixationPea** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#NitrogenFixationPea_panel'>Show/Hide Model Details</button>
<div id="NitrogenFixationPea_panel" class="collapse">

### Configuring a NitrogenFixationPea Model <a name="p4.1"></a>

#### Dynamics settings <a name="p4.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : NitrogenFixationPea

#### Parameters settings <a name="p4.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersSoil`](#annex.p.soil) (`da_C0`, `da_C0_opt`, `ep_C0`, `RUmax`) and [`ParametersPlantPea`](#annex.p.plt.pea) (`Profnod_opt`, `Profnod_min`, `damax_C0`, `begin_date`, `date_S`, `date_FLO`, `NJstress_nod_seuil`, `coefNsol_fix1`, `a_pNfix`, `pNfix_max`, `coefNsol_fix2`, `b_pNfix`, `STfixmax_seuil`, `coef_fixmax1`, `coef_fixmax2`, `coef_tot_a`) to get all available parameters

#### Input settings <a name="p4.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **dQN_besoin** : Besoin de la culture le jour j (kg N/ha) (nosync)
* **QN_tot** : Quantite d azote totale accumulee par la culture le jour j (kg N/ha) (nosync)
* **Stade** : current phenological stage (see [shared defined stage](#annex.pheno.stages)) (sync)
* **RU_C0** : Reserve utile dans la couche C0 le jour j avec la prise en compte du drainage issu de cette couche (??) (sync)
* **ProfRacmax** : profondeur racinaire maximum (mm) (sync)
* **QNaccess_C0** : Quantite d'azote accessible par les racines pour la culture dans la couche C0 (kg/ha) (sync)
* **STapLEVEE** : Temps thermique depuis la levee (°C.d) (sync)
* **date_DRG** : date DRG (??) (sync)
* **MS** :  matiere seche accumulee depuis l initilisation jusqu au jour j (g/m²) (sync)
* **coef_fixmax3** : ?? (??) (sync)

#### Output settings <a name="p4.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **Profnod** : Profondeur des nodosites jusqu'au jour j (mm)
* **RU_nod** : Quantite d'eau dans la couche avec nodosites (mm)
* **stressH_nod** : Presence d'un stress hydrique pour le jour j (0 ou 1)
* **NJstressH_nod** : Nombre de jours ou la couche avec nodosites est en stress hydrique par rapport a la fixation (d)
* **pNac_fix** : Fixation %total accumule qui provient de la fixation le jour j (traduit l'inhibition de la fixation due a la presence d'azote mineral dans le sol) (%)
* **dQNfixmax_tot** : Quantite max d'azote fixe par jour (ne prend pas en compte l'inhibition de la fixation par la presence d'N mineral dans le sol) (kgN/ha)
* **dQNfix_besoin** : Quantite d'azote dont la plante a besoin qui est issu de la fixation (proportion des besoins totaux)(kgN/ha)
* **dQNfix_tot** : Quantite totale d'azote fixe le jour j (kgN/ha)
* **QNfix** : Quantite d'azote dans les parties aeriennes issu de la fixation jusqu'au jour j (kgN/ha)
* **Ndfa** : Nitrogen derived from fixation: proportion d'azote fixe sur l'azote total accumule dans les parties aeriennes jusqu'au jour j (%)

### Details <a name="p4.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p4.2.1"></a>

> $$Profnod(t)=\left\{
  \begin{array}{@{}ll@{}}
    Profnod\_opt, & \text{if}\ da\_C0 \leq da\_C0\_opt \\
    \frac{(da\_C0 - da\_C0\_opt) * (Profnod\_opt - Profnod\_min)}{da\_C0\_opt - damax\_C0} + Profnod\_opt, & \text{else if}\ da\_C0 \leq damax\_C0 \\
    Profnod\_min, & \text{otherwise}
  \end{array}\right. 
\label{eq:Profnod}$$

> $$RU\_nod(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ \text{current date is before date_S} \\
    \frac{RU\_C0(t) * Profnod(t)}{ep\_C0}, & \text{otherwise}
  \end{array}\right. 
\label{eq:RU_nod}$$

> $$stressH\_nod(t)=\left\{
  \begin{array}{@{}ll@{}}
    1, & \text{if}\ RU\_nod(t) < \frac{Profnod(t)}{ProfRacmax(t)} * \frac{RUmax}{3} \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:stressH_nod}$$

> $$NJstressH\_nod(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ \text{current date is before date_FLO} \\
    NJstressH\_nod(t-1), & \text{else if}\ NJstressH\_nod(t-1) \leq NJstress\_nod\_seuil \\
    0, & \text{else if}\ stressH\_nod(t) == 0.0 \\
    NJstressH\_nod(t-1) + stressH\_nod(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:NJstressH_nod}$$

> $$pNac\_fix(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    \max\left(
        \begin{array}{@{}ll@{}}
          0.0 \\
        \min\left(
            \begin{array}{@{}ll@{}}
            pNfix\_max \\
            coefNsol\_fix1 * QNaccess\_C0(t) + a\_pNfix
            \end{array}\right) \\
        \end{array}\right), & \text{else if}\ \text{current date is before date_FLO} \\
    \max\left(
        \begin{array}{@{}ll@{}}
          0.0 \\
        \min\left(
            \begin{array}{@{}ll@{}}
            pNfix\_max \\
            coefNsol\_fix2 * QNaccess\_C0(t) + b\_pNfix
            \end{array}\right) \\
        \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:pNac_fix}$$

> $$dQNfixmax\_tot(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ STapLEVEE(t) < STfixmax\_seuil \\
    \max\left(
        \begin{array}{@{}ll@{}}
          0.0 \\
          coef\_tot\_a * (MS(t) - MS(t-1)) * 10.0 * (coef\_fixmax1 * STapLEVEE(t) + coef\_fixmax3(t))
        \end{array}\right), & \text{else if}\ Stade(t) \text{before DRG_FSLA} \\
    \max\left(
        \begin{array}{@{}ll@{}}
          0.0 \\
          (MS(t) - MS(t-1)) * 10.0 * coef\_fixmax2 * coef\_tot\_a
        \end{array}\right), & \text{else if}\ Stade(t) \text{before FSLA_MP} \\
    0.0, & \text{otherwise}
  \end{array}\right. 
\label{eq:dQNfixmax_tot}$$

>$dQNfix\_besoin(t) = dQN\_besoin(t-1) * \frac{pNac\_fix(t-1)}{100}$

> $$dQNfix\_tot(t)=\left\{
  \begin{array}{@{}ll@{}}
    \min\left(
        \begin{array}{@{}ll@{}}
          dQNfix\_besoin(t) \\
          dQNfixmax\_tot(t)
        \end{array}\right), & \text{if}\ \text{current date is before date_FLO or } NJstressH\_nod(t-1) \leq NJstress\_nod\_seuil \\
    0.0, & \text{otherwise}
  \end{array}\right. 
\label{eq:dQNfix_tot}$$

> $$QNfix(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    \frac{dQNfix\_tot(t)}{coef\_tot\_a} + QNfix(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNfix}$$

> $$Ndfa(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    \frac{QNfix(t-1)}{QN\_tot(t-1)} * \frac{100}{coef\_tot\_a}, & \text{otherwise}
  \end{array}\right. 
\label{eq:Ndfa}$$

</div>

--- 

## Atomic model NitrogenPlantPea <a name="p5"></a>

The **NitrogenPlantPea** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#NitrogenPlantPea_panel'>Show/Hide Model Details</button>
<div id="NitrogenPlantPea_panel" class="collapse">

### Configuring a NitrogenPlantPea Model <a name="p5.1"></a>

#### Dynamics settings <a name="p5.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : NitrogenPlantPea

#### Parameters settings <a name="p5.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantPea`](#annex.p.plt.pea) (`QNa_init`, `MS_Nc_seuil`, `tNc_Nc`, `coef_tot_a`, `coef_Nc`, `pente_Nc`, `MS_Nmax_seuil`, `tNmax_Nmax`, `coef_Nmax`, `pente_Nmax`, `pMSapDRG`, `Vmax`, `MS_Nmax_seuil`) to get all available parameters

#### Input settings <a name="p5.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Stade** : current phenological stage (see [shared defined stage](#annex.pheno.stages)) (sync)
* **MS** :  matiere seche accumulee depuis l initilisation jusqu au jour j (g/m²) (sync)
* **dQNfix_tot** : Quantite totale d'azote fixe le jour j (kgN/ha) (sync)
* **Tmoy** : daily average temperature (°C) (sync)
* **QN_offre** : Quantite totale d'azote qui sera disponible pour la culture (??) (sync)
* **QNaccess_C0** : Quantite d'azote accessible par les racines pour la culture dans la couche C0 (kg/ha) (sync)
* **QNaccess_C1** : Quantite d'azote contenue dans la couche C1 (??) (sync)
* **densRac** : densite racinaire (%)) (sync)
* **QNveg** : Quantite d'azote cumulee dans les parties vegetatives aériennes au jour j (kg/ha) (sync)
* **MSG** : Accumulation de MS dans les graines jusqu'au jour j (g/m²) (sync)
* **MS_DRG** : matiere seche accumulee depuis l initilisation jusqu a DRG (g/m²) (sync)


#### Output settings <a name="p5.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **QN_tot** : Quantite d azote totale accumulee par la culture le jour j (kg N/ha)
* **dQN_besoin** : Besoin de la culture le jour j (kg N/ha)
* **dQN_demande** : Demande en azote total de la culture en prenant en compte ses besoins et sa vitesse maximale d'absorption  (kg N/ha)
* **dQNabs** : Quantite d azote absorbe par la culture le jour j (kg.ha-1)
* **dQNabs_C0** : Quantites d'azote absorbees dans C0 (kg/ha)
* **dQNabs_C1** : Quantites d'azote absorbees dans C1 (kg/ha)
* **tNa** : Teneur en azote aerien (%)
* **tNa_DF** : Teneur en azote des parties aériennes à début floraison (%)
* **tNveg** : Teneur en azote de la partie vegetative, partie aerienne(sans les graines) (%)
* **tNveg_MP** : Teneur en azote des parties aériennes végétatives à maturité physiologique (%)  
* **QNc** : Quantite d azote critique le jour j (kg N/ha)

### Details <a name="p5.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p5.2.1"></a>

> $QN\_tot(t) = QN\_tot(t-1) + dQNabs(t-1) + dQNfix\_tot(t-1)$ after the crop emergence  
> Value is 0 before.   
> On the day of Emergence value is initialized with the following:  
> If the QNa_init parameter value IS provided:  
> $QN\_tot\_init(t_{Emergence}) = QNa\_init * coef\_tot\_a$  
> If the QNa_init parameter value IS NOT provided:  
> $$QN\_tot\_init(t_{Emergence})=\left\{
  \begin{array}{@{}ll@{}}
    \frac{MS(t_{Emergence})}{100} * 10.0 * tNc\_Nc * coef\_tot\_a, & \text{if}\ \frac{MS(t_{Emergence})}{100} < MS\_Nc\_seuil \\
    \frac{MS(t_{Emergence})}{100} * 10.0 * coef\_Nc * (\frac{MS(t_{Emergence})}{100})^{pente\_Nc} * coef\_tot\_a, & \text{otherwise}
  \end{array}\right. 
\label{eq:QN_tot_init}$$

> Before the crop emergence and after the physiological maturity  
> $$dQN\_besoin(t) = 0.0$$  
> Between the crop emergence and `DRG_FSLA`  
> $$dQN\_besoin(t)=\left\{
  \begin{array}{@{}ll@{}}
    \max\left(
        \begin{array}{@{}ll@{}}
          0.0 \\
          (\frac{MS(t) - MS(t-1)}{100}) * coef\_tot\_a * 10.0 * tNmax\_Nmax
        \end{array}\right), & \text{if}\ \frac{MS(t)}{100} < MS\_Nmax\_seuil \\
    \max\left(
        \begin{array}{@{}ll@{}}
          0.0 \\
          (\frac{MS(t)}{100} * (\frac{MS(t)}{100})^{pente\_Nmax} - \frac{MS(t-1)}{100} * (\frac{MS(t-1)}{100})^{pente\_Nmax}) * coef\_tot\_a * 10.0 * coef\_Nmax
        \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:dQN_besoin1}$$
> From `DRG_FSLA` until the physiological maturity  
> $$dQN\_besoin(t)=\max\left(
  \begin{array}{@{}ll@{}}
    0.0 \\
    (\frac{MS(t) - MS(t-1)}{100}) * coef\_tot\_a * 10.0 * coef\_Nmax * (MS\_DRG(t) * pMSapDRG / 100)^{pente\_Nmax}
  \end{array}\right)
\label{eq:dQN_besoin2}$$  

> $$dQN\_demande(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \text{before VEGETATIF or } Tmoy(t) \leq 0.0 \\
    \min\left(
        \begin{array}{@{}ll@{}}
          dQN\_besoin(t) \\
        Vmax * \max\left(
            \begin{array}{@{}ll@{}}
            0.0 \\
            Tmoy(t-1)
            \end{array}\right) \\
        \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:dQN_demande}$$

> $$dQNabs(t)=\left\{
  \begin{array}{@{}ll@{}}
    0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    \max\left(
        \begin{array}{@{}ll@{}}
          0.0 \\
        \min\left(
            \begin{array}{@{}ll@{}}
            dQN\_demande(t) - dQNfix\_tot(t) \\
            \frac{QN\_offre(t) * densRac(t)}{100}
            \end{array}\right) \\
        \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:dQNabs}$$

> $$dQNabs\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ QN\_offre(t) == 0.0 \\
    \frac{QNaccess\_C0(t)}{QNaccess\_C0(t) + QNaccess\_C1(t)} * dQNabs(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:dQNabs_C0}$$

> $$dQNabs\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ QN\_offre(t) == 0.0 \\
    \frac{QNaccess\_C1(t)}{QNaccess\_C0(t) + QNaccess\_C1(t)} * dQNabs(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:dQNabs_C1}$$

> $$tNa(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    tNmax\_Nmax, & \text{else if}\ \frac{MS(t)}{100} < MS\_Nmax\_seuil \\
    100 * \frac{\frac{QN\_tot(t)}{coef\_tot\_a}}{MS(t)*10}, & \text{otherwise}
  \end{array}\right. 
\label{eq:tNa}$$

> $$tNa\_DF(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before DF_DRG} \\
    tNa(t), & \text{else if}\ \text{on the first day of DF_DRG} \\
    tNa\_DF(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:tNa_DF}$$

> $$tNveg(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    tNa(t), & \text{else if}\ Stade(t) \text{before FSLA_MP} \\
    \min\left(
      \begin{array}{@{}ll@{}}
      tNveg(t-1) \\
      \frac{100 * QNveg(t)}{(MS(t) - MSG(t)) * 10}
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:tNveg}$$

> $$tNveg\_MP(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before MP} \\
    tNveg(t), & \text{else if}\ \text{on the first day of MP} \\
    tNveg\_MP(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:tNveg_MP}$$

> Before DRG_FSLA  
> $$QNc(t)=\left\{
  \begin{array}{@{}ll@{}}
    \frac{MS(t)}{100} * 10.0 * tNc\_Nc, & \text{if}\ \frac{MS(t)}{100} < MS\_Nmax\_seuil \\
    \frac{MS(t)}{100} * 10.0 * coef\_Nc * (\frac{MS(t)}{100})^{pente\_Nc}, & \text{otherwise}
  \end{array}\right. 
\label{eq:QNc1}$$
> From `DRG_FSLA` until the physiological maturity  
> $$QNc(t) = \frac{MS(t)}{100} * 10.0 * coef\_Nc * (MS\_DRG(t) * pMSapDRG/100)^{pente\_Nc}
\label{eq:QNc2}$$  

</div>

--- 

## Atomic model NitrogenSoil <a name="p6"></a>

The **NitrogenSoil** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#NitrogenSoil_panel'>Show/Hide Model Details</button>
<div id="NitrogenSoil_panel" class="collapse">

### Configuring a NitrogenSoil Model <a name="p6.1"></a>

#### Dynamics settings <a name="p6.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : NitrogenSoil

#### Parameters settings <a name="p6.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersSoil`](#annex.p.soil) (`a`, `b`, `c`, `d`, `f`, `g`, `Y`, `alpha`, `beta`, `delta`, `gamma`, `Tref`, `para_kres1`, `C_res`, `C_Nres`, `para_kreshum`, `paraC_Nres1`, `paraC_Nres4`, `C_Nhum`, `fsdc`, `coef_k2`, `Arg`, `CaCO3`, `ep_C0`, `da_C0`, `tNorg`, `QNsol_init_C0`, `QNsol_init_tot`, `Prof_Reliquat_tot`, `QNND`) and [`ParametersPlant`](#annex.p.plt) (`begin_date`, `date_init_miner_res`, `date_init_N`, `date_init_MS`) to get all available parameters

#### Input settings <a name="p6.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **QNEng** : ?? (??) (nosync)
* **QNabs_C0** : Quantites d'azote absorbees dans C0 (kg/ha) (nosync)
* **QNabs_C1** : Quantites d'azote absorbees dans C1 (kg/ha) (nosync)
* **Tmoy** : Daily average temperature (°C) (sync)
* **stressH_C0** : Facteur de stress hydrique pour la couche C0 (varie entre 0 et 1) (sync)
* **ProfRac** : profondeur racinaire le jour j (mm) (sync)
* **ProfRacmax** : profondeur racinaire maximum (mm) (sync)
* **StockEauD_C0** : Drainage de C0 vers la couche en dessous (mm) (sync)
* **RUmax_C0** : Quantite d eau maximale disponible contenue dans la couche C0 (mm) (sync)
* **StockEauD_C1** : Drainage de C1 vers la couche C2 (mm) (sync)
* **RUmax_C1** : Quantite d eau maximale disponible contenue dans la couche C1 (mm) (sync)
* **StockEauD_C2** : Drainage de C2 vers la couche en dessous (mm) (sync)
* **RUmax_C2** : Quantite d eau maximale disponible contenue dans la couche C2 (mm) (sync)

#### Output settings <a name="p6.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **Ft_min** : Fonction temperature pour le caclul de la minéralistion des résidus (??)
* **JN** : Fonction de calcul des jours normalisés (??)
* **k_res** : Taux de décomposition des résidus (??)
* **QC_res** : Quantite de carbone dans le pool des résidus (??)
* **QN_res** : Quantite d Azode dans le pool des residus (??)
* **k_micr** : Taux de décomposition de la biomasse microbienne (??)
* **QC_micr** : Quantite de carbonne dans le pool de la biomasse microbienne (??)
* **QN_micr** : Quantite d Azode dans le pool de la biomasse microbienne (??)
* **k_hum** : Taux de décomposition de l'humus (??)
* **QC_hum** : Quantite de carbone dans le pool de l'humus (??)
* **QN_hum** : Quantite d Azode dans le pool de l'humus (??)
* **dQN_Mr** : Quantite d'azote liberes (??)
* **dQC_Mr** : Quantite de carbone liberes (??)
* **dQN_Mh** : Mineralisation de l'humus (kg/ha)
* **pNlixiv_C0** : Fraction (proportion) d'azote qui sera lixivee en provenance de la couche C0 (calculee a partir du drainage de la couche C0) (??)
* **QNlixiv_C0** : Quantite d'azote lixiviee en provenance de la couche C0 (??)
* **QN_C0** : Quantite totale d'azote contenue dans la couche C0 le jour j (kg/ha)
* **QNaccess_C0** : Quantite d'azote accessible par les racines pour la culture dans la couche C0 (kg/ha)
* **QNaccess_C1** : Quantite d'azote contenue dans la couche C1 (??)
* **pNlixiv_C1** : Fraction (proportion) d'azote qui sera lixivee en provenance de la couche C1 (calculee a partir du drainage de la couche C0) (??)
* **QNlixiv_C1** : Quantite d'azote lixiviee en provenance de la couche C1 (??)
* **QN_C2** : Quantite d'azote contenue dans la couche C2 (??)
* **pNlixiv_C2** : Fraction (proportion) d'azote qui sera lixivee en provenance de la couche C2 (calculee a partir du drainage de la couche C1) (??)
* **QNlixiv_C2** : Quantite d'azote lixiviee en provenance de la couche C2 (??)
* **TN_C2** : Quantite d'azote par cm de sol dans la couche C2 (??)
* **QNlixiv_tot** : Quantite d'azote lixivie cumule depuis l'initialisation (??)
* **QNsol_tot** : Quantite d'azote minerale totale dans le sol (??)
* **QNnondispo** : Azote non disponible pour la culture (??)
* **QN_offre** : Quantite totale d'azote qui sera disponible pour la culture (??)

### Details <a name="p6.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p6.2.1"></a>

> $$Ft\_min(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Tmoy(t) \leq 0.0 \\
    (a + b * e^{c * \frac{Tmoy(t)}{Tref}})^{d}, & \text{otherwise}
  \end{array}\right. 
\label{eq:Ft_min}$$

> $$JN(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_miner_res}\\
    JN(t-1) + Ft\_min(t) * stressH\_C0(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:JN}$$

> $$k\_res(t) = e^{-para\_kres1 * JN(t)}
\label{eq:k_res}$$

> $$QC\_res(t) = C\_res * k\_res(t)
\label{eq:QC_res}$$

> $$QN\_res(t) = \frac{QC\_res(t)}{C\_Nres}
\label{eq:QN_res}$$

> $$k\_micr(t) = \frac{para\_kres1 * Y}{para\_kreshum - para\_kres1} * (k\_res(t) - e^{-para\_kreshum * JN(t)})
\label{eq:k_micr}$$

> $$QC\_micr(t) = C\_res * k\_micr(t)
\label{eq:QC_micr}$$

> $$QN\_micr(t)=\left\{
  \begin{array}{@{}ll@{}}
    \frac{QC\_micr(t)}{paraC\_Nres4}, & \text{if}\ C\_Nres < paraC\_Nres1 \\
    \frac{QC\_micr(t)}{paraC\_Nres1 + \frac{paraC\_Nres1}{C\_Nres}}, & \text{otherwise}
  \end{array}\right. 
\label{eq:QN_micr}$$

> $$k\_hum(t) = \max\left(
      \begin{array}{@{}ll@{}}
      0.0 \\
      Y * h + (coef\_khum * (para\_kres1 * e^{-para\_kreshum * JN(t)})) - para\_kreshum* k\_res(t))
      \end{array}\right)
\label{eq:k_hum}$$
> with  
> $$h = 1 - \frac{f * C\_Nres}{g + C\_Nres}
>\label{eq:h}$$
> and  
> $$coef\_khum = \frac{Y * h}{para\_kreshum - para\_kres1}
>\label{eq:coef_khum}$$

> $$QC\_hum(t) = C\_res * k\_hum(t)
\label{eq:QC_hum}$$

> $$QN\_hum(t) = \frac{QC\_hum(t)}{C\_Nhum}
\label{eq:QN_hum}$$

> $$dQN\_Mr(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{date before date_init_N} \\
    (\frac{C\_res}{C\_Nres} - (QN\_res(t) + QN\_hum(t) + QN\_micr(t))) - (\frac{C\_res}{C\_Nres} - (QN\_res(t-1) + QN\_hum(t-1) + QN\_micr(t-1))), & \text{otherwise}
  \end{array}\right. 
\label{eq:dQN_Mr}$$

> $$dQC\_Mr(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{date before date_init_N} \\
    (C\_res - (QC\_res(t) + QC\_hum(t) + QC\_micr(t))) - (C\_res - (QC\_res(t-1) + QC\_hum(t-1) + QC\_micr(t-1))), & \text{otherwise}
  \end{array}\right. 
\label{eq:dQC_Mr}$$

> $$dQN\_Mh(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Tmoy(t) < 0 \text{ or date before date_init_N} \\
    \frac{stressH\_C0(t) * fsdc * coef\_k2 * e^{alpha * (Tmoy(t) - Tref)}}{(Arg + gamma) * (CaCO3 + delta)} * ep\_C0 / 10.0 * da\_C0 * 1000.0 * tNorg * beta, & \text{otherwise}
  \end{array}\right. 
\label{eq:dQN_Mh}$$

> $$pNlixiv\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ (StockEauD\_C0(t) + RUmax\_C0(t)) == 0.0 \\
    (\frac{StockEauD\_C0(t)}{StockEauD\_C0(t) + \frac{RUmax\_C0(t)}{100}})^{ep\_C0/10.0/2.0}, & \text{otherwise}
  \end{array}\right. 
\label{eq:pNlixiv_C0}$$

> $$QNlixiv\_C0(t) = QN\_C0(t-1) * pNlixiv\_C0(t)
\label{eq:QNlixiv_C0}$$

> $$QN\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_N}\\
    QNsol\_init\_C0, & \text{else if}\ \text{current day is date_init_N}\\
    \max\left(
      \begin{array}{@{}ll@{}}
      0.0 \\
      QN\_C0(t-1) + dQN\_Mr(t-1) + dQN\_Mh(t-1) + QNEng(t-1) - QNlixiv\_C0(t) - QNabs\_C0(t-1)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:QN_C0}$$

> $$QNaccess\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    QN\_C0(t) * \frac{ProfRac(t)}{ep\_C0}, & \text{if}\ ProfRac(t) \leq ep\_C0 \\
    QN\_C0(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNaccess_C0}$$

> $$QNaccess\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_N or} ProfRac(t) \leq ep\_C0 \\
    \frac{QNsol\_init\_tot - QNsol\_init\_C0}{Prof\_Reliquat\_tot - ep\_C0} * (ProfRac(t) - ep\_C0), & \text{else if}\ \text{current day is date_init_N and} ProfRac(t) > ep\_C0 \\
    \max\left(
      \begin{array}{@{}ll@{}}
      0.0 \\
      QNaccess\_C1(t-1) + QNlixiv\_C0(t-1) - QNlixiv\_C1(t-1) - QNabs\_C1(t-1) - (ProfRac(t) - ProfRac(t-1)) * TN\_C2(t-1)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNaccess_C1}$$

> $$pNlixiv\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ (StockEauD\_C1(t) + RUmax\_C1(t)) == 0.0 \\
    (\frac{StockEauD\_C1(t)}{StockEauD\_C1(t) + \frac{RUmax\_C1(t)}{100.0}})^{(ProfRac(t) - ep\_C0)/10.0/2.0}, & \text{otherwise}
  \end{array}\right. 
\label{eq:pNlixiv_C1}$$

> $$QNlixiv\_C1(t) = QNaccess\_C1(t) * pNlixiv\_C1(t)
\label{eq:QNlixiv_C1}$$

> $$QN\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_N} \\
    \frac{QNsol\_init\_tot - QNsol\_init\_C0}{Prof\_Reliquat\_tot- ep\_C0} * \min\left(
      \begin{array}{@{}ll@{}}
      ProfRacmax(t) - ep\_C0 \\
      ProfRacmax(t) - ProfRac(t)
      \end{array}\right), & \text{if}\ \text{current day is date_init_N} \\
    \max\left(
      \begin{array}{@{}ll@{}}
      0.0 \\
      QN\_C2(t-1) + QNlixiv\_C1(t-1) - QNlixiv\_C2(-1) - (ProfRac(t) - ProfRac(t-1)) * TN\_C2(t-1)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:QN_C2}$$

> $$pNlixiv\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ (StockEauD\_C2(t) + RUmax\_C2(t)) == 0.0 \\
    (\frac{StockEauD\_C2(t)}{StockEauD\_C2(t) + \frac{RUmax\_C2(t)}{100.0}})^{(ProfRacmax(t) - ProfRac(t))/10.0/2.0}, & \text{otherwise}
  \end{array}\right. 
\label{eq:pNlixiv_C2}$$

> $$QNlixiv\_C2(t) = QN\_C2(t-1) * pNlixiv\_C2(t)
\label{eq:QNlixiv_C2}$$

> $$TN\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ ProfRac(t) == ProfRacmax(t) \\
    \frac{QN\_C2(t)}{ProfRacmax() - \max\left(
      \begin{array}{@{}ll@{}}
      ProfRac(t) \\
      ep\_C0
      \end{array}\right)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:TN_C2}$$

> $$QNlixiv\_tot(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_N} \\
    QNlixiv\_tot(t-1) + QNlixiv\_C2(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNlixiv_tot}$$

> $$QNsol\_tot(t) = QN\_C0(t) + QNaccess\_C1(t) + QN\_C2(t)
\label{eq:QNsol_tot}$$

> $$QNnondispo(t) = QNND * \frac{ProfRac(t)}{100.0}
\label{eq:QNnondispo}$$

> $$QN\_offre(t) = \max\left(
      \begin{array}{@{}ll@{}}
      0.0 \\
      QNaccess\_C0(t) + QNaccess\_C1(t) - QNnondispo(t)
      \end{array}\right)
\label{eq:QN_offre}$$

</div>

---

## Atomic model PhenologyPea <a name="p7"></a>

The **PhenologyPea** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate [Crop phenological stage](#annex.pheno.stages) based on thermal times and dates 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#PhenologyPea_panel'>Show/Hide Model Details</button>
<div id="PhenologyPea_panel" class="collapse">

### Configuring a PhenologyPea Model <a name="p7.1"></a>

#### Dynamics settings <a name="p7.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : PhenologyPea

#### Parameters settings <a name="p7.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantPea`](#annex.p.plt.pea) (`Tbase`, `begin_date`, `date_S`, `ST_S_LEVEE`, `date_FLO`, `ST_DF_DRG`, `DRG_ET`, `NETpot`, `STmax_FSLA_MP`) to get all available parameters

#### Input settings <a name="p7.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Tmin** : Daily minimal temperature (°C) (sync)
* **Tmax** : Daily maximal temperature (°C) (sync)
* **IndicMP** : indicateur de la date de maturite physiologique (??) (nosync)

#### Output settings <a name="p7.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **Tmoy** : Daily average temperature (°C)
* **Tmoy_base** : Degrés efficaces pour le developement de la plante, en prenant en compte une temperature de base (°C)
* **STapS** : Temps thermique depuis le semis (°C.d)
* **Stade** : Current phenological stage (see [shared defined stage](#annex.pheno.stages))
* **date_LEVEE** : date levee (??)
* **date_DRG** : date DRG (??)
* **date_FSLA** : date FSLA (??)
* **date_MP** : date MP (??)
* **STapLEVEE** : Temps thermique depuis la levee (°C.d)
* **STapDF** : Temps thermique depuis  debut floraison (°C.d)
* **STapDRG** : Temps thermique depuis DRG (°C.d)
* **STapFSLA** : Temps thermique depuis  FSLA (°C.d)
* **Nj_LEVEE** : Nombre de jours depuis le jour de la levee (d)
* **Nj_DRG** : Nombre de jours depuis le debut de remplissage des grains (d)
* **Nj_FSLA** : Nombre de jours depuis la date Fsla (d)
* **Nj_MP** : Nombre de jours depuis la maturite physio (d)
* **date_MP_prev** : maturite previsionnelle (??)
* **Nj_S_DF** : Nombre de jours entre le semis et la date de début floraison (d)
* **Nj_S_MP** : Nombre de jours entre le semis et la date de de maturité physiologique (d)

### Details <a name="p7.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p7.2.1"></a>

> $$Tmoy(t) = \frac{Tmin(t) + Tmax(t)}{2.0}
\label{eq:Tmoy}$$

> $$Tmoy\_base(t) = Tmoy(t) - Tbase
\label{eq:Tmoy_base}$$

> $$STapS(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current date is before date_S} \\
    STapS(t-1) + Tmoy\_base(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:STapS}$$

> $$Stade(t)$$
> init value SOL_NU  
> ![Stade(t)](phenoPea.svg "Stade(t)")

> $$date\_LEVEE(t)=\left\{
  \begin{array}{@{}ll@{}}
    current date, & \text{if}\ \text{first day of VEGETATIF} \\
    date\_LEVEE(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:date_LEVEE}$$

> $$date\_DRG(t)=\left\{
  \begin{array}{@{}ll@{}}
    current date, & \text{if}\ \text{first day of DRG_FSLA} \\
    date\_DRG(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:date_DRG}$$

> $$date\_FSLA(t)=\left\{
  \begin{array}{@{}ll@{}}
    current date, & \text{if}\ \text{first day of FSLA_MP} \\
    date\_FSLA(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:date_FSLA}$$

> $$date\_MP(t)=\left\{
  \begin{array}{@{}ll@{}}
    current date, & \text{if}\ \text{first day of MP} \\
    date\_MP(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:date_MP}$$

> $$STapLEVEE(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    STapLEVEE(t-1) + Tmoy\_base(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:STapLEVEE}$$

> $$STapDF(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before DF_DRG} \\
    STapDF(t-1) + Tmoy\_base(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:STapDF}$$

> $$STapDRG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before DRG_FSLA} \\
    STapDRG(t-1) + Tmoy\_base(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:STapDRG}$$

> $$STapFSLA(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before FSLA_MP} \\
    STapFSLA(t-1) + Tmoy\_base(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:STapFSLA}$$

> $$Nj\_LEVEE(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    Nj\_LEVEE(t-1) + 1, & \text{otherwise}
  \end{array}\right. 
\label{eq:Nj_LEVEE}$$

> $$Nj\_DRG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before DRG_FSLA} \\
    Nj\_DRG(t-1) + 1, & \text{otherwise}
  \end{array}\right. 
\label{eq:Nj_DRG}$$

> $$Nj\_FSLA(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before FSLA_MP} \\
    Nj\_FSLA(t-1) + 1, & \text{otherwise}
  \end{array}\right. 
\label{eq:Nj_FSLA}$$

> $$Nj\_MP(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before MP} \\
    Nj\_MP(t-1) + 1, & \text{otherwise}
  \end{array}\right. 
\label{eq:Nj_MP}$$

> $$date\_MP\_prev(t)=\left\{
  \begin{array}{@{}ll@{}}
    current date, & \text{if}\ Nj\_MP(t)==7 \\
    date\_MP\_prev(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:date_MP_prev}$$

> $$Nj\_S\_DF(t)=\left\{
  \begin{array}{@{}ll@{}}
    date\_FLO - date\_S, & \text{if}\ Stade(t) \text{after VEGETATIF} \\
    Nj\_S\_DF(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:Nj_S_DF}$$

> $$Nj\_S\_MP(t)=\left\{
  \begin{array}{@{}ll@{}}
    date\_MP(t) - date\_S, & \text{if}\ Stade(t) \text{after FSLA_MP} \\
    Nj\_S\_MP(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:Nj_S_MP}$$

</div>

--- 

## Atomic model PlantGrowthPea <a name="p8"></a>

The **PlantGrowthPea** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#PlantGrowthPea_panel'>Show/Hide Model Details</button>
<div id="PlantGrowthPea_panel" class="collapse">

### Configuring a PlantGrowthPea Model <a name="p8.1"></a>

#### Dynamics settings <a name="p8.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : PlantGrowthPea

#### Parameters settings <a name="p8.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantPea`](#annex.p.plt.pea) (`FLO_ET`, `NETpot`, `coef_demMS_demN`, `tNveg_seuil`, `Eimax`, `k`, `tNstruct`, `tNveg_seuil`, `begin_date`, `date_FLO`, `Ebv`, `Ebr`, `ST_DF_DRG`, `MS_init`, `densite_semis`, `PMGmoy_var`, `coef_MS_init`, `Ec`) to get all available parameters

#### Input settings <a name="p8.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **IndicMP** : indicateur de la date de maturite physiologique (??) (nosync)
* **tNveg** : Teneur en azote de la partie vegetative, partie aerienne(sans les graines) (%) (nosync)
* **QNc** : Quantite d azote critique le jour j (kg N/ha) (nosync)
* **stressN_Eb** : Facteur de diminution du Eb en fonction de l INN (de 0 :pas de stress a 1 : stress TOTAL) (nosync)
* **stressH** : Facteur de stress hydrique total qui prend en compte la fraction d’eau transportable sur les couches C0 et C1 (varie entre 0 et 1) (nosync)
* **stressT** : Facteur de diminution de l’efficience de conversion du rayonement en fonction de la température (sans unité) (nosync)
* **stressGEL** : destruction de la biomasse (de 0: pas de stress à 1 : stress TOTAL) (nosync)
* **Stade** : Current phenological stage (see [shared defined stage](#annex.pheno.stages)) (sync)
* **STapS** : Temps thermique depuis le semis (°C.d) (sync)
* **STapDF** : Temps thermique depuis  debut floraison (°C.d) (sync)
* **Tmoy** : Daily average temperature (°C) (sync)
* **RG** : Rayonnement Global (joules/cm2) (sync)

#### Output settings <a name="p8.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **LAI** : indice foliaire (??)
* **Ei** : efficience d'interception  du rayonnement globale (??)
* **Ebpot** : Efficience de conversion potentielle du rayonnement (sans stress N et H) (g/MJ)
* **Eb** : efficience de conversion du rayonnement (g/MJ)
* **MS** : matiere seche accumulee depuis l initilisation jusqu au jour j (g/m²)
* **MS_DF** : matiere seche accumulee depuis l initilisation jusqu a DF (g/m²)
* **MS_DRG** : matiere seche accumulee depuis l initilisation jusqu a DRG (g/m²)
* **MS_FSLA** : matiere seche accumulee depuis l initilisation jusqu a FSLA (g/m²)
* **MS_MP** : matiere seche à maturité physiologique (g/m²)
* **NjstressN_Eb** : Nombre de jour de stressN_Eb<1 (d)
* **stressN_cumul** : Intensité cumulée de stressN_Eb sur tout le cycle: cumul de 1-stressN_Eb (??)
* **NjstressH** : Nombre de jour de stressH : Cumul des jours où stressH<1 (d)
* **stressH_cumul** : Intensité cumulée de stressH sur tout le cycle: cumul de 1-stressH (??)
* **NjstressT** : Nombre de jour de stressT : Cumul des jours où stressT<0.8 (d)
* **stressT_cumul** : Intensité cumulée de stressT sur tout le cycle, jours ou stressT<0.8: cumul de 1-stressH (??)

### Details <a name="p8.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p8.2.1"></a>

> $$LAI(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF or IndicMP(t-1)=1} \\
    coef\_demMS\_demN * QNc(t-1), & \text{else if}\ STapDF(t-1) < (FLO\_ET * (NETpot - 1)) \\
    LAI(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:LAI}$$

> $$Ei(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF or IndicMP(t-1)=1} \\
    Eimax * (1 - e^{-k * LAI(t)}) * \frac{tNveg(t-1) - tNstruct}{tNveg\_seuil - tNstruct}, & \text{else if}\ Stade(t) \text{after DF_DRG and } tNveg(t-1) < tNveg\_seuil \\
    Eimax * (1 - e^{-k * LAI(t)}), & \text{otherwise}
  \end{array}\right. 
\label{eq:Ei}$$

> $$Ebpot(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF or IndicMP(t-1)=1} \\
    Ebv, & \text{if}\ \text{current date is before date_FLO} \\
    \max\left(
      \begin{array}{@{}ll@{}}
      0.0 \\
      \min\left(
        \begin{array}{@{}ll@{}}
        Ebr \\
        Ebr * \frac{tNveg(t-1) - tNstruct}{tNveg\_seuil - tNstruct}
        \end{array}\right)
      \end{array}\right), & \text{else if}\ Stade(t) \text{after DF_DRG and } tNveg(t-1) < tNveg\_seuil \\
    Ebv + (Ebr - Ebv) *\frac{STapDF(t)}{ST\_DF\_DRG}, & \text{otherwise}
  \end{array}\right. 
\label{eq:Ebpot}$$

> $$Eb(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    Ebpot(t) * stressT(t-1) * stressH(t-1) * stressN\_Eb(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:Eb}$$

> $$MS(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    \frac{densite\_semis * \frac{PMGmoy\_var}{1000}}{coef\_MS\_init}, & \text{else if}\ \text{first day of VEGETATIF and} MS\_init == -1 \\
    MS\_init, & \text{else if}\ \text{first day of VEGETATIF and} MS\_init \ne -1 \\
    MS(t-1) * stressGEL(t-1), & \text{else if}\ Ei(t)==0.0 \\
    (MS(t-1) + \frac{RG(t)}{100.0} * Ec * Ei(t) * Eb(t)) * stressGEL(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:MS}$$

> $$MS\_DF(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current date is before date_FLO} \\
    MS(t), & \text{else if}\ \text{current date is date_FLO} \\
    MS\_DF(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:MS_DF}$$

> $$MS\_DRG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before DRG_FSLA} \\
    MS(t), & \text{else if}\ \text{on the first day of DRG_FSLA} \\
    MS\_DRG(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:MS_DRG}$$

> $$MS\_FSLA(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before FSLA_MP} \\
    MS(t), & \text{else if}\ \text{on the first day of FSLA_MP} \\
    MS\_FSLA(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:MS_FSLA}$$

> $$MS\_MP(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before MP} \\
    MS(t), & \text{else if}\ \text{on the first day of MP} \\
    MS\_MP(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:MS_MP}$$

> $$NjstressN\_Eb(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    NjstressN\_Eb(t-1), & \text{if}\ Stade(t) \text{after MP or } stressN\_Eb(t) == 1 \\
    NjstressN\_Eb(t-1) + 1, & \text{otherwise}
  \end{array}\right. 
\label{eq:NjstressN_Eb}$$

> $$stressN\_cumul(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    stressN\_cumul(t-1), & \text{if}\ Stade(t) \text{after MP} \\
    stressN\_cumul(t-1) + (1 - stressN\_Eb(t)), & \text{otherwise}
  \end{array}\right. 
\label{eq:stressN_cumul}$$

> $$NjstressH(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    NjstressH(t-1), & \text{if}\ Stade(t) \text{after MP or } stressH(t) == 1 \\
    NjstressH(t-1) + 1, & \text{otherwise}
  \end{array}\right. 
\label{eq:NjstressH}$$

> $$stressH\_cumul(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    stressH\_cumul(t-1), & \text{if}\ Stade(t) \text{after MP} \\
    stressH\_cumul(t-1) + (1 - stressH(t)), & \text{otherwise}
  \end{array}\right. 
\label{eq:stressH_cumul}$$

> $$NjstressT(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    NjstressT(t-1), & \text{if}\ Stade(t) \text{after MP or } stressT(t) \leq 0.8 \\
    NjstressT(t-1) + 1, & \text{otherwise}
  \end{array}\right. 
\label{eq:NjstressT}$$

> $$stressT\_cumul(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    stressT\_cumul(t-1), & \text{if}\ Stade(t) \text{after MP or } stressT(t) \leq 0.8 \\
    stressT\_cumul(t-1) + (1 - stressT(t)), & \text{otherwise}
  \end{array}\right. 
\label{eq:stressT_cumul}$$

</div>

--- 

## Atomic model RootGrowthPea <a name="p9"></a>

The **RootGrowthPea** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#RootGrowthPea_panel'>Show/Hide Model Details</button>
<div id="RootGrowthPea_panel" class="collapse">

### Configuring a RootGrowthPea Model <a name="p9.1"></a>

#### Dynamics settings <a name="p9.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : RootGrowthPea

#### Parameters settings <a name="p9.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersSoil`](#annex.p.soil) (`MottesDelta`) and [`ParametersPlantPea`](#annex.p.plt.pea) (`begin_date`, `date_S`, `vRac_RG`, `ProfRacmax_pot`, `coef_Mottes_Profmax`, `date_FLO`, `densRac1`, `densRac2`) to get all available parameters

#### Input settings <a name="p9.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Tmoy** : Daily average temperature (°C) (sync)
* **RG** : Rayonnement Global (joules/cm2) (sync)

#### Output settings <a name="p9.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **ElongRac** : elongation racinaire le jour j (mm/d)
* **ProfRacmax** : profondeur racinaire maximum (mm)
* **ProfRac** : profondeur racinaire le jour j (mm)
* **densRac** : densite racinaire (%)

### Details <a name="p9.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p9.2.1"></a>

> $$ElongRac(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current date is before date_S or } Tmoy(t) < 0.0 \\
    \frac{RG(t)}{100.0} * vRac\_RG, & \text{otherwise}
  \end{array}\right. 
\label{eq:ElongRac}$$

> $$ProfRacmax(t) = ProfRacmax\_pot * (1 - coef\_Mottes\_Profmax * MottesDelta)
\label{eq:ProfRacmax}$$

> $$ProfRac(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current date is before date_S} \\
    \min\left(
      \begin{array}{@{}ll@{}}
        ProfRacmax(t) \\
        ProfRac(t-1) + ElongRac(t)
      \end{array}\right), & \text{else if}\ \text{current date is before date_FLO and } ProfRac(t-1) < ProfRacmax(t) \\
    ProfRac(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:ProfRac}$$

> $$densRac(t) = densRac1 * MottesDelta + densRac2
\label{eq:densRac}$$

</div>

--- 

## Atomic model StressNitrogenPea <a name="p10"></a>

The **StressNitrogenPea** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#StressNitrogenPea_panel'>Show/Hide Model Details</button>
<div id="StressNitrogenPea_panel" class="collapse">

### Configuring a StressNitrogenPea Model <a name="p10.1"></a>

#### Dynamics settings <a name="p10.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : StressNitrogenPea

#### Parameters settings <a name="p10.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantPea`](#annex.p.plt.pea) (`stressN_Eb1`, `stressN_Eb2`, `stressN_Eb3`) to get all available parameters

#### Input settings <a name="p10.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Stade** : Current phenological stage (see [shared defined stage](#annex.pheno.stages)) (sync)
* **QNG** : Quantite d'azote accumule dans les graines jusqu'au jour j (??) (sync)
* **QN_tot** : Quantite d azote totale accumulee par la culture le jour j (kg N/ha) (sync)
* **QNc** : Quantite d azote critique le jour j (kg N/ha) (sync)
* **MS** : matiere seche accumulee depuis l initilisation jusqu au jour j (??) (sync)
* **MSG** : Accumulation de MS dans les graines jusqu'au jour j (g/m²) (sync)
* **Tmoy** : Daily average temperature (°C)) (sync)
* **STapLEVEE** : Temps thermique depuis la levee (°C.d) (sync)
* **QNveg** : Quantite d'azote cumulee dans les parties vegetatives aériennes au jour j (kg/ha) (sync)
* **MS_DRG** : matiere seche accumulee depuis l initilisation jusqu a DRG (g/m²) (sync)


#### Output settings <a name="p10.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **INN** : indice de nutrition azotee le jour j  (sans unite)
* **aireINN** : Aire INN cumule depuis initialisation (??)
* **INNint** : INN integre le jour j (??)
* **stressN_Eb** : Facteur de diminution du Eb en fonction de l INN (de 0 :pas de stress a 1 : stress TOTAL)
* **INNint_DF** : Indice de nutrition azotée à début floraison (??)

### Details <a name="p10.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p10.2.1"></a>

> $$INN(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    \frac{QNveg(t)}{QNc(t)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:INN}$$

> $$aireINN(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        INN(t) * Tmoy(t)
      \end{array}\right), & \text{else if}\ \text{on the first day of VEGETATIF} \\
    aireINN(t-1) + \frac{\min\left(
      \begin{array}{@{}ll@{}}
        1.0 \\
        INN(t)
      \end{array}\right) + \min\left(
      \begin{array}{@{}ll@{}}
        1.0 \\
        INN(t-1)
      \end{array}\right)}{2.0} * \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        Tmoy(t)
      \end{array}\right), & \text{else if}\ Stade(t) \text{before DRG_FSLA} \\
    aireINN(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:aireINN}$$

> $$INNint(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before VEGETATIF} \\
   \frac{aireINN(t)}{Tmoy(t)}, & \text{else if}\ \text{on the first day of VEGETATIF} \\
    \frac{aireINN(t)}{STapLEVEE(t)}, & \text{else if}\ Stade(t) \text{before DRG_FSLA} \\
    INNint(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:INNint}$$

> $$stressN\_Eb(t)=\left\{
  \begin{array}{@{}ll@{}}
    1.0, & \text{if}\ Stade(t) \text{before VEGETATIF or on the first day of VEGETATIF}  \\
    \min\left(
      \begin{array}{@{}ll@{}}
        1.0 \\
        stressN\_Eb2 * (1.0 - stressN\_Eb3 * e^{stressN\_Eb1 * INNint(t)})
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:stressN_Eb}$$

> $$INNint\_DF(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before DF_DRG} \\
    INNint(t), & \text{else if}\ \text{on the first day of DF_DRG} \\
    INNint\_DF(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:INNint_DF}$$

</div>

--- 

## Atomic model StressT <a name="p11"></a>

The **StressT** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#StressT_panel'>Show/Hide Model Details</button>
<div id="StressT_panel" class="collapse">

### Configuring a StressT Model <a name="p11.1"></a>

#### Dynamics settings <a name="p11.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : StressT

#### Parameters settings <a name="p11.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlant`](#annex.p.plt) (`Topt_plante`, `Tmin_plante`, `Tmax_plante`) to get all available parameters

#### Input settings <a name="p11.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **Tmoy** : daily average temperature (°C) (sync)

#### Output settings <a name="p11.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **stressT** : Facteur de diminution de l’efficience de conversion du rayonement en fonction de la température (sans unité)

### Details <a name="p11.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p11.2.1"></a>

> $$stressT(t)=\left\{
  \begin{array}{@{}ll@{}}
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        1.0 - (\frac{Tmoy(t) - Topt\_plante}{Tmin\_plante - Topt\_plante})^2 
      \end{array}\right), & \text{if}\ Tmoy(t) \leq Topt\_plante \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        1.0 - (\frac{Tmoy(t) - Topt\_plante}{Tmax\_plante - Topt\_plante})^2 
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:stressT}$$

</div>

--- 

## Atomic model WaterSoil <a name="p12"></a>

The **WaterSoil** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#WaterSoil_panel'>Show/Hide Model Details</button>
<div id="WaterSoil_panel" class="collapse">

### Configuring a WaterSoil Model <a name="p12.1"></a>

#### Dynamics settings <a name="p12.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : WaterSoil

#### Parameters settings <a name="p12.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersSoil`](#annex.p.soil) (`Trans`, `ApportEau_seuil`, `JSA_seuil`, `EVAPO`, `ep_C0`, `RUmax`, `RU_init`, `MottesDelta`, `Mottes_seuil`, `coef_Mottes`, `stressH1`, `stressH2`, `stressH3`, `stressH4`, `stressH_seuil`) and [`ParametersPlant`](#annex.p.plt) (`begin_date`, `date_init_MS`, `date_init_H`) to get all available parameters

#### Input settings <a name="p12.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **ETP** : ?? (??) (Nosync)
* **P** : ?? (??) (Nosync)
* **I** : daily Irrigation amount (mm) (Nosync)
* **Ei** : efficience d'interception  du rayonnement globale (??) (Nosync)
* **ProfRacmax** : profondeur racinaire maximum (mm) (sync)
* **ProfRac** : profondeur racinaire le jour j (mm) (sync)

#### Output settings <a name="p12.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **TR** : Quantite d’eau perdue par transpiration de la culture le jour j issue de la couche C0 et de la couche C1 (mm)
* **TR_C0** : Quantite d’eau perdue par transpiration de la culture le jour j issue de la couche C0 (mm)
* **JSA** : Nombre de jours cumules sans apport d eau depuis le dernier apport (d)
* **rEV** : Reduction de l evaporation en fonction du nombre de jours sans apports d eau (varie entre 0 et 1)
* **EV** : Quantite d eau perdue par evaporation du sol le jour j issu de la couche C0 (mm)
* **RUmax_C0** : Quantite d eau maximale disponible contenue dans la couche C0 (mm)
* **RUavdr_C0** : Quantite d eau dans la couche C0 le jour j avant la prise en compte du drainage issu de cette couche (mm)
* **RU_C0** : Reserve utile dans la couche C0 le jour j avec la prise en compte du drainage issu de cette couche (??)
* **FTSW_C0** : Fraction d’eau transpirable dans le sol en C0 (varie entre 0 et 1)
* **stressH_C0** : Facteur de stress hydrique pour la couche C0 (varie entre 0 et 1)
* **StockEauD_C0** : Drainage de C0 vers la couche en dessous (mm)
* **RUmax_C1** : Quantite d eau maximale disponible contenue dans la couche C1 (mm)
* **TR_C1** : Quantité d’eau perdue par transpiration de la culture le jour j issue de la couche C1 (mm)
* **tEau_C2** : Teneur en eau de la couche C2 qui va être ajoutée à C1 et retranchée à C2 (mm eau/mm sol)
* **RUavdr_C1** : Stock d eau dans la couche C1 le jour j avant la prise en compte du drainage issu de cette couche (mm)
* **RU_C1** : Reserve utile dans la couche C1 le jour j avec la prise en compte du drainage issu de cette couche (??)
* **StockEauD_C1** : Drainage de C1 vers la couche C2 (mm)
* **RUmax_C2** : Quantite d eau maximale disponible contenue dans la couche C2 (mm)
* **RUavdr_C2** : Reserve utile dans la couche C2 le jour j avant la prise en compte du drainage issu de cette couche (mm)
* **RU_C2** : Réserve utile dans la couche C2 le jour j avec la prise en compte du drainage issu de cette couche (mm)
* **StockEauD_C2** : Drainage de C2 vers la couche en dessous (mm)
* **StockEauD_tot** : Somme de l’eau drainee issue de C2 depuis l’initialisation jusqu’au jour j (mm)
* **FTSW** : Fraction d’eau transpirable dans le sol en C0 et en C1 (varie entre 0 et 1)
* **stressH** : Facteur de stress hydrique total qui prend en compte la fraction d’eau transportable sur les couches C0 et C1 (varie entre 0 et 1)

### Details <a name="p12.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p12.2.1"></a>

> $$TR(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        \min\left(
          \begin{array}{@{}ll@{}}
            RU\_C0(t-1) + RU\_C1(t-1) \\
            ETP(t-1) * Trans * stressH(t-1)
          \end{array}\right)
      \end{array}\right), & \text{else if}\ \text{current day before date_init_MS} \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        \min\left(
          \begin{array}{@{}ll@{}}
            RU\_C0(t-1) + RU\_C1(t-1) \\
            ETP(t-1) * Ei(t-1) * Trans * stressH(t-1)
          \end{array}\right)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:TR}$$

> $$TR\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    0.0, & \text{else if}\ (RU\_C0(t-1) + RU\_C1(t-1))==0.0 \\
    \frac{TR(t) * RU\_C0(t-1)}{RU\_C0(t-1) + RU\_C1(t-1)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:TR_C0}$$

> $$JSA(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    JSA(t-1) + 1, & \text{else if}\ (P(t-1) + I(t-1)) < ApportEau\_seuil \\
    0.0, & \text{otherwise}
  \end{array}\right. 
\label{eq:JSA}$$

> $$rEV(t)=\left\{
  \begin{array}{@{}ll@{}}
    (JSA(t) + 1)^{EVAPO} - (JSA(t))^{EVAPO}, & \text{if}\ JSA(t) \geq JSA\_seuil \\
    1.0, & \text{otherwise}
  \end{array}\right. 
\label{eq:rEV}$$

> $$EV(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        ETP(t-1) * (1 - Ei(t-1)) * rEV(t)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:EV}$$

> $$RUmax\_C0(t) = \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        ep\_C0 * \frac{RUmax}{ProfRacmax(t)}
      \end{array}\right)
\label{eq:RUmax_C0}$$

> $$RUavdr\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        ep\_C0 * \frac{RU\_init}{ProfRacmax(t)}
      \end{array}\right), & \text{else if}\ \text{current day is date_init_H} \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        RU\_C0(t-1) + P(t-1) + I(t-1) - EV(t) - TR\_C0(t)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RUavdr_C0}$$

> $$RU\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        ep\_C0 * \frac{RU\_init}{ProfRacmax(t)}
      \end{array}\right), & \text{else if}\ \text{current day is date_init_H} \\
    \min\left(
      \begin{array}{@{}ll@{}}
        RUmax\_C0(t) \\
        RUavdr\_C0(t)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RU_C0}$$

> $$FTSW\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    coef\_Mottes * \frac{RU\_C0(t)}{RUmax\_C0(t)}, & \text{else if}\ MottesDelta > Mottes\_seuil \\
    \frac{RU\_C0(t)}{RUmax\_C0(t)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:FTSW_C0}$$

> $$stressH\_C0(t)=\left\{
  \begin{array}{@{}ll@{}}
    1.0, & \text{if}\ \text{current day before date_init_H} \\
    \frac{stressH1}{1 + stressH2 * e^{-stressH3 * (FTSW\_C0(t) - stressH4))}}, & \text{otherwise}
  \end{array}\right. 
\label{eq:stressH_C0}$$

> $$StockEauD\_C0(t) = \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        RUavdr\_C0(t) - RUmax\_C0(t)
      \end{array}\right)
\label{eq:StockEauD_C0}$$

> $$RUmax\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ ProfRac(t) \leq ep\_C0 \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        (ProfRac(t) - ep\_C0) * \frac{RUmax}{ProfRacmax(t)}
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RUmax_C1}$$

> $$TR\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    0.0, & \text{else if}\ (RU\_C0(t-1) + RU\_C1(t-1))==0.0 \\
    \frac{TR(t) * RU\_C1(t-1)}{\max\left(
      \begin{array}{@{}ll@{}}
        0.001 \\
        RU\_C0(t-1) + RU\_C1(t-1)
      \end{array}\right)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:TR_C1}$$

> $$tEau\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    \frac{RU\_C2(t-1)}{ProfRacmax(t) - \max\left(
      \begin{array}{@{}ll@{}}
        ep\_C0 \\
        ProfRac(t-1)
      \end{array}\right)}, & \text{if}\ ProfRac(t) < ProfRacmax(t) \\
    0.0, & \text{otherwise}
  \end{array}\right. 
\label{eq:tEau_C2}$$

> $$RUavdr\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ ProfRac(t) \leq ep\_C0 \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        RU\_C1(t-1) + (ProfRac(t) - ProfRac(t-1)) * tEau\_C2(t) + StockEauD\_C0(t) - TR\_C1(t)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RUavdr_C1}$$

> $$RU\_C1(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    \min\left(
      \begin{array}{@{}ll@{}}
        RUmax\_C1(t) \\
        RUavdr\_C1(t)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RU_C1}$$

> $$StockEauD\_C1(t) = \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        RUavdr\_C1(t) - RUmax\_C1(t)
      \end{array}\right)
\label{eq:StockEauD_C1}$$

> $$RUmax\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        (ProfRacmax(t) - ep\_C0) * \frac{RUmax}{ProfRacmax(t)}
      \end{array}\right), & \text{if}\ ProfRac(t) \leq ep\_C0 \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        (ProfRacmax(t) - ProfRac(t)) * \frac{RUmax}{ProfRacmax(t)}
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RUmax_C2}$$

> $$RUavdr\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        (ProfRacmax(t) - \max\left(
          \begin{array}{@{}ll@{}}
            ep\_C0 \\
            ProfRac(t)
          \end{array}\right)) * \frac{RU\_init}{ProfRacmax(t)}
      \end{array}\right), & \text{else if}\ \text{current day is date_init_H} \\
    RU\_C2(t-1) + StockEauD\_C0(t), & \text{else if}\ ProfRac(t) \leq ep\_C0 \\
    \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        RU\_C2(t-1) - (ProfRac(t) - ProfRac(t-1)) * tEau\_C2(t) + StockEauD\_C1(t)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RUavdr_C2}$$

> $$RU\_C2(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    \min\left(
      \begin{array}{@{}ll@{}}
        RUmax\_C2(t) \\
        RUavdr\_C2(t)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:RU_C2}$$

> $$StockEauD\_C2(t) = \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        RUavdr\_C2(t)-RUmax\_C2(t)
      \end{array}\right)
\label{eq:StockEauD_C2}$$

> $$StockEauD\_tot(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    StockEauD\_tot(t-1) + StockEauD\_C2(t), & \text{otherwise}
  \end{array}\right. 
\label{eq:StockEauD_tot}$$

> $$FTSW(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    FTSW\_C0(t), & \text{else if}\ ProfRac(t) \leq ep\_C0 \\
    coef\_Mottes * \frac{RU\_C0(t) + RU\_C1(t)}{RUmax\_C0(t)+RUmax\_C1(t)}, & \text{else if}\ MottesDelta > Mottes\_seuil \\
    \frac{RU\_C0(t) + RU\_C1(t)}{RUmax\_C0(t)+RUmax\_C1(t)}, & \text{otherwise}
  \end{array}\right. 
\label{eq:FTSW}$$

> $$stressH(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ \text{current day before date_init_H} \\
    \max\left(
      \begin{array}{@{}ll@{}}
        stressH\_seuil \\
        \frac{stressH1}{1 + stressH2 * e^{- stressH3 * (FTSW(t) - stressH4)}}
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:stressH}$$

</div>

--- 

## Atomic model YieldPea <a name="p13"></a>

The **YieldPea** model (using extension class [`vle.discrete-time`](https://github.com/vle-forge/packages/tree/master2.1/vle.discrete-time)) estimate ?? based on ?? 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#YieldPea_panel'>Show/Hide Model Details</button>
<div id="YieldPea_panel" class="collapse">

### Configuring a YieldPea Model <a name="p13.1"></a>

#### Dynamics settings <a name="p13.1.1"></a>

To define the dynamic:

* **library** : AZODYN
* **name** : YieldPea

#### Parameters settings <a name="p13.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`ParametersPlantPea`](#annex.p.plt.pea) (`coef_Ngunit1`, `coef_Ngunit2`, `PMGmoy_var`, `coef_vcrgr1`, `coef_vcrgr2`, `pNfsla`, `coef_tot_a`, `tNstruct`, `pMSapDRG`, `vNmax1`, `vNmax2`, `coef_vN`, `coef_prot_gr`) to get all available parameters

#### Input settings <a name="p13.1.3"></a>

List and information about all possible input ports (name, type, description with units).

* **QN_tot** : Quantite d azote totale accumulee par la culture le jour j (kg N/ha) (Nosync)
* **Stade** : Current phenological stage (see [shared defined stage](#annex.pheno.stages)) (sync)
* **date_DRG** : date DRG (??) (sync)
* **MS** : matiere seche accumulee depuis l initilisation jusqu au jour j (??) (sync)
* **MS_DRG** : matiere seche accumulee depuis l initilisation jusqu a DRG (g/m²) (sync)
* **MS_DF** : matiere seche accumulee depuis l initilisation jusqu a DF (g/m²) (sync)
* **Nj_FSLA** : Nombre de jours depuis la date Fsla (d) (sync)
* **MS_FSLA** : matiere seche accumulee depuis l initilisation jusqu a FSLA (g/m²) (sync)
* **STapFSLA** : Temps thermique depuis  FSLA (°C.d) (sync)
* **STapDF** : Temps thermique depuis  debut floraison (°C.d) (sync)
* **STapDRG** : Temps thermique depuis DRG (°C.d) (sync)
* **Tmoy** : daily average temperature (°C) (sync)
* **MS_MP** : matiere seche à maturité physiologique (g/m²) (sync)

#### Output settings <a name="p13.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **Ngunit** : Paramètre de correlation entre la vitesse de croissance des plantes et le nombre de grains (??)
* **NG** : nombre de grains(grain/m²)
* **vcrgr** : vitesse de croissance d'une graine (en gramme/grain/deg jours)
* **MSG** : Accumulation de MS dans les graines jusqu'au jour j (g/m²)
* **QNGfsla** : quantité d'azote dans les graines le jour de FFSLA (kg/ha)
* **QNveg** : Quantite d'azote cumulee dans les parties vegetatives aériennes au jour j (kg/ha)
* **QNdispo** : quantité d'azote dans les parties vegetatives aériennes disponibles pour la remobilisation (sans l'azote structural) pour une graine (mgN/gr)
* **vNmax** : vitesse max d'accumulation d'azote par graine (µgN/gr/DJ)
* **vN** : vitesse d'accumulation d'azote par graine (µgN/gr/DJ)
* **QNG** : Quantite d'azote accumule dans les graines jusqu'au jour j (??)
* **P1G** : poids d'une graine à 0% d'humidité (??)
* **P1G14** : poids d'une graine à 14% d'humidité (??)
* **IndicMP** : indicateur de la date de maturite physiologique (??)
* **Rdt** : rendement a 0% d'humidite (??)
* **Rdt14** : rendement a 14% d'humidite (??)
* **TP** : teneur en protéine pour ce rendement (%)
* **MSG_MP** : Matière sèche des grains à maturité physiologique (g/m²)
* **MSveg_MP** : Matière sèche aérienne végétative à maturité physiologique (g/m²)
* **IR** : Indice de récolte (??)

### Details <a name="p13.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

#### State variables equations <a name="p13.2.1"></a>

> $$Ngunit(t) = coef\_Ngunit1 * \frac{PMGmoy\_var}{1000} + coef\_Ngunit2
\label{eq:Ngunit}$$

> $$NG(t)=\left\{
  \begin{array}{@{}ll@{}}
    Ngunit(t) * \frac{MS\_FSLA(t) - MS\_DF(t)}{STapDF(t) - STapFSLA(t)}, & \text{if}\ Nj\_FSLA(t) > 0 \\
    0.0, & \text{otherwise}
  \end{array}\right. 
\label{eq:NG}$$

> $$vcrgr(t) = coef\_vcrgr1 * \frac{PMGmoy\_var}{1000} + coef\_vcrgr2
\label{eq:vcrgr}$$

> $$MSG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before FSLA_MP} \\
    MSG(t-1), & \text{else if}\ Stade(t) \text{is MP} \\
    \frac{NG(t)}{2} * (STapDRG(t) - STapFSLA(t)) * vcrgr(t), & \text{else if}\ Nj\_FSLA(t) == 1 \\
    \min\left(
      \begin{array}{@{}ll@{}}
        MS(t) \\
        MSG(t-1) + NG(t) * Tmoy(t) * vcrgr(t)
      \end{array}\right), & \text{else if}\ Nj\_FSLA(t) \leq 20 \\
    MSG(t-1) + \min\left(
      \begin{array}{@{}ll@{}}
        NG(t) * Tmoy(t) * vcrgr(t) \\
        MS(t) - MS(t-1)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:MSG}$$

> $$QNGfsla(t)=\left\{
  \begin{array}{@{}ll@{}}
    \frac{pNfsla}{100} * MSG(t) * 10, & \text{if}\ Nj\_FSLA(t) == 1 \\
    QNGfsla(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNGfsla}$$

> $$QNveg(t)=\left\{
  \begin{array}{@{}ll@{}}
    \frac{QN\_tot(t)}{coef\_tot\_a} - QNGfsla(t), & \text{if}\ Nj\_FSLA(t) == 1 \\
    \max\left(
      \begin{array}{@{}ll@{}}
        \frac{tNstruct}{100} * MS\_DRG(t) * 10 * pMSapDRG \\
        \frac{QN\_tot(t)}{coef\_tot\_a} - QNG(t-1)
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNveg}$$

> $$QNdispo(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before FSLA_MP} \\
    100 * \max\left(
      \begin{array}{@{}ll@{}}
        0.0 \\
        \frac{QNveg(t) - (\frac{tNstruct}{100} * MS\_DRG(t) * 10 * pMSapDRG)}{NG(t)}
      \end{array}\right), & \text{otherwise}
  \end{array}\right. 
\label{eq:QNdispo}$$

> $$vNmax(t) = vNmax1 * PMGmoy\_var + vNmax2
\label{eq:vNmax}$$

> $$vN(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before FSLA_MP} \\
    vNmax(t) * (1 - e^{coef\_vN * QNdispo(t)} , & \text{otherwise}
  \end{array}\right. 
\label{eq:vN}$$

> $$QNG(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before FSLA_MP} \\
    QNGfsla(t), & \text{else if}\ Nj\_FSLA(t) == 1 \\
    QNG(t-1), & \text{else if}\ MSG(t) - MSG(t-1) \leq 0 \\
    QNG(t-1) + \frac{NG(t) * vN(t) * Tmoy(t)}{100000}, & \text{otherwise}
  \end{array}\right. 
\label{eq:QNG}$$

> $$P1G(t)=\left\{
  \begin{array}{@{}ll@{}}
    \min\left(
      \begin{array}{@{}ll@{}}
        \frac{MSG(t)}{NG(t)} \\
        \frac{PMGmax\_var}{1000}
      \end{array}\right), & \text{if}\ NG(t) > 0 \\
    0.0, & \text{otherwise}
  \end{array}\right. 
\label{eq:P1G}$$

> $$P1G14(t)=\left\{
  \begin{array}{@{}ll@{}}
    \frac{P1G(t)}{1 - 0.14}, & \text{if}\ NG(t) > 0 \\
    0.0, & \text{otherwise}
  \end{array}\right. 
\label{eq:P1G14}$$

> $$IndicMP(t)=\left\{
  \begin{array}{@{}ll@{}}
    1, & \text{if}\ Stade(t) \text{after DRG_FSLA and } P1G(t) == P1G(t-1) \\
    0, & \text{otherwise}
  \end{array}\right. 
\label{eq:IndicMP}$$

> $$Rdt(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before MP} \\
    \frac{MSG(t)}{10}, & \text{else if}\ \text{on the first day of MP} \\
    Rdt(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:Rdt}$$

> $$Rdt14(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before MP} \\
    \frac{Rdt(t)}{1 - 0.14}, & \text{else if}\ \text{on the first day of MP} \\
    Rdt14(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:Rdt14}$$

> $$TP(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before MP} \\
    100 * \frac{QNG(t)}{MSG(t) * 10} * coef\_prot\_gr, & \text{else if}\ \text{on the first day of MP} \\
    TP(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:TP}$$

> $$MSG\_MP(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before MP} \\
    MSG(t), & \text{else if}\ \text{on the first day of MP} \\
    MSG\_MP(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:MSG_MP}$$

> $$MSveg\_MP(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before MP} \\
    MS\_MP(t) - MSG\_MP(t), & \text{else if}\ \text{on the first day of MP} \\
    MSveg\_MP(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:MSveg_MP}$$

> $$IR(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ Stade(t) \text{before MP} \\
    \frac{MSG\_MP(t)}{MS\_MP(t)}, & \text{else if}\ \text{on the first day of MP} \\
    IR(t-1), & \text{otherwise}
  \end{array}\right. 
\label{eq:IR}$$

</div>

--- 


## Shared structures  <a name="annex"></a>

### Phenological stages <a name="annex.pheno.stages"></a>

Defines the 5 + 1 available crop phenological stages. 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#pheno_stages_panel'>Show/Hide Table</button>
<div id="pheno_stages_panel" class="collapse">

| Stage name    | Integer code | Description                                        |
| ------------- | :----------: | -------------------------------------------------- |
| **SOL_NU**    |      0       | bare soil                                          |
| **VEGETATIF** |      1       | ??                                                 |
| **DF_DRG**    |      2       | Debut Floraison -> Debut de Remplissage des Grains |
| **DRG_FSLA**  |      3       | -> Fin du Stade Limite d'Avortement                |
| **FSLA_MP**   |      4       | -> maturite physiologique                          |
| **MP**        |      5       | ???                                                |
</div>

### ParametersSoil <a name="annex.p.soil"></a>

A set of 47 soil related parameters that can be used in multiple atomic models.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#p_soil_panel'>Show/Hide Table</button>
<div id="p_soil_panel" class="collapse">

| Parameter name | Type | Is mandatory? | Description | Internal type |
| -------------- | :--: | :-----------: | ----------- | :-----------: |
| **a** |  double  |      [x]      | parametre pour effet de la temperature sur la mineralisation (??)     |      double       |
| **alpha** |  double  |      [x]      | parametre pour effet temperature sur mineralisation dans equation du k2 (??)     |      double       |
| **ApportEau_seuil** |  double  |      [x]      | Seuil pour considerer que la pluie et l'irrigation ont un effet sur l'Evaporation du sol (??)     |      double       |
| **Arg** |  double  |      [x]      | teneur en argile de la couche labouree du sol (%)     |      double       |
| **b** |  double  |      [x]      | parametre pour effet de la temperature sur la mineralisation (??)     |      double       |
| **beta** |  double  |      [x]      | parametre pour calcul mineralisation de l'humus (tient compte de la mineralisation sous la couche labouree) (??)     |      double       |
| **c** |  double  |      [x]      | parametre pour effet de la temperature sur la mineralisation (??)     |      double       |
| **C_Nhum** |  double  |      [x]      | ratio C/N de l humus nouvellement formé  (-)     |      double       |
| **C_Nres** |  double  |      [x]      | Ratio C/N des résidus de la culture precedente (-)     |      double       |
| **C_res** |  double  |      [x]      | Quantite de carbone decomposable issu des residus (??)     |      double       |
| **CaCO3** |  double  |      [x]      | teneur en calcaire de la couche labouree du sol (%)     |      double       |
| **coef_k2** |  double  |      [x]      | parametre pour calcul mineralisation de l'humus dans l'equation du k2 (??)     |      double       |
| **coef_Mottes** |  double  |      [x]      | Pourcentage de reduction de la FTSW en conditions de sol tasse (= plus de Mottes_seuil % de Mottes delta) (??)     |      double       |
| **d** |  double  |      [x]      | parametre pour effet de la temperature sur la mineralisation (??)     |      double       |
| **da_C0** |  double  |      [x]      | densite apparente de la couche de mineralisation du sol (-)     |      double       |
| **da_C0_opt** |  double  |      [] (-1 if missing)     | densite apparente optimale de la couche de mineralisation du sol (-)     |      double       |
| **delta** |  double  |      [x]      | parametre pour calcul mineralisation de l'humus (coef CaCO3 dans equation k2) (??)     |      double       |
| **ep_C0** |  double  |      [x]      | epaisseur de la couche labouree (mm)     |      double       |
| **EVAPO** |  double  |      [x]      | Pour calcul de l'effet des jours sans precipitation sur levaporation (??)     |      double       |
| **f** |  double  |      [x]      | parametre pour le calcul du taux d'humification de la biomasse (??)     |      double       |
| **fsdc** |  double  |      [x]      | impact du système de culture sur la mineralisation de l'humus (-)     |      double       |
| **g** |  double  |      [x]      | parametre pour le calcul du taux d'humification de la biomasse (??)     |      double       |
| **gamma** |  double  |      [x]      | parametre pour calcul mineralisation de l'humus (coef argile dans equation k2) (??)     |      double       |
| **JSA_seuil** |  double  |      [x]      | Nombre de jour seuil en dessous duquel le nombre de jour sans apport d'eau n'a pas d'impact sur l'evaporation (d)     |      double       |
| **Mottes_seuil** |  double  |      [x]      | Seuil de mottes delta au dessus duquel la FTSW est affectee  (%)     |      double       |
| **MottesDelta** |  double  |      [x]      | pourcentage de mottes delta ( = tassement du sol) (%)     |      double       |
| **para_kres1** |  double  |      [x]      | parametre pour le calcul du taux de décomposition des résidus (??)     |      double       |
| **para_kreshum** |  double  |      [x]      | parametre pour le calcul du taux de décomposition de la biomasse microbienne (??)     |      double       |
| **paraC_Nres1** |  double  |      [x]      | parametre pour le calcul de la biomasse microbienne (??)     |      double       |
| **paraC_Nres4** |  double  |      [x]      | parametre pour le calcul de la biomasse microbienne (??)     |      double       |
| **Prof_Reliquat_tot** |  double  |      [x]      | Profondeur sur laquelle a ete mesuree le reliquat total (mm)     |      double       |
| **QNND** |  double  |      [x]      | Azote non disponible pour la culture par couche de 10 cm de sol (kg/ha)      |      double       |
| **QNsol_init_C0** |  double  |      []      | reliquat d'azote min dans le sol à l'initialisation dans la couche labouree (30cm) (kg/ha)     |      double       |
| **pQNsol_init_C0** |  double  |      []      | Part du reliquat d'azote min dans le sol à l'initialisation dans la couche labouree (30cm) (kg/ha) par rapport au total     |      double       |
| **QNsol_init_tot** |  double  |      [x]      | reliquat d'azote mineral dans le sol a l'initialisation sur l'ensemble de la couche prelevee (kg/ha)     |      double       |
| **RU_init** |  double  |      []      | Reserve utile a la date d'initialisation du module eau (mm)     |      double       |
| **pRU_init** |  double  |      []      | Part de la Reserve utile a la date d'initialisation du module eau (mm)     |      double       |
| **RUmax** |  double  |      [x]      | Reserve utile maximale, prend en compte les potentielles remontees capilaires d'eau en fond de profil de sol (mm)     |      double       |
| **stressH_seuil** |  double  |      [x]      | parametre seuil pour le stress hydrique (??)     |      double       |
| **stressH1** |  double  |      [x]      | parametre pour calcul de la transpiration et stress hydrique (??)     |      double       |
| **stressH2** |  double  |      [x]      | parametre pour calcul de la transpiration et stress hydrique (??)     |      double       |
| **stressH3** |  double  |      [x]      | parametre pour calcul de la transpiration et stress hydrique (??)     |      double       |
| **stressH4** |  double  |      [x]      | parametre pour calcul de la transpiration et stress hydrique (??)     |      double       |
| **tNorg** |  double  |      [x]      | teneur en azote organique dans le sol (couche de mineralisation) pour le calcul de la minéralisation de l'humus (kg/ha)     |      double       |
| **Trans** |  double  |      [x]      | parametre pour calcul de la transpiration (??)     |      double       |
| **Tref** |  double  |      [x]      | temperature de reference pour la mineralisation de l'humus (°C)     |      double       |
| **Y** |  double  |      [x]      | parametre rendement d'asimilation des résidus de carbonne par la biomasse microbienne (??)     |      double       |

Mandatory relations to respect between parameter values:  

> Only one of `QNsol_init_C0` and `pQNsol_init_C0` should be $\neq -1$  
> if `QNsol_init_C0` is provided then  
> $pQNsol\_init\_C0 = \frac{QNsol\_init\_tot}{QNsol\_init\_C0}$  
> else if `pQNsol_init_C0` is provided then  
> $QNsol\_init\_C0 = QNsol\_init\_tot * pQNsol\_init\_C0$  

> Only one of `RU_init` and `pRU_init` should be $\neq -1$  
> if `RU_init` is provided then  
> $pRU\_init = \frac{RUmax}{RU\_init}$  
> else if `pRU_init` is provided then  
> $RU\_init = RUmax * pRU\_init$  


</div>

### ParametersPlant <a name="annex.p.plt"></a>

A set of 44 Crop related parameters that can be used in multiple atomic models.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#p_plt_panel'>Show/Hide Table</button>
<div id="p_plt_panel" class="collapse">

| Parameter name | Type | Is mandatory? | Description | Internal type |
| -------------- | :--: | :-----------: | ----------- | :-----------: |
| **begin_date** |  Date  |      [x]      | julian date of the begin date (??)     |      double       |
| **coef_demMS_demN** |  double  |      [x]      | Coefficient liant la demande en Ms et la demande en N des grains (??)     |      double       |
| **coef_Nc** |  double  |      [x]      | Paramètre de courbe critique (??)     |      double       |
| **coef_Nmax** |  double  |      [x]      | Paramètre de courbe max (??)     |      double       |
| **coef_tot_a** |  double  |      [x]      | Rapport entre matière sèche totale et aérienne ou Quantité d’azote totale sur aérien (??)     |      double       |
| **date_fin_GEL** |  Date  |      [x]      | date de fin de l'application du gel hivernal (??)     |      double       |
| **date_FLO** |  Date  |      [x]      | date floraison (??)     |      double       |
| **date_init_H** |  Date  |      [x]      | date d'initialisation du module hydrique (??)     |      double       |
| **date_init_miner_res** |  Date  |      [x]      | date d initialisation du module mineralisation (va generallement etre a la recolte du precedent cultural) (??)     |      double       |
| **date_init_MS** |  Date  |      [x]      | date d'initialisation de la matiere seche (??)     |      double       |
| **date_init_N** |  Date  |      [x]      | date a laquelle on initialise les calculs pour le bilan azoté. (??)     |      double       |
| **date_S** | Date  |      [x]      | date semis (??)     |      double       |
| **densite_semis** |  double  |      [x]      | Densité de semis (Plante/m²)     |      double       |
| **Ebr** |  double  |      [x]      | Efficience maximale de conversion du rayonnement lumineux après EP1 après DF en pois (??)     |      double       |
| **Ec** |  double  |      [x]      | Coefficient conversion rayonnement global en actif (??)     |      double       |
| **Eimax** |  double  |      [x]      | Efficience maximale d’interception du rayonnement lumineux (??)     |      double       |
| **GELmax_seuil** |  double  |      [x]      | Seuil de température au dela duquel la destruction du couvert est total (°C)     |      double       |
| **k** |  double  |      [x]      | Coefficient d’extinction du rayonnement (??)     |      double       |
| **MS_init** |  double  |      [x]      | Matière sèche à date_init_MS (g/m²)     |      double       |
| **MS_Nc_seuil** |  double  |      [x]      | Seuil de MS pour la courbe critique (??)     |      double       |
| **MS_Nmax_seuil** |  double  |      [x]      | Seuil de MS pour courbe max (??)     |      double       |
| **NbFf** |  double  |      [x]      | nb feuilles seuil à partir duquel la resistance se stabilise (??)     |      double       |
| **NbFi** |  double  |      [x]      | nb feuilles seuil à partir duquel on augmente la résistance maximale (??)     |      double       |
| **Njdes** |  double  |      [x]      | Nombre de jours de desendurcisssement (d)     |      double       |
| **Njend** |  double  |      [x]      | Nombre de jours necessaires pour un endurcissement total de la plante (d)     |      double       |
| **pente_Nc** |  double  |      [x]      | Paramètre de courbe critique (??)     |      double       |
| **pente_Nmax** |  double  |      [x]      | Paramètre de courbe max (??)     |      double       |
| **phyllo** |  double  |      [x]      | phyllochron (°C.d/feuille)     |      double       |
| **QNa_init** |  double  |      []      | QNa_init ; (if == -1) alors utilisation equation qui utilise stressINN_init (??)     |      double       |
| **R_Coleo** |  double  |      [x]      | resistance maximale de la plante au froid au stade coleoptile (??)     |      double       |
| **Rmax_var** |  double  |      [x]      | resistance maximale au froid (??)     |      double       |
| **Rmin** |  double  |      [x]      | resistance minimale au froid de la plante (??)     |      double       |
| **stressN_Eb1** |  double  |      [x]      | Réduction du Eb en fonction de l’INN (??)     |      double       |
| **stressN_Eb2** |  double  |      [x]      | Réduction du Eb en fonction de l’INN (??)     |      double       |
| **stressN_Eb3** |  double  |      [x]      | Réduction du Eb en fonction de l’INN (??)     |      double       |
| **Tbase** |  double  |      [x]      | temperature de base de la plante (°C)     |      double       |
| **Tmax_plante** |  double  |      [x]      | temperature maximale pour la plante (°C)     |      double       |
| **Tmax_R** |  double  |      [x]      | temperature en dessous de laquelle on va commencer a avoir un endurcissement de la plante (°C)     |      double       |
| **Tmin_plante** |  double  |      [x]      | temperature minimale pour la plante (°C)     |      double       |
| **Tmin_R** |  double  |      [x]      | temperature en dessous de laquelle la resitance potentielle est maximale (°C)     |      double       |
| **tNc_Nc** |  double  |      [x]      | Teneur en N critique pour des biomasses faibles (??)     |      double       |
| **tNmax_Nmax** |  double  |      [x]      | Teneur en azote max pour des biomasses faibles (??)     |      double       |
| **Topt_plante** |  double  |      [x]      | temperature optimale pour la plante (°C)     |      double       |
| **Vmax** |  double  |      [x]      | Vitesse maximale d’absorption de l’azote pour l’ensemble de la plante (aérien + racinaire) (??)     |      double       |


Mandatory relations to respect between parameter values:  

> $$date\_init\_miner\_res \leq date\_init\_N$$  

> $$date\_init\_H \leq date\_init\_N$$  

> $$date\_S \leq date\_FLO$$  



### ParametersPlantPea <a name="annex.p.plt.pea"></a>

A set of 63 Crop related parameters that can be used in multiple atomic models.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#p_plt_pea_panel'>Show/Hide Table</button>
<div id="p_plt_pea_panel" class="collapse">

| Parameter name | Type | Is mandatory? | Description | Internal type |
| -------------- | :--: | :-----------: | ----------- | :-----------: |
| **a_pNfix** |  double  |      [x]      | sert pour calculer pNac_fix (??)     |      double       |
| **b_pNfix** |  double  |      [x]      | sert pour calculer pNac_fix (??)     |      double       |
| **coef_fixmax1** |  double  |      [x]      | sert pour calculer QNfixmax (??)     |      double       |
| **coef_fixmax2** |  double  |      [x]      | sert pour calculer QNfixmax (??)     |      double       |
| **coef_Mottes_Profmax** |  double  |      [x]      | paramtre de reduction de la profondeur maximale en fonction du pourcentage de MotteDelta (??)     |      double       |
| **coef_MS_init** |  double  |      [x]      | ?? (??)     |      double       |
| **coef_Ngunit1** |  double  |      [x]      | parametre varietal de conversion de la vitesse de croissance en nombre de grains (??)     |      double       |
| **coef_Ngunit2** |  double  |      [x]      | parametre varietal de conversion de la vitesse de croissance en nombre de grains (??)     |      double       |
| **coef_prot_gr** |  double  |      [x]      | facteur de conversion pour passer d'un pourcentage d'azote en taux de proteines (??)     |      double       |
| **coef_vcrgr1** |  double  |      [x]      | parametres pour le calcul de la vitesse de croissance d'une graine (??)     |      double       |
| **coef_vcrgr2** |  double  |      [x]      | parametres pour le calcul de la vitesse de croissance d'une graine (??)     |      double       |
| **coef_vN** |  double  |      [x]      | coefficient permetant de calculer vN (??)     |      double       |
| **coefNsol_fix1** |  double  |      [x]      | sert pour calculer pNac_fix (??)     |      double       |
| **coefNsol_fix2** |  double  |      [x]      | sert pour calculer pNac_fix (??)     |      double       |
| **damax_C0** |  double  |      [x]      | densite apparente au dessus de laquelle la profondeur des nodosites est minimale (??)     |      double       |
| **densRac1** |  double  |      [x]      | parametre de la densite racinaire (??)     |      double       |
| **densRac2** |  double  |      [x]      | parametre de la densite racinaire (??)     |      double       |
| **DRG_ET** |  double  |      [x]      | nombre de degre jours necessaires pour remplir les grains d'un etage (°C.d)     |      double       |
| **Ebv** |  double  |      [x]      | efficience biologique pendant la phase vegetative (??)     |      double       |
| **FLO_ET** |  double  |      [x]      | nombre de degre jours necessaires pour la floraison d'un etage (°C.d)     |      double       |
| **NETpot** |  double  |      [x]      | nb etages florals potentiel (??)     |      double       |
| **NJstress_nod_seuil** |  double  |      [x]      | nbre de jours consecutifs de stress hydrique à partir duquel l'arret de la fixation est irreversible (d)     |      double       |
| **PMGmoy_var** |  double  |      [x]      | Poids de mille grain moyen (??)     |      double       |
| **pMSapDRG** |  double  |      [x]      | pourcentage de MS restant apres DRG suite a la remobilisation du carbone (%)     |      double       |
| **pNfix_max** |  double  |      [x]      | sert pour calculer pNac_fix, proportion maximale d'azote venant de la fixation (??)     |      double       |
| **pNfsla** |  double  |      [x]      | proportion d'azote moyen d'une graine au stade FSLA (??)     |      double       |
| **Profnod_min** |  double  |      [x]      | profondeur minimale des nodosites si tassement du sol (mm)     |      double       |
| **Profnod_opt** |  double  |      [x]      | profondeur optimale des nodosites (mm)     |      double       |
| **ProfRacmax_pot** |  double  |      [x]      | profondeur racinaire macimale potentielle sans tassement (??)     |      double       |
| **ST_DF_DRG** |  double  |      [x]      | somme des temperatures entre floraison et remplissage des graines (°C.d)     |      double       |
| **ST_S_LEVEE** |  double  |      [x]      | somme des temperatures entre semis et levee (°C.d)     |      double       |
| **STfixmax_seuil** |  double  |      [x]      | seuil des sommes de température en dessous duquel la fixation maximale est à 0 (°C.d)     |      double       |
| **STmax_FSLA_MP** |  double  |      [x]      | somme des temperatures entre fin du stade limite d'avortement et maturite physilogique (°C.d)     |      double       |
| **tNstruct** |  double  |      [x]      | la teneur en azote minimale des parties végétatives (azote non mobilisables) (??)     |      double       |
| **tNveg_seuil** |  double  |      [x]      | teneur en azote seuil en dessous de laquelle on impacte Ei et Eb (??)     |      double       |
| **vNmax1** |  double  |      [x]      | coefficient permetant de calculer vNmax (??)     |      double       |
| **vNmax2** |  double  |      [x]      | coefficient permetant de calculer vNmax (??)     |      double       |
| **vRac_RG** |  double  |      [x]      | paramtere d'legonation racinaire en fonctions du rayonnement (mm/Mj/m^2)     |      double       |


25 are inherited from [`ParametersPlant`](#annex.p.plt)

| Parameter name | Type | Is mandatory? | Description | Internal type |
| -------------- | :--: | :-----------: | ----------- | :-----------: |
| **begin_date** |  Date  |      [x]      | julian date of the begin date (??)     |      double       |
| **coef_demMS_demN** |  double  |      [x]      | Coefficient liant la demande en Ms et la demande en N des grains (??)     |      double       |
| **coef_Nc** |  double  |      [x]      | Paramètre de courbe critique (??)     |      double       |
| **coef_Nmax** |  double  |      [x]      | Paramètre de courbe max (??)     |      double       |
| **coef_tot_a** |  double  |      [x]      | Rapport entre matière sèche totale et aérienne ou Quantité d’azote totale sur aérien (??)     |      double       |
| **date_FLO** |  Date  |      [x]      | date floraison (??)     |      double       |
| **date_S** | Date  |      [x]      | date semis (??)     |      double       |
| **densite_semis** |  double  |      [x]      | Densité de semis (Plante/m²)     |      double       |
| **Ebr** |  double  |      [x]      | Efficience maximale de conversion du rayonnement lumineux après EP1 après DF en pois (??)     |      double       |
| **Ec** |  double  |      [x]      | Coefficient conversion rayonnement global en actif (??)     |      double       |
| **Eimax** |  double  |      [x]      | Efficience maximale d’interception du rayonnement lumineux (??)     |      double       |
| **k** |  double  |      [x]      | Coefficient d’extinction du rayonnement (??)     |      double       |
| **MS_init** |  double  |      [x]      | Matière sèche à date_init_MS (g/m²)     |      double       |
| **MS_Nc_seuil** |  double  |      [x]      | Seuil de MS pour la courbe critique (??)     |      double       |
| **MS_Nmax_seuil** |  double  |      [x]      | Seuil de MS pour courbe max (??)     |      double       |
| **pente_Nc** |  double  |      [x]      | Paramètre de courbe critique (??)     |      double       |
| **pente_Nmax** |  double  |      [x]      | Paramètre de courbe max (??)     |      double       |
| **QNa_init** |  double  |      []      | QNa_init ; (if == -1) alors utilisation equation qui utilise stressINN_init (??)     |      double       |
| **stressN_Eb1** |  double  |      [x]      | Réduction du Eb en fonction de l’INN (??)     |      double       |
| **stressN_Eb2** |  double  |      [x]      | Réduction du Eb en fonction de l’INN (??)     |      double       |
| **stressN_Eb3** |  double  |      [x]      | Réduction du Eb en fonction de l’INN (??)     |      double       |
| **Tbase** |  double  |      [x]      | temperature de base de la plante (°C)     |      double       |
| **tNc_Nc** |  double  |      [x]      | Teneur en N critique pour des biomasses faibles (??)     |      double       |
| **tNmax_Nmax** |  double  |      [x]      | Teneur en azote max pour des biomasses faibles (??)     |      double       |
| **Vmax** |  double  |      [x]      | Vitesse maximale d’absorption de l’azote pour l’ensemble de la plante (aérien + racinaire) (??)     |      double       |

</div>


