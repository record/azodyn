###
#example fo simulations of AZODYN using vle.recursive R API
##

library(rvle)
source(paste(sep="", Sys.getenv("VLE_HOME"),
             "/pkgs-2.0/vle.recursive/R/vle.recursive.R"))

# init the zheat simulator
f = vle.recursive.init(pkg="AZODYN", file="AzodynWheat.vpz")

# show the content
vle.recursive.showEmbedded(f)

# build an experiment plan of 3 simulations zith different sowing density,
# and different max root depth
simData = data.frame(cPlant.densite_semis = c(200,215,230),
                     cPlant.ProfRacmax = c(1000,1300,1600));

# config the simulator with the simulations
simData = vle.recursive.parseSim(file_sim=simData, rvle_handle=f);

# get available simulqtion outputs
out_vars = vle.recursive.getAvailOutputs(f)

# configure selected outputs
vle.recursive.configOutputs(f, 
  output_vars = c(LAI=out_vars$LAI, MSpot=out_vars$MSpot),
  integration='all')

# simulate
res = vle.recursive.simulate(rvle_handle=f)

# plot
vle.recursive.plot(res, output_vars = c("LAI","MSpot"))

